package com.eden.wechat.domain;

import com.eden.common.annotation.Excel;
import com.eden.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 微信首页导航对象 wx_icon_list
 *
 * @author jiaoyang
 * @date 2020-12-02
 */
public class WxIconList extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Integer id;

    /** 图标 */
    @Excel(name = "图标")
    private String icon;

    /** 颜色 */
    @Excel(name = "颜色")
    private String color;

    /** 消息数 */
    @Excel(name = "消息数")
    private String badge;

    /** 名称 */
    @Excel(name = "名称")
    private String name;

    public void setId(Integer id)
    {
        this.id = id;
    }

    public Integer getId()
    {
        return id;
    }
    public void setIcon(String icon)
    {
        this.icon = icon;
    }

    public String getIcon()
    {
        return icon;
    }
    public void setColor(String color)
    {
        this.color = color;
    }

    public String getColor()
    {
        return color;
    }
    public void setBadge(String badge)
    {
        this.badge = badge;
    }

    public String getBadge()
    {
        return badge;
    }
    public void setName(String name)
    {
        this.name = name;
    }

    public String getName()
    {
        return name;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("icon", getIcon())
                .append("color", getColor())
                .append("badge", getBadge())
                .append("name", getName())
                .toString();
    }
}