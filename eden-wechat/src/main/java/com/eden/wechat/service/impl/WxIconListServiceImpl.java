package com.eden.wechat.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.eden.wechat.mapper.WxIconListMapper;
import com.eden.wechat.domain.WxIconList;
import com.eden.wechat.service.IWxIconListService;
import com.eden.common.core.text.Convert;

/**
 * 微信首页导航Service业务层处理
 *
 * @author jiaoyang
 * @date 2020-12-02
 */
@Service
public class WxIconListServiceImpl implements IWxIconListService
{
    @Autowired
    private WxIconListMapper wxIconListMapper;

    /**
     * 查询微信首页导航
     *
     * @param id 微信首页导航ID
     * @return 微信首页导航
     */
    @Override
    public WxIconList selectWxIconListById(Integer id)
    {
        return wxIconListMapper.selectWxIconListById(id);
    }

    /**
     * 查询微信首页导航列表
     *
     * @param wxIconList 微信首页导航
     * @return 微信首页导航
     */
    @Override
    public List<WxIconList> selectWxIconListList(WxIconList wxIconList)
    {
        return wxIconListMapper.selectWxIconListList(wxIconList);
    }

    /**
     * 新增微信首页导航
     *
     * @param wxIconList 微信首页导航
     * @return 结果
     */
    @Override
    public int insertWxIconList(WxIconList wxIconList)
    {
        return wxIconListMapper.insertWxIconList(wxIconList);
    }

    /**
     * 修改微信首页导航
     *
     * @param wxIconList 微信首页导航
     * @return 结果
     */
    @Override
    public int updateWxIconList(WxIconList wxIconList)
    {
        return wxIconListMapper.updateWxIconList(wxIconList);
    }

    /**
     * 删除微信首页导航对象
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteWxIconListByIds(String ids)
    {
        return wxIconListMapper.deleteWxIconListByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除微信首页导航信息
     *
     * @param id 微信首页导航ID
     * @return 结果
     */
    @Override
    public int deleteWxIconListById(Integer id)
    {
        return wxIconListMapper.deleteWxIconListById(id);
    }
}
