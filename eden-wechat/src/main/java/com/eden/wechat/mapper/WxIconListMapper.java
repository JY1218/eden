package com.eden.wechat.mapper;

import java.util.List;
import com.eden.wechat.domain.WxIconList;

/**
 * 微信首页导航Mapper接口
 *
 * @author jiaoyang
 * @date 2020-12-02
 */
public interface WxIconListMapper
{
    /**
     * 查询微信首页导航
     *
     * @param id 微信首页导航ID
     * @return 微信首页导航
     */
    public WxIconList selectWxIconListById(Integer id);

    /**
     * 查询微信首页导航列表
     *
     * @param wxIconList 微信首页导航
     * @return 微信首页导航集合
     */
    public List<WxIconList> selectWxIconListList(WxIconList wxIconList);

    /**
     * 新增微信首页导航
     *
     * @param wxIconList 微信首页导航
     * @return 结果
     */
    public int insertWxIconList(WxIconList wxIconList);

    /**
     * 修改微信首页导航
     *
     * @param wxIconList 微信首页导航
     * @return 结果
     */
    public int updateWxIconList(WxIconList wxIconList);

    /**
     * 删除微信首页导航
     *
     * @param id 微信首页导航ID
     * @return 结果
     */
    public int deleteWxIconListById(Integer id);

    /**
     * 批量删除微信首页导航
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteWxIconListByIds(String[] ids);
}