package com.eden.websocket.service.impl;

import java.util.List;
import com.eden.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.eden.websocket.mapper.MessageMapper;
import com.eden.websocket.domain.Message;
import com.eden.websocket.service.IMessageService;
import com.eden.common.core.text.Convert;

/**
 * 消息Service业务层处理
 *
 * @author eden
 * @date 2020-12-21
 */
@Service
public class MessageServiceImpl implements IMessageService
{
    @Autowired
    private MessageMapper messageMapper;

    /**
     * 查询消息
     *
     * @param msgId 消息ID
     * @return 消息
     */
    @Override
    public Message selectMessageById(Integer msgId)
    {
        return messageMapper.selectMessageById(msgId);
    }

    /**
     * 查询消息列表
     *
     * @param message 消息
     * @return 消息
     */
    @Override
    public List<Message> selectMessageList(Message message)
    {
        return messageMapper.selectMessageList(message);
    }

    /**
     * 新增消息
     *
     * @param message 消息
     * @return 结果
     */
    @Override
    public int insertMessage(Message message)
    {
        message.setCreateTime(DateUtils.getNowDate());
        return messageMapper.insertMessage(message);
    }

    /**
     * 修改消息
     *
     * @param message 消息
     * @return 结果
     */
    @Override
    public int updateMessage(Message message)
    {
        return messageMapper.updateMessage(message);
    }

    /**
     * 删除消息对象
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteMessageByIds(String ids)
    {
        return messageMapper.deleteMessageByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除消息信息
     *
     * @param msgId 消息ID
     * @return 结果
     */
    @Override
    public int deleteMessageById(Integer msgId)
    {
        return messageMapper.deleteMessageById(msgId);
    }
}