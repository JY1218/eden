package com.eden.websocket.domain;

import java.util.List;

/**
 * 群消息转发
 */
public class Group  {
    //群组Id
    private Long GroupId;
    //群组名称
    private String GroupName;
    //消息
    private  List<Message> messages ;

    public Group() {
    }

    public Group(Long groupId, String groupName, List<Message> messages) {
        GroupId = groupId;
        GroupName = groupName;
        this.messages = messages;
    }

    public Long getGroupId() {
        return GroupId;
    }

    public void setGroupId(Long groupId) {
        GroupId = groupId;
    }

    public String getGroupName() {
        return GroupName;
    }

    public void setGroupName(String groupName) {
        GroupName = groupName;
    }

    public List<Message> getMessages() {
        return messages;
    }

    public void setMessages(List<Message> messages) {
        this.messages = messages;
    }
}
