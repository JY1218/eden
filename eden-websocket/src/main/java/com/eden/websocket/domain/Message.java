package com.eden.websocket.domain;

import com.eden.common.annotation.Excel;
import com.eden.common.core.domain.BaseEntity;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

/**
 * 消息对象 message
 *
 * @author eden
 * @date 2020-12-21
 */
public class Message extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 消息id,自增长 */
    private Integer msgId;

    /** 类型 1：文字；2：图片;3：音频；4：文件；5位置； */
    @Excel(name = "类型 1：文字；2：图片;3：音频；4：文件；5位置；")
    private Integer msgType;

    /** 消息接收人Id */
    @Excel(name = "消息接收人Id")
    private Long userId;

    /** 消息接收人名称 */
    @Excel(name = "消息接收人名称")
    private String userName;

    /** 消息内容 */
    @Excel(name = "消息内容")
    private String content;

    /** 消息发送人Id */
    @Excel(name = "消息发送人Id")
    private Long fromUserId;

    /** 消息发送人名称 */
    @Excel(name = "消息发送人名称")
    private String fromUserName;

    public void setMsgId(Integer msgId)
    {
        this.msgId = msgId;
    }

    public Integer getMsgId()
    {
        return msgId;
    }
    public void setMsgType(Integer msgType)
    {
        this.msgType = msgType;
    }

    public Integer getMsgType()
    {
        return msgType;
    }
    public void setUserId(Long userId)
    {
        this.userId = userId;
    }

    public Long getUserId()
    {
        return userId;
    }
    public void setUserName(String userName)
    {
        this.userName = userName;
    }

    public String getUserName()
    {
        return userName;
    }
    public void setContent(String content)
    {
        this.content = content;
    }

    public String getContent()
    {
        return content;
    }
    public void setFromUserId(Long fromUserId)
    {
        this.fromUserId = fromUserId;
    }

    public Long getFromUserId()
    {
        return fromUserId;
    }
    public void setFromUserName(String fromUserName)
    {
        this.fromUserName = fromUserName;
    }

    public String getFromUserName()
    {
        return fromUserName;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("msgId", getMsgId())
                .append("msgType", getMsgType())
                .append("userId", getUserId())
                .append("userName", getUserName())
                .append("createTime", getCreateTime())
                .append("content", getContent())
                .append("fromUserId", getFromUserId())
                .append("fromUserName", getFromUserName())
                .toString();
    }
}