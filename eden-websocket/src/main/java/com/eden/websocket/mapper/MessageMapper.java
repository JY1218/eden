package com.eden.websocket.mapper;

import java.util.List;
import com.eden.websocket.domain.Message;

/**
 * 消息Mapper接口
 *
 * @author eden
 * @date 2020-12-21
 */
public interface MessageMapper
{
    /**
     * 查询消息
     *
     * @param msgId 消息ID
     * @return 消息
     */
    public Message selectMessageById(Integer msgId);

    /**
     * 查询消息列表
     *
     * @param message 消息
     * @return 消息集合
     */
    public List<Message> selectMessageList(Message message);

    /**
     * 新增消息
     *
     * @param message 消息
     * @return 结果
     */
    public int insertMessage(Message message);

    /**
     * 修改消息
     *
     * @param message 消息
     * @return 结果
     */
    public int updateMessage(Message message);

    /**
     * 删除消息
     *
     * @param msgId 消息ID
     * @return 结果
     */
    public int deleteMessageById(Integer msgId);

    /**
     * 批量删除消息
     *
     * @param msgIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteMessageByIds(String[] msgIds);
}