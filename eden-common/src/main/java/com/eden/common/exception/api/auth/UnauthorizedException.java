package com.eden.common.exception.api.auth;

/**
 * @description:
 * @author: wenqing
 * @date: 2020/4/18 17:40
 * @version: 1.0
 */

public class UnauthorizedException extends ApiAuthException {

    public UnauthorizedException(Integer code, String message) {
        super(code, message);
    }
}
