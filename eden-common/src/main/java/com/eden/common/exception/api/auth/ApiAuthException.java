package com.eden.common.exception.api.auth;

import com.eden.common.constant.HttpStatus;
import com.eden.common.exception.api.ApiException;

/**
 * @description: api权限认证异常
 * @author: wenqing
 * @date: 2019/11/05 11:24
 */


public class ApiAuthException extends ApiException {

    private Integer code;

    public ApiAuthException(String message) {
        super(message);
        this.code = HttpStatus.UNAUTHORIZED;
    }

    public ApiAuthException(Integer code, String message) {
        super(message);
        this.code = code;
    }

    public ApiAuthException(String message, Throwable cause) {
        super(message, cause);
        this.code = HttpStatus.UNAUTHORIZED;
    }

    public ApiAuthException(Integer code, String message, Throwable cause) {
        super(message, cause);
        this.code = code;
    }

    public Integer getCode() {
        return code;
    }
}

