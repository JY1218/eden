package com.eden.common.exception.api.auth;

/**
 * @description:
 * @author: wenqing
 * @date: 2020/4/18 17:38
 * @version: 1.0
 */

public class UnauthenticatedException extends ApiAuthException {

    public UnauthenticatedException(Integer code, String message) {
        super(code, message);
    }
}
