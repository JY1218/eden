package com.eden.common.exception.api;

/**
 * @description: Api相关异常
 * @author: wenqing
 * @date: 2020/2/25 14:58
 * @version: 1.0
 */

public class ApiException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    protected final String message;

    public ApiException(String message)
    {
        this.message = message;
    }

    public ApiException(String message, Throwable e)
    {
        super(message, e);
        this.message = message;
    }

    @Override
    public String getMessage()
    {
        return message;
    }
}
