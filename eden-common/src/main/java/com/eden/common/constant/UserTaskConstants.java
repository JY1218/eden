package com.eden.common.constant;

/**
 * @program: eden
 * @description: ${description}
 * @author: Mr.Jiao
 * @create: 2020-09-04 10:47
 */
public class UserTaskConstants {

    /**
     * 待检查状态
     */
    public static final String WAIT_CHECK_STATUS = "0";

    /**
     * 检查通过状态
     */
    public static final String CHECK_PASS_STATUS = "1";
    /**
     * 检查未通过状态
     */
    public static final String CHECK_UNPASS_STATUS = "2";

}
