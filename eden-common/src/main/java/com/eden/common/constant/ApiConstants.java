package com.eden.common.constant;

/**
 * @description: jwt相关常量
 * @author: jiaoyang
 * @date: 2020/1/10 14:04
 * @version: 1.0
 */

public class ApiConstants extends Constants {

    public final static String RET_CODE_SUCCESS = "00000000000000";
    public final static String RET_MSG_SUCCESS = "交易成功";

    public static final String RET_CODE_RECV_ERROR = "400760S9999999";
    public static final String RET_MSG_RECV_ERROR = "接收ESB数据错误或接收超时";

    public static final String RET_CODE_TRAN_FAIL = "400760S0000000";
    public static final String RET_MSG_TRAN_FAIL= "交易失败";

    public static final String RET_CODE_TRAN_UNKOWN_ERROR = "400760S1111111";
    public static final String RET_MSG_TRAN_UNKOWN_ERROR = "接口交易返回未知状态";

    public static final String RET_CODE_CHECK_FAILED = "400760B0000002";
    public static final String RET_MSG_CHECK_FAILED = "交易校验失败";

    public static final String RET_CODE_SYSTEM_ERROR = "400760S9999999";
    public static final String RET_MSG_SYSTEM_ERROR = "系统错误";

    public static final String USERORPASS_ERROR = "用户名或密码错误";

    public static final String USER_NOTEXIST = "用户名不存在";

    public static final String USERROLE_NOTEXIST = "该用户角色未定义，无权访问";

    public static final String TERMINAL_NOTEXIST = "终端号不存在";

    public static final String BANKUSER_NOTEXIST = "登录失败，柜员只能在本机构登录";

    public static final String USERLOGIN_ERROR = "帐号异常退出5分钟后重新操作";

    public static final String PM_PARAM = "跑马";

    public static final String MSG_DELIMITER = "|";

    public static final String COMMA_DELIMITER = ",";

    public static final String PT_SPLIT = " ";

    public static final String SVARS_ILLEGAL_REQUEST = "非法请求，拒绝交易";

    public static final String SVARS_SERVICE_ERROR = "服务器响应出错，请联系管理员";

    public static final String USER_NOTLOGIN = "当前用户非登录状态，请重新登录";

    public static final String FIELD_SPLIT = "#";

    public static final String ROWS_SPLIT = "&";

    public static final String SVARS_SYSTEM_CODE = "400760";

}
