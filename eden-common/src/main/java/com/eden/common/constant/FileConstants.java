package com.eden.common.constant;

/**
 * @program: eden
 * @description: 文件通用常量
 * @author: Mr.Jiao
 * @create: 2020-09-11 12:48
 */
public class FileConstants {
    /**
     * 文件初始状态 可用
     */
    public static final String FILE_USE_STATUS = "0";
    /**
     * 文件状态 不可用
     */
    public static final String FILE_UNUSE_STATUS = "1";

}
