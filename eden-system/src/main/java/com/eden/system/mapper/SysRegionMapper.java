package com.eden.system.mapper;

import java.util.List;
import com.eden.system.domain.SysRegion;

/**
 * 地区Mapper接口
 * 
 * @author jiaoyang
 * @date 2020-09-01
 */
public interface SysRegionMapper 
{
    /**
     * 查询地区
     * 
     * @param regionId 地区ID
     * @return 地区
     */
    public SysRegion selectSysRegionById(String regionId);

    /**
     * 查询地区列表
     * 
     * @param sysRegion 地区
     * @return 地区集合
     */
    public List<SysRegion> selectSysRegionList(SysRegion sysRegion);

    /**
     * 新增地区
     * 
     * @param sysRegion 地区
     * @return 结果
     */
    public int insertSysRegion(SysRegion sysRegion);

    /**
     * 修改地区
     * 
     * @param sysRegion 地区
     * @return 结果
     */
    public int updateSysRegion(SysRegion sysRegion);

    /**
     * 删除地区
     * 
     * @param regionId 地区ID
     * @return 结果
     */
    public int deleteSysRegionById(String regionId);

    /**
     * 批量删除地区
     * 
     * @param regionIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteSysRegionByIds(String[] regionIds);
}
