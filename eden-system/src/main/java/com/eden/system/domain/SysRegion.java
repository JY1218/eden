package com.eden.system.domain;

import com.eden.common.annotation.Excel;
import com.fasterxml.jackson.annotation.JsonInclude;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.List;

/**
 * 地区对象 sys_region
 *
 * @author jiaoyang
 * @date 2020-09-01
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SysRegion {
    /**
     * 行政地区编号
     */
    @Excel(name = "行政地区编号")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String regionCode;

    /**
     * 地区名称
     */
    @Excel(name = "地区名称")
    private String n;

    /**
     * 地区父id
     */
    @Excel(name = "地区父id")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String regionParentId;

    /**
     * 地区级别 1-省、自治区、直辖市 2-地级市、地区、自治州、盟 3-市辖区、县级市、县
     */
    @Excel(name = "地区级别")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Integer regionLevel;

    private List<SysRegion> s;

    public List<SysRegion> getS() {
        return s;
    }

    public void setS(List<SysRegion> s) {
        this.s = s;
    }
    public void setRegionCode(String regionCode) {
        this.regionCode = regionCode;
    }

    public String getRegionCode() {
        return regionCode;
    }

    public void setRegionParentId(String regionParentId) {
        this.regionParentId = regionParentId;
    }

    public String getRegionParentId() {
        return regionParentId;
    }

    public void setRegionLevel(Integer regionLevel) {
        this.regionLevel = regionLevel;
    }

    public Integer getRegionLevel() {
        return regionLevel;
    }

    public String getN() {
        return n;
    }

    public void setN(String n) {
        this.n = n;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("regionCode", getRegionCode())
                .append("n", getN())
                .append("regionParentId", getRegionParentId())
                .append("regionLevel", getRegionLevel())
                .toString();
    }
}
