package com.eden.system.service;

import com.eden.system.domain.SysRegion;

import java.util.List;

/**
 * 地区Service接口
 *
 * @author jiaoyang
 * @date 2020-09-01
 */
public interface ISysRegionService {
    /**
     * 查询地区
     *
     * @param regionId 地区ID
     * @return 地区
     */
    public SysRegion selectSysRegionById(String regionId);

    /**
     * 查询地区列表
     *
     * @param sysRegion 地区
     * @return 地区集合
     */
    public List<SysRegion> selectSysRegionList(SysRegion sysRegion);

    /**
     * 查询地区列表
     *
     * @param sysRegion 地区
     * @return 地区集合
     */
    public String selectSysRegionJson(SysRegion sysRegion);

    /**
     * 新增地区
     *
     * @param sysRegion 地区
     * @return 结果
     */
    public int insertSysRegion(SysRegion sysRegion);

    /**
     * 修改地区
     *
     * @param sysRegion 地区
     * @return 结果
     */
    public int updateSysRegion(SysRegion sysRegion);

    /**
     * 批量删除地区
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteSysRegionByIds(String ids);

    /**
     * 删除地区信息
     *
     * @param regionId 地区ID
     * @return 结果
     */
    public int deleteSysRegionById(String regionId);
}
