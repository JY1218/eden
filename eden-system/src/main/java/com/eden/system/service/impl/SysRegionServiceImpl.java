package com.eden.system.service.impl;

import com.alibaba.fastjson.JSON;
import com.eden.common.core.text.Convert;
import com.eden.system.domain.SysRegion;
import com.eden.system.mapper.SysRegionMapper;
import com.eden.system.service.ISysRegionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 地区Service业务层处理
 *
 * @author jiaoyang
 * @date 2020-09-01
 */
@Service
public class SysRegionServiceImpl implements ISysRegionService {
    @Autowired
    private SysRegionMapper sysRegionMapper;

    /**
     * 查询地区
     *
     * @param regionId 地区ID
     * @return 地区
     */
    @Override
    public SysRegion selectSysRegionById(String regionId) {
        return sysRegionMapper.selectSysRegionById(regionId);
    }

    /**
     * 查询地区列表 转变成Json格式的数据
     *
     * @param sysRegion 地区
     * @return 地区
     */
    @Override
    public List<SysRegion> selectSysRegionList(SysRegion sysRegion) {
        return sysRegionMapper.selectSysRegionList(sysRegion);
    }

    /**
     * Json数据获取
     */
    public String selectSysRegionJson(SysRegion sysRegion) {
        List<SysRegion> ChinaRegions = new ArrayList<>();
        List<SysRegion> regions = sysRegionMapper.selectSysRegionList(sysRegion);
        List<SysRegion> provinces = new ArrayList<>();
        Map<SysRegion, String> citysMap = new HashMap<>();
        Map<SysRegion, String> areasMap = new HashMap<>();
        for (SysRegion region : regions) {
            if (region.getRegionLevel() == 1) {
                SysRegion province = new SysRegion();
                province.setN(region.getN());
                province.setRegionCode(region.getRegionCode());
                provinces.add(province);
            } else if (region.getRegionLevel() == 2) {
                SysRegion city = new SysRegion();
                city.setN(region.getN());
                city.setRegionCode(region.getRegionCode());
                citysMap.put(city, region.getRegionParentId());
            } else if (region.getRegionLevel() == 3) {
                SysRegion area = new SysRegion();
                area.setN(region.getN());
                //   area.setRegionCode(region.getRegionCode());
                areasMap.put(area, region.getRegionParentId());
            }
        }

        for (Map.Entry<SysRegion, String> city : citysMap.entrySet()) {
            List<SysRegion> areas = new ArrayList<>();
            for (Map.Entry<SysRegion, String> area : areasMap.entrySet()) {
                if (city.getKey().getRegionCode().equals(area.getValue())) {
                    areas.add(area.getKey());
                }
            }
            //将区域给了城市
            city.getKey().setS(areas);
        }
        //将城市给省
        for (SysRegion province : provinces) {
            List<SysRegion> citys = new ArrayList<>();
            for (Map.Entry<SysRegion, String> city : citysMap.entrySet()) {
                if (province.getRegionCode().equals(city.getValue())) {
                    city.getKey().setRegionCode(null);
                    citys.add(city.getKey());
                }
            }
            province.setRegionCode(null);
            province.setS(citys);
        }
        ChinaRegions.addAll(provinces);
        String jsonRegions = JSON.toJSONString(ChinaRegions);
        return jsonRegions;
    }

    /**
     * 新增地区
     *
     * @param sysRegion 地区
     * @return 结果
     */
    @Override
    public int insertSysRegion(SysRegion sysRegion) {
        return sysRegionMapper.insertSysRegion(sysRegion);
    }

    /**
     * 修改地区
     *
     * @param sysRegion 地区
     * @return 结果
     */
    @Override
    public int updateSysRegion(SysRegion sysRegion) {
        return sysRegionMapper.updateSysRegion(sysRegion);
    }

    /**
     * 删除地区对象
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteSysRegionByIds(String ids) {
        return sysRegionMapper.deleteSysRegionByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除地区信息
     *
     * @param regionId 地区ID
     * @return 结果
     */
    @Override
    public int deleteSysRegionById(String regionId) {
        return sysRegionMapper.deleteSysRegionById(regionId);
    }
}
