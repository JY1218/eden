package com.eden.task.domain;

import com.eden.common.annotation.Excel;
import com.eden.common.core.domain.BaseEntity;
import com.eden.system.domain.SysFileInfo;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 任务图片对象 task_picture
 *
 * @author jiaoyang
 * @date 2020-09-08
 */
public class TaskPicture extends SysFileInfo
{
    private static final long serialVersionUID = 1L;

    /** 图片Id */
    private String pictureId;

    /** 任务Id */
    @Excel(name = "任务Id")
    private String taskCpid;

    /** 图片地址 */
    @Excel(name = "图片地址")
    private String pictureUrl;

    /** 图片状态 */
    @Excel(name = "图片状态")
    private String pictureStatus;

    /** 图片名称 */
    @Excel(name = "图片名称")
    private String pictureName;

    public void setPictureId(String pictureId)
    {
        this.pictureId = pictureId;
    }

    public String getPictureId()
    {
        return pictureId;
    }
    public void setTaskCpid(String taskCpid)
    {
        this.taskCpid = taskCpid;
    }

    public String getTaskCpid()
    {
        return taskCpid;
    }
    public void setPictureUrl(String pictureUrl)
    {
        this.pictureUrl = pictureUrl;
    }

    public String getPictureUrl()
    {
        return pictureUrl;
    }
    public void setPictureStatus(String pictureStatus)
    {
        this.pictureStatus = pictureStatus;
    }

    public String getPictureStatus()
    {
        return pictureStatus;
    }
    public void setPictureName(String pictureName)
    {
        this.pictureName = pictureName;
    }

    public String getPictureName()
    {
        return pictureName;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("pictureId", getPictureId())
                .append("taskCpid", getTaskCpid())
                .append("pictureUrl", getPictureUrl())
                .append("createBy", getCreateBy())
                .append("createTime", getCreateTime())
                .append("updateBy", getUpdateBy())
                .append("updateTime", getUpdateTime())
                .append("pictureStatus", getPictureStatus())
                .append("pictureName", getPictureName())
                .toString();
    }
}