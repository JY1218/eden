package com.eden.task.domain;

import com.eden.common.annotation.Excel;
import com.eden.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 审核任务回退原因对象 check_opinion
 * 
 * @author jiaoyang
 * @date 2020-09-14
 */
public class CheckOpinion extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 质检退回ID */
    private String checkOpinionId;

    /** 质检不通过原因 */
    @Excel(name = "质检不通过原因")
    private String content;

    /** 该订单的下一个流程 */
    @Excel(name = "该订单的下一个流程")
    private String nextFlow;

    public void setCheckOpinionId(String checkOpinionId) 
    {
        this.checkOpinionId = checkOpinionId;
    }

    public String getCheckOpinionId() 
    {
        return checkOpinionId;
    }
    public void setContent(String content) 
    {
        this.content = content;
    }

    public String getContent() 
    {
        return content;
    }
    public void setNextFlow(String nextFlow) 
    {
        this.nextFlow = nextFlow;
    }

    public String getNextFlow() 
    {
        return nextFlow;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
            .append("checkOpinionId", getCheckOpinionId())
            .append("content", getContent())
            .append("nextFlow", getNextFlow())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
