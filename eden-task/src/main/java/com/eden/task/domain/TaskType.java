package com.eden.task.domain;

import com.eden.common.annotation.Excel;
import com.eden.common.core.domain.BaseEntity;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

/**
 * 任务类型对象 task_type
 *
 * @author jiaoyang
 * @date 2021-01-03
 */
public class TaskType extends BaseEntity
{
    private static final long serialVersionUID = 1L;
    /** 任务类型id */
    private Integer typeId;

    /** 任务类型标签 */
    @Excel(name = "任务类型标签")
    private String typeLabel;

    /** 任务类型状态 */
    @Excel(name = "任务类型状态")
    private String typeStatus;

    /** 任务类型图标 */
    @Excel(name = "任务类型图标")
    private String icon;

    /** 任务类型颜色 */
    @Excel(name = "任务类型颜色")
    private String color;

    /** 任务类型消息数 */
    @Excel(name = "任务类型消息数")
    private String badge;

    public void setTypeId(Integer typeId)
    {
        this.typeId = typeId;
    }

    public Integer getTypeId()
    {
        return typeId;
    }
    public void setTypeLabel(String typeLabel)
    {
        this.typeLabel = typeLabel;
    }

    public String getTypeLabel()
    {
        return typeLabel;
    }
    public void setTypeStatus(String typeStatus)
    {
        this.typeStatus = typeStatus;
    }

    public String getTypeStatus()
    {
        return typeStatus;
    }
    public void setIcon(String icon)
    {
        this.icon = icon;
    }

    public String getIcon()
    {
        return icon;
    }
    public void setColor(String color)
    {
        this.color = color;
    }

    public String getColor()
    {
        return color;
    }
    public void setBadge(String badge)
    {
        this.badge = badge;
    }

    public String getBadge()
    {
        return badge;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
                .append("typeId", getTypeId())
                .append("typeLabel", getTypeLabel())
                .append("typeStatus", getTypeStatus())
                .append("createBy", getCreateBy())
                .append("icon", getIcon())
                .append("color", getColor())
                .append("createTime", getCreateTime())
                .append("updateBy", getUpdateBy())
                .append("badge", getBadge())
                .append("updateTime", getUpdateTime())
                .toString();
    }
}