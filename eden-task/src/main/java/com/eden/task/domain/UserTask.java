package com.eden.task.domain;

import com.eden.common.annotation.Excel;
import com.eden.common.core.domain.BaseEntity;
import com.eden.school.domain.SchoolUser;
import com.eden.system.domain.SysDept;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.List;

/**
 * 用户发布任务对象 user_task
 *
 * @author jiaoyang
 * @date 2020-08-31
 */
public class UserTask extends BaseEntity {
    private static final long serialVersionUID = 1L;
    /**
     * 任务id
     */
    private String taskId;

    /**
     * 任务名称
     */
    @Excel(name = "任务名称")
    private String taskName;

    /**
     * 任务描述
     */
    @Excel(name = "任务描述")
    private String taskDescription;

    /**
     * 任务状态 0：初始状态 1：待审核 2：审核通过 3：审核不通过 4：发布成功 5：发布失败
     */
    @Excel(name = "任务状态 0：初始状态 1：待审核 2：审核通过 3：审核不通过 4：发布成功 5：发布失败")
    private String taskStatus;

    /**
     * 赏金
     */
    @Excel(name = "赏金")
    private String money;

    /**
     * 任务类型 0：悬赏、1....
     */
    @Excel(name = "任务类型 0：悬赏、1....")
    private String taskType;

    /**
     * 电话号码
     */
    @Excel(name = "电话号码")
    private String phonenumber;

    /**
     * 地址
     */
    @Excel(name = "地址")
    private String address;

    /**
     * 审核不通过的意见
     */
    @Excel(name = "审核不通过的意见")
    private String checkOpinion;

    /**
     * 检查者Id
     */
    @Excel(name = "检查者Id")
    private String checkId;

    /**
     * 审核人
     */
    @Excel(name = "审核人")
    private String checkName;
    /**
     * 任务路径
     */
    private List<String> pictureUrls;
    /**
     * 任务图片
     */
    private List<TaskPicture> taskPictures;
    /**
     * 任务视频
     */
    private List<TaskVideo> taskVideos;
    /**
     * 校园用户
     */
    private SchoolUser schoolUser;
    /**
     * 部门
     */
    private SysDept sysDept;
    /**
     * 部门名称
     */
//    private String deptName;

    public SysDept getSysDept() {
        return sysDept;
    }

    public void setSysDept(SysDept sysDept) {
        this.sysDept = sysDept;
    }

//    public String getDeptName() {
//        return deptName;
//    }

//    public void setDeptName(String deptName) {
//        this.deptName = deptName;
//    }

    public SchoolUser getSchoolUser() {
        return schoolUser;
    }

    public void setSchoolUser(SchoolUser schoolUser) {
        this.schoolUser = schoolUser;
    }

    public List<String> getPictureUrls() {
        return pictureUrls;
    }

    public void setPictureUrls(List<String> pictureUrls) {
        this.pictureUrls = pictureUrls;
    }

    public List<TaskPicture> getTaskPictures() {
        return taskPictures;
    }

    public void setTaskPictures(List<TaskPicture> taskPictures) {
        this.taskPictures = taskPictures;
    }

    public List<TaskVideo> getTaskVideos() {
        return taskVideos;
    }

    public void setTaskVideos(List<TaskVideo> taskVideos) {
        this.taskVideos = taskVideos;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    public String getTaskId() {
        return taskId;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskDescription(String taskDescription) {
        this.taskDescription = taskDescription;
    }

    public String getTaskDescription() {
        return taskDescription;
    }

    public void setTaskStatus(String taskStatus) {
        this.taskStatus = taskStatus;
    }

    public String getTaskStatus() {
        return taskStatus;
    }

    public void setMoney(String money) {
        this.money = money;
    }

    public String getMoney() {
        return money;
    }

    public void setTaskType(String taskType) {
        this.taskType = taskType;
    }

    public String getTaskType() {
        return taskType;
    }

    public void setPhonenumber(String phonenumber) {
        this.phonenumber = phonenumber;
    }

    public String getPhonenumber() {
        return phonenumber;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAddress() {
        return address;
    }

    public void setCheckOpinion(String checkOpinion) {
        this.checkOpinion = checkOpinion;
    }

    public String getCheckOpinion() {
        return checkOpinion;
    }

    public void setCheckId(String checkId) {
        this.checkId = checkId;
    }

    public String getCheckId() {
        return checkId;
    }

    public void setCheckName(String checkName) {
        this.checkName = checkName;
    }

    public String getCheckName() {
        return checkName;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("taskId", getTaskId())
                .append("taskName", getTaskName())
                .append("taskDescription", getTaskDescription())
                .append("taskStatus", getTaskStatus())
                .append("money", getMoney())
                .append("taskType", getTaskType())
                .append("phonenumber", getPhonenumber())
                .append("address", getAddress())
                .append("createBy", getCreateBy())
                .append("createTime", getCreateTime())
                .append("updateBy", getUpdateBy())
                .append("updateTime", getUpdateTime())
                .append("checkOpinion", getCheckOpinion())
                .append("checkId", getCheckId())
                .append("checkName", getCheckName())
                .toString();
    }
}
