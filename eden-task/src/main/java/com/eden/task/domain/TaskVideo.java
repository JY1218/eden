package com.eden.task.domain;

import com.eden.common.annotation.Excel;
import com.eden.system.domain.SysFileInfo;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 用户发布视频对象 task_video
 *
 * @author jiaoyang
 * @date 2020-09-08
 */
public class TaskVideo extends SysFileInfo {
    private static final long serialVersionUID = 1L;

    /**
     * 视频Id
     */
    private String videoId;

    /**
     * 视频上传路径
     */
    @Excel(name = "视频上传路径")
    private String videoUrl;

    /**
     * 任务Id
     */
    @Excel(name = "任务Id")
    private String taskCvid;

    /**
     * 视频状态
     */
    @Excel(name = "视频状态")
    private String videoStatus;

    /**
     * 视频名称
     */
    @Excel(name = "视频名称")
    private String videoName;

    public void setVideoId(String videoId) {
        this.videoId = videoId;
    }

    public String getVideoId() {
        return videoId;
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }

    public String getVideoUrl() {
        return videoUrl;
    }

    public void setTaskCvid(String taskCvid) {
        this.taskCvid = taskCvid;
    }

    public String getTaskCvid() {
        return taskCvid;
    }

    public void setVideoStatus(String videoStatus) {
        this.videoStatus = videoStatus;
    }

    public String getVideoStatus() {
        return videoStatus;
    }

    public void setVideoName(String videoName) {
        this.videoName = videoName;
    }

    public String getVideoName() {
        return videoName;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("videoId", getVideoId())
                .append("videoUrl", getVideoUrl())
                .append("createBy", getCreateBy())
                .append("createTime", getCreateTime())
                .append("taskCvid", getTaskCvid())
                .append("updateBy", getUpdateBy())
                .append("updateTime", getUpdateTime())
                .append("videoStatus", getVideoStatus())
                .append("videoName", getVideoName())
                .toString();
    }
}