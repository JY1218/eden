package com.eden.task.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.eden.common.annotation.Log;
import com.eden.common.enums.BusinessType;
import com.eden.task.domain.CheckOpinion;
import com.eden.task.service.ICheckOpinionService;
import com.eden.common.core.controller.BaseController;
import com.eden.common.core.domain.AjaxResult;
import com.eden.common.utils.poi.ExcelUtil;
import com.eden.common.core.page.TableDataInfo;

/**
 * 审核任务回退原因Controller
 * 
 * @author jiaoyang
 * @date 2020-09-14
 */
@Controller
@RequestMapping("/task/opinion")
public class CheckOpinionController extends BaseController
{
    private String prefix = "task/opinion";

    @Autowired
    private ICheckOpinionService checkOpinionService;

    @RequiresPermissions("task:opinion:view")
    @GetMapping()
    public String opinion()
    {
        return prefix + "/opinion";
    }

    /**
     * 查询审核任务回退原因列表
     */
    @RequiresPermissions("task:opinion:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(CheckOpinion checkOpinion)
    {
        startPage();
        List<CheckOpinion> list = checkOpinionService.selectCheckOpinionList(checkOpinion);
        return getDataTable(list);
    }

    /**
     * 导出审核任务回退原因列表
     */
    @RequiresPermissions("task:opinion:export")
    @Log(title = "审核任务回退原因", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(CheckOpinion checkOpinion)
    {
        List<CheckOpinion> list = checkOpinionService.selectCheckOpinionList(checkOpinion);
        ExcelUtil<CheckOpinion> util = new ExcelUtil<CheckOpinion>(CheckOpinion.class);
        return util.exportExcel(list, "opinion");
    }

    /**
     * 新增审核任务回退原因
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存审核任务回退原因
     */
    @RequiresPermissions("task:opinion:add")
    @Log(title = "审核任务回退原因", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(CheckOpinion checkOpinion)
    {
        return toAjax(checkOpinionService.insertCheckOpinion(checkOpinion));
    }

    /**
     * 修改审核任务回退原因
     */
    @GetMapping("/edit/{checkOpinionId}")
    public String edit(@PathVariable("checkOpinionId") String checkOpinionId, ModelMap mmap)
    {
        CheckOpinion checkOpinion = checkOpinionService.selectCheckOpinionById(checkOpinionId);
        mmap.put("checkOpinion", checkOpinion);
        return prefix + "/edit";
    }

    /**
     * 修改保存审核任务回退原因
     */
    @RequiresPermissions("task:opinion:edit")
    @Log(title = "审核任务回退原因", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(CheckOpinion checkOpinion)
    {
        return toAjax(checkOpinionService.updateCheckOpinion(checkOpinion));
    }

    /**
     * 删除审核任务回退原因
     */
    @RequiresPermissions("task:opinion:remove")
    @Log(title = "审核任务回退原因", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(checkOpinionService.deleteCheckOpinionByIds(ids));
    }
}
