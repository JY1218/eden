package com.eden.task.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.eden.common.annotation.Log;
import com.eden.common.enums.BusinessType;
import com.eden.task.domain.TaskVideo;
import com.eden.task.service.ITaskVideoService;
import com.eden.common.core.controller.BaseController;
import com.eden.common.core.domain.AjaxResult;
import com.eden.common.utils.poi.ExcelUtil;
import com.eden.common.core.page.TableDataInfo;

/**
 * 用户发布视频Controller
 * 
 * @author jiaoyang
 * @date 2020-09-05
 */
@Controller
@RequestMapping("/task/userTaskVideo")
public class TaskVideoController extends BaseController
{
    private String prefix = "task/userTaskVideo";

    @Autowired
    private ITaskVideoService taskVideoService;

    @RequiresPermissions("task:userTaskVideo:view")
    @GetMapping()
    public String userTaskVideo()
    {
        return prefix + "/userTaskVideo";
    }

    /**
     * 查询用户发布视频列表
     */
    @RequiresPermissions("task:userTaskVideo:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(TaskVideo taskVideo)
    {
        startPage();
        List<TaskVideo> list = taskVideoService.selectTaskVideoList(taskVideo);
        return getDataTable(list);
    }

    /**
     * 导出用户发布视频列表
     */
    @RequiresPermissions("task:userTaskVideo:export")
    @Log(title = "用户发布视频", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(TaskVideo taskVideo)
    {
        List<TaskVideo> list = taskVideoService.selectTaskVideoList(taskVideo);
        ExcelUtil<TaskVideo> util = new ExcelUtil<TaskVideo>(TaskVideo.class);
        return util.exportExcel(list, "userTaskVideo");
    }

    /**
     * 新增用户发布视频
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存用户发布视频
     */
    @RequiresPermissions("task:userTaskVideo:add")
    @Log(title = "用户发布视频", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(TaskVideo taskVideo)
    {
        return toAjax(taskVideoService.insertTaskVideo(taskVideo));
    }

    /**
     * 修改用户发布视频
     */
    @GetMapping("/edit/{videoId}")
    public String edit(@PathVariable("videoId") String videoId, ModelMap mmap)
    {
        TaskVideo taskVideo = taskVideoService.selectTaskVideoById(videoId);
        mmap.put("taskVideo", taskVideo);
        return prefix + "/edit";
    }

    /**
     * 修改保存用户发布视频
     */
    @RequiresPermissions("task:userTaskVideo:edit")
    @Log(title = "用户发布视频", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(TaskVideo taskVideo)
    {
        return toAjax(taskVideoService.updateTaskVideo(taskVideo));
    }

    /**
     * 删除用户发布视频
     */
    @RequiresPermissions("task:userTaskVideo:remove")
    @Log(title = "用户发布视频", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(taskVideoService.deleteTaskVideoByIds(ids));
    }
}
