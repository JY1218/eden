package com.eden.task.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.eden.common.annotation.Log;
import com.eden.common.enums.BusinessType;
import com.eden.task.domain.TaskPicture;
import com.eden.task.service.ITaskPictureService;
import com.eden.common.core.controller.BaseController;
import com.eden.common.core.domain.AjaxResult;
import com.eden.common.utils.poi.ExcelUtil;
import com.eden.common.core.page.TableDataInfo;

/**
 * 任务图片Controller
 * 
 * @author jiaoyang
 * @date 2020-09-03
 */
@Controller
@RequestMapping("/task/picture")
public class TaskPictureController extends BaseController
{
    private String prefix = "task/picture";

    @Autowired
    private ITaskPictureService taskPictureService;

    @RequiresPermissions("task:picture:view")
    @GetMapping()
    public String picture()
    {
        return prefix + "/picture";
    }

    /**
     * 查询任务图片列表
     */
    @RequiresPermissions("task:picture:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(TaskPicture taskPicture)
    {
        startPage();
        List<TaskPicture> list = taskPictureService.selectTaskPictureList(taskPicture);
        return getDataTable(list);
    }

    /**
     * 导出任务图片列表
     */
    @RequiresPermissions("task:picture:export")
    @Log(title = "任务图片", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(TaskPicture taskPicture)
    {
        List<TaskPicture> list = taskPictureService.selectTaskPictureList(taskPicture);
        ExcelUtil<TaskPicture> util = new ExcelUtil<TaskPicture>(TaskPicture.class);
        return util.exportExcel(list, "picture");
    }

    /**
     * 新增任务图片
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存任务图片
     */
    @RequiresPermissions("task:picture:add")
    @Log(title = "任务图片", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(TaskPicture taskPicture)
    {
        return toAjax(taskPictureService.insertTaskPicture(taskPicture));
    }

    /**
     * 修改任务图片
     */
    @GetMapping("/edit/{pictureId}")
    public String edit(@PathVariable("pictureId") String pictureId, ModelMap mmap)
    {
        TaskPicture taskPicture = taskPictureService.selectTaskPictureById(pictureId);
        mmap.put("taskPicture", taskPicture);
        return prefix + "/edit";
    }

    /**
     * 修改保存任务图片
     */
    @RequiresPermissions("task:picture:edit")
    @Log(title = "任务图片", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(TaskPicture taskPicture)
    {
        return toAjax(taskPictureService.updateTaskPicture(taskPicture));
    }

    /**
     * 删除任务图片
     */
    @RequiresPermissions("task:picture:remove")
    @Log(title = "任务图片", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(taskPictureService.deleteTaskPictureByIds(ids));
    }
}
