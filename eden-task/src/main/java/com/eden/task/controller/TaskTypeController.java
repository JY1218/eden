package com.eden.task.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.eden.common.annotation.Log;
import com.eden.common.enums.BusinessType;
import com.eden.task.domain.TaskType;
import com.eden.task.service.ITaskTypeService;
import com.eden.common.core.controller.BaseController;
import com.eden.common.core.domain.AjaxResult;
import com.eden.common.utils.poi.ExcelUtil;
import com.eden.common.core.page.TableDataInfo;

/**
 * 任务类型Controller
 *
 * @author jiaoyang
 * @date 2021-01-03
 */
@Controller
@RequestMapping("/task/taskType")
public class TaskTypeController extends BaseController
{
    private String prefix = "task/taskType";

    @Autowired
    private ITaskTypeService taskTypeService;

    @RequiresPermissions("task:taskType:view")
    @GetMapping()
    public String taskType()
    {
        return prefix + "/taskType";
    }

    /**
     * 查询任务类型列表
     */
    @RequiresPermissions("task:taskType:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(TaskType taskType)
    {
        startPage();
        List<TaskType> list = taskTypeService.selectTaskTypeList(taskType);
        return getDataTable(list);
    }

    /**
     * 导出任务类型列表
     */
    @RequiresPermissions("task:taskType:export")
    @Log(title = "任务类型", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(TaskType taskType)
    {
        List<TaskType> list = taskTypeService.selectTaskTypeList(taskType);
        ExcelUtil<TaskType> util = new ExcelUtil<TaskType>(TaskType.class);
        return util.exportExcel(list, "taskType");
    }

    /**
     * 新增任务类型
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存任务类型
     */
    @RequiresPermissions("task:taskType:add")
    @Log(title = "任务类型", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(TaskType taskType)
    {
        return toAjax(taskTypeService.insertTaskType(taskType));
    }

    /**
     * 修改任务类型
     */
    @GetMapping("/edit/{typeId}")
    public String edit(@PathVariable("typeId") Integer typeId, ModelMap mmap)
    {
        TaskType taskType = taskTypeService.selectTaskTypeById(typeId);
        mmap.put("taskType", taskType);
        return prefix + "/edit";
    }

    /**
     * 修改保存任务类型
     */
    @RequiresPermissions("task:taskType:edit")
    @Log(title = "任务类型", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(TaskType taskType)
    {
        return toAjax(taskTypeService.updateTaskType(taskType));
    }

    /**
     * 删除任务类型
     */
    @RequiresPermissions("task:taskType:remove")
    @Log(title = "任务类型", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(taskTypeService.deleteTaskTypeByIds(ids));
    }
}