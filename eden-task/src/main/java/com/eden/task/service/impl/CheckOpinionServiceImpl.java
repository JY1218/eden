package com.eden.task.service.impl;

import java.util.List;
import com.eden.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.eden.task.mapper.CheckOpinionMapper;
import com.eden.task.domain.CheckOpinion;
import com.eden.task.service.ICheckOpinionService;
import com.eden.common.core.text.Convert;

/**
 * 审核任务回退原因Service业务层处理
 * 
 * @author jiaoyang
 * @date 2020-09-14
 */
@Service
public class CheckOpinionServiceImpl implements ICheckOpinionService 
{
    @Autowired
    private CheckOpinionMapper checkOpinionMapper;

    /**
     * 查询审核任务回退原因
     * 
     * @param checkOpinionId 审核任务回退原因ID
     * @return 审核任务回退原因
     */
    @Override
    public CheckOpinion selectCheckOpinionById(String checkOpinionId)
    {
        return checkOpinionMapper.selectCheckOpinionById(checkOpinionId);
    }

    /**
     * 查询审核任务回退原因列表
     * 
     * @param checkOpinion 审核任务回退原因
     * @return 审核任务回退原因
     */
    @Override
    public List<CheckOpinion> selectCheckOpinionList(CheckOpinion checkOpinion)
    {
        return checkOpinionMapper.selectCheckOpinionList(checkOpinion);
    }

    /**
     * 新增审核任务回退原因
     * 
     * @param checkOpinion 审核任务回退原因
     * @return 结果
     */
    @Override
    public int insertCheckOpinion(CheckOpinion checkOpinion)
    {
        checkOpinion.setCreateTime(DateUtils.getNowDate());
        return checkOpinionMapper.insertCheckOpinion(checkOpinion);
    }

    /**
     * 修改审核任务回退原因
     * 
     * @param checkOpinion 审核任务回退原因
     * @return 结果
     */
    @Override
    public int updateCheckOpinion(CheckOpinion checkOpinion)
    {
        checkOpinion.setUpdateTime(DateUtils.getNowDate());
        return checkOpinionMapper.updateCheckOpinion(checkOpinion);
    }

    /**
     * 删除审核任务回退原因对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteCheckOpinionByIds(String ids)
    {
        return checkOpinionMapper.deleteCheckOpinionByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除审核任务回退原因信息
     * 
     * @param checkOpinionId 审核任务回退原因ID
     * @return 结果
     */
    @Override
    public int deleteCheckOpinionById(String checkOpinionId)
    {
        return checkOpinionMapper.deleteCheckOpinionById(checkOpinionId);
    }
}
