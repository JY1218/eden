package com.eden.task.service;

import java.util.List;
import com.eden.task.domain.TaskVideo;

/**
 * 用户发布视频Service接口
 * 
 * @author jiaoyang
 * @date 2020-09-05
 */
public interface ITaskVideoService 
{
    /**
     * 查询用户发布视频
     * 
     * @param videoId 用户发布视频ID
     * @return 用户发布视频
     */
    public TaskVideo selectTaskVideoById(String videoId);

    /**
     * 查询用户发布视频列表
     * 
     * @param taskVideo 用户发布视频
     * @return 用户发布视频集合
     */
    public List<TaskVideo> selectTaskVideoList(TaskVideo taskVideo);

    /**
     * 新增用户发布视频
     * 
     * @param taskVideo 用户发布视频
     * @return 结果
     */
    public int insertTaskVideo(TaskVideo taskVideo);

    /**
     * 修改用户发布视频
     * 
     * @param taskVideo 用户发布视频
     * @return 结果
     */
    public int updateTaskVideo(TaskVideo taskVideo);

    /**
     * 批量删除用户发布视频
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteTaskVideoByIds(String ids);

    /**
     * 删除用户发布视频信息
     * 
     * @param videoId 用户发布视频ID
     * @return 结果
     */
    public int deleteTaskVideoById(String videoId);
}
