package com.eden.task.service.impl;

import com.eden.common.core.text.Convert;
import com.eden.common.utils.DateUtils;
import com.eden.framework.util.ShiroUtils;
import com.eden.task.domain.UserTask;
import com.eden.task.mapper.UserTaskMapper;
import com.eden.task.service.IUserTaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 用户发布任务Service业务层处理
 *
 * @author jiaoyang
 * @date 2020-09-03
 */
@Service
public class UserTaskServiceImpl implements IUserTaskService {
    @Autowired
    private UserTaskMapper userTaskMapper;

    /**
     * 查询用户发布任务
     *
     * @param taskId 用户发布任务ID
     * @return 用户发布任务
     */
    @Override
    public UserTask selectUserTaskById(String taskId) {
        return userTaskMapper.selectUserTaskById(taskId);
    }

    /**
     * 查询用户发布任务列表
     *
     * @param userTask 用户发布任务
     * @return 用户发布任务
     */
    @Override
    public List<UserTask> selectUserTaskList(UserTask userTask) {
        return userTaskMapper.selectUserTaskList(userTask);
    }

    @Override
    public List<UserTask> selectUserTaskListApi(UserTask userTask) {
        return userTaskMapper.selectUserTaskListApi(userTask);
    }


    /**
     * 新增用户发布任务
     *
     * @param userTask 用户发布任务
     * @return 结果
     */
    @Override
    public int insertUserTask(UserTask userTask) {
        userTask.setCreateTime(DateUtils.getNowDate());
        return userTaskMapper.insertUserTask(userTask);
    }

    /**
     * 修改用户发布任务
     *
     * @param userTask 用户发布任务
     * @return 结果
     */
    @Override
    public int updateUserTask(UserTask userTask) {
        userTask.setUpdateTime(DateUtils.getNowDate());
        userTask.setUpdateBy(ShiroUtils.getLoginName());
        return userTaskMapper.updateUserTask(userTask);
    }

    /**
     * 删除用户发布任务对象
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteUserTaskByIds(String ids) {
        return userTaskMapper.deleteUserTaskByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除用户发布任务信息
     *
     * @param taskId 用户发布任务ID
     * @return 结果
     */
    @Override
    public int deleteUserTaskById(String taskId) {
        return userTaskMapper.deleteUserTaskById(taskId);
    }

    @Override
    public List<UserTask> selectUserCheckedTaskList(UserTask userTask) {
        return userTaskMapper.selectUserTaskList(userTask);
    }

    @Override
    public List<UserTask> selectUserTaskListByTaskName(String taskName) {
        return userTaskMapper.selectUserTaskListByTaskName(taskName);
    }

    @Override
    public UserTask selectTaskDetailApi(UserTask userTask) {
        return userTaskMapper.selectTaskDetailApi(userTask);
    }
}