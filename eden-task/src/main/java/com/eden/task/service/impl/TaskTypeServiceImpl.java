package com.eden.task.service.impl;

import java.util.List;
import com.eden.common.utils.DateUtils;
import com.eden.framework.util.ShiroUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.eden.task.mapper.TaskTypeMapper;
import com.eden.task.domain.TaskType;
import com.eden.task.service.ITaskTypeService;
import com.eden.common.core.text.Convert;

/**
 * 任务类型Service业务层处理
 *
 * @author jiaoyang
 * @date 2021-01-03
 */
@Service
public class TaskTypeServiceImpl implements ITaskTypeService
{
    @Autowired
    private TaskTypeMapper taskTypeMapper;

    /**
     * 查询任务类型
     *
     * @param typeId 任务类型ID
     * @return 任务类型
     */
    @Override
    public TaskType selectTaskTypeById(Integer typeId)
    {
        return taskTypeMapper.selectTaskTypeById(typeId);
    }

    /**
     * 查询任务类型列表
     *
     * @param taskType 任务类型
     * @return 任务类型
     */
    @Override
    public List<TaskType> selectTaskTypeList(TaskType taskType)
    {
        return taskTypeMapper.selectTaskTypeList(taskType);
    }

    /**
     * 新增任务类型
     *
     * @param taskType 任务类型
     * @return 结果
     */
    @Override
    public int insertTaskType(TaskType taskType)
    {
        taskType.setCreateTime(DateUtils.getNowDate());
        taskType.setCreateBy(ShiroUtils.getSysUser().getLoginName());
        return taskTypeMapper.insertTaskType(taskType);
    }

    /**
     * 修改任务类型
     *
     * @param taskType 任务类型
     * @return 结果
     */
    @Override
    public int updateTaskType(TaskType taskType)
    {
        taskType.setUpdateTime(DateUtils.getNowDate());
        taskType.setUpdateBy(ShiroUtils.getSysUser().getLoginName());
        return taskTypeMapper.updateTaskType(taskType);
    }

    /**
     * 删除任务类型对象
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteTaskTypeByIds(String ids)
    {
        return taskTypeMapper.deleteTaskTypeByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除任务类型信息
     *
     * @param typeId 任务类型ID
     * @return 结果
     */
    @Override
    public int deleteTaskTypeById(Integer typeId)
    {
        return taskTypeMapper.deleteTaskTypeById(typeId);
    }
}