package com.eden.task.service.impl;

import com.eden.common.core.text.Convert;
import com.eden.common.utils.DateUtils;
import com.eden.framework.util.ShiroUtils;
import com.eden.task.domain.TaskVideo;
import com.eden.task.mapper.TaskVideoMapper;
import com.eden.task.service.ITaskVideoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 用户发布视频Service业务层处理
 *
 * @author jiaoyang
 * @date 2020-09-05
 */
@Service
public class TaskVideoServiceImpl implements ITaskVideoService {
    @Autowired
    private TaskVideoMapper taskVideoMapper;

    /**
     * 查询用户发布视频
     *
     * @param videoId 用户发布视频ID
     * @return 用户发布视频
     */
    @Override
    public TaskVideo selectTaskVideoById(String videoId) {
        return taskVideoMapper.selectTaskVideoById(videoId);
    }

    /**
     * 查询用户发布视频列表
     *
     * @param taskVideo 用户发布视频
     * @return 用户发布视频
     */
    @Override
    public List<TaskVideo> selectTaskVideoList(TaskVideo taskVideo) {
        return taskVideoMapper.selectTaskVideoList(taskVideo);
    }

    /**
     * 新增用户发布视频
     *
     * @param taskVideo 用户发布视频
     * @return 结果
     */
    @Override
    public int insertTaskVideo(TaskVideo taskVideo) {
        taskVideo.setCreateTime(DateUtils.getNowDate());
        taskVideo.setCreateBy(ShiroUtils.getLoginName());
        return taskVideoMapper.insertTaskVideo(taskVideo);
    }

    /**
     * 修改用户发布视频
     *
     * @param taskVideo 用户发布视频
     * @return 结果
     */
    @Override
    public int updateTaskVideo(TaskVideo taskVideo) {
        taskVideo.setUpdateTime(DateUtils.getNowDate());
        return taskVideoMapper.updateTaskVideo(taskVideo);
    }

    /**
     * 删除用户发布视频对象
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteTaskVideoByIds(String ids) {
        return taskVideoMapper.deleteTaskVideoByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除用户发布视频信息
     *
     * @param videoId 用户发布视频ID
     * @return 结果
     */
    @Override
    public int deleteTaskVideoById(String videoId) {
        return taskVideoMapper.deleteTaskVideoById(videoId);
    }
}
