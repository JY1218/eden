package com.eden.task.service.impl;

import com.eden.common.core.text.Convert;
import com.eden.common.utils.DateUtils;
import com.eden.framework.util.ShiroUtils;
import com.eden.task.domain.TaskPicture;
import com.eden.task.mapper.TaskPictureMapper;
import com.eden.task.service.ITaskPictureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 任务图片Service业务层处理
 *
 * @author jiaoyang
 * @date 2020-09-03
 */
@Service
public class TaskPictureServiceImpl implements ITaskPictureService {
    @Autowired
    private TaskPictureMapper taskPictureMapper;

    /**
     * 查询任务图片
     *
     * @param pictureId 任务图片ID
     * @return 任务图片
     */
    @Override
    public TaskPicture selectTaskPictureById(String pictureId) {
        return taskPictureMapper.selectTaskPictureById(pictureId);
    }

    /**
     * 查询任务图片列表
     *
     * @param taskPicture 任务图片
     * @return 任务图片
     */
    @Override
    public List<TaskPicture> selectTaskPictureList(TaskPicture taskPicture) {
        return taskPictureMapper.selectTaskPictureList(taskPicture);
    }

    /**
     * 新增任务图片
     *
     * @param taskPicture 任务图片
     * @return 结果
     */
    @Override
    public int insertTaskPicture(TaskPicture taskPicture) {
        taskPicture.setCreateTime(DateUtils.getNowDate());
        return taskPictureMapper.insertTaskPicture(taskPicture);
    }

    /**
     * 修改任务图片
     *
     * @param taskPicture 任务图片
     * @return 结果
     */
    @Override
    public int updateTaskPicture(TaskPicture taskPicture) {
        taskPicture.setUpdateTime(DateUtils.getNowDate());
        return taskPictureMapper.updateTaskPicture(taskPicture);
    }

    /**
     * 删除任务图片对象
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteTaskPictureByIds(String ids) {
        return taskPictureMapper.deleteTaskPictureByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除任务图片信息
     *
     * @param pictureId 任务图片ID
     * @return 结果
     */
    @Override
    public int deleteTaskPictureById(String pictureId) {
        return taskPictureMapper.deleteTaskPictureById(pictureId);
    }
}
