package com.eden.task.service;

import java.util.List;
import com.eden.task.domain.TaskType;

/**
 * 任务类型Service接口
 *
 * @author jiaoyang
 * @date 2021-01-03
 */
public interface ITaskTypeService
{
    /**
     * 查询任务类型
     *
     * @param typeId 任务类型ID
     * @return 任务类型
     */
    public TaskType selectTaskTypeById(Integer typeId);

    /**
     * 查询任务类型列表
     *
     * @param taskType 任务类型
     * @return 任务类型集合
     */
    public List<TaskType> selectTaskTypeList(TaskType taskType);

    /**
     * 新增任务类型
     *
     * @param taskType 任务类型
     * @return 结果
     */
    public int insertTaskType(TaskType taskType);

    /**
     * 修改任务类型
     *
     * @param taskType 任务类型
     * @return 结果
     */
    public int updateTaskType(TaskType taskType);

    /**
     * 批量删除任务类型
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteTaskTypeByIds(String ids);

    /**
     * 删除任务类型信息
     *
     * @param typeId 任务类型ID
     * @return 结果
     */
    public int deleteTaskTypeById(Integer typeId);
}