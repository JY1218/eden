package com.eden.task.service;

import com.eden.task.domain.UserTask;

import java.util.List;

/**
 * 用户发布任务Service接口
 *
 * @author jiaoyang
 * @date 2020-09-03
 */
public interface IUserTaskService {
    /**
     * 查询用户发布任务
     *
     * @param taskId 用户发布任务ID
     * @return 用户发布任务
     */
    public UserTask selectUserTaskById(String taskId);

    /**
     * 查询用户发布任务列表
     *
     * @param userTask 用户发布任务
     * @return 用户发布任务集合
     */
    public List<UserTask> selectUserTaskList(UserTask userTask);

    public List<UserTask> selectUserTaskListApi(UserTask userTask);

    /**
     * 新增用户发布任务
     *
     * @param userTask 用户发布任务
     * @return 结果
     */
    public int insertUserTask(UserTask userTask);

    /**
     * 修改用户发布任务
     *
     * @param userTask 用户发布任务
     * @return 结果
     */
    public int updateUserTask(UserTask userTask);

    /**
     * 批量删除用户发布任务
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteUserTaskByIds(String ids);

    /**
     * 删除用户发布任务信息
     *
     * @param taskId 用户发布任务ID
     * @return 结果
     */
    public int deleteUserTaskById(String taskId);

    /**
     * 查询用户发布后审核通过的信息
     *
     * @param userTask
     * @return
     */
    List<UserTask> selectUserCheckedTaskList(UserTask userTask);

    /**
     * 根据任务名称查询任务信息集合
     * @param taskName
     * @return
     */
    List<UserTask> selectUserTaskListByTaskName(String taskName);
    /**
     * 根据任务名称查询任务详细信息
     * @param userTask
     * @return
     */
    UserTask selectTaskDetailApi(UserTask userTask);
}