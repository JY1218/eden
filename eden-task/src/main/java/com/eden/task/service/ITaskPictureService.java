package com.eden.task.service;

import java.util.List;
import com.eden.task.domain.TaskPicture;

/**
 * 任务图片Service接口
 * 
 * @author jiaoyang
 * @date 2020-09-03
 */
public interface ITaskPictureService 
{
    /**
     * 查询任务图片
     * 
     * @param pictureId 任务图片ID
     * @return 任务图片
     */
    public TaskPicture selectTaskPictureById(String pictureId);

    /**
     * 查询任务图片列表
     * 
     * @param taskPicture 任务图片
     * @return 任务图片集合
     */
    public List<TaskPicture> selectTaskPictureList(TaskPicture taskPicture);

    /**
     * 新增任务图片
     * 
     * @param taskPicture 任务图片
     * @return 结果
     */
    public int insertTaskPicture(TaskPicture taskPicture);

    /**
     * 修改任务图片
     * 
     * @param taskPicture 任务图片
     * @return 结果
     */
    public int updateTaskPicture(TaskPicture taskPicture);

    /**
     * 批量删除任务图片
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteTaskPictureByIds(String ids);

    /**
     * 删除任务图片信息
     * 
     * @param pictureId 任务图片ID
     * @return 结果
     */
    public int deleteTaskPictureById(String pictureId);
}
