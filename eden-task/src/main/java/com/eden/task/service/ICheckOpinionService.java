package com.eden.task.service;

import java.util.List;
import com.eden.task.domain.CheckOpinion;

/**
 * 审核任务回退原因Service接口
 * 
 * @author jiaoyang
 * @date 2020-09-14
 */
public interface ICheckOpinionService 
{
    /**
     * 查询审核任务回退原因
     * 
     * @param checkOpinionId 审核任务回退原因ID
     * @return 审核任务回退原因
     */
    public CheckOpinion selectCheckOpinionById(String checkOpinionId);

    /**
     * 查询审核任务回退原因列表
     * 
     * @param checkOpinion 审核任务回退原因
     * @return 审核任务回退原因集合
     */
    public List<CheckOpinion> selectCheckOpinionList(CheckOpinion checkOpinion);

    /**
     * 新增审核任务回退原因
     * 
     * @param checkOpinion 审核任务回退原因
     * @return 结果
     */
    public int insertCheckOpinion(CheckOpinion checkOpinion);

    /**
     * 修改审核任务回退原因
     * 
     * @param checkOpinion 审核任务回退原因
     * @return 结果
     */
    public int updateCheckOpinion(CheckOpinion checkOpinion);

    /**
     * 批量删除审核任务回退原因
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteCheckOpinionByIds(String ids);

    /**
     * 删除审核任务回退原因信息
     * 
     * @param checkOpinionId 审核任务回退原因ID
     * @return 结果
     */
    public int deleteCheckOpinionById(String checkOpinionId);
}
