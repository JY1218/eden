package com.eden.task.mapper;

import com.eden.task.domain.TaskPicture;

import java.util.List;

/**
 * 任务图片Mapper接口
 *
 * @author jiaoyang
 * @date 2020-09-03
 */
public interface TaskPictureMapper {
    /**
     * 查询任务图片
     *
     * @param pictureId 任务图片ID
     * @return 任务图片
     */
    public TaskPicture selectTaskPictureById(String pictureId);

    /**
     * 查询任务图片列表
     *
     * @param taskPicture 任务图片
     * @return 任务图片集合
     */
    public List<TaskPicture> selectTaskPictureList(TaskPicture taskPicture);

    /**
     * 查询任务图片列表通过外键taskId
     *
     * @param taskPicture 任务图片
     * @return 任务图片集合
     */
    public List<TaskPicture> selectTaskPictureListByTaskCpid(TaskPicture taskPicture);

    /**
     * 新增任务图片
     *
     * @param taskPicture 任务图片
     * @return 结果
     */
    public int insertTaskPicture(TaskPicture taskPicture);

    /**
     * 修改任务图片
     *
     * @param taskPicture 任务图片
     * @return 结果
     */
    public int updateTaskPicture(TaskPicture taskPicture);

    /**
     * 删除任务图片
     *
     * @param pictureId 任务图片ID
     * @return 结果
     */
    public int deleteTaskPictureById(String pictureId);

    /**
     * 批量删除任务图片
     *
     * @param pictureIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteTaskPictureByIds(String[] pictureIds);
}
