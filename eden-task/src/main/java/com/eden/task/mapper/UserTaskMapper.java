package com.eden.task.mapper;

import com.eden.task.domain.UserTask;

import java.util.List;

/**
 * 用户发布任务Mapper接口
 *
 * @author jiaoyang
 * @date 2020-08-31
 */
public interface UserTaskMapper {
    /**
     * 查询用户发布任务
     *
     * @param taskId 用户发布任务ID
     * @return 用户发布任务
     */
    public UserTask selectUserTaskById(String taskId);

    /**
     * 查询用户发布任务列表
     *
     * @param userTask 用户发布任务
     * @return 用户发布任务集合
     */
    public List<UserTask> selectUserTaskList(UserTask userTask);

    /**
     * 客户端 查询用户发布任务列表
     *
     * @param userTask 用户发布任务
     * @return 用户发布任务集合
     */
    public List<UserTask> selectUserTaskListApi(UserTask userTask);

    /**
     * 新增用户发布任务
     *
     * @param userTask 用户发布任务
     * @return 结果
     */
    public int insertUserTask(UserTask userTask);

    /**
     * 修改用户发布任务
     *
     * @param userTask 用户发布任务
     * @return 结果
     */
    public int updateUserTask(UserTask userTask);

    /**
     * 删除用户发布任务
     *
     * @param taskId 用户发布任务ID
     * @return 结果
     */
    public int deleteUserTaskById(String taskId);

    /**
     * 批量删除用户发布任务
     *
     * @param taskIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteUserTaskByIds(String[] taskIds);

    List<UserTask> selectUserTaskListByTaskName(String taskName);

    UserTask selectTaskDetailApi(UserTask userTask);
}
