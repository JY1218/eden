package com.eden.task.mapper;

import com.eden.task.domain.TaskVideo;

import java.util.List;

/**
 * 用户发布视频Mapper接口
 *
 * @author jiaoyang
 * @date 2020-09-05
 */
public interface TaskVideoMapper {
    /**
     * 查询用户发布视频
     *
     * @param videoId 用户发布视频ID
     * @return 用户发布视频
     */
    public TaskVideo selectTaskVideoById(String videoId);

    /**
     * 查询用户发布视频列表
     *
     * @param taskVideo 用户发布视频
     * @return 用户发布视频集合
     */
    public List<TaskVideo> selectTaskVideoList(TaskVideo taskVideo);

    /**
     * 查询用户发布视频列表通过外键taskId
     *
     * @param taskVideo 用户发布视频
     * @return 用户发布视频集合
     */
    public List<TaskVideo> selectTaskVideoListByTaskCvid(TaskVideo taskVideo);

    /**
     * 新增用户发布视频
     *
     * @param taskVideo 用户发布视频
     * @return 结果
     */
    public int insertTaskVideo(TaskVideo taskVideo);

    /**
     * 修改用户发布视频
     *
     * @param taskVideo 用户发布视频
     * @return 结果
     */
    public int updateTaskVideo(TaskVideo taskVideo);

    /**
     * 删除用户发布视频
     *
     * @param videoId 用户发布视频ID
     * @return 结果
     */
    public int deleteTaskVideoById(String videoId);

    /**
     * 批量删除用户发布视频
     *
     * @param videoIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteTaskVideoByIds(String[] videoIds);
}
