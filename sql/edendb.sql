/*
 Navicat MySQL Data Transfer

 Source Server         : 本地数据库
 Source Server Type    : MySQL
 Source Server Version : 50641
 Source Host           : localhost:3306
 Source Schema         : edendb

 Target Server Type    : MySQL
 Target Server Version : 50641
 File Encoding         : 65001

 Date: 03/11/2020 14:39:33
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for check_opinion
-- ----------------------------
DROP TABLE IF EXISTS `check_opinion`;
CREATE TABLE `check_opinion` (
  `check_opinion_id` varchar(64) NOT NULL COMMENT '质检退回ID',
  `content` varchar(255) NOT NULL COMMENT '质检不通过原因',
  `next_flow` varchar(50) NOT NULL COMMENT '该订单的下一个流程（1：该订单需要重录；2：该订单需要进行差错处理）',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`check_opinion_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='审核任务回退原因表';

-- ----------------------------
-- Records of check_opinion
-- ----------------------------
BEGIN;
INSERT INTO `check_opinion` VALUES ('C00000000001', '视频含有不良信息', '1', 'admin', '2020-07-16 21:03:45', 'admin', '2020-08-12 09:18:34');
INSERT INTO `check_opinion` VALUES ('C00000000002', '图片含有不良信息', '1', 'admin', '2020-07-16 21:03:58', '', NULL);
INSERT INTO `check_opinion` VALUES ('C00000000003', '任务含有不良信息', '1', 'admin', '2020-07-16 21:04:08', '', NULL);
INSERT INTO `check_opinion` VALUES ('C00000000004', '任务含有虚假诈骗信息', '1', 'admin', '2020-07-16 21:04:27', '', NULL);
INSERT INTO `check_opinion` VALUES ('C00000000005', '视频含有虚假诈骗信息', '1', 'admin', '2020-07-16 21:04:37', '', NULL);
INSERT INTO `check_opinion` VALUES ('C00000000006', '图片含有虚假诈骗信息', '1', 'admin', '2020-07-16 21:04:48', 'admin', '2020-08-12 09:18:04');
INSERT INTO `check_opinion` VALUES ('C00000000007', '其他违规行为', '1', 'admin', '2020-07-16 21:04:57', '', NULL);
COMMIT;

-- ----------------------------
-- Table structure for gen_table
-- ----------------------------
DROP TABLE IF EXISTS `gen_table`;
CREATE TABLE `gen_table` (
  `table_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `table_name` varchar(200) DEFAULT '' COMMENT '表名称',
  `table_comment` varchar(500) DEFAULT '' COMMENT '表描述',
  `sub_table_name` varchar(64) DEFAULT NULL COMMENT '关联子表的表名',
  `sub_table_fk_name` varchar(64) DEFAULT NULL COMMENT '子表关联的外键名',
  `class_name` varchar(100) DEFAULT '' COMMENT '实体类名称',
  `tpl_category` varchar(200) DEFAULT 'crud' COMMENT '使用的模板（crud单表操作 tree树表操作 sub主子表操作）',
  `package_name` varchar(100) DEFAULT NULL COMMENT '生成包路径',
  `module_name` varchar(30) DEFAULT NULL COMMENT '生成模块名',
  `business_name` varchar(30) DEFAULT NULL COMMENT '生成业务名',
  `function_name` varchar(50) DEFAULT NULL COMMENT '生成功能名',
  `function_author` varchar(50) DEFAULT NULL COMMENT '生成功能作者',
  `gen_type` char(1) DEFAULT '0' COMMENT '生成代码方式（0zip压缩包 1自定义路径）',
  `gen_path` varchar(200) DEFAULT '/' COMMENT '生成路径（不填默认项目路径）',
  `options` varchar(1000) DEFAULT NULL COMMENT '其它生成选项',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`table_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='代码生成业务表';

-- ----------------------------
-- Records of gen_table
-- ----------------------------
BEGIN;
INSERT INTO `gen_table` VALUES (2, 'task_picture', '任务图片表', NULL, NULL, 'TaskPicture', 'crud', 'com.eden.task', 'task', 'picture', '任务图片', 'jiaoyang', '0', '/', '{\"parentMenuId\":\"1066\",\"treeName\":\"\",\"treeParentCode\":\"\",\"parentMenuName\":\"校园任务管理\",\"treeCode\":\"\"}', 'admin', '2020-08-30 19:14:37', '', '2020-09-03 16:29:59', '');
INSERT INTO `gen_table` VALUES (3, 'user_task', '用户发布任务表', NULL, NULL, 'UserTask', 'crud', 'com.eden.task', 'task', 'usertask', '用户发布任务', 'jiaoyang', '0', '/', '{\"parentMenuId\":\"1064\",\"treeName\":\"\",\"treeParentCode\":\"\",\"parentMenuName\":\"校园管理\",\"treeCode\":\"\"}', 'admin', '2020-08-30 19:14:38', '', '2020-09-03 17:03:29', '');
INSERT INTO `gen_table` VALUES (4, 'user_task_picture', '任务图片联表', NULL, NULL, 'UserTaskPicture', 'crud', 'com.eden.system', 'system', 'picture', '任务图片联', 'jiaoyang', '0', '/', NULL, 'admin', '2020-08-30 19:14:38', '', NULL, NULL);
INSERT INTO `gen_table` VALUES (5, 'school_user', '学校用户信息表', NULL, NULL, 'SchoolUser', 'crud', 'com.eden.school', 'school', 'schoolUser', '学校用户信息', 'jiaoyang', '0', '/', '{\"parentMenuId\":\"1064\",\"treeName\":\"\",\"treeParentCode\":\"\",\"parentMenuName\":\"校园管理\",\"treeCode\":\"\"}', 'admin', '2020-09-01 09:24:45', '', '2020-09-01 09:29:33', '');
INSERT INTO `gen_table` VALUES (6, 'sys_region', '地区表', NULL, NULL, 'SysRegion', 'crud', 'com.eden.system', 'system', 'region', '地区', 'jiaoyang', '0', '/', '{\"parentMenuId\":\"1\",\"treeName\":\"\",\"treeParentCode\":\"\",\"parentMenuName\":\"系统管理\",\"treeCode\":\"\"}', 'admin', '2020-09-01 13:54:11', '', '2020-09-01 13:55:58', '');
INSERT INTO `gen_table` VALUES (7, 'task_video', '用户发布视频表', NULL, NULL, 'TaskVideo', 'crud', 'com.eden.task', 'task', 'userTaskVideo', '用户发布视频', 'jiaoyang', '0', '/', '{\"parentMenuId\":\"1068\",\"treeName\":\"\",\"treeParentCode\":\"\",\"parentMenuName\":\"校园任务审查\",\"treeCode\":\"\"}', 'admin', '2020-09-05 11:54:23', '', '2020-09-05 11:57:08', '');
INSERT INTO `gen_table` VALUES (8, 'sys_file_info', '文件信息表', NULL, NULL, 'SysFileInfo', 'crud', 'com.eden.system', 'system', 'info', '文件信息', 'jiaoyang', '0', '/', '{\"parentMenuId\":\"1\",\"treeName\":\"\",\"treeParentCode\":\"\",\"parentMenuName\":\"系统管理\",\"treeCode\":\"\"}', 'admin', '2020-09-07 11:30:33', '', '2020-09-07 11:32:26', '');
INSERT INTO `gen_table` VALUES (9, 'check_opinion', '审核任务回退原因表', NULL, NULL, 'CheckOpinion', 'crud', 'com.eden.task', 'task', 'opinion', '审核任务回退原因', 'jiaoyang', '0', '/', '{\"parentMenuId\":\"1068\",\"treeName\":\"\",\"treeParentCode\":\"\",\"parentMenuName\":\"校园任务审查\",\"treeCode\":\"\"}', 'admin', '2020-09-14 12:44:54', '', '2020-09-14 12:55:22', '');
INSERT INTO `gen_table` VALUES (10, 'school_user_role', '学校用户角色关联表', NULL, NULL, 'SchoolUserRole', 'crud', 'com.eden.system', 'system', 'role', '学校用户角色关联', 'eden', '0', '/', NULL, 'admin', '2020-09-16 13:38:26', '', NULL, NULL);
COMMIT;

-- ----------------------------
-- Table structure for gen_table_column
-- ----------------------------
DROP TABLE IF EXISTS `gen_table_column`;
CREATE TABLE `gen_table_column` (
  `column_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `table_id` varchar(64) DEFAULT NULL COMMENT '归属表编号',
  `column_name` varchar(200) DEFAULT NULL COMMENT '列名称',
  `column_comment` varchar(500) DEFAULT NULL COMMENT '列描述',
  `column_type` varchar(100) DEFAULT NULL COMMENT '列类型',
  `java_type` varchar(500) DEFAULT NULL COMMENT 'JAVA类型',
  `java_field` varchar(200) DEFAULT NULL COMMENT 'JAVA字段名',
  `is_pk` char(1) DEFAULT NULL COMMENT '是否主键（1是）',
  `is_increment` char(1) DEFAULT NULL COMMENT '是否自增（1是）',
  `is_required` char(1) DEFAULT NULL COMMENT '是否必填（1是）',
  `is_insert` char(1) DEFAULT NULL COMMENT '是否为插入字段（1是）',
  `is_edit` char(1) DEFAULT NULL COMMENT '是否编辑字段（1是）',
  `is_list` char(1) DEFAULT NULL COMMENT '是否列表字段（1是）',
  `is_query` char(1) DEFAULT NULL COMMENT '是否查询字段（1是）',
  `query_type` varchar(200) DEFAULT 'EQ' COMMENT '查询方式（等于、不等于、大于、小于、范围）',
  `html_type` varchar(200) DEFAULT NULL COMMENT '显示类型（文本框、文本域、下拉框、复选框、单选框、日期控件）',
  `dict_type` varchar(200) DEFAULT '' COMMENT '字典类型',
  `sort` int(11) DEFAULT NULL COMMENT '排序',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`column_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=92 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='代码生成业务表字段';

-- ----------------------------
-- Records of gen_table_column
-- ----------------------------
BEGIN;
INSERT INTO `gen_table_column` VALUES (16, '2', 'picture_id', '图片Id', 'varchar(64)', 'String', 'pictureId', '1', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2020-08-30 19:14:38', NULL, '2020-09-03 16:29:59');
INSERT INTO `gen_table_column` VALUES (17, '2', 'picture_address', '图片地址', 'varchar(255)', 'String', 'pictureAddress', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 2, 'admin', '2020-08-30 19:14:38', NULL, '2020-09-03 16:29:59');
INSERT INTO `gen_table_column` VALUES (18, '3', 'task_id', '任务id', 'varchar(64)', 'String', 'taskId', '1', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2020-08-30 19:14:38', NULL, '2020-09-03 17:03:29');
INSERT INTO `gen_table_column` VALUES (19, '3', 'task_name', '任务名称', 'varchar(64)', 'String', 'taskName', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', '', 2, 'admin', '2020-08-30 19:14:38', NULL, '2020-09-03 17:03:29');
INSERT INTO `gen_table_column` VALUES (20, '3', 'task_description', '任务描述', 'text', 'String', 'taskDescription', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 3, 'admin', '2020-08-30 19:14:38', NULL, '2020-09-03 17:03:29');
INSERT INTO `gen_table_column` VALUES (21, '3', 'task_status', '任务状态 0：初始状态 1：待审核 2：审核通过 3：审核不通过 4：发布成功 5：发布失败', 'char(1)', 'String', 'taskStatus', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'radio', '', 4, 'admin', '2020-08-30 19:14:38', NULL, '2020-09-03 17:03:29');
INSERT INTO `gen_table_column` VALUES (22, '3', 'money', '赏金', 'varchar(64)', 'String', 'money', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 5, 'admin', '2020-08-30 19:14:38', NULL, '2020-09-03 17:03:29');
INSERT INTO `gen_table_column` VALUES (23, '3', 'phonenumber', '电话号码', 'varchar(11)', 'String', 'phonenumber', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 6, 'admin', '2020-08-30 19:14:38', NULL, '2020-09-03 17:03:29');
INSERT INTO `gen_table_column` VALUES (24, '3', 'address', '地址', 'varchar(128)', 'String', 'address', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 7, 'admin', '2020-08-30 19:14:38', NULL, '2020-09-03 17:03:29');
INSERT INTO `gen_table_column` VALUES (25, '3', 'create_by', '创建者', 'varchar(64)', 'String', 'createBy', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 8, 'admin', '2020-08-30 19:14:38', NULL, '2020-09-03 17:03:29');
INSERT INTO `gen_table_column` VALUES (26, '3', 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'datetime', '', 9, 'admin', '2020-08-30 19:14:38', NULL, '2020-09-03 17:03:29');
INSERT INTO `gen_table_column` VALUES (27, '3', 'update_by', '更新者', 'varchar(64)', 'String', 'updateBy', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'input', '', 10, 'admin', '2020-08-30 19:14:38', NULL, '2020-09-03 17:03:29');
INSERT INTO `gen_table_column` VALUES (28, '3', 'update_time', '更新时间', 'datetime', 'Date', 'updateTime', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'datetime', '', 11, 'admin', '2020-08-30 19:14:38', NULL, '2020-09-03 17:03:29');
INSERT INTO `gen_table_column` VALUES (29, '3', 'check_opinion', '审核不通过的意见', 'varchar(255)', 'String', 'checkOpinion', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 12, 'admin', '2020-08-30 19:14:38', NULL, '2020-09-03 17:03:29');
INSERT INTO `gen_table_column` VALUES (30, '3', 'check_name', '审核人', 'varchar(30)', 'String', 'checkName', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', '', 13, 'admin', '2020-08-30 19:14:38', NULL, '2020-09-03 17:03:29');
INSERT INTO `gen_table_column` VALUES (31, '4', 'task_id', '任务 id', 'varchar(64)', 'String', 'taskId', '1', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2020-08-30 19:14:38', '', NULL);
INSERT INTO `gen_table_column` VALUES (32, '4', 'picture_id', '图片Id', 'varchar(64)', 'String', 'pictureId', '1', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 2, 'admin', '2020-08-30 19:14:38', '', NULL);
INSERT INTO `gen_table_column` VALUES (33, '5', 'user_id', '用户ID', 'bigint(20)', 'Long', 'userId', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2020-09-01 09:24:45', NULL, '2020-09-01 09:29:33');
INSERT INTO `gen_table_column` VALUES (34, '5', 'dept_id', '专业ID', 'bigint(20)', 'Long', 'deptId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 2, 'admin', '2020-09-01 09:24:45', NULL, '2020-09-01 09:29:33');
INSERT INTO `gen_table_column` VALUES (35, '5', 'login_name', '登录账号', 'varchar(30)', 'String', 'loginName', '0', '0', '1', '1', '1', '1', '1', 'LIKE', 'input', '', 3, 'admin', '2020-09-01 09:24:45', NULL, '2020-09-01 09:29:33');
INSERT INTO `gen_table_column` VALUES (36, '5', 'user_name', '用户昵称', 'varchar(30)', 'String', 'userName', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', '', 4, 'admin', '2020-09-01 09:24:45', NULL, '2020-09-01 09:29:33');
INSERT INTO `gen_table_column` VALUES (37, '5', 'user_type', '用户类型（00系统用户 01注册用户）', 'varchar(2)', 'String', 'userType', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'select', '', 5, 'admin', '2020-09-01 09:24:45', NULL, '2020-09-01 09:29:33');
INSERT INTO `gen_table_column` VALUES (38, '5', 'email', '用户邮箱', 'varchar(50)', 'String', 'email', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 6, 'admin', '2020-09-01 09:24:45', NULL, '2020-09-01 09:29:33');
INSERT INTO `gen_table_column` VALUES (39, '5', 'phonenumber', '手机号码', 'varchar(11)', 'String', 'phonenumber', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 7, 'admin', '2020-09-01 09:24:45', NULL, '2020-09-01 09:29:33');
INSERT INTO `gen_table_column` VALUES (40, '5', 'sex', '用户性别（0男 1女 2未知）', 'char(1)', 'String', 'sex', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'select', '', 8, 'admin', '2020-09-01 09:24:45', NULL, '2020-09-01 09:29:33');
INSERT INTO `gen_table_column` VALUES (41, '5', 'avatar', '头像路径', 'varchar(100)', 'String', 'avatar', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 9, 'admin', '2020-09-01 09:24:45', NULL, '2020-09-01 09:29:33');
INSERT INTO `gen_table_column` VALUES (42, '5', 'password', '密码', 'varchar(50)', 'String', 'password', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 10, 'admin', '2020-09-01 09:24:45', NULL, '2020-09-01 09:29:33');
INSERT INTO `gen_table_column` VALUES (43, '5', 'salt', '盐加密', 'varchar(20)', 'String', 'salt', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 11, 'admin', '2020-09-01 09:24:45', NULL, '2020-09-01 09:29:33');
INSERT INTO `gen_table_column` VALUES (44, '5', 'status', '帐号状态（0正常 1停用）', 'char(1)', 'String', 'status', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'radio', '', 12, 'admin', '2020-09-01 09:24:45', NULL, '2020-09-01 09:29:33');
INSERT INTO `gen_table_column` VALUES (45, '5', 'del_flag', '删除标志（0代表存在 2代表删除）', 'char(1)', 'String', 'delFlag', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 13, 'admin', '2020-09-01 09:24:45', NULL, '2020-09-01 09:29:33');
INSERT INTO `gen_table_column` VALUES (46, '5', 'login_ip', '最后登陆IP', 'varchar(50)', 'String', 'loginIp', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 14, 'admin', '2020-09-01 09:24:45', NULL, '2020-09-01 09:29:33');
INSERT INTO `gen_table_column` VALUES (47, '5', 'login_date', '最后登陆时间', 'datetime', 'Date', 'loginDate', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'datetime', '', 15, 'admin', '2020-09-01 09:24:45', NULL, '2020-09-01 09:29:33');
INSERT INTO `gen_table_column` VALUES (48, '5', 'create_by', '创建者', 'varchar(64)', 'String', 'createBy', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 16, 'admin', '2020-09-01 09:24:45', NULL, '2020-09-01 09:29:33');
INSERT INTO `gen_table_column` VALUES (49, '5', 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'datetime', '', 17, 'admin', '2020-09-01 09:24:45', NULL, '2020-09-01 09:29:33');
INSERT INTO `gen_table_column` VALUES (50, '5', 'update_by', '更新者', 'varchar(64)', 'String', 'updateBy', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'input', '', 18, 'admin', '2020-09-01 09:24:45', NULL, '2020-09-01 09:29:33');
INSERT INTO `gen_table_column` VALUES (51, '5', 'update_time', '更新时间', 'datetime', 'Date', 'updateTime', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'datetime', '', 19, 'admin', '2020-09-01 09:24:45', NULL, '2020-09-01 09:29:33');
INSERT INTO `gen_table_column` VALUES (52, '5', 'remark', '备注', 'varchar(500)', 'String', 'remark', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'textarea', '', 20, 'admin', '2020-09-01 09:24:45', NULL, '2020-09-01 09:29:33');
INSERT INTO `gen_table_column` VALUES (53, '5', 'address', '用户家庭住址', 'varchar(126)', 'String', 'address', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 21, 'admin', '2020-09-01 09:24:45', NULL, '2020-09-01 09:29:33');
INSERT INTO `gen_table_column` VALUES (54, '6', 'region_id', '地区主键编号', 'varchar(10)', 'String', 'regionId', '1', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2020-09-01 13:54:11', NULL, '2020-09-01 13:55:58');
INSERT INTO `gen_table_column` VALUES (55, '6', 'region_name', '地区名称', 'varchar(50)', 'String', 'regionName', '0', '0', '1', '1', '1', '1', '1', 'LIKE', 'input', '', 2, 'admin', '2020-09-01 13:54:11', NULL, '2020-09-01 13:55:58');
INSERT INTO `gen_table_column` VALUES (56, '6', 'region_short_name', '地区缩写', 'varchar(10)', 'String', 'regionShortName', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', '', 3, 'admin', '2020-09-01 13:54:11', NULL, '2020-09-01 13:55:58');
INSERT INTO `gen_table_column` VALUES (57, '6', 'region_code', '行政地区编号', 'varchar(20)', 'String', 'regionCode', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 4, 'admin', '2020-09-01 13:54:11', NULL, '2020-09-01 13:55:58');
INSERT INTO `gen_table_column` VALUES (58, '6', 'region_parent_id', '地区父id', 'varchar(10)', 'String', 'regionParentId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 5, 'admin', '2020-09-01 13:54:11', NULL, '2020-09-01 13:55:58');
INSERT INTO `gen_table_column` VALUES (59, '6', 'region_level', '地区级别', 'int(2)', 'Integer', 'regionLevel', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 6, 'admin', '2020-09-01 13:54:11', NULL, '2020-09-01 13:55:58');
INSERT INTO `gen_table_column` VALUES (60, '2', 'create_by', '创建者', 'varchar(30)', 'String', 'createBy', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 3, '', '2020-09-03 16:28:49', NULL, '2020-09-03 16:29:59');
INSERT INTO `gen_table_column` VALUES (61, '2', 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'datetime', '', 4, '', '2020-09-03 16:28:49', NULL, '2020-09-03 16:29:59');
INSERT INTO `gen_table_column` VALUES (62, '2', 'update_by', '更新者', 'varchar(30)', 'String', 'updateBy', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'input', '', 5, '', '2020-09-03 16:28:49', NULL, '2020-09-03 16:29:59');
INSERT INTO `gen_table_column` VALUES (63, '2', 'update_time', '更新时间', 'datetime', 'Date', 'updateTime', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'datetime', '', 6, '', '2020-09-03 16:28:49', NULL, '2020-09-03 16:29:59');
INSERT INTO `gen_table_column` VALUES (64, '2', 'picture_status', '图片状态', 'char(1)', 'String', 'pictureStatus', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'radio', '', 7, '', '2020-09-03 16:28:49', NULL, '2020-09-03 16:29:59');
INSERT INTO `gen_table_column` VALUES (65, '3', 'check_id', '检查者Id', 'varchar(30)', 'String', 'checkId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 12, '', '2020-09-04 10:27:57', '', NULL);
INSERT INTO `gen_table_column` VALUES (66, '7', 'video_id', '视频Id', 'varchar(255)', 'String', 'videoId', '1', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2020-09-05 11:54:23', NULL, '2020-09-05 11:57:08');
INSERT INTO `gen_table_column` VALUES (67, '7', 'video_url', '视频上传路径', 'varchar(128)', 'String', 'videoUrl', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 2, 'admin', '2020-09-05 11:54:23', NULL, '2020-09-05 11:57:08');
INSERT INTO `gen_table_column` VALUES (68, '7', 'create_by', '创建时间', 'varchar(30)', 'String', 'createBy', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 3, 'admin', '2020-09-05 11:54:23', NULL, '2020-09-05 11:57:08');
INSERT INTO `gen_table_column` VALUES (69, '7', 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'datetime', '', 4, 'admin', '2020-09-05 11:54:23', NULL, '2020-09-05 11:57:08');
INSERT INTO `gen_table_column` VALUES (70, '7', 'update_by', '更新者', 'varchar(30)', 'String', 'updateBy', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'input', '', 5, 'admin', '2020-09-05 11:54:23', NULL, '2020-09-05 11:57:08');
INSERT INTO `gen_table_column` VALUES (71, '7', 'update_time', '更新时间', 'datetime', 'Date', 'updateTime', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'datetime', '', 6, 'admin', '2020-09-05 11:54:23', NULL, '2020-09-05 11:57:08');
INSERT INTO `gen_table_column` VALUES (72, '7', 'video_status', '视频状态', 'char(1)', 'String', 'videoStatus', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'radio', '', 7, 'admin', '2020-09-05 11:54:23', NULL, '2020-09-05 11:57:08');
INSERT INTO `gen_table_column` VALUES (73, '8', 'file_id', '文件id', 'int(11)', 'Long', 'fileId', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2020-09-07 11:30:33', NULL, '2020-09-07 11:32:26');
INSERT INTO `gen_table_column` VALUES (74, '8', 'file_name', '文件名称', 'varchar(50)', 'String', 'fileName', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', '', 2, 'admin', '2020-09-07 11:30:33', NULL, '2020-09-07 11:32:26');
INSERT INTO `gen_table_column` VALUES (75, '8', 'file_path', '文件路径', 'varchar(255)', 'String', 'filePath', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 3, 'admin', '2020-09-07 11:30:33', NULL, '2020-09-07 11:32:26');
INSERT INTO `gen_table_column` VALUES (76, '2', 'picture_name', '图片名称', 'varchar(128)', 'String', 'pictureName', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', '', 8, '', '2020-09-07 13:28:29', '', NULL);
INSERT INTO `gen_table_column` VALUES (77, '7', 'video_name', '视频名称', 'varchar(128)', 'String', 'videoName', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', '', 8, '', '2020-09-07 13:31:56', '', NULL);
INSERT INTO `gen_table_column` VALUES (80, '7', 'task_cvid', '任务Id', 'varchar(64)', 'String', 'taskCvid', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 5, '', '2020-09-08 13:07:46', '', NULL);
INSERT INTO `gen_table_column` VALUES (81, '2', 'task_cpid', '任务Id', 'varchar(64)', 'String', 'taskCpid', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 2, '', '2020-09-08 13:07:50', '', NULL);
INSERT INTO `gen_table_column` VALUES (82, '9', 'check_opinion_id', '质检退回ID', 'varchar(64)', 'String', 'checkOpinionId', '1', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2020-09-14 12:44:54', NULL, '2020-09-14 12:55:22');
INSERT INTO `gen_table_column` VALUES (83, '9', 'content', '质检不通过原因', 'varchar(255)', 'String', 'content', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'summernote', '', 2, 'admin', '2020-09-14 12:44:54', NULL, '2020-09-14 12:55:22');
INSERT INTO `gen_table_column` VALUES (84, '9', 'next_flow', '该订单的下一个流程', 'varchar(50)', 'String', 'nextFlow', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 3, 'admin', '2020-09-14 12:44:54', NULL, '2020-09-14 12:55:22');
INSERT INTO `gen_table_column` VALUES (85, '9', 'create_by', '创建者', 'varchar(64)', 'String', 'createBy', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 4, 'admin', '2020-09-14 12:44:54', NULL, '2020-09-14 12:55:22');
INSERT INTO `gen_table_column` VALUES (86, '9', 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'datetime', '', 5, 'admin', '2020-09-14 12:44:54', NULL, '2020-09-14 12:55:22');
INSERT INTO `gen_table_column` VALUES (87, '9', 'update_by', '更新者', 'varchar(64)', 'String', 'updateBy', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'input', '', 6, 'admin', '2020-09-14 12:44:54', NULL, '2020-09-14 12:55:22');
INSERT INTO `gen_table_column` VALUES (88, '9', 'update_time', '更新时间', 'datetime', 'Date', 'updateTime', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'datetime', '', 7, 'admin', '2020-09-14 12:44:54', NULL, '2020-09-14 12:55:22');
INSERT INTO `gen_table_column` VALUES (89, '10', 'school_user_id', '学校用户Id', 'bigint(20)', 'Long', 'schoolUserId', '1', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2020-09-16 13:38:26', '', NULL);
INSERT INTO `gen_table_column` VALUES (90, '10', 'sys_role_id', '角色Id', 'bigint(20)', 'Long', 'sysRoleId', '1', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 2, 'admin', '2020-09-16 13:38:26', '', NULL);
INSERT INTO `gen_table_column` VALUES (91, '5', 'role_id', '角色Id', 'bigint(20)', 'Long', 'roleId', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 3, '', '2020-09-16 14:00:09', '', NULL);
COMMIT;

-- ----------------------------
-- Table structure for qrtz_blob_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_blob_triggers`;
CREATE TABLE `qrtz_blob_triggers` (
  `sched_name` varchar(120) NOT NULL,
  `trigger_name` varchar(200) NOT NULL,
  `trigger_group` varchar(200) NOT NULL,
  `blob_data` blob,
  PRIMARY KEY (`sched_name`,`trigger_name`,`trigger_group`) USING BTREE,
  CONSTRAINT `qrtz_blob_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `qrtz_triggers` (`sched_name`, `trigger_name`, `trigger_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Table structure for qrtz_calendars
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_calendars`;
CREATE TABLE `qrtz_calendars` (
  `sched_name` varchar(120) NOT NULL,
  `calendar_name` varchar(200) NOT NULL,
  `calendar` blob NOT NULL,
  PRIMARY KEY (`sched_name`,`calendar_name`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Table structure for qrtz_cron_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_cron_triggers`;
CREATE TABLE `qrtz_cron_triggers` (
  `sched_name` varchar(120) NOT NULL,
  `trigger_name` varchar(200) NOT NULL,
  `trigger_group` varchar(200) NOT NULL,
  `cron_expression` varchar(200) NOT NULL,
  `time_zone_id` varchar(80) DEFAULT NULL,
  PRIMARY KEY (`sched_name`,`trigger_name`,`trigger_group`) USING BTREE,
  CONSTRAINT `qrtz_cron_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `qrtz_triggers` (`sched_name`, `trigger_name`, `trigger_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of qrtz_cron_triggers
-- ----------------------------
BEGIN;
INSERT INTO `qrtz_cron_triggers` VALUES ('EdenScheduler', 'TASK_CLASS_NAME1', 'DEFAULT', '0/10 * * * * ?', 'Asia/Shanghai');
INSERT INTO `qrtz_cron_triggers` VALUES ('EdenScheduler', 'TASK_CLASS_NAME2', 'DEFAULT', '0/15 * * * * ?', 'Asia/Shanghai');
INSERT INTO `qrtz_cron_triggers` VALUES ('EdenScheduler', 'TASK_CLASS_NAME3', 'DEFAULT', '0/20 * * * * ?', 'Asia/Shanghai');
INSERT INTO `qrtz_cron_triggers` VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME1', 'DEFAULT', '0/10 * * * * ?', 'Asia/Shanghai');
INSERT INTO `qrtz_cron_triggers` VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME2', 'DEFAULT', '0/15 * * * * ?', 'Asia/Shanghai');
INSERT INTO `qrtz_cron_triggers` VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME3', 'DEFAULT', '0/20 * * * * ?', 'Asia/Shanghai');
COMMIT;

-- ----------------------------
-- Table structure for qrtz_fired_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_fired_triggers`;
CREATE TABLE `qrtz_fired_triggers` (
  `sched_name` varchar(120) NOT NULL,
  `entry_id` varchar(95) NOT NULL,
  `trigger_name` varchar(200) NOT NULL,
  `trigger_group` varchar(200) NOT NULL,
  `instance_name` varchar(200) NOT NULL,
  `fired_time` bigint(13) NOT NULL,
  `sched_time` bigint(13) NOT NULL,
  `priority` int(11) NOT NULL,
  `state` varchar(16) NOT NULL,
  `job_name` varchar(200) DEFAULT NULL,
  `job_group` varchar(200) DEFAULT NULL,
  `is_nonconcurrent` varchar(1) DEFAULT NULL,
  `requests_recovery` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`sched_name`,`entry_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Table structure for qrtz_job_details
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_job_details`;
CREATE TABLE `qrtz_job_details` (
  `sched_name` varchar(120) NOT NULL,
  `job_name` varchar(200) NOT NULL,
  `job_group` varchar(200) NOT NULL,
  `description` varchar(250) DEFAULT NULL,
  `job_class_name` varchar(250) NOT NULL,
  `is_durable` varchar(1) NOT NULL,
  `is_nonconcurrent` varchar(1) NOT NULL,
  `is_update_data` varchar(1) NOT NULL,
  `requests_recovery` varchar(1) NOT NULL,
  `job_data` blob,
  PRIMARY KEY (`sched_name`,`job_name`,`job_group`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of qrtz_job_details
-- ----------------------------
BEGIN;
INSERT INTO `qrtz_job_details` VALUES ('EdenScheduler', 'TASK_CLASS_NAME1', 'DEFAULT', NULL, 'com.eden.quartz.util.QuartzDisallowConcurrentExecution', '0', '1', '0', '0', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000174000F5441534B5F50524F504552544945537372001D636F6D2E6564656E2E71756172747A2E646F6D61696E2E5379734A6F6200000000000000010200084C000A636F6E63757272656E747400124C6A6176612F6C616E672F537472696E673B4C000E63726F6E45787072657373696F6E71007E00094C000C696E766F6B6554617267657471007E00094C00086A6F6247726F757071007E00094C00056A6F6249647400104C6A6176612F6C616E672F4C6F6E673B4C00076A6F624E616D6571007E00094C000D6D697366697265506F6C69637971007E00094C000673746174757371007E000978720026636F6D2E6564656E2E636F6D6D6F6E2E636F72652E646F6D61696E2E42617365456E7469747900000000000000010200074C0008637265617465427971007E00094C000A63726561746554696D657400104C6A6176612F7574696C2F446174653B4C0006706172616D7371007E00034C000672656D61726B71007E00094C000B73656172636856616C756571007E00094C0008757064617465427971007E00094C000A75706461746554696D6571007E000C787074000561646D696E7372000E6A6176612E7574696C2E44617465686A81014B59741903000078707708000001622CDE29E078707400007070707400013174000E302F3130202A202A202A202A203F74001172795461736B2E72794E6F506172616D7374000744454641554C547372000E6A6176612E6C616E672E4C6F6E673B8BE490CC8F23DF0200014A000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B02000078700000000000000001740018E7B3BBE7BB9FE9BB98E8AEA4EFBC88E697A0E58F82EFBC8974000133740001317800);
INSERT INTO `qrtz_job_details` VALUES ('EdenScheduler', 'TASK_CLASS_NAME2', 'DEFAULT', NULL, 'com.eden.quartz.util.QuartzDisallowConcurrentExecution', '0', '1', '0', '0', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000174000F5441534B5F50524F504552544945537372001D636F6D2E6564656E2E71756172747A2E646F6D61696E2E5379734A6F6200000000000000010200084C000A636F6E63757272656E747400124C6A6176612F6C616E672F537472696E673B4C000E63726F6E45787072657373696F6E71007E00094C000C696E766F6B6554617267657471007E00094C00086A6F6247726F757071007E00094C00056A6F6249647400104C6A6176612F6C616E672F4C6F6E673B4C00076A6F624E616D6571007E00094C000D6D697366697265506F6C69637971007E00094C000673746174757371007E000978720026636F6D2E6564656E2E636F6D6D6F6E2E636F72652E646F6D61696E2E42617365456E7469747900000000000000010200074C0008637265617465427971007E00094C000A63726561746554696D657400104C6A6176612F7574696C2F446174653B4C0006706172616D7371007E00034C000672656D61726B71007E00094C000B73656172636856616C756571007E00094C0008757064617465427971007E00094C000A75706461746554696D6571007E000C787074000561646D696E7372000E6A6176612E7574696C2E44617465686A81014B59741903000078707708000001622CDE29E078707400007070707400013174000E302F3135202A202A202A202A203F74001572795461736B2E7279506172616D7328277279272974000744454641554C547372000E6A6176612E6C616E672E4C6F6E673B8BE490CC8F23DF0200014A000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B02000078700000000000000002740018E7B3BBE7BB9FE9BB98E8AEA4EFBC88E69C89E58F82EFBC8974000133740001317800);
INSERT INTO `qrtz_job_details` VALUES ('EdenScheduler', 'TASK_CLASS_NAME3', 'DEFAULT', NULL, 'com.eden.quartz.util.QuartzDisallowConcurrentExecution', '0', '1', '0', '0', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000174000F5441534B5F50524F504552544945537372001D636F6D2E6564656E2E71756172747A2E646F6D61696E2E5379734A6F6200000000000000010200084C000A636F6E63757272656E747400124C6A6176612F6C616E672F537472696E673B4C000E63726F6E45787072657373696F6E71007E00094C000C696E766F6B6554617267657471007E00094C00086A6F6247726F757071007E00094C00056A6F6249647400104C6A6176612F6C616E672F4C6F6E673B4C00076A6F624E616D6571007E00094C000D6D697366697265506F6C69637971007E00094C000673746174757371007E000978720026636F6D2E6564656E2E636F6D6D6F6E2E636F72652E646F6D61696E2E42617365456E7469747900000000000000010200074C0008637265617465427971007E00094C000A63726561746554696D657400104C6A6176612F7574696C2F446174653B4C0006706172616D7371007E00034C000672656D61726B71007E00094C000B73656172636856616C756571007E00094C0008757064617465427971007E00094C000A75706461746554696D6571007E000C787074000561646D696E7372000E6A6176612E7574696C2E44617465686A81014B59741903000078707708000001622CDE29E078707400007070707400013174000E302F3230202A202A202A202A203F74003872795461736B2E72794D756C7469706C65506172616D7328277279272C20747275652C20323030304C2C203331362E3530442C203130302974000744454641554C547372000E6A6176612E6C616E672E4C6F6E673B8BE490CC8F23DF0200014A000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B02000078700000000000000003740018E7B3BBE7BB9FE9BB98E8AEA4EFBC88E5A49AE58F82EFBC8974000133740001317800);
INSERT INTO `qrtz_job_details` VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME1', 'DEFAULT', NULL, 'com.eden.quartz.util.QuartzDisallowConcurrentExecution', '0', '1', '0', '0', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000174000F5441534B5F50524F504552544945537372001D636F6D2E6564656E2E71756172747A2E646F6D61696E2E5379734A6F6200000000000000010200084C000A636F6E63757272656E747400124C6A6176612F6C616E672F537472696E673B4C000E63726F6E45787072657373696F6E71007E00094C000C696E766F6B6554617267657471007E00094C00086A6F6247726F757071007E00094C00056A6F6249647400104C6A6176612F6C616E672F4C6F6E673B4C00076A6F624E616D6571007E00094C000D6D697366697265506F6C69637971007E00094C000673746174757371007E000978720026636F6D2E6564656E2E636F6D6D6F6E2E636F72652E646F6D61696E2E42617365456E7469747900000000000000010200074C0008637265617465427971007E00094C000A63726561746554696D657400104C6A6176612F7574696C2F446174653B4C0006706172616D7371007E00034C000672656D61726B71007E00094C000B73656172636856616C756571007E00094C0008757064617465427971007E00094C000A75706461746554696D6571007E000C787074000561646D696E7372000E6A6176612E7574696C2E44617465686A81014B59741903000078707708000001622CDE29E078707400007070707400013174000E302F3130202A202A202A202A203F74001172795461736B2E72794E6F506172616D7374000744454641554C547372000E6A6176612E6C616E672E4C6F6E673B8BE490CC8F23DF0200014A000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B02000078700000000000000001740018E7B3BBE7BB9FE9BB98E8AEA4EFBC88E697A0E58F82EFBC8974000133740001317800);
INSERT INTO `qrtz_job_details` VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME2', 'DEFAULT', NULL, 'com.eden.quartz.util.QuartzDisallowConcurrentExecution', '0', '1', '0', '0', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000174000F5441534B5F50524F504552544945537372001D636F6D2E6564656E2E71756172747A2E646F6D61696E2E5379734A6F6200000000000000010200084C000A636F6E63757272656E747400124C6A6176612F6C616E672F537472696E673B4C000E63726F6E45787072657373696F6E71007E00094C000C696E766F6B6554617267657471007E00094C00086A6F6247726F757071007E00094C00056A6F6249647400104C6A6176612F6C616E672F4C6F6E673B4C00076A6F624E616D6571007E00094C000D6D697366697265506F6C69637971007E00094C000673746174757371007E000978720026636F6D2E6564656E2E636F6D6D6F6E2E636F72652E646F6D61696E2E42617365456E7469747900000000000000010200074C0008637265617465427971007E00094C000A63726561746554696D657400104C6A6176612F7574696C2F446174653B4C0006706172616D7371007E00034C000672656D61726B71007E00094C000B73656172636856616C756571007E00094C0008757064617465427971007E00094C000A75706461746554696D6571007E000C787074000561646D696E7372000E6A6176612E7574696C2E44617465686A81014B59741903000078707708000001622CDE29E078707400007070707400013174000E302F3135202A202A202A202A203F74001572795461736B2E7279506172616D7328277279272974000744454641554C547372000E6A6176612E6C616E672E4C6F6E673B8BE490CC8F23DF0200014A000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B02000078700000000000000002740018E7B3BBE7BB9FE9BB98E8AEA4EFBC88E69C89E58F82EFBC8974000133740001317800);
INSERT INTO `qrtz_job_details` VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME3', 'DEFAULT', NULL, 'com.eden.quartz.util.QuartzDisallowConcurrentExecution', '0', '1', '0', '0', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000174000F5441534B5F50524F504552544945537372001D636F6D2E6564656E2E71756172747A2E646F6D61696E2E5379734A6F6200000000000000010200084C000A636F6E63757272656E747400124C6A6176612F6C616E672F537472696E673B4C000E63726F6E45787072657373696F6E71007E00094C000C696E766F6B6554617267657471007E00094C00086A6F6247726F757071007E00094C00056A6F6249647400104C6A6176612F6C616E672F4C6F6E673B4C00076A6F624E616D6571007E00094C000D6D697366697265506F6C69637971007E00094C000673746174757371007E000978720026636F6D2E6564656E2E636F6D6D6F6E2E636F72652E646F6D61696E2E42617365456E7469747900000000000000010200074C0008637265617465427971007E00094C000A63726561746554696D657400104C6A6176612F7574696C2F446174653B4C0006706172616D7371007E00034C000672656D61726B71007E00094C000B73656172636856616C756571007E00094C0008757064617465427971007E00094C000A75706461746554696D6571007E000C787074000561646D696E7372000E6A6176612E7574696C2E44617465686A81014B59741903000078707708000001622CDE29E078707400007070707400013174000E302F3230202A202A202A202A203F74003872795461736B2E72794D756C7469706C65506172616D7328277279272C20747275652C20323030304C2C203331362E3530442C203130302974000744454641554C547372000E6A6176612E6C616E672E4C6F6E673B8BE490CC8F23DF0200014A000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B02000078700000000000000003740018E7B3BBE7BB9FE9BB98E8AEA4EFBC88E5A49AE58F82EFBC8974000133740001317800);
COMMIT;

-- ----------------------------
-- Table structure for qrtz_locks
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_locks`;
CREATE TABLE `qrtz_locks` (
  `sched_name` varchar(120) NOT NULL,
  `lock_name` varchar(40) NOT NULL,
  PRIMARY KEY (`sched_name`,`lock_name`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of qrtz_locks
-- ----------------------------
BEGIN;
INSERT INTO `qrtz_locks` VALUES ('EdenScheduler', 'STATE_ACCESS');
INSERT INTO `qrtz_locks` VALUES ('EdenScheduler', 'TRIGGER_ACCESS');
INSERT INTO `qrtz_locks` VALUES ('RuoyiScheduler', 'STATE_ACCESS');
INSERT INTO `qrtz_locks` VALUES ('RuoyiScheduler', 'TRIGGER_ACCESS');
COMMIT;

-- ----------------------------
-- Table structure for qrtz_paused_trigger_grps
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_paused_trigger_grps`;
CREATE TABLE `qrtz_paused_trigger_grps` (
  `sched_name` varchar(120) NOT NULL,
  `trigger_group` varchar(200) NOT NULL,
  PRIMARY KEY (`sched_name`,`trigger_group`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Table structure for qrtz_scheduler_state
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_scheduler_state`;
CREATE TABLE `qrtz_scheduler_state` (
  `sched_name` varchar(120) NOT NULL,
  `instance_name` varchar(200) NOT NULL,
  `last_checkin_time` bigint(13) NOT NULL,
  `checkin_interval` bigint(13) NOT NULL,
  PRIMARY KEY (`sched_name`,`instance_name`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of qrtz_scheduler_state
-- ----------------------------
BEGIN;
INSERT INTO `qrtz_scheduler_state` VALUES ('EdenScheduler', 'jiaoyangdeMacBook-Pro.local1604296773223', 1604310155336, 15000);
INSERT INTO `qrtz_scheduler_state` VALUES ('RuoyiScheduler', 'admin1598403090779', 1598404714065, 15000);
COMMIT;

-- ----------------------------
-- Table structure for qrtz_simple_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_simple_triggers`;
CREATE TABLE `qrtz_simple_triggers` (
  `sched_name` varchar(120) NOT NULL,
  `trigger_name` varchar(200) NOT NULL,
  `trigger_group` varchar(200) NOT NULL,
  `repeat_count` bigint(7) NOT NULL,
  `repeat_interval` bigint(12) NOT NULL,
  `times_triggered` bigint(10) NOT NULL,
  PRIMARY KEY (`sched_name`,`trigger_name`,`trigger_group`) USING BTREE,
  CONSTRAINT `qrtz_simple_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `qrtz_triggers` (`sched_name`, `trigger_name`, `trigger_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Table structure for qrtz_simprop_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_simprop_triggers`;
CREATE TABLE `qrtz_simprop_triggers` (
  `sched_name` varchar(120) NOT NULL,
  `trigger_name` varchar(200) NOT NULL,
  `trigger_group` varchar(200) NOT NULL,
  `str_prop_1` varchar(512) DEFAULT NULL,
  `str_prop_2` varchar(512) DEFAULT NULL,
  `str_prop_3` varchar(512) DEFAULT NULL,
  `int_prop_1` int(11) DEFAULT NULL,
  `int_prop_2` int(11) DEFAULT NULL,
  `long_prop_1` bigint(20) DEFAULT NULL,
  `long_prop_2` bigint(20) DEFAULT NULL,
  `dec_prop_1` decimal(13,4) DEFAULT NULL,
  `dec_prop_2` decimal(13,4) DEFAULT NULL,
  `bool_prop_1` varchar(1) DEFAULT NULL,
  `bool_prop_2` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`sched_name`,`trigger_name`,`trigger_group`) USING BTREE,
  CONSTRAINT `qrtz_simprop_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `qrtz_triggers` (`sched_name`, `trigger_name`, `trigger_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Table structure for qrtz_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_triggers`;
CREATE TABLE `qrtz_triggers` (
  `sched_name` varchar(120) NOT NULL,
  `trigger_name` varchar(200) NOT NULL,
  `trigger_group` varchar(200) NOT NULL,
  `job_name` varchar(200) NOT NULL,
  `job_group` varchar(200) NOT NULL,
  `description` varchar(250) DEFAULT NULL,
  `next_fire_time` bigint(13) DEFAULT NULL,
  `prev_fire_time` bigint(13) DEFAULT NULL,
  `priority` int(11) DEFAULT NULL,
  `trigger_state` varchar(16) NOT NULL,
  `trigger_type` varchar(8) NOT NULL,
  `start_time` bigint(13) NOT NULL,
  `end_time` bigint(13) DEFAULT NULL,
  `calendar_name` varchar(200) DEFAULT NULL,
  `misfire_instr` smallint(2) DEFAULT NULL,
  `job_data` blob,
  PRIMARY KEY (`sched_name`,`trigger_name`,`trigger_group`) USING BTREE,
  KEY `sched_name` (`sched_name`,`job_name`,`job_group`) USING BTREE,
  CONSTRAINT `qrtz_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `job_name`, `job_group`) REFERENCES `qrtz_job_details` (`sched_name`, `job_name`, `job_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of qrtz_triggers
-- ----------------------------
BEGIN;
INSERT INTO `qrtz_triggers` VALUES ('EdenScheduler', 'TASK_CLASS_NAME1', 'DEFAULT', 'TASK_CLASS_NAME1', 'DEFAULT', NULL, 1604296780000, -1, 5, 'PAUSED', 'CRON', 1604296773000, 0, NULL, 2, '');
INSERT INTO `qrtz_triggers` VALUES ('EdenScheduler', 'TASK_CLASS_NAME2', 'DEFAULT', 'TASK_CLASS_NAME2', 'DEFAULT', NULL, 1604296785000, -1, 5, 'PAUSED', 'CRON', 1604296773000, 0, NULL, 2, '');
INSERT INTO `qrtz_triggers` VALUES ('EdenScheduler', 'TASK_CLASS_NAME3', 'DEFAULT', 'TASK_CLASS_NAME3', 'DEFAULT', NULL, 1604296780000, -1, 5, 'PAUSED', 'CRON', 1604296773000, 0, NULL, 2, '');
INSERT INTO `qrtz_triggers` VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME1', 'DEFAULT', 'TASK_CLASS_NAME1', 'DEFAULT', NULL, 1598403090000, -1, 5, 'PAUSED', 'CRON', 1598403090000, 0, NULL, 2, '');
INSERT INTO `qrtz_triggers` VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME2', 'DEFAULT', 'TASK_CLASS_NAME2', 'DEFAULT', NULL, 1598403090000, -1, 5, 'PAUSED', 'CRON', 1598403090000, 0, NULL, 2, '');
INSERT INTO `qrtz_triggers` VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME3', 'DEFAULT', 'TASK_CLASS_NAME3', 'DEFAULT', NULL, 1598403100000, -1, 5, 'PAUSED', 'CRON', 1598403090000, 0, NULL, 2, '');
COMMIT;

-- ----------------------------
-- Table structure for school_user
-- ----------------------------
DROP TABLE IF EXISTS `school_user`;
CREATE TABLE `school_user` (
  `user_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '用户ID',
  `dept_id` bigint(20) NOT NULL COMMENT '专业ID',
  `role_id` bigint(20) NOT NULL COMMENT '角色Id',
  `login_name` varchar(30) NOT NULL COMMENT '登录账号',
  `user_name` varchar(30) DEFAULT '' COMMENT '用户昵称',
  `user_type` varchar(2) DEFAULT '00' COMMENT '用户类型（00系统用户 01注册用户）',
  `email` varchar(50) DEFAULT '' COMMENT '用户邮箱',
  `phonenumber` varchar(11) DEFAULT '' COMMENT '手机号码',
  `sex` char(1) DEFAULT '0' COMMENT '用户性别（0男 1女 2未知）',
  `avatar` varchar(100) DEFAULT '' COMMENT '头像路径',
  `password` varchar(50) DEFAULT '' COMMENT '密码',
  `salt` varchar(20) DEFAULT '' COMMENT '盐加密',
  `status` char(1) DEFAULT '0' COMMENT '帐号状态（0正常 1停用）',
  `del_flag` char(1) DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `login_ip` varchar(50) DEFAULT '' COMMENT '最后登陆IP',
  `login_date` datetime DEFAULT NULL COMMENT '最后登陆时间',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  `address` varchar(126) DEFAULT '' COMMENT '用户家庭住址',
  PRIMARY KEY (`user_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='学校用户信息表';

-- ----------------------------
-- Records of school_user
-- ----------------------------
BEGIN;
INSERT INTO `school_user` VALUES (1, 103, 1, 'admin', '起源', '00', 'eden@163.com', '17761628991', '1', '', '29c67a30398638269fe600f73a054934', '111111', '0', '0', '127.0.0.1', '2020-09-04 10:58:47', 'admin', '2018-03-16 11:33:00', 'ry', '2020-09-04 10:58:47', '管理员', '');
INSERT INTO `school_user` VALUES (4, 129, 9, '1715925168', '焦洋', '01', '838855804@qq.com', '17761628992', '0', '', 'cdcff9708594fc5c5469c7d754ba9d85', 'cd3299', '0', '0', '127.0.0.1', '2020-09-22 18:05:05', 'admin', '2020-09-01 12:04:04', '', '2020-09-22 18:05:05', '', '');
INSERT INTO `school_user` VALUES (5, 121, 10, '1715925169', '小红', '01', '838855805@qq.com', '17761628997', '1', '', 'bbe103a5b8bcd1e9fa7cb8ec313380f6', '526b97', '0', '0', '', NULL, 'admin', '2020-09-03 13:59:12', '', '2020-09-03 15:41:00', '测试用户', '');
INSERT INTO `school_user` VALUES (6, 120, 9, '1715925160', '小强', '01', '83885580@qq.com', '17761628994', '0', '', 'b65a11e754d20662f2a2f5189a5028fe', '49bafd', '0', '0', '', NULL, 'admin', '2020-09-03 15:43:16', '', '2020-09-03 15:44:24', '', '');
INSERT INTO `school_user` VALUES (7, 119, 9, '1715925167', 'kk', '01', '838855806@qq.com', '17761628999', '0', '', 'ce4f246aff92aa45aaa80902b4613dcf', 'f9718b', '0', '0', '', NULL, 'admin', '2020-09-03 15:57:41', '', '2020-09-03 15:59:01', '', '');
INSERT INTO `school_user` VALUES (8, 128, 9, '1715925161', '小刚', '01', '838855801@qq.com', '17761628993', '0', '', '785000c309c9cf6113e2ec653f01e070', '34edf5', '0', '0', '', NULL, 'admin', '2020-09-17 11:54:26', '', NULL, '', '');
INSERT INTO `school_user` VALUES (9, 144, 9, '1715925165', '小美', '01', '838855807@qq.com', '17761628995', '1', '', '3df379af6277e70de32823c305d9e771', '718aab', '0', '0', '', NULL, 'admin', '2020-09-17 13:28:56', '', NULL, '', '');
INSERT INTO `school_user` VALUES (10, 120, 7, '1715925102', '小明', '01', '838855855@qq.com', '17761628902', '0', '', '2e6acb11dc9a7d2cfac775b2b8841fb1', '8edcec', '0', '0', '', NULL, 'admin', '2020-09-17 13:30:14', '', '2020-09-17 17:16:52', '', '');
COMMIT;

-- ----------------------------
-- Table structure for sys_config
-- ----------------------------
DROP TABLE IF EXISTS `sys_config`;
CREATE TABLE `sys_config` (
  `config_id` int(5) NOT NULL AUTO_INCREMENT COMMENT '参数主键',
  `config_name` varchar(100) DEFAULT '' COMMENT '参数名称',
  `config_key` varchar(100) DEFAULT '' COMMENT '参数键名',
  `config_value` varchar(500) DEFAULT '' COMMENT '参数键值',
  `config_type` char(1) DEFAULT 'N' COMMENT '系统内置（Y是 N否）',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`config_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='参数配置表';

-- ----------------------------
-- Records of sys_config
-- ----------------------------
BEGIN;
INSERT INTO `sys_config` VALUES (1, '主框架页-默认皮肤样式名称', 'sys.index.skinName', 'skin-blue', 'Y', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '蓝色 skin-blue、绿色 skin-green、紫色 skin-purple、红色 skin-red、黄色 skin-yellow');
INSERT INTO `sys_config` VALUES (2, '用户管理-账号初始密码', 'sys.user.initPassword', '123456', 'Y', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '初始化密码 123456');
INSERT INTO `sys_config` VALUES (3, '主框架页-侧边栏主题', 'sys.index.sideTheme', 'theme-dark', 'Y', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '深黑主题theme-dark，浅色主题theme-light，深蓝主题theme-blue');
INSERT INTO `sys_config` VALUES (4, '账号自助-是否开启用户注册功能', 'sys.account.registerUser', 'false', 'Y', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '是否开启注册用户功能');
INSERT INTO `sys_config` VALUES (5, '用户管理-密码字符范围', 'sys.account.chrtype', '0', 'Y', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '默认任意字符范围，0任意（密码可以输入任意字符），1数字（密码只能为0-9数字），2英文字母（密码只能为a-z和A-Z字母），3字母和数字（密码必须包含字母，数字）,4字母数组和特殊字符（密码必须包含字母，数字，特殊字符-_）');
COMMIT;

-- ----------------------------
-- Table structure for sys_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_dept`;
CREATE TABLE `sys_dept` (
  `dept_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '部门id',
  `parent_id` bigint(20) DEFAULT '0' COMMENT '父部门id',
  `ancestors` varchar(50) DEFAULT '' COMMENT '祖级列表',
  `dept_name` varchar(30) DEFAULT '' COMMENT '部门名称',
  `order_num` int(4) DEFAULT '0' COMMENT '显示顺序',
  `leader` varchar(20) DEFAULT NULL COMMENT '负责人',
  `phone` varchar(11) DEFAULT NULL COMMENT '联系电话',
  `email` varchar(50) DEFAULT NULL COMMENT '邮箱',
  `status` char(1) DEFAULT '0' COMMENT '部门状态（0正常 1停用）',
  `del_flag` char(1) DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`dept_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=177 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='部门表';

-- ----------------------------
-- Records of sys_dept
-- ----------------------------
BEGIN;
INSERT INTO `sys_dept` VALUES (100, 0, '0', '南阳理工学院', 0, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-08-29 21:23:56');
INSERT INTO `sys_dept` VALUES (101, 100, '0,100', '智能制造学院', 0, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-08-29 21:07:31');
INSERT INTO `sys_dept` VALUES (102, 100, '0,100', '计算机于软件学院', 3, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-08-29 20:58:34');
INSERT INTO `sys_dept` VALUES (103, 100, '0,100', '建筑学院', 1, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-08-29 20:58:15');
INSERT INTO `sys_dept` VALUES (104, 100, '0,100', '市场部门', 2, '若依', '15888888888', 'ry@qq.com', '0', '2', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00');
INSERT INTO `sys_dept` VALUES (105, 100, '0,100', '数理学院', 5, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-08-29 21:00:26');
INSERT INTO `sys_dept` VALUES (106, 100, '0,100', '教师教育学院', 6, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-08-29 21:00:50');
INSERT INTO `sys_dept` VALUES (107, 100, '0,100', '外国语学院', 4, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-08-29 21:01:48');
INSERT INTO `sys_dept` VALUES (108, 100, '0,100', '生物与化学工程学院', 2, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-08-29 20:59:55');
INSERT INTO `sys_dept` VALUES (109, 100, '0,100', '财务部门', 2, '若依', '15888888888', 'ry@qq.com', '0', '2', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00');
INSERT INTO `sys_dept` VALUES (111, 100, '0,100', '体育教学部', 7, '', '', '', '0', '2', 'admin', '2020-08-29 21:02:11', 'admin', '2020-08-29 21:02:18');
INSERT INTO `sys_dept` VALUES (112, 100, '0,100', '信息工程学院', 8, NULL, NULL, NULL, '0', '2', 'admin', '2020-08-29 21:02:35', '', NULL);
INSERT INTO `sys_dept` VALUES (113, 100, '0,100', '数字媒体与艺术设计学院', 9, NULL, NULL, NULL, '0', '0', 'admin', '2020-08-29 21:02:45', '', NULL);
INSERT INTO `sys_dept` VALUES (114, 100, '0,100', '土木工程学院', 10, NULL, NULL, NULL, '0', '0', 'admin', '2020-08-29 21:02:58', 'admin', '2020-08-29 21:23:56');
INSERT INTO `sys_dept` VALUES (115, 100, '0,100', '张仲景国医国药学院', 11, NULL, NULL, NULL, '0', '0', 'admin', '2020-08-29 21:03:11', '', NULL);
INSERT INTO `sys_dept` VALUES (116, 100, '0,100', '范蠡商学院', 12, NULL, NULL, NULL, '0', '0', 'admin', '2020-08-29 21:03:20', '', NULL);
INSERT INTO `sys_dept` VALUES (117, 100, '0,100', '传媒学院', 13, NULL, NULL, NULL, '0', '0', 'admin', '2020-08-29 21:03:29', '', NULL);
INSERT INTO `sys_dept` VALUES (118, 100, '0,100', '马克思主义学院', 14, NULL, NULL, NULL, '0', '2', 'admin', '2020-08-29 21:03:38', '', NULL);
INSERT INTO `sys_dept` VALUES (119, 101, '0,100,101', '机械设计制造及其自动化', 0, NULL, NULL, NULL, '0', '0', 'admin', '2020-08-29 21:05:02', '', NULL);
INSERT INTO `sys_dept` VALUES (120, 101, '0,100,101', '电气工程及其自动化', 1, NULL, NULL, NULL, '0', '0', 'admin', '2020-08-29 21:05:14', '', NULL);
INSERT INTO `sys_dept` VALUES (121, 101, '0,100,101', '测控技术与仪器', 2, NULL, NULL, NULL, '0', '0', 'admin', '2020-08-29 21:05:25', '', NULL);
INSERT INTO `sys_dept` VALUES (122, 101, '0,100,101', '自动化', 3, NULL, NULL, NULL, '0', '0', 'admin', '2020-08-29 21:05:40', '', NULL);
INSERT INTO `sys_dept` VALUES (123, 101, '0,100,101', '材料成型与控制工程', 4, '', '', '', '0', '0', 'admin', '2020-08-29 21:05:54', 'admin', '2020-08-29 21:06:05');
INSERT INTO `sys_dept` VALUES (124, 101, '0,100,101', '汽车服务工程', 5, NULL, NULL, NULL, '0', '0', 'admin', '2020-08-29 21:06:23', '', NULL);
INSERT INTO `sys_dept` VALUES (125, 101, '0,100,101', '机器人工程', 6, NULL, NULL, NULL, '0', '0', 'admin', '2020-08-29 21:06:34', '', NULL);
INSERT INTO `sys_dept` VALUES (126, 101, '0,100,101', '机械设计制造及其自动化（中外）', 7, '', '', '', '0', '0', 'admin', '2020-08-29 21:07:22', 'admin', '2020-08-29 21:07:31');
INSERT INTO `sys_dept` VALUES (127, 101, '0,100,101', '电气工程及其自动化（中外）', 8, NULL, NULL, NULL, '0', '0', 'admin', '2020-08-29 21:07:56', '', NULL);
INSERT INTO `sys_dept` VALUES (128, 102, '0,100,102', '计算机科学与技术', 1, NULL, NULL, NULL, '0', '0', 'admin', '2020-08-29 21:09:13', '', NULL);
INSERT INTO `sys_dept` VALUES (129, 102, '0,100,102', '数据科学与大数据技术', 2, NULL, NULL, NULL, '0', '0', 'admin', '2020-08-29 21:09:30', '', NULL);
INSERT INTO `sys_dept` VALUES (130, 102, '0,100,102', '软件工程', 3, NULL, NULL, NULL, '0', '0', 'admin', '2020-08-29 21:09:47', '', NULL);
INSERT INTO `sys_dept` VALUES (131, 130, '0,100,102,130', '移动设备应用开发', 1, NULL, NULL, NULL, '0', '0', 'admin', '2020-08-29 21:10:08', '', NULL);
INSERT INTO `sys_dept` VALUES (132, 130, '0,100,102,130', '云计算', 2, NULL, NULL, NULL, '0', '0', 'admin', '2020-08-29 21:10:24', '', NULL);
INSERT INTO `sys_dept` VALUES (133, 130, '0,100,102,130', '智能软件开发', 3, NULL, NULL, NULL, '0', '0', 'admin', '2020-08-29 21:10:39', '', NULL);
INSERT INTO `sys_dept` VALUES (134, 130, '0,100,102,130', '数据库技术', 4, NULL, NULL, NULL, '0', '0', 'admin', '2020-08-29 21:10:50', '', NULL);
INSERT INTO `sys_dept` VALUES (135, 130, '0,100,102,130', '游戏开发技术', 5, NULL, NULL, NULL, '0', '0', 'admin', '2020-08-29 21:11:00', '', NULL);
INSERT INTO `sys_dept` VALUES (136, 130, '0,100,102,130', '渗透与测试', 6, NULL, NULL, NULL, '0', '0', 'admin', '2020-08-29 21:11:09', '', NULL);
INSERT INTO `sys_dept` VALUES (137, 103, '0,100,103', '建筑学', 1, NULL, NULL, NULL, '0', '0', 'admin', '2020-08-29 21:12:33', '', NULL);
INSERT INTO `sys_dept` VALUES (138, 103, '0,100,103', '城乡规划', 2, NULL, NULL, NULL, '0', '0', 'admin', '2020-08-29 21:12:43', '', NULL);
INSERT INTO `sys_dept` VALUES (139, 103, '0,100,103', '历史建筑保护工程', 3, NULL, NULL, NULL, '0', '0', 'admin', '2020-08-29 21:12:51', '', NULL);
INSERT INTO `sys_dept` VALUES (140, 103, '0,100,103', '环境设计', 4, NULL, NULL, NULL, '0', '0', 'admin', '2020-08-29 21:12:59', '', NULL);
INSERT INTO `sys_dept` VALUES (141, 108, '0,100,108', '生物工程', 1, NULL, NULL, NULL, '0', '0', 'admin', '2020-08-29 21:13:43', '', NULL);
INSERT INTO `sys_dept` VALUES (142, 108, '0,100,108', '化学工程与工艺', 2, NULL, NULL, NULL, '0', '0', 'admin', '2020-08-29 21:13:59', '', NULL);
INSERT INTO `sys_dept` VALUES (143, 108, '0,100,108', '应用化学', 3, NULL, NULL, NULL, '0', '0', 'admin', '2020-08-29 21:14:15', '', NULL);
INSERT INTO `sys_dept` VALUES (144, 107, '0,100,107', '英语', 1, NULL, NULL, NULL, '0', '0', 'admin', '2020-08-29 21:14:55', '', NULL);
INSERT INTO `sys_dept` VALUES (145, 107, '0,100,107', '商务英语', 2, NULL, NULL, NULL, '0', '0', 'admin', '2020-08-29 21:15:10', '', NULL);
INSERT INTO `sys_dept` VALUES (146, 107, '0,100,107', '日语', 3, NULL, NULL, NULL, '0', '0', 'admin', '2020-08-29 21:15:24', '', NULL);
INSERT INTO `sys_dept` VALUES (147, 105, '0,100,105', '数学与应用数学', 1, NULL, NULL, NULL, '0', '0', 'admin', '2020-08-29 21:15:58', '', NULL);
INSERT INTO `sys_dept` VALUES (148, 105, '0,100,105', '应用统计学', 2, NULL, NULL, NULL, '0', '0', 'admin', '2020-08-29 21:16:11', '', NULL);
INSERT INTO `sys_dept` VALUES (149, 106, '0,100,106', '学前教育', 1, NULL, NULL, NULL, '0', '0', 'admin', '2020-08-29 21:16:48', '', NULL);
INSERT INTO `sys_dept` VALUES (150, 106, '0,100,106', '小学教育', 2, NULL, NULL, NULL, '0', '0', 'admin', '2020-08-29 21:16:59', '', NULL);
INSERT INTO `sys_dept` VALUES (151, 106, '0,100,106', '音乐学', 3, NULL, NULL, NULL, '0', '0', 'admin', '2020-08-29 21:17:11', '', NULL);
INSERT INTO `sys_dept` VALUES (152, 106, '0,100,106', '音乐表演', 4, NULL, NULL, NULL, '0', '0', 'admin', '2020-08-29 21:17:35', '', NULL);
INSERT INTO `sys_dept` VALUES (153, 113, '0,100,113', '数字媒体技术', 1, NULL, NULL, NULL, '0', '0', 'admin', '2020-08-29 21:20:58', '', NULL);
INSERT INTO `sys_dept` VALUES (154, 113, '0,100,113', '视觉传达设计', 2, NULL, NULL, NULL, '0', '0', 'admin', '2020-08-29 21:21:09', '', NULL);
INSERT INTO `sys_dept` VALUES (155, 113, '0,100,113', '动画', 3, NULL, NULL, NULL, '0', '0', 'admin', '2020-08-29 21:21:24', '', NULL);
INSERT INTO `sys_dept` VALUES (156, 113, '0,100,113', '数字媒体艺术', 4, NULL, NULL, NULL, '0', '0', 'admin', '2020-08-29 21:21:38', '', NULL);
INSERT INTO `sys_dept` VALUES (157, 114, '0,100,114', '土木工程', 1, NULL, NULL, NULL, '0', '0', 'admin', '2020-08-29 21:22:05', '', NULL);
INSERT INTO `sys_dept` VALUES (158, 157, '0,100,114,157', '建筑工程', 1, NULL, NULL, NULL, '0', '0', 'admin', '2020-08-29 21:22:15', '', NULL);
INSERT INTO `sys_dept` VALUES (159, 157, '0,100,114,157', '地下工程', 2, NULL, NULL, NULL, '0', '0', 'admin', '2020-08-29 21:22:24', '', NULL);
INSERT INTO `sys_dept` VALUES (160, 114, '0,100,114', '工程管理', 2, NULL, NULL, NULL, '0', '0', 'admin', '2020-08-29 21:22:36', '', NULL);
INSERT INTO `sys_dept` VALUES (161, 114, '0,100,114', '给排水科学与工程', 3, '', '', '', '0', '0', 'admin', '2020-08-29 21:22:59', 'admin', '2020-08-29 21:23:09');
INSERT INTO `sys_dept` VALUES (162, 114, '0,100,114', '道路桥梁与渡河工程', 4, '', '', '', '0', '0', 'admin', '2020-08-29 21:23:26', 'admin', '2020-08-29 21:23:56');
INSERT INTO `sys_dept` VALUES (163, 115, '0,100,115', '中医学', 1, NULL, NULL, NULL, '0', '0', 'admin', '2020-08-29 21:26:09', '', NULL);
INSERT INTO `sys_dept` VALUES (164, 115, '0,100,115', '中药学', 2, NULL, NULL, NULL, '0', '0', 'admin', '2020-08-29 21:26:19', '', NULL);
INSERT INTO `sys_dept` VALUES (165, 115, '0,100,115', '护理学', 3, NULL, NULL, NULL, '0', '0', 'admin', '2020-08-29 21:26:31', '', NULL);
INSERT INTO `sys_dept` VALUES (166, 115, '0,100,115', '食品科学与工程', 4, NULL, NULL, NULL, '0', '0', 'admin', '2020-08-29 21:26:44', '', NULL);
INSERT INTO `sys_dept` VALUES (167, 116, '0,100,116', '工商管理', 1, NULL, NULL, NULL, '0', '0', 'admin', '2020-08-29 21:27:12', '', NULL);
INSERT INTO `sys_dept` VALUES (168, 116, '0,100,116', '财务管理', 2, NULL, NULL, NULL, '0', '0', 'admin', '2020-08-29 21:27:20', '', NULL);
INSERT INTO `sys_dept` VALUES (169, 116, '0,100,116', '国际经济与贸易', 3, NULL, NULL, NULL, '0', '0', 'admin', '2020-08-29 21:27:31', '', NULL);
INSERT INTO `sys_dept` VALUES (170, 116, '0,100,116', '市场营销', 4, NULL, NULL, NULL, '0', '0', 'admin', '2020-08-29 21:27:43', '', NULL);
INSERT INTO `sys_dept` VALUES (171, 116, '0,100,116', '电子商务', 5, NULL, NULL, NULL, '0', '0', 'admin', '2020-08-29 21:27:53', '', NULL);
INSERT INTO `sys_dept` VALUES (172, 116, '0,100,116', '国际商务', 6, NULL, NULL, NULL, '0', '0', 'admin', '2020-08-29 21:28:04', '', NULL);
INSERT INTO `sys_dept` VALUES (173, 117, '0,100,117', '播音与主持艺术', 1, NULL, NULL, NULL, '0', '0', 'admin', '2020-08-29 21:28:35', '', NULL);
INSERT INTO `sys_dept` VALUES (174, 117, '0,100,117', '广播电视编导', 2, NULL, NULL, NULL, '0', '0', 'admin', '2020-08-29 21:28:45', '', NULL);
INSERT INTO `sys_dept` VALUES (175, 117, '0,100,117', '汉语言文学', 3, NULL, NULL, NULL, '0', '0', 'admin', '2020-08-29 21:28:59', '', NULL);
INSERT INTO `sys_dept` VALUES (176, 117, '0,100,117', '法学', 4, NULL, NULL, NULL, '0', '0', 'admin', '2020-08-29 21:29:07', '', NULL);
COMMIT;

-- ----------------------------
-- Table structure for sys_dict_data
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_data`;
CREATE TABLE `sys_dict_data` (
  `dict_code` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '字典编码',
  `dict_sort` int(4) DEFAULT '0' COMMENT '字典排序',
  `dict_label` varchar(100) DEFAULT '' COMMENT '字典标签',
  `dict_value` varchar(100) DEFAULT '' COMMENT '字典键值',
  `dict_type` varchar(100) DEFAULT '' COMMENT '字典类型',
  `css_class` varchar(100) DEFAULT NULL COMMENT '样式属性（其他样式扩展）',
  `list_class` varchar(100) DEFAULT NULL COMMENT '表格回显样式',
  `is_default` char(1) DEFAULT 'N' COMMENT '是否默认（Y是 N否）',
  `status` char(1) DEFAULT '0' COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`dict_code`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='字典数据表';

-- ----------------------------
-- Records of sys_dict_data
-- ----------------------------
BEGIN;
INSERT INTO `sys_dict_data` VALUES (1, 1, '男', '0', 'sys_user_sex', '', '', 'Y', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '性别男');
INSERT INTO `sys_dict_data` VALUES (2, 2, '女', '1', 'sys_user_sex', '', '', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '性别女');
INSERT INTO `sys_dict_data` VALUES (3, 3, '未知', '2', 'sys_user_sex', '', '', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '性别未知');
INSERT INTO `sys_dict_data` VALUES (4, 1, '显示', '0', 'sys_show_hide', '', 'primary', 'Y', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '显示菜单');
INSERT INTO `sys_dict_data` VALUES (5, 2, '隐藏', '1', 'sys_show_hide', '', 'danger', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '隐藏菜单');
INSERT INTO `sys_dict_data` VALUES (6, 1, '正常', '0', 'sys_normal_disable', '', 'primary', 'Y', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '正常状态');
INSERT INTO `sys_dict_data` VALUES (7, 2, '停用', '1', 'sys_normal_disable', '', 'danger', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '停用状态');
INSERT INTO `sys_dict_data` VALUES (8, 1, '正常', '0', 'sys_job_status', '', 'primary', 'Y', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '正常状态');
INSERT INTO `sys_dict_data` VALUES (9, 2, '暂停', '1', 'sys_job_status', '', 'danger', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '停用状态');
INSERT INTO `sys_dict_data` VALUES (10, 1, '默认', 'DEFAULT', 'sys_job_group', '', '', 'Y', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '默认分组');
INSERT INTO `sys_dict_data` VALUES (11, 2, '系统', 'SYSTEM', 'sys_job_group', '', '', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '系统分组');
INSERT INTO `sys_dict_data` VALUES (12, 1, '是', 'Y', 'sys_yes_no', '', 'primary', 'Y', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '系统默认是');
INSERT INTO `sys_dict_data` VALUES (13, 2, '否', 'N', 'sys_yes_no', '', 'danger', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '系统默认否');
INSERT INTO `sys_dict_data` VALUES (14, 1, '通知', '1', 'sys_notice_type', '', 'warning', 'Y', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '通知');
INSERT INTO `sys_dict_data` VALUES (15, 2, '公告', '2', 'sys_notice_type', '', 'success', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '公告');
INSERT INTO `sys_dict_data` VALUES (16, 1, '正常', '0', 'sys_notice_status', '', 'primary', 'Y', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '正常状态');
INSERT INTO `sys_dict_data` VALUES (17, 2, '关闭', '1', 'sys_notice_status', '', 'danger', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '关闭状态');
INSERT INTO `sys_dict_data` VALUES (18, 99, '其他', '0', 'sys_oper_type', '', 'info', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '其他操作');
INSERT INTO `sys_dict_data` VALUES (19, 1, '新增', '1', 'sys_oper_type', '', 'info', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '新增操作');
INSERT INTO `sys_dict_data` VALUES (20, 2, '修改', '2', 'sys_oper_type', '', 'info', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '修改操作');
INSERT INTO `sys_dict_data` VALUES (21, 3, '删除', '3', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '删除操作');
INSERT INTO `sys_dict_data` VALUES (22, 4, '授权', '4', 'sys_oper_type', '', 'primary', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '授权操作');
INSERT INTO `sys_dict_data` VALUES (23, 5, '导出', '5', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '导出操作');
INSERT INTO `sys_dict_data` VALUES (24, 6, '导入', '6', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '导入操作');
INSERT INTO `sys_dict_data` VALUES (25, 7, '强退', '7', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '强退操作');
INSERT INTO `sys_dict_data` VALUES (26, 8, '生成代码', '8', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '生成操作');
INSERT INTO `sys_dict_data` VALUES (27, 9, '清空数据', '9', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '清空操作');
INSERT INTO `sys_dict_data` VALUES (28, 1, '成功', '0', 'sys_common_status', '', 'primary', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '正常状态');
INSERT INTO `sys_dict_data` VALUES (29, 2, '失败', '1', 'sys_common_status', '', 'danger', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '停用状态');
INSERT INTO `sys_dict_data` VALUES (30, 0, '待审核', '0', 'user_task_status', '', 'primary', 'Y', '0', 'admin', '2020-09-04 10:19:20', 'admin', '2020-09-04 10:20:12', '用户任务待审核状态');
INSERT INTO `sys_dict_data` VALUES (31, 1, '审核通过', '1', 'user_task_status', NULL, 'success', 'Y', '0', 'admin', '2020-09-04 10:19:57', '', NULL, '用户任务审核通过状态');
INSERT INTO `sys_dict_data` VALUES (32, 2, '审核未通过', '2', 'user_task_status', NULL, 'danger', 'Y', '0', 'admin', '2020-09-04 10:22:22', '', NULL, '任务审核未通过状态');
COMMIT;

-- ----------------------------
-- Table structure for sys_dict_type
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_type`;
CREATE TABLE `sys_dict_type` (
  `dict_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '字典主键',
  `dict_name` varchar(100) DEFAULT '' COMMENT '字典名称',
  `dict_type` varchar(100) DEFAULT '' COMMENT '字典类型',
  `status` char(1) DEFAULT '0' COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`dict_id`) USING BTREE,
  UNIQUE KEY `dict_type` (`dict_type`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='字典类型表';

-- ----------------------------
-- Records of sys_dict_type
-- ----------------------------
BEGIN;
INSERT INTO `sys_dict_type` VALUES (1, '用户性别', 'sys_user_sex', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '用户性别列表');
INSERT INTO `sys_dict_type` VALUES (2, '菜单状态', 'sys_show_hide', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '菜单状态列表');
INSERT INTO `sys_dict_type` VALUES (3, '系统开关', 'sys_normal_disable', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '系统开关列表');
INSERT INTO `sys_dict_type` VALUES (4, '任务状态', 'sys_job_status', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '任务状态列表');
INSERT INTO `sys_dict_type` VALUES (5, '任务分组', 'sys_job_group', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '任务分组列表');
INSERT INTO `sys_dict_type` VALUES (6, '系统是否', 'sys_yes_no', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '系统是否列表');
INSERT INTO `sys_dict_type` VALUES (7, '通知类型', 'sys_notice_type', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '通知类型列表');
INSERT INTO `sys_dict_type` VALUES (8, '通知状态', 'sys_notice_status', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '通知状态列表');
INSERT INTO `sys_dict_type` VALUES (9, '操作类型', 'sys_oper_type', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '操作类型列表');
INSERT INTO `sys_dict_type` VALUES (10, '系统状态', 'sys_common_status', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '登录状态列表');
INSERT INTO `sys_dict_type` VALUES (11, '用户任务', 'user_task_status', '0', 'admin', '2020-09-04 10:17:37', '', NULL, '用户发布任务状态');
COMMIT;

-- ----------------------------
-- Table structure for sys_file_info
-- ----------------------------
DROP TABLE IF EXISTS `sys_file_info`;
CREATE TABLE `sys_file_info` (
  `file_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '文件id',
  `file_name` varchar(50) DEFAULT '' COMMENT '文件名称',
  `file_path` varchar(255) DEFAULT '' COMMENT '文件路径',
  PRIMARY KEY (`file_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='文件信息表';

-- ----------------------------
-- Table structure for sys_job
-- ----------------------------
DROP TABLE IF EXISTS `sys_job`;
CREATE TABLE `sys_job` (
  `job_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '任务ID',
  `job_name` varchar(64) NOT NULL DEFAULT '' COMMENT '任务名称',
  `job_group` varchar(64) NOT NULL DEFAULT 'DEFAULT' COMMENT '任务组名',
  `invoke_target` varchar(500) NOT NULL COMMENT '调用目标字符串',
  `cron_expression` varchar(255) DEFAULT '' COMMENT 'cron执行表达式',
  `misfire_policy` varchar(20) DEFAULT '3' COMMENT '计划执行错误策略（1立即执行 2执行一次 3放弃执行）',
  `concurrent` char(1) DEFAULT '1' COMMENT '是否并发执行（0允许 1禁止）',
  `status` char(1) DEFAULT '0' COMMENT '状态（0正常 1暂停）',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT '' COMMENT '备注信息',
  PRIMARY KEY (`job_id`,`job_name`,`job_group`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='定时任务调度表';

-- ----------------------------
-- Records of sys_job
-- ----------------------------
BEGIN;
INSERT INTO `sys_job` VALUES (1, '系统默认（无参）', 'DEFAULT', 'ryTask.ryNoParams', '0/10 * * * * ?', '3', '1', '1', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_job` VALUES (2, '系统默认（有参）', 'DEFAULT', 'ryTask.ryParams(\'ry\')', '0/15 * * * * ?', '3', '1', '1', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_job` VALUES (3, '系统默认（多参）', 'DEFAULT', 'ryTask.ryMultipleParams(\'ry\', true, 2000L, 316.50D, 100)', '0/20 * * * * ?', '3', '1', '1', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
COMMIT;

-- ----------------------------
-- Table structure for sys_job_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_job_log`;
CREATE TABLE `sys_job_log` (
  `job_log_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '任务日志ID',
  `job_name` varchar(64) NOT NULL COMMENT '任务名称',
  `job_group` varchar(64) NOT NULL COMMENT '任务组名',
  `invoke_target` varchar(500) NOT NULL COMMENT '调用目标字符串',
  `job_message` varchar(500) DEFAULT NULL COMMENT '日志信息',
  `status` char(1) DEFAULT '0' COMMENT '执行状态（0正常 1失败）',
  `exception_info` varchar(2000) DEFAULT '' COMMENT '异常信息',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`job_log_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='定时任务调度日志表';

-- ----------------------------
-- Table structure for sys_logininfor
-- ----------------------------
DROP TABLE IF EXISTS `sys_logininfor`;
CREATE TABLE `sys_logininfor` (
  `info_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '访问ID',
  `login_name` varchar(50) DEFAULT '' COMMENT '登录账号',
  `ipaddr` varchar(50) DEFAULT '' COMMENT '登录IP地址',
  `login_location` varchar(255) DEFAULT '' COMMENT '登录地点',
  `browser` varchar(50) DEFAULT '' COMMENT '浏览器类型',
  `os` varchar(50) DEFAULT '' COMMENT '操作系统',
  `status` char(1) DEFAULT '0' COMMENT '登录状态（0成功 1失败）',
  `msg` varchar(255) DEFAULT '' COMMENT '提示消息',
  `login_time` datetime DEFAULT NULL COMMENT '访问时间',
  PRIMARY KEY (`info_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=428 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='系统访问记录';

-- ----------------------------
-- Records of sys_logininfor
-- ----------------------------
BEGIN;
INSERT INTO `sys_logininfor` VALUES (100, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-08-26 09:26:18');
INSERT INTO `sys_logininfor` VALUES (101, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-08-26 09:49:32');
INSERT INTO `sys_logininfor` VALUES (102, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-08-26 09:57:46');
INSERT INTO `sys_logininfor` VALUES (103, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-08-27 13:13:05');
INSERT INTO `sys_logininfor` VALUES (104, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-08-28 21:01:22');
INSERT INTO `sys_logininfor` VALUES (105, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-08-29 12:49:37');
INSERT INTO `sys_logininfor` VALUES (106, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-08-29 15:33:34');
INSERT INTO `sys_logininfor` VALUES (107, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-08-29 20:27:36');
INSERT INTO `sys_logininfor` VALUES (108, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-08-29 20:53:38');
INSERT INTO `sys_logininfor` VALUES (109, '1715925168', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-08-29 21:35:33');
INSERT INTO `sys_logininfor` VALUES (110, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-08-29 21:40:46');
INSERT INTO `sys_logininfor` VALUES (111, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-08-29 22:00:38');
INSERT INTO `sys_logininfor` VALUES (112, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '退出成功', '2020-08-29 22:05:08');
INSERT INTO `sys_logininfor` VALUES (113, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-08-29 22:05:13');
INSERT INTO `sys_logininfor` VALUES (114, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '退出成功', '2020-08-29 22:05:16');
INSERT INTO `sys_logininfor` VALUES (115, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-08-29 22:05:23');
INSERT INTO `sys_logininfor` VALUES (116, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-08-29 23:11:33');
INSERT INTO `sys_logininfor` VALUES (117, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-08-29 23:34:03');
INSERT INTO `sys_logininfor` VALUES (118, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-08-29 23:40:38');
INSERT INTO `sys_logininfor` VALUES (119, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-08-29 23:42:26');
INSERT INTO `sys_logininfor` VALUES (120, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '退出成功', '2020-08-29 23:44:33');
INSERT INTO `sys_logininfor` VALUES (121, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-08-29 23:44:36');
INSERT INTO `sys_logininfor` VALUES (122, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '1', '验证码错误', '2020-08-30 14:04:32');
INSERT INTO `sys_logininfor` VALUES (123, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-08-30 14:04:35');
INSERT INTO `sys_logininfor` VALUES (124, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-08-30 17:03:31');
INSERT INTO `sys_logininfor` VALUES (125, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '退出成功', '2020-08-30 17:18:03');
INSERT INTO `sys_logininfor` VALUES (126, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-08-30 17:18:26');
INSERT INTO `sys_logininfor` VALUES (127, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-08-30 17:55:51');
INSERT INTO `sys_logininfor` VALUES (128, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-08-30 19:10:06');
INSERT INTO `sys_logininfor` VALUES (129, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-08-31 09:10:11');
INSERT INTO `sys_logininfor` VALUES (130, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-08-31 09:25:48');
INSERT INTO `sys_logininfor` VALUES (131, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-08-31 09:33:04');
INSERT INTO `sys_logininfor` VALUES (132, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-08-31 10:51:07');
INSERT INTO `sys_logininfor` VALUES (133, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-08-31 14:30:05');
INSERT INTO `sys_logininfor` VALUES (134, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-08-31 15:25:26');
INSERT INTO `sys_logininfor` VALUES (135, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-08-31 15:46:43');
INSERT INTO `sys_logininfor` VALUES (136, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-08-31 17:30:52');
INSERT INTO `sys_logininfor` VALUES (137, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-01 08:41:35');
INSERT INTO `sys_logininfor` VALUES (138, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-01 08:47:57');
INSERT INTO `sys_logininfor` VALUES (139, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-01 09:23:39');
INSERT INTO `sys_logininfor` VALUES (140, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-01 09:38:08');
INSERT INTO `sys_logininfor` VALUES (141, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-01 10:48:26');
INSERT INTO `sys_logininfor` VALUES (142, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-01 10:55:42');
INSERT INTO `sys_logininfor` VALUES (143, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-01 11:49:58');
INSERT INTO `sys_logininfor` VALUES (144, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-01 11:58:46');
INSERT INTO `sys_logininfor` VALUES (145, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-01 12:03:23');
INSERT INTO `sys_logininfor` VALUES (146, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-01 12:07:23');
INSERT INTO `sys_logininfor` VALUES (147, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-01 12:49:55');
INSERT INTO `sys_logininfor` VALUES (148, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-01 12:51:56');
INSERT INTO `sys_logininfor` VALUES (149, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-02 12:04:24');
INSERT INTO `sys_logininfor` VALUES (150, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-02 12:13:07');
INSERT INTO `sys_logininfor` VALUES (151, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-02 16:50:44');
INSERT INTO `sys_logininfor` VALUES (152, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-03 11:02:15');
INSERT INTO `sys_logininfor` VALUES (153, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-03 11:06:13');
INSERT INTO `sys_logininfor` VALUES (154, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-03 11:13:10');
INSERT INTO `sys_logininfor` VALUES (155, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-03 11:15:41');
INSERT INTO `sys_logininfor` VALUES (156, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-03 11:19:19');
INSERT INTO `sys_logininfor` VALUES (157, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-03 11:30:47');
INSERT INTO `sys_logininfor` VALUES (158, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-03 11:37:05');
INSERT INTO `sys_logininfor` VALUES (159, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-03 12:12:57');
INSERT INTO `sys_logininfor` VALUES (160, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-03 12:14:00');
INSERT INTO `sys_logininfor` VALUES (161, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-03 12:28:58');
INSERT INTO `sys_logininfor` VALUES (162, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-03 12:43:39');
INSERT INTO `sys_logininfor` VALUES (163, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-03 12:45:30');
INSERT INTO `sys_logininfor` VALUES (164, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-03 12:46:26');
INSERT INTO `sys_logininfor` VALUES (165, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-03 12:52:25');
INSERT INTO `sys_logininfor` VALUES (166, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-03 12:53:32');
INSERT INTO `sys_logininfor` VALUES (167, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-03 12:57:48');
INSERT INTO `sys_logininfor` VALUES (168, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-03 12:59:08');
INSERT INTO `sys_logininfor` VALUES (169, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-03 13:15:33');
INSERT INTO `sys_logininfor` VALUES (170, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-03 13:16:53');
INSERT INTO `sys_logininfor` VALUES (171, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-03 13:25:47');
INSERT INTO `sys_logininfor` VALUES (172, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-03 13:28:59');
INSERT INTO `sys_logininfor` VALUES (173, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-03 13:30:56');
INSERT INTO `sys_logininfor` VALUES (174, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-03 13:58:38');
INSERT INTO `sys_logininfor` VALUES (175, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-03 14:03:35');
INSERT INTO `sys_logininfor` VALUES (176, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-03 14:05:20');
INSERT INTO `sys_logininfor` VALUES (177, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-03 14:07:45');
INSERT INTO `sys_logininfor` VALUES (178, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-03 14:20:23');
INSERT INTO `sys_logininfor` VALUES (179, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-03 14:22:09');
INSERT INTO `sys_logininfor` VALUES (180, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-03 14:27:38');
INSERT INTO `sys_logininfor` VALUES (181, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-03 14:38:10');
INSERT INTO `sys_logininfor` VALUES (182, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-03 14:51:59');
INSERT INTO `sys_logininfor` VALUES (183, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-03 14:55:19');
INSERT INTO `sys_logininfor` VALUES (184, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-03 15:21:46');
INSERT INTO `sys_logininfor` VALUES (185, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-03 15:38:09');
INSERT INTO `sys_logininfor` VALUES (186, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-03 15:40:41');
INSERT INTO `sys_logininfor` VALUES (187, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-03 15:56:11');
INSERT INTO `sys_logininfor` VALUES (188, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-03 17:10:16');
INSERT INTO `sys_logininfor` VALUES (189, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-04 08:58:11');
INSERT INTO `sys_logininfor` VALUES (190, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-04 09:15:47');
INSERT INTO `sys_logininfor` VALUES (191, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-04 09:29:23');
INSERT INTO `sys_logininfor` VALUES (192, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-04 09:34:38');
INSERT INTO `sys_logininfor` VALUES (193, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-04 09:58:50');
INSERT INTO `sys_logininfor` VALUES (194, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-04 10:58:47');
INSERT INTO `sys_logininfor` VALUES (195, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-04 11:12:32');
INSERT INTO `sys_logininfor` VALUES (196, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-04 11:17:38');
INSERT INTO `sys_logininfor` VALUES (197, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-04 12:54:19');
INSERT INTO `sys_logininfor` VALUES (198, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-04 13:25:00');
INSERT INTO `sys_logininfor` VALUES (199, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-04 13:27:19');
INSERT INTO `sys_logininfor` VALUES (200, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-04 13:42:43');
INSERT INTO `sys_logininfor` VALUES (201, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-04 14:19:14');
INSERT INTO `sys_logininfor` VALUES (202, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-04 15:37:49');
INSERT INTO `sys_logininfor` VALUES (203, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-04 15:42:36');
INSERT INTO `sys_logininfor` VALUES (204, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-04 20:15:07');
INSERT INTO `sys_logininfor` VALUES (205, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-05 11:07:35');
INSERT INTO `sys_logininfor` VALUES (206, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-05 11:50:33');
INSERT INTO `sys_logininfor` VALUES (207, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-05 11:56:09');
INSERT INTO `sys_logininfor` VALUES (208, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-05 14:24:04');
INSERT INTO `sys_logininfor` VALUES (209, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-07 08:49:39');
INSERT INTO `sys_logininfor` VALUES (210, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-07 09:05:53');
INSERT INTO `sys_logininfor` VALUES (211, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-07 10:02:47');
INSERT INTO `sys_logininfor` VALUES (212, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-07 10:05:27');
INSERT INTO `sys_logininfor` VALUES (213, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-07 10:13:42');
INSERT INTO `sys_logininfor` VALUES (214, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-07 10:16:26');
INSERT INTO `sys_logininfor` VALUES (215, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-07 10:18:18');
INSERT INTO `sys_logininfor` VALUES (216, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-07 10:20:02');
INSERT INTO `sys_logininfor` VALUES (217, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-07 13:28:21');
INSERT INTO `sys_logininfor` VALUES (218, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-07 14:05:35');
INSERT INTO `sys_logininfor` VALUES (219, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-07 15:08:02');
INSERT INTO `sys_logininfor` VALUES (220, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-07 16:57:09');
INSERT INTO `sys_logininfor` VALUES (221, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-08 08:32:45');
INSERT INTO `sys_logininfor` VALUES (222, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-08 08:57:03');
INSERT INTO `sys_logininfor` VALUES (223, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-08 09:00:51');
INSERT INTO `sys_logininfor` VALUES (224, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-08 09:13:32');
INSERT INTO `sys_logininfor` VALUES (225, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-08 09:31:00');
INSERT INTO `sys_logininfor` VALUES (226, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-08 09:46:39');
INSERT INTO `sys_logininfor` VALUES (227, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-08 12:49:59');
INSERT INTO `sys_logininfor` VALUES (228, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-08 13:59:29');
INSERT INTO `sys_logininfor` VALUES (229, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-10 08:43:39');
INSERT INTO `sys_logininfor` VALUES (230, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-10 08:55:46');
INSERT INTO `sys_logininfor` VALUES (231, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '退出成功', '2020-09-10 09:20:46');
INSERT INTO `sys_logininfor` VALUES (232, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-10 09:21:34');
INSERT INTO `sys_logininfor` VALUES (233, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-10 10:03:13');
INSERT INTO `sys_logininfor` VALUES (234, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-10 11:18:45');
INSERT INTO `sys_logininfor` VALUES (235, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-10 12:10:12');
INSERT INTO `sys_logininfor` VALUES (236, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-10 12:52:12');
INSERT INTO `sys_logininfor` VALUES (237, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-10 12:54:08');
INSERT INTO `sys_logininfor` VALUES (238, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-10 13:00:33');
INSERT INTO `sys_logininfor` VALUES (239, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-10 13:02:16');
INSERT INTO `sys_logininfor` VALUES (240, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-10 14:42:17');
INSERT INTO `sys_logininfor` VALUES (241, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-10 16:09:47');
INSERT INTO `sys_logininfor` VALUES (242, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-11 08:49:42');
INSERT INTO `sys_logininfor` VALUES (243, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-11 09:37:48');
INSERT INTO `sys_logininfor` VALUES (244, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-11 10:28:34');
INSERT INTO `sys_logininfor` VALUES (245, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-11 10:39:09');
INSERT INTO `sys_logininfor` VALUES (246, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-11 11:41:08');
INSERT INTO `sys_logininfor` VALUES (247, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-11 11:47:02');
INSERT INTO `sys_logininfor` VALUES (248, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-11 12:24:09');
INSERT INTO `sys_logininfor` VALUES (249, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-11 12:30:35');
INSERT INTO `sys_logininfor` VALUES (250, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-11 12:37:39');
INSERT INTO `sys_logininfor` VALUES (251, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-11 12:41:55');
INSERT INTO `sys_logininfor` VALUES (252, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-11 12:45:36');
INSERT INTO `sys_logininfor` VALUES (253, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-11 13:22:53');
INSERT INTO `sys_logininfor` VALUES (254, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-11 13:30:21');
INSERT INTO `sys_logininfor` VALUES (255, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-11 13:50:45');
INSERT INTO `sys_logininfor` VALUES (256, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-11 14:41:24');
INSERT INTO `sys_logininfor` VALUES (257, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-11 14:52:14');
INSERT INTO `sys_logininfor` VALUES (258, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-11 14:56:51');
INSERT INTO `sys_logininfor` VALUES (259, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-11 15:10:47');
INSERT INTO `sys_logininfor` VALUES (260, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-11 16:13:16');
INSERT INTO `sys_logininfor` VALUES (261, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-11 16:16:06');
INSERT INTO `sys_logininfor` VALUES (262, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-11 16:18:22');
INSERT INTO `sys_logininfor` VALUES (263, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-11 16:19:50');
INSERT INTO `sys_logininfor` VALUES (264, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-14 08:45:04');
INSERT INTO `sys_logininfor` VALUES (265, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-14 10:08:01');
INSERT INTO `sys_logininfor` VALUES (266, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-14 12:10:54');
INSERT INTO `sys_logininfor` VALUES (267, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-14 12:44:17');
INSERT INTO `sys_logininfor` VALUES (268, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-14 14:01:36');
INSERT INTO `sys_logininfor` VALUES (269, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-14 14:14:31');
INSERT INTO `sys_logininfor` VALUES (270, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-14 14:38:37');
INSERT INTO `sys_logininfor` VALUES (271, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-14 14:48:14');
INSERT INTO `sys_logininfor` VALUES (272, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-14 14:49:44');
INSERT INTO `sys_logininfor` VALUES (273, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-14 14:55:53');
INSERT INTO `sys_logininfor` VALUES (274, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-14 16:30:41');
INSERT INTO `sys_logininfor` VALUES (275, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-14 17:18:17');
INSERT INTO `sys_logininfor` VALUES (276, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-15 09:21:50');
INSERT INTO `sys_logininfor` VALUES (277, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-15 09:53:50');
INSERT INTO `sys_logininfor` VALUES (278, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-15 10:46:09');
INSERT INTO `sys_logininfor` VALUES (279, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-15 11:12:46');
INSERT INTO `sys_logininfor` VALUES (280, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-15 11:16:59');
INSERT INTO `sys_logininfor` VALUES (281, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-15 13:45:27');
INSERT INTO `sys_logininfor` VALUES (282, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-15 14:13:18');
INSERT INTO `sys_logininfor` VALUES (283, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-15 14:14:07');
INSERT INTO `sys_logininfor` VALUES (284, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-15 14:36:55');
INSERT INTO `sys_logininfor` VALUES (285, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-15 15:00:00');
INSERT INTO `sys_logininfor` VALUES (286, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-15 15:06:53');
INSERT INTO `sys_logininfor` VALUES (287, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-15 15:09:51');
INSERT INTO `sys_logininfor` VALUES (288, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-15 15:13:49');
INSERT INTO `sys_logininfor` VALUES (289, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-15 15:25:36');
INSERT INTO `sys_logininfor` VALUES (290, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-15 15:25:50');
INSERT INTO `sys_logininfor` VALUES (291, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-15 15:30:43');
INSERT INTO `sys_logininfor` VALUES (292, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-15 15:31:28');
INSERT INTO `sys_logininfor` VALUES (293, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-15 15:32:28');
INSERT INTO `sys_logininfor` VALUES (294, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '1', '密码输入错误1次', '2020-09-15 16:50:06');
INSERT INTO `sys_logininfor` VALUES (295, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '1', '密码输入错误2次', '2020-09-15 16:50:16');
INSERT INTO `sys_logininfor` VALUES (296, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '1', '密码输入错误3次', '2020-09-15 16:50:38');
INSERT INTO `sys_logininfor` VALUES (297, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '1', '密码输入错误4次', '2020-09-15 16:50:54');
INSERT INTO `sys_logininfor` VALUES (298, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '1', '密码输入错误5次', '2020-09-15 16:51:13');
INSERT INTO `sys_logininfor` VALUES (299, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '1', '密码输入错误5次，帐户锁定10分钟', '2020-09-15 16:52:23');
INSERT INTO `sys_logininfor` VALUES (300, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '1', '密码输入错误5次，帐户锁定10分钟', '2020-09-15 16:52:35');
INSERT INTO `sys_logininfor` VALUES (301, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '1', '密码输入错误5次，帐户锁定10分钟', '2020-09-15 16:52:46');
INSERT INTO `sys_logininfor` VALUES (302, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-15 16:53:52');
INSERT INTO `sys_logininfor` VALUES (303, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-15 16:54:57');
INSERT INTO `sys_logininfor` VALUES (304, 'admin', '127.0.0.1', '内网IP', 'Unknown', 'Unknown', '0', '登录成功', '2020-09-15 16:55:09');
INSERT INTO `sys_logininfor` VALUES (305, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-16 09:06:18');
INSERT INTO `sys_logininfor` VALUES (306, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '1', '用户不存在/密码错误', '2020-09-16 09:07:24');
INSERT INTO `sys_logininfor` VALUES (307, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-16 09:09:58');
INSERT INTO `sys_logininfor` VALUES (308, '1715925168', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '1', '用户不存在/密码错误', '2020-09-16 09:11:01');
INSERT INTO `sys_logininfor` VALUES (309, '1715925168', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '1', '用户不存在/密码错误', '2020-09-16 09:23:12');
INSERT INTO `sys_logininfor` VALUES (310, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-16 09:25:08');
INSERT INTO `sys_logininfor` VALUES (311, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-16 09:29:05');
INSERT INTO `sys_logininfor` VALUES (312, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-16 09:29:25');
INSERT INTO `sys_logininfor` VALUES (313, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-16 09:30:23');
INSERT INTO `sys_logininfor` VALUES (314, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-16 09:34:42');
INSERT INTO `sys_logininfor` VALUES (315, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-16 09:39:28');
INSERT INTO `sys_logininfor` VALUES (316, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-16 09:40:35');
INSERT INTO `sys_logininfor` VALUES (317, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-16 09:41:34');
INSERT INTO `sys_logininfor` VALUES (318, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-16 11:36:26');
INSERT INTO `sys_logininfor` VALUES (319, '1715925168', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '1', '用户不存在/密码错误', '2020-09-16 11:38:23');
INSERT INTO `sys_logininfor` VALUES (320, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-16 11:40:10');
INSERT INTO `sys_logininfor` VALUES (321, '1715925168', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-16 11:41:36');
INSERT INTO `sys_logininfor` VALUES (322, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-16 12:40:01');
INSERT INTO `sys_logininfor` VALUES (323, '1715925168', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-16 12:41:23');
INSERT INTO `sys_logininfor` VALUES (324, 'admin', '127.0.0.1', '内网IP', 'Unknown', 'Unknown', '0', '登录成功', '2020-09-16 13:25:12');
INSERT INTO `sys_logininfor` VALUES (325, '1715925168', '127.0.0.1', '内网IP', 'Unknown', 'Unknown', '0', '登录成功', '2020-09-16 13:28:39');
INSERT INTO `sys_logininfor` VALUES (326, '1715925168', '127.0.0.1', '内网IP', 'Unknown', 'Unknown', '0', '登录成功', '2020-09-16 13:30:41');
INSERT INTO `sys_logininfor` VALUES (327, '1715925168', '127.0.0.1', '内网IP', 'Unknown', 'Unknown', '0', '登录成功', '2020-09-16 13:32:12');
INSERT INTO `sys_logininfor` VALUES (328, '1715925168', '127.0.0.1', '内网IP', 'Unknown', 'Unknown', '0', '登录成功', '2020-09-16 13:32:36');
INSERT INTO `sys_logininfor` VALUES (329, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-16 13:37:21');
INSERT INTO `sys_logininfor` VALUES (330, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-16 13:53:23');
INSERT INTO `sys_logininfor` VALUES (331, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-16 14:08:24');
INSERT INTO `sys_logininfor` VALUES (332, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-16 15:08:15');
INSERT INTO `sys_logininfor` VALUES (333, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-16 15:12:15');
INSERT INTO `sys_logininfor` VALUES (334, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-16 15:23:51');
INSERT INTO `sys_logininfor` VALUES (335, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-16 15:30:04');
INSERT INTO `sys_logininfor` VALUES (336, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-16 15:36:45');
INSERT INTO `sys_logininfor` VALUES (337, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-16 15:39:50');
INSERT INTO `sys_logininfor` VALUES (338, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-16 15:42:46');
INSERT INTO `sys_logininfor` VALUES (339, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-16 15:45:08');
INSERT INTO `sys_logininfor` VALUES (340, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-16 15:46:45');
INSERT INTO `sys_logininfor` VALUES (341, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-16 15:53:20');
INSERT INTO `sys_logininfor` VALUES (342, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-16 15:59:27');
INSERT INTO `sys_logininfor` VALUES (343, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-16 16:02:10');
INSERT INTO `sys_logininfor` VALUES (344, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-16 16:03:23');
INSERT INTO `sys_logininfor` VALUES (345, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-16 16:07:14');
INSERT INTO `sys_logininfor` VALUES (346, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-16 16:08:26');
INSERT INTO `sys_logininfor` VALUES (347, 'admin', '127.0.0.1', '内网IP', 'Unknown', 'Unknown', '0', '登录成功', '2020-09-16 16:12:59');
INSERT INTO `sys_logininfor` VALUES (348, '1715925168', '127.0.0.1', '内网IP', 'Unknown', 'Unknown', '0', '登录成功', '2020-09-16 16:13:22');
INSERT INTO `sys_logininfor` VALUES (349, 'admin', '127.0.0.1', '内网IP', 'Unknown', 'Unknown', '0', '登录成功', '2020-09-16 16:16:04');
INSERT INTO `sys_logininfor` VALUES (350, '1715925168', '127.0.0.1', '内网IP', 'Unknown', 'Unknown', '0', '登录成功', '2020-09-16 16:16:21');
INSERT INTO `sys_logininfor` VALUES (351, 'admin', '127.0.0.1', '内网IP', 'Unknown', 'Unknown', '0', '登录成功', '2020-09-16 16:30:50');
INSERT INTO `sys_logininfor` VALUES (352, '1715925168', '127.0.0.1', '内网IP', 'Unknown', 'Unknown', '0', '登录成功', '2020-09-16 16:31:00');
INSERT INTO `sys_logininfor` VALUES (353, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-17 11:47:12');
INSERT INTO `sys_logininfor` VALUES (354, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-17 12:09:04');
INSERT INTO `sys_logininfor` VALUES (355, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-17 13:26:37');
INSERT INTO `sys_logininfor` VALUES (356, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-17 13:29:26');
INSERT INTO `sys_logininfor` VALUES (357, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '退出成功', '2020-09-17 13:36:57');
INSERT INTO `sys_logininfor` VALUES (358, '1715925168', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-17 13:37:09');
INSERT INTO `sys_logininfor` VALUES (359, '1715925168', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '退出成功', '2020-09-17 13:37:25');
INSERT INTO `sys_logininfor` VALUES (360, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-17 13:37:26');
INSERT INTO `sys_logininfor` VALUES (361, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '退出成功', '2020-09-17 13:38:00');
INSERT INTO `sys_logininfor` VALUES (362, '1715925168', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-17 13:38:11');
INSERT INTO `sys_logininfor` VALUES (363, '1715925168', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '退出成功', '2020-09-17 13:39:11');
INSERT INTO `sys_logininfor` VALUES (364, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-17 13:39:13');
INSERT INTO `sys_logininfor` VALUES (365, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '退出成功', '2020-09-17 13:40:12');
INSERT INTO `sys_logininfor` VALUES (366, '1715925168', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-17 13:40:21');
INSERT INTO `sys_logininfor` VALUES (367, '1715925168', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '退出成功', '2020-09-17 13:40:47');
INSERT INTO `sys_logininfor` VALUES (368, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-17 13:40:49');
INSERT INTO `sys_logininfor` VALUES (369, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '退出成功', '2020-09-17 13:42:45');
INSERT INTO `sys_logininfor` VALUES (370, '1715925168', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-17 13:42:58');
INSERT INTO `sys_logininfor` VALUES (371, '1715925168', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '退出成功', '2020-09-17 13:43:55');
INSERT INTO `sys_logininfor` VALUES (372, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-17 13:43:57');
INSERT INTO `sys_logininfor` VALUES (373, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '退出成功', '2020-09-17 14:29:09');
INSERT INTO `sys_logininfor` VALUES (374, '1715925168', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-17 14:29:17');
INSERT INTO `sys_logininfor` VALUES (375, '1715925168', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '退出成功', '2020-09-17 14:32:32');
INSERT INTO `sys_logininfor` VALUES (376, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-17 14:32:34');
INSERT INTO `sys_logininfor` VALUES (377, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-17 16:11:50');
INSERT INTO `sys_logininfor` VALUES (378, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-17 16:40:46');
INSERT INTO `sys_logininfor` VALUES (379, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-17 17:10:09');
INSERT INTO `sys_logininfor` VALUES (380, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-22 10:25:57');
INSERT INTO `sys_logininfor` VALUES (381, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-22 10:43:05');
INSERT INTO `sys_logininfor` VALUES (382, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-22 10:44:20');
INSERT INTO `sys_logininfor` VALUES (383, '1715925168', '127.0.0.1', '内网IP', 'Unknown', 'Unknown', '0', '登录成功', '2020-09-22 12:43:21');
INSERT INTO `sys_logininfor` VALUES (384, '1715925168', '127.0.0.1', '内网IP', 'Unknown', 'Unknown', '0', '登录成功', '2020-09-22 12:44:27');
INSERT INTO `sys_logininfor` VALUES (385, '1715925168', '127.0.0.1', '内网IP', 'Unknown', 'Unknown', '0', '登录成功', '2020-09-22 12:47:44');
INSERT INTO `sys_logininfor` VALUES (386, '1715925168', '127.0.0.1', '内网IP', 'Unknown', 'Unknown', '0', '登录成功', '2020-09-22 12:48:16');
INSERT INTO `sys_logininfor` VALUES (387, '1715925168', '127.0.0.1', '内网IP', 'Unknown', 'Unknown', '0', '登录成功', '2020-09-22 13:12:33');
INSERT INTO `sys_logininfor` VALUES (388, '1715925168', '127.0.0.1', '内网IP', 'Unknown', 'Unknown', '0', '登录成功', '2020-09-22 14:14:57');
INSERT INTO `sys_logininfor` VALUES (389, '1715925168', '127.0.0.1', '内网IP', 'Unknown', 'Unknown', '0', '登录成功', '2020-09-22 15:10:11');
INSERT INTO `sys_logininfor` VALUES (390, '1715925168', '127.0.0.1', '内网IP', 'Unknown', 'Unknown', '0', '登录成功', '2020-09-22 15:27:27');
INSERT INTO `sys_logininfor` VALUES (391, '1715925168', '127.0.0.1', '内网IP', 'Unknown', 'Unknown', '0', '登录成功', '2020-09-22 15:28:14');
INSERT INTO `sys_logininfor` VALUES (392, '1715925168', '127.0.0.1', '内网IP', 'Unknown', 'Unknown', '0', '登录成功', '2020-09-22 15:28:38');
INSERT INTO `sys_logininfor` VALUES (393, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-22 15:50:03');
INSERT INTO `sys_logininfor` VALUES (394, '1715925168', '127.0.0.1', '内网IP', 'Unknown', 'Unknown', '0', '登录成功', '2020-09-22 15:55:46');
INSERT INTO `sys_logininfor` VALUES (395, '1715925168', '127.0.0.1', '内网IP', 'Unknown', 'Unknown', '0', '登录成功', '2020-09-22 15:57:45');
INSERT INTO `sys_logininfor` VALUES (396, '1715925168', '127.0.0.1', '内网IP', 'Unknown', 'Unknown', '0', '登录成功', '2020-09-22 16:02:07');
INSERT INTO `sys_logininfor` VALUES (397, '1715925168', '127.0.0.1', '内网IP', 'Unknown', 'Unknown', '0', '登录成功', '2020-09-22 16:02:41');
INSERT INTO `sys_logininfor` VALUES (398, '1715925168', '127.0.0.1', '内网IP', 'Unknown', 'Unknown', '0', '登录成功', '2020-09-22 16:05:42');
INSERT INTO `sys_logininfor` VALUES (399, '1715925168', '127.0.0.1', '内网IP', 'Unknown', 'Unknown', '0', '登录成功', '2020-09-22 16:07:12');
INSERT INTO `sys_logininfor` VALUES (400, '1715925168', '127.0.0.1', '内网IP', 'Unknown', 'Unknown', '0', '登录成功', '2020-09-22 16:13:33');
INSERT INTO `sys_logininfor` VALUES (401, '1715925168', '127.0.0.1', '内网IP', 'Unknown', 'Unknown', '0', '登录成功', '2020-09-22 16:20:43');
INSERT INTO `sys_logininfor` VALUES (402, '1715925168', '127.0.0.1', '内网IP', 'Unknown', 'Unknown', '0', '登录成功', '2020-09-22 17:00:27');
INSERT INTO `sys_logininfor` VALUES (403, '1715925168', '127.0.0.1', '内网IP', 'Unknown', 'Unknown', '0', '登录成功', '2020-09-22 17:01:37');
INSERT INTO `sys_logininfor` VALUES (404, '1715925168', '127.0.0.1', '内网IP', 'Unknown', 'Unknown', '0', '登录成功', '2020-09-22 17:03:02');
INSERT INTO `sys_logininfor` VALUES (405, '1715925168', '127.0.0.1', '内网IP', 'Unknown', 'Unknown', '0', '登录成功', '2020-09-22 17:03:36');
INSERT INTO `sys_logininfor` VALUES (406, '1715925168', '127.0.0.1', '内网IP', 'Unknown', 'Unknown', '0', '登录成功', '2020-09-22 17:04:53');
INSERT INTO `sys_logininfor` VALUES (407, '1715925168', '127.0.0.1', '内网IP', 'Unknown', 'Unknown', '0', '登录成功', '2020-09-22 17:05:35');
INSERT INTO `sys_logininfor` VALUES (408, '1715925168', '127.0.0.1', '内网IP', 'Unknown', 'Unknown', '0', '登录成功', '2020-09-22 17:06:09');
INSERT INTO `sys_logininfor` VALUES (409, '1715925168', '127.0.0.1', '内网IP', 'Unknown', 'Unknown', '0', '登录成功', '2020-09-22 17:06:43');
INSERT INTO `sys_logininfor` VALUES (410, '1715925168', '127.0.0.1', '内网IP', 'Unknown', 'Unknown', '0', '登录成功', '2020-09-22 17:06:43');
INSERT INTO `sys_logininfor` VALUES (411, '1715925168', '127.0.0.1', '内网IP', 'Unknown', 'Unknown', '0', '登录成功', '2020-09-22 17:07:04');
INSERT INTO `sys_logininfor` VALUES (412, '1715925168', '127.0.0.1', '内网IP', 'Unknown', 'Unknown', '0', '登录成功', '2020-09-22 17:07:18');
INSERT INTO `sys_logininfor` VALUES (413, '1715925168', '127.0.0.1', '内网IP', 'Unknown', 'Unknown', '0', '登录成功', '2020-09-22 17:08:39');
INSERT INTO `sys_logininfor` VALUES (414, '1715925168', '127.0.0.1', '内网IP', 'Unknown', 'Unknown', '0', '登录成功', '2020-09-22 17:12:18');
INSERT INTO `sys_logininfor` VALUES (415, '1715925168', '127.0.0.1', '内网IP', 'Unknown', 'Unknown', '0', '登录成功', '2020-09-22 17:12:48');
INSERT INTO `sys_logininfor` VALUES (416, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-22 17:18:04');
INSERT INTO `sys_logininfor` VALUES (417, '1715925168', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-22 17:18:30');
INSERT INTO `sys_logininfor` VALUES (418, '1715925168', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '1', '密码输入错误1次', '2020-09-22 17:19:26');
INSERT INTO `sys_logininfor` VALUES (419, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-22 17:25:45');
INSERT INTO `sys_logininfor` VALUES (420, '1715925168', '127.0.0.1', '内网IP', 'Mobile Safari', 'Mac OS X (iPhone)', '0', '登录成功', '2020-09-22 18:05:04');
INSERT INTO `sys_logininfor` VALUES (421, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-23 10:28:11');
INSERT INTO `sys_logininfor` VALUES (422, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-09-24 16:44:59');
INSERT INTO `sys_logininfor` VALUES (423, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-10-30 16:26:17');
INSERT INTO `sys_logininfor` VALUES (424, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-10-30 16:27:49');
INSERT INTO `sys_logininfor` VALUES (425, 'admin', '127.0.0.1', '内网IP', 'Safari', 'Mac OS X', '0', '登录成功', '2020-11-02 09:25:20');
INSERT INTO `sys_logininfor` VALUES (426, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Mac OS X', '0', '登录成功', '2020-11-02 11:40:46');
INSERT INTO `sys_logininfor` VALUES (427, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Mac OS X', '0', '登录成功', '2020-11-02 13:59:54');
COMMIT;

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu` (
  `menu_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '菜单ID',
  `menu_name` varchar(50) NOT NULL COMMENT '菜单名称',
  `parent_id` bigint(20) DEFAULT '0' COMMENT '父菜单ID',
  `order_num` int(4) DEFAULT '0' COMMENT '显示顺序',
  `url` varchar(200) DEFAULT '#' COMMENT '请求地址',
  `target` varchar(20) DEFAULT '' COMMENT '打开方式（menuItem页签 menuBlank新窗口）',
  `menu_type` char(1) DEFAULT '' COMMENT '菜单类型（M目录 C菜单 F按钮）',
  `visible` char(1) DEFAULT '0' COMMENT '菜单状态（0显示 1隐藏）',
  `perms` varchar(100) DEFAULT NULL COMMENT '权限标识',
  `icon` varchar(100) DEFAULT '#' COMMENT '菜单图标',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`menu_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1083 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='菜单权限表';

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
BEGIN;
INSERT INTO `sys_menu` VALUES (1, '系统管理', 0, 1, '#', '', 'M', '0', '', 'fa fa-gear', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '系统管理目录');
INSERT INTO `sys_menu` VALUES (2, '系统监控', 0, 2, '#', '', 'M', '0', '', 'fa fa-video-camera', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '系统监控目录');
INSERT INTO `sys_menu` VALUES (3, '系统工具', 0, 3, '#', '', 'M', '0', '', 'fa fa-bars', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '系统工具目录');
INSERT INTO `sys_menu` VALUES (100, '用户管理', 1, 1, '/system/user', '', 'C', '0', 'system:user:view', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '用户管理菜单');
INSERT INTO `sys_menu` VALUES (101, '角色管理', 1, 2, '/system/role', '', 'C', '0', 'system:role:view', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '角色管理菜单');
INSERT INTO `sys_menu` VALUES (102, '菜单管理', 1, 3, '/system/menu', '', 'C', '0', 'system:menu:view', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '菜单管理菜单');
INSERT INTO `sys_menu` VALUES (103, '部门管理', 1, 4, '/system/dept', '', 'C', '0', 'system:dept:view', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '部门管理菜单');
INSERT INTO `sys_menu` VALUES (104, '岗位管理', 1, 5, '/system/post', '', 'C', '0', 'system:post:view', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '岗位管理菜单');
INSERT INTO `sys_menu` VALUES (105, '字典管理', 1, 6, '/system/dict', '', 'C', '0', 'system:dict:view', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '字典管理菜单');
INSERT INTO `sys_menu` VALUES (106, '参数设置', 1, 7, '/system/config', '', 'C', '0', 'system:config:view', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '参数设置菜单');
INSERT INTO `sys_menu` VALUES (107, '通知公告', 1, 8, '/system/notice', '', 'C', '0', 'system:notice:view', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '通知公告菜单');
INSERT INTO `sys_menu` VALUES (108, '日志管理', 1, 9, '#', '', 'M', '0', '', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '日志管理菜单');
INSERT INTO `sys_menu` VALUES (109, '在线用户', 2, 1, '/monitor/online', '', 'C', '0', 'monitor:online:view', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '在线用户菜单');
INSERT INTO `sys_menu` VALUES (110, '定时任务', 2, 2, '/monitor/job', '', 'C', '0', 'monitor:job:view', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '定时任务菜单');
INSERT INTO `sys_menu` VALUES (111, '数据监控', 2, 3, '/monitor/data', '', 'C', '0', 'monitor:data:view', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '数据监控菜单');
INSERT INTO `sys_menu` VALUES (112, '服务监控', 2, 3, '/monitor/server', '', 'C', '0', 'monitor:server:view', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '服务监控菜单');
INSERT INTO `sys_menu` VALUES (113, '表单构建', 3, 1, '/tool/build', '', 'C', '0', 'tool:build:view', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '表单构建菜单');
INSERT INTO `sys_menu` VALUES (114, '代码生成', 3, 2, '/tool/gen', '', 'C', '0', 'tool:gen:view', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '代码生成菜单');
INSERT INTO `sys_menu` VALUES (115, '客户端接口', 3, 3, '/tool/swagger', 'menuItem', 'C', '0', 'tool:swagger:view', '#', 'admin', '2018-03-16 11:33:00', 'admin', '2020-09-16 09:26:55', '系统接口菜单');
INSERT INTO `sys_menu` VALUES (500, '操作日志', 108, 1, '/monitor/operlog', '', 'C', '0', 'monitor:operlog:view', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '操作日志菜单');
INSERT INTO `sys_menu` VALUES (501, '登录日志', 108, 2, '/monitor/logininfor', '', 'C', '0', 'monitor:logininfor:view', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '登录日志菜单');
INSERT INTO `sys_menu` VALUES (1000, '用户查询', 100, 1, '#', '', 'F', '0', 'system:user:list', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1001, '用户新增', 100, 2, '#', '', 'F', '0', 'system:user:add', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1002, '用户修改', 100, 3, '#', '', 'F', '0', 'system:user:edit', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1003, '用户删除', 100, 4, '#', '', 'F', '0', 'system:user:remove', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1004, '用户导出', 100, 5, '#', '', 'F', '0', 'system:user:export', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1005, '用户导入', 100, 6, '#', '', 'F', '0', 'system:user:import', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1006, '重置密码', 100, 7, '#', '', 'F', '0', 'system:user:resetPwd', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1007, '角色查询', 101, 1, '#', '', 'F', '0', 'system:role:list', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1008, '角色新增', 101, 2, '#', '', 'F', '0', 'system:role:add', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1009, '角色修改', 101, 3, '#', '', 'F', '0', 'system:role:edit', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1010, '角色删除', 101, 4, '#', '', 'F', '0', 'system:role:remove', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1011, '角色导出', 101, 5, '#', '', 'F', '0', 'system:role:export', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1012, '菜单查询', 102, 1, '#', '', 'F', '0', 'system:menu:list', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1013, '菜单新增', 102, 2, '#', '', 'F', '0', 'system:menu:add', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1014, '菜单修改', 102, 3, '#', '', 'F', '0', 'system:menu:edit', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1015, '菜单删除', 102, 4, '#', '', 'F', '0', 'system:menu:remove', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1016, '部门查询', 103, 1, '#', '', 'F', '0', 'system:dept:list', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1017, '部门新增', 103, 2, '#', '', 'F', '0', 'system:dept:add', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1018, '部门修改', 103, 3, '#', '', 'F', '0', 'system:dept:edit', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1019, '部门删除', 103, 4, '#', '', 'F', '0', 'system:dept:remove', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1020, '岗位查询', 104, 1, '#', '', 'F', '0', 'system:post:list', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1021, '岗位新增', 104, 2, '#', '', 'F', '0', 'system:post:add', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1022, '岗位修改', 104, 3, '#', '', 'F', '0', 'system:post:edit', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1023, '岗位删除', 104, 4, '#', '', 'F', '0', 'system:post:remove', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1024, '岗位导出', 104, 5, '#', '', 'F', '0', 'system:post:export', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1025, '字典查询', 105, 1, '#', '', 'F', '0', 'system:dict:list', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1026, '字典新增', 105, 2, '#', '', 'F', '0', 'system:dict:add', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1027, '字典修改', 105, 3, '#', '', 'F', '0', 'system:dict:edit', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1028, '字典删除', 105, 4, '#', '', 'F', '0', 'system:dict:remove', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1029, '字典导出', 105, 5, '#', '', 'F', '0', 'system:dict:export', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1030, '参数查询', 106, 1, '#', '', 'F', '0', 'system:config:list', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1031, '参数新增', 106, 2, '#', '', 'F', '0', 'system:config:add', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1032, '参数修改', 106, 3, '#', '', 'F', '0', 'system:config:edit', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1033, '参数删除', 106, 4, '#', '', 'F', '0', 'system:config:remove', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1034, '参数导出', 106, 5, '#', '', 'F', '0', 'system:config:export', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1035, '公告查询', 107, 1, '#', '', 'F', '0', 'system:notice:list', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1036, '公告新增', 107, 2, '#', '', 'F', '0', 'system:notice:add', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1037, '公告修改', 107, 3, '#', '', 'F', '0', 'system:notice:edit', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1038, '公告删除', 107, 4, '#', '', 'F', '0', 'system:notice:remove', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1039, '操作查询', 500, 1, '#', '', 'F', '0', 'monitor:operlog:list', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1040, '操作删除', 500, 2, '#', '', 'F', '0', 'monitor:operlog:remove', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1041, '详细信息', 500, 3, '#', '', 'F', '0', 'monitor:operlog:detail', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1042, '日志导出', 500, 4, '#', '', 'F', '0', 'monitor:operlog:export', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1043, '登录查询', 501, 1, '#', '', 'F', '0', 'monitor:logininfor:list', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1044, '登录删除', 501, 2, '#', '', 'F', '0', 'monitor:logininfor:remove', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1045, '日志导出', 501, 3, '#', '', 'F', '0', 'monitor:logininfor:export', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1046, '账户解锁', 501, 4, '#', '', 'F', '0', 'monitor:logininfor:unlock', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1047, '在线查询', 109, 1, '#', '', 'F', '0', 'monitor:online:list', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1048, '批量强退', 109, 2, '#', '', 'F', '0', 'monitor:online:batchForceLogout', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1049, '单条强退', 109, 3, '#', '', 'F', '0', 'monitor:online:forceLogout', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1050, '任务查询', 110, 1, '#', '', 'F', '0', 'monitor:job:list', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1051, '任务新增', 110, 2, '#', '', 'F', '0', 'monitor:job:add', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1052, '任务修改', 110, 3, '#', '', 'F', '0', 'monitor:job:edit', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1053, '任务删除', 110, 4, '#', '', 'F', '0', 'monitor:job:remove', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1054, '状态修改', 110, 5, '#', '', 'F', '0', 'monitor:job:changeStatus', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1055, '任务详细', 110, 6, '#', '', 'F', '0', 'monitor:job:detail', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1056, '任务导出', 110, 7, '#', '', 'F', '0', 'monitor:job:export', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1057, '生成查询', 114, 1, '#', '', 'F', '0', 'tool:gen:list', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1058, '生成修改', 114, 2, '#', '', 'F', '0', 'tool:gen:edit', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1059, '生成删除', 114, 3, '#', '', 'F', '0', 'tool:gen:remove', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1060, '预览代码', 114, 4, '#', '', 'F', '0', 'tool:gen:preview', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1061, '生成代码', 114, 5, '#', '', 'F', '0', 'tool:gen:code', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1064, '校园管理', 0, 4, '#', 'menuItem', 'M', '0', NULL, 'fa fa-university', 'admin', '2020-08-31 15:34:11', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1065, '校园用户管理', 1064, 1, '/school/schoolUser', 'menuItem', 'C', '0', 'school:schoolUser:view', '#', 'admin', '2020-08-31 15:44:26', 'admin', '2020-09-01 09:37:02', '');
INSERT INTO `sys_menu` VALUES (1066, '校园任务管理', 1064, 2, '/task/usertask', 'menuItem', 'C', '0', 'task:usertask:view', '#', 'admin', '2020-08-31 15:59:28', 'admin', '2020-08-31 17:18:18', '');
INSERT INTO `sys_menu` VALUES (1068, '校园任务审查', 1064, 3, 'task/usertask/check', 'menuItem', 'C', '0', 'task:usertask:view', '#', 'admin', '2020-08-31 17:58:01', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1070, '校园用户查看权限', 1065, 1, '#', 'menuItem', 'F', '0', 'school:schoolUser:list', '#', 'admin', '2020-09-17 13:47:19', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1071, '校园用户添加权限', 1065, 2, '#', 'menuItem', 'F', '0', 'school:schoolUser:add', '#', 'admin', '2020-09-17 13:47:55', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1072, '校园用户删除权限', 1065, 3, '#', 'menuItem', 'F', '0', 'school:schoolUser:remove', '#', 'admin', '2020-09-17 13:48:37', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1073, '校园用户修改权限', 1065, 4, '#', 'menuItem', 'F', '0', 'school:schoolUser:edit', '#', 'admin', '2020-09-17 13:49:17', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1074, '校园用户导出权限', 1065, 5, '#', 'menuItem', 'F', '0', 'school:schoolUser:export', '#', 'admin', '2020-09-17 13:50:02', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1075, '校园用户密码修改权限', 1065, 6, '#', 'menuItem', 'F', '0', 'school:schoolUser:resetPwd', '#', 'admin', '2020-09-17 13:50:47', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1076, '校园任务查看权限', 1066, 1, '#', 'menuItem', 'F', '0', 'task:usertask:list', '#', 'admin', '2020-09-17 13:52:13', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1077, '校园任务删除权限', 1066, 2, '#', 'menuItem', 'F', '0', 'task:usertask:remove', '#', 'admin', '2020-09-17 13:53:01', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1078, '校园用户添加权限', 1066, 3, '#', 'menuItem', 'F', '0', 'task:usertask:add', '#', 'admin', '2020-09-17 13:53:32', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1079, '校园任务导出权限', 1066, 4, '#', 'menuItem', 'F', '0', 'task:usertask:export', '#', 'admin', '2020-09-17 13:54:48', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1080, '校园任务审核权限', 1068, 1, '#', 'menuItem', 'F', '0', 'task:usertask:check', '#', 'admin', '2020-09-17 14:23:19', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1081, '校园任务导出权限', 1068, 2, '#', 'menuItem', 'F', '0', 'task:usertask:export', '#', 'admin', '2020-09-17 14:24:48', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1082, '校园任务删除权限', 1068, 3, '#', 'menuItem', 'F', '0', 'task:usertask:remove', '#', 'admin', '2020-09-17 14:25:20', '', NULL, '');
COMMIT;

-- ----------------------------
-- Table structure for sys_notice
-- ----------------------------
DROP TABLE IF EXISTS `sys_notice`;
CREATE TABLE `sys_notice` (
  `notice_id` int(4) NOT NULL AUTO_INCREMENT COMMENT '公告ID',
  `notice_title` varchar(50) NOT NULL COMMENT '公告标题',
  `notice_type` char(1) NOT NULL COMMENT '公告类型（1通知 2公告）',
  `notice_content` varchar(2000) DEFAULT NULL COMMENT '公告内容',
  `status` char(1) DEFAULT '0' COMMENT '公告状态（0正常 1关闭）',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`notice_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='通知公告表';

-- ----------------------------
-- Records of sys_notice
-- ----------------------------
BEGIN;
INSERT INTO `sys_notice` VALUES (1, '温馨提醒：2018-07-01 若依新版本发布啦', '2', '新版本内容', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '管理员');
INSERT INTO `sys_notice` VALUES (2, '维护通知：2018-07-01 若依系统凌晨维护', '1', '维护内容', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '管理员');
COMMIT;

-- ----------------------------
-- Table structure for sys_oper_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_oper_log`;
CREATE TABLE `sys_oper_log` (
  `oper_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '日志主键',
  `title` varchar(50) DEFAULT '' COMMENT '模块标题',
  `business_type` int(2) DEFAULT '0' COMMENT '业务类型（0其它 1新增 2修改 3删除）',
  `method` varchar(100) DEFAULT '' COMMENT '方法名称',
  `request_method` varchar(10) DEFAULT '' COMMENT '请求方式',
  `operator_type` int(1) DEFAULT '0' COMMENT '操作类别（0其它 1后台用户 2手机端用户）',
  `oper_name` varchar(50) DEFAULT '' COMMENT '操作人员',
  `dept_name` varchar(50) DEFAULT '' COMMENT '部门名称',
  `oper_url` varchar(255) DEFAULT '' COMMENT '请求URL',
  `oper_ip` varchar(50) DEFAULT '' COMMENT '主机地址',
  `oper_location` varchar(255) DEFAULT '' COMMENT '操作地点',
  `oper_param` varchar(2000) DEFAULT '' COMMENT '请求参数',
  `json_result` varchar(2000) DEFAULT '' COMMENT '返回参数',
  `status` int(1) DEFAULT '0' COMMENT '操作状态（0正常 1异常）',
  `error_msg` varchar(2000) DEFAULT '' COMMENT '错误消息',
  `oper_time` datetime DEFAULT NULL COMMENT '操作时间',
  PRIMARY KEY (`oper_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=300 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='操作日志记录';

-- ----------------------------
-- Records of sys_oper_log
-- ----------------------------
BEGIN;
INSERT INTO `sys_oper_log` VALUES (1, '代码生成', 6, 'com.eden.generator.controller.GenController.importTableSave()', 'POST', 1, 'admin', '研发部门', '/tool/gen/importTable', '127.0.0.1', '内网IP', '{\"tables\":[\"sys_menu\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-08-28 21:02:28');
INSERT INTO `sys_oper_log` VALUES (2, '用户管理', 2, 'com.eden.web.controller.system.SysUserController.changeStatus()', 'POST', 1, 'admin', '研发部门', '/system/user/changeStatus', '127.0.0.1', '内网IP', '{\"userId\":[\"1\"],\"status\":[\"1\"]}', 'null', 1, '不允许操作超级管理员用户', '2020-08-29 20:35:02');
INSERT INTO `sys_oper_log` VALUES (3, '代码生成', 3, 'com.eden.generator.controller.GenController.remove()', 'POST', 1, 'admin', '研发部门', '/tool/gen/remove', '127.0.0.1', '内网IP', '{\"ids\":[\"1\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-08-29 20:35:39');
INSERT INTO `sys_oper_log` VALUES (4, '部门管理', 2, 'com.eden.web.controller.system.SysDeptController.editSave()', 'POST', 1, 'admin', '建筑学院', '/system/dept/edit', '127.0.0.1', '内网IP', '{\"deptId\":[\"108\"],\"parentId\":[\"100\"],\"parentName\":[\"南阳理工学院\"],\"deptName\":[\"生物与化学工程学院\"],\"orderNum\":[\"1\"],\"leader\":[\"若依\"],\"phone\":[\"15888888888\"],\"email\":[\"ry@qq.com\"],\"status\":[\"0\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-08-29 20:57:51');
INSERT INTO `sys_oper_log` VALUES (5, '部门管理', 2, 'com.eden.web.controller.system.SysDeptController.editSave()', 'POST', 1, 'admin', '建筑学院', '/system/dept/edit', '127.0.0.1', '内网IP', '{\"deptId\":[\"101\"],\"parentId\":[\"100\"],\"parentName\":[\"南阳理工学院\"],\"deptName\":[\"智能制造学院\"],\"orderNum\":[\"0\"],\"leader\":[\"若依\"],\"phone\":[\"15888888888\"],\"email\":[\"ry@qq.com\"],\"status\":[\"0\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-08-29 20:58:10');
INSERT INTO `sys_oper_log` VALUES (6, '部门管理', 2, 'com.eden.web.controller.system.SysDeptController.editSave()', 'POST', 1, 'admin', '建筑学院', '/system/dept/edit', '127.0.0.1', '内网IP', '{\"deptId\":[\"103\"],\"parentId\":[\"100\"],\"parentName\":[\"南阳理工学院\"],\"deptName\":[\"建筑学院\"],\"orderNum\":[\"1\"],\"leader\":[\"若依\"],\"phone\":[\"15888888888\"],\"email\":[\"ry@qq.com\"],\"status\":[\"0\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-08-29 20:58:15');
INSERT INTO `sys_oper_log` VALUES (7, '部门管理', 2, 'com.eden.web.controller.system.SysDeptController.editSave()', 'POST', 1, 'admin', '建筑学院', '/system/dept/edit', '127.0.0.1', '内网IP', '{\"deptId\":[\"108\"],\"parentId\":[\"100\"],\"parentName\":[\"南阳理工学院\"],\"deptName\":[\"生物与化学工程学院\"],\"orderNum\":[\"2\"],\"leader\":[\"若依\"],\"phone\":[\"15888888888\"],\"email\":[\"ry@qq.com\"],\"status\":[\"0\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-08-29 20:58:25');
INSERT INTO `sys_oper_log` VALUES (8, '部门管理', 2, 'com.eden.web.controller.system.SysDeptController.editSave()', 'POST', 1, 'admin', '建筑学院', '/system/dept/edit', '127.0.0.1', '内网IP', '{\"deptId\":[\"102\"],\"parentId\":[\"100\"],\"parentName\":[\"南阳理工学院\"],\"deptName\":[\"计算机于软件学院\"],\"orderNum\":[\"3\"],\"leader\":[\"若依\"],\"phone\":[\"15888888888\"],\"email\":[\"ry@qq.com\"],\"status\":[\"0\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-08-29 20:58:34');
INSERT INTO `sys_oper_log` VALUES (9, '部门管理', 2, 'com.eden.web.controller.system.SysDeptController.editSave()', 'POST', 1, 'admin', '建筑学院', '/system/dept/edit', '127.0.0.1', '内网IP', '{\"deptId\":[\"108\"],\"parentId\":[\"100\"],\"parentName\":[\"南阳理工学院\"],\"deptName\":[\"生物与化学工程学院\"],\"orderNum\":[\"4\"],\"leader\":[\"若依\"],\"phone\":[\"15888888888\"],\"email\":[\"ry@qq.com\"],\"status\":[\"0\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-08-29 20:59:06');
INSERT INTO `sys_oper_log` VALUES (10, '部门管理', 3, 'com.eden.web.controller.system.SysDeptController.remove()', 'GET', 1, 'admin', '建筑学院', '/system/dept/remove/104', '127.0.0.1', '内网IP', NULL, '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-08-29 20:59:31');
INSERT INTO `sys_oper_log` VALUES (11, '部门管理', 3, 'com.eden.web.controller.system.SysDeptController.remove()', 'GET', 1, 'admin', '建筑学院', '/system/dept/remove/109', '127.0.0.1', '内网IP', NULL, '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-08-29 20:59:43');
INSERT INTO `sys_oper_log` VALUES (12, '部门管理', 2, 'com.eden.web.controller.system.SysDeptController.editSave()', 'POST', 1, 'admin', '建筑学院', '/system/dept/edit', '127.0.0.1', '内网IP', '{\"deptId\":[\"108\"],\"parentId\":[\"100\"],\"parentName\":[\"南阳理工学院\"],\"deptName\":[\"生物与化学工程学院\"],\"orderNum\":[\"2\"],\"leader\":[\"若依\"],\"phone\":[\"15888888888\"],\"email\":[\"ry@qq.com\"],\"status\":[\"0\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-08-29 20:59:55');
INSERT INTO `sys_oper_log` VALUES (13, '部门管理', 2, 'com.eden.web.controller.system.SysDeptController.editSave()', 'POST', 1, 'admin', '建筑学院', '/system/dept/edit', '127.0.0.1', '内网IP', '{\"deptId\":[\"105\"],\"parentId\":[\"100\"],\"parentName\":[\"南阳理工学院\"],\"deptName\":[\"数理学院\"],\"orderNum\":[\"5\"],\"leader\":[\"若依\"],\"phone\":[\"15888888888\"],\"email\":[\"ry@qq.com\"],\"status\":[\"0\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-08-29 21:00:26');
INSERT INTO `sys_oper_log` VALUES (14, '部门管理', 2, 'com.eden.web.controller.system.SysDeptController.editSave()', 'POST', 1, 'admin', '建筑学院', '/system/dept/edit', '127.0.0.1', '内网IP', '{\"deptId\":[\"106\"],\"parentId\":[\"100\"],\"parentName\":[\"南阳理工学院\"],\"deptName\":[\"教师教育学院\"],\"orderNum\":[\"6\"],\"leader\":[\"若依\"],\"phone\":[\"15888888888\"],\"email\":[\"ry@qq.com\"],\"status\":[\"0\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-08-29 21:00:50');
INSERT INTO `sys_oper_log` VALUES (15, '部门管理', 2, 'com.eden.web.controller.system.SysDeptController.editSave()', 'POST', 1, 'admin', '建筑学院', '/system/dept/edit', '127.0.0.1', '内网IP', '{\"deptId\":[\"107\"],\"parentId\":[\"100\"],\"parentName\":[\"南阳理工学院\"],\"deptName\":[\"外国语学院\"],\"orderNum\":[\"6\"],\"leader\":[\"若依\"],\"phone\":[\"15888888888\"],\"email\":[\"ry@qq.com\"],\"status\":[\"0\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-08-29 21:01:08');
INSERT INTO `sys_oper_log` VALUES (16, '部门管理', 2, 'com.eden.web.controller.system.SysDeptController.editSave()', 'POST', 1, 'admin', '建筑学院', '/system/dept/edit', '127.0.0.1', '内网IP', '{\"deptId\":[\"107\"],\"parentId\":[\"100\"],\"parentName\":[\"南阳理工学院\"],\"deptName\":[\"外国语学院\"],\"orderNum\":[\"4\"],\"leader\":[\"若依\"],\"phone\":[\"15888888888\"],\"email\":[\"ry@qq.com\"],\"status\":[\"0\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-08-29 21:01:48');
INSERT INTO `sys_oper_log` VALUES (17, '部门管理', 1, 'com.eden.web.controller.system.SysDeptController.addSave()', 'POST', 1, 'admin', '建筑学院', '/system/dept/add', '127.0.0.1', '内网IP', '{\"parentId\":[\"100\"],\"deptName\":[\"体育教学部\"],\"orderNum\":[\"8\"],\"leader\":[\"\"],\"phone\":[\"\"],\"email\":[\"\"],\"status\":[\"0\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-08-29 21:02:11');
INSERT INTO `sys_oper_log` VALUES (18, '部门管理', 2, 'com.eden.web.controller.system.SysDeptController.editSave()', 'POST', 1, 'admin', '建筑学院', '/system/dept/edit', '127.0.0.1', '内网IP', '{\"deptId\":[\"111\"],\"parentId\":[\"100\"],\"parentName\":[\"南阳理工学院\"],\"deptName\":[\"体育教学部\"],\"orderNum\":[\"7\"],\"leader\":[\"\"],\"phone\":[\"\"],\"email\":[\"\"],\"status\":[\"0\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-08-29 21:02:18');
INSERT INTO `sys_oper_log` VALUES (19, '部门管理', 1, 'com.eden.web.controller.system.SysDeptController.addSave()', 'POST', 1, 'admin', '建筑学院', '/system/dept/add', '127.0.0.1', '内网IP', '{\"parentId\":[\"100\"],\"deptName\":[\"信息工程学院\"],\"orderNum\":[\"8\"],\"leader\":[\"\"],\"phone\":[\"\"],\"email\":[\"\"],\"status\":[\"0\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-08-29 21:02:35');
INSERT INTO `sys_oper_log` VALUES (20, '部门管理', 1, 'com.eden.web.controller.system.SysDeptController.addSave()', 'POST', 1, 'admin', '建筑学院', '/system/dept/add', '127.0.0.1', '内网IP', '{\"parentId\":[\"100\"],\"deptName\":[\"数字媒体与艺术设计学院\"],\"orderNum\":[\"9\"],\"leader\":[\"\"],\"phone\":[\"\"],\"email\":[\"\"],\"status\":[\"0\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-08-29 21:02:45');
INSERT INTO `sys_oper_log` VALUES (21, '部门管理', 1, 'com.eden.web.controller.system.SysDeptController.addSave()', 'POST', 1, 'admin', '建筑学院', '/system/dept/add', '127.0.0.1', '内网IP', '{\"parentId\":[\"100\"],\"deptName\":[\"土木工程学院\"],\"orderNum\":[\"10\"],\"leader\":[\"\"],\"phone\":[\"\"],\"email\":[\"\"],\"status\":[\"0\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-08-29 21:02:58');
INSERT INTO `sys_oper_log` VALUES (22, '部门管理', 1, 'com.eden.web.controller.system.SysDeptController.addSave()', 'POST', 1, 'admin', '建筑学院', '/system/dept/add', '127.0.0.1', '内网IP', '{\"parentId\":[\"100\"],\"deptName\":[\"张仲景国医国药学院\"],\"orderNum\":[\"11\"],\"leader\":[\"\"],\"phone\":[\"\"],\"email\":[\"\"],\"status\":[\"0\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-08-29 21:03:11');
INSERT INTO `sys_oper_log` VALUES (23, '部门管理', 1, 'com.eden.web.controller.system.SysDeptController.addSave()', 'POST', 1, 'admin', '建筑学院', '/system/dept/add', '127.0.0.1', '内网IP', '{\"parentId\":[\"100\"],\"deptName\":[\"范蠡商学院\"],\"orderNum\":[\"12\"],\"leader\":[\"\"],\"phone\":[\"\"],\"email\":[\"\"],\"status\":[\"0\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-08-29 21:03:20');
INSERT INTO `sys_oper_log` VALUES (24, '部门管理', 1, 'com.eden.web.controller.system.SysDeptController.addSave()', 'POST', 1, 'admin', '建筑学院', '/system/dept/add', '127.0.0.1', '内网IP', '{\"parentId\":[\"100\"],\"deptName\":[\"传媒学院\"],\"orderNum\":[\"13\"],\"leader\":[\"\"],\"phone\":[\"\"],\"email\":[\"\"],\"status\":[\"0\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-08-29 21:03:29');
INSERT INTO `sys_oper_log` VALUES (25, '部门管理', 1, 'com.eden.web.controller.system.SysDeptController.addSave()', 'POST', 1, 'admin', '建筑学院', '/system/dept/add', '127.0.0.1', '内网IP', '{\"parentId\":[\"100\"],\"deptName\":[\"马克思主义学院\"],\"orderNum\":[\"14\"],\"leader\":[\"\"],\"phone\":[\"\"],\"email\":[\"\"],\"status\":[\"0\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-08-29 21:03:38');
INSERT INTO `sys_oper_log` VALUES (26, '部门管理', 1, 'com.eden.web.controller.system.SysDeptController.addSave()', 'POST', 1, 'admin', '建筑学院', '/system/dept/add', '127.0.0.1', '内网IP', '{\"parentId\":[\"101\"],\"deptName\":[\"机械设计制造及其自动化\"],\"orderNum\":[\"0\"],\"leader\":[\"\"],\"phone\":[\"\"],\"email\":[\"\"],\"status\":[\"0\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-08-29 21:05:02');
INSERT INTO `sys_oper_log` VALUES (27, '部门管理', 1, 'com.eden.web.controller.system.SysDeptController.addSave()', 'POST', 1, 'admin', '建筑学院', '/system/dept/add', '127.0.0.1', '内网IP', '{\"parentId\":[\"101\"],\"deptName\":[\"电气工程及其自动化\"],\"orderNum\":[\"1\"],\"leader\":[\"\"],\"phone\":[\"\"],\"email\":[\"\"],\"status\":[\"0\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-08-29 21:05:14');
INSERT INTO `sys_oper_log` VALUES (28, '部门管理', 1, 'com.eden.web.controller.system.SysDeptController.addSave()', 'POST', 1, 'admin', '建筑学院', '/system/dept/add', '127.0.0.1', '内网IP', '{\"parentId\":[\"101\"],\"deptName\":[\"测控技术与仪器\"],\"orderNum\":[\"2\"],\"leader\":[\"\"],\"phone\":[\"\"],\"email\":[\"\"],\"status\":[\"0\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-08-29 21:05:25');
INSERT INTO `sys_oper_log` VALUES (29, '部门管理', 1, 'com.eden.web.controller.system.SysDeptController.addSave()', 'POST', 1, 'admin', '建筑学院', '/system/dept/add', '127.0.0.1', '内网IP', '{\"parentId\":[\"101\"],\"deptName\":[\"自动化\"],\"orderNum\":[\"3\"],\"leader\":[\"\"],\"phone\":[\"\"],\"email\":[\"\"],\"status\":[\"0\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-08-29 21:05:40');
INSERT INTO `sys_oper_log` VALUES (30, '部门管理', 1, 'com.eden.web.controller.system.SysDeptController.addSave()', 'POST', 1, 'admin', '建筑学院', '/system/dept/add', '127.0.0.1', '内网IP', '{\"parentId\":[\"101\"],\"deptName\":[\"材料成型与控制工程\"],\"orderNum\":[\"5\"],\"leader\":[\"\"],\"phone\":[\"\"],\"email\":[\"\"],\"status\":[\"0\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-08-29 21:05:54');
INSERT INTO `sys_oper_log` VALUES (31, '部门管理', 2, 'com.eden.web.controller.system.SysDeptController.editSave()', 'POST', 1, 'admin', '建筑学院', '/system/dept/edit', '127.0.0.1', '内网IP', '{\"deptId\":[\"123\"],\"parentId\":[\"101\"],\"parentName\":[\"智能制造学院\"],\"deptName\":[\"材料成型与控制工程\"],\"orderNum\":[\"4\"],\"leader\":[\"\"],\"phone\":[\"\"],\"email\":[\"\"],\"status\":[\"0\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-08-29 21:06:05');
INSERT INTO `sys_oper_log` VALUES (32, '部门管理', 1, 'com.eden.web.controller.system.SysDeptController.addSave()', 'POST', 1, 'admin', '建筑学院', '/system/dept/add', '127.0.0.1', '内网IP', '{\"parentId\":[\"101\"],\"deptName\":[\"汽车服务工程\"],\"orderNum\":[\"5\"],\"leader\":[\"\"],\"phone\":[\"\"],\"email\":[\"\"],\"status\":[\"0\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-08-29 21:06:23');
INSERT INTO `sys_oper_log` VALUES (33, '部门管理', 1, 'com.eden.web.controller.system.SysDeptController.addSave()', 'POST', 1, 'admin', '建筑学院', '/system/dept/add', '127.0.0.1', '内网IP', '{\"parentId\":[\"101\"],\"deptName\":[\"机器人工程\"],\"orderNum\":[\"6\"],\"leader\":[\"\"],\"phone\":[\"\"],\"email\":[\"\"],\"status\":[\"0\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-08-29 21:06:34');
INSERT INTO `sys_oper_log` VALUES (34, '部门管理', 1, 'com.eden.web.controller.system.SysDeptController.addSave()', 'POST', 1, 'admin', '建筑学院', '/system/dept/add', '127.0.0.1', '内网IP', '{\"parentId\":[\"101\"],\"deptName\":[\"机械设计制造及其自动化（中外）\"],\"orderNum\":[\"6\"],\"leader\":[\"\"],\"phone\":[\"\"],\"email\":[\"\"],\"status\":[\"0\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-08-29 21:07:22');
INSERT INTO `sys_oper_log` VALUES (35, '部门管理', 2, 'com.eden.web.controller.system.SysDeptController.editSave()', 'POST', 1, 'admin', '建筑学院', '/system/dept/edit', '127.0.0.1', '内网IP', '{\"deptId\":[\"126\"],\"parentId\":[\"101\"],\"parentName\":[\"智能制造学院\"],\"deptName\":[\"机械设计制造及其自动化（中外）\"],\"orderNum\":[\"7\"],\"leader\":[\"\"],\"phone\":[\"\"],\"email\":[\"\"],\"status\":[\"0\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-08-29 21:07:31');
INSERT INTO `sys_oper_log` VALUES (36, '部门管理', 1, 'com.eden.web.controller.system.SysDeptController.addSave()', 'POST', 1, 'admin', '建筑学院', '/system/dept/add', '127.0.0.1', '内网IP', '{\"parentId\":[\"101\"],\"deptName\":[\"电气工程及其自动化（中外）\"],\"orderNum\":[\"8\"],\"leader\":[\"\"],\"phone\":[\"\"],\"email\":[\"\"],\"status\":[\"0\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-08-29 21:07:56');
INSERT INTO `sys_oper_log` VALUES (37, '部门管理', 1, 'com.eden.web.controller.system.SysDeptController.addSave()', 'POST', 1, 'admin', '建筑学院', '/system/dept/add', '127.0.0.1', '内网IP', '{\"parentId\":[\"102\"],\"deptName\":[\"计算机科学与技术\"],\"orderNum\":[\"1\"],\"leader\":[\"\"],\"phone\":[\"\"],\"email\":[\"\"],\"status\":[\"0\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-08-29 21:09:13');
INSERT INTO `sys_oper_log` VALUES (38, '部门管理', 1, 'com.eden.web.controller.system.SysDeptController.addSave()', 'POST', 1, 'admin', '建筑学院', '/system/dept/add', '127.0.0.1', '内网IP', '{\"parentId\":[\"102\"],\"deptName\":[\"数据科学与大数据技术\"],\"orderNum\":[\"2\"],\"leader\":[\"\"],\"phone\":[\"\"],\"email\":[\"\"],\"status\":[\"0\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-08-29 21:09:30');
INSERT INTO `sys_oper_log` VALUES (39, '部门管理', 1, 'com.eden.web.controller.system.SysDeptController.addSave()', 'POST', 1, 'admin', '建筑学院', '/system/dept/add', '127.0.0.1', '内网IP', '{\"parentId\":[\"102\"],\"deptName\":[\"软件工程\"],\"orderNum\":[\"3\"],\"leader\":[\"\"],\"phone\":[\"\"],\"email\":[\"\"],\"status\":[\"0\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-08-29 21:09:47');
INSERT INTO `sys_oper_log` VALUES (40, '部门管理', 1, 'com.eden.web.controller.system.SysDeptController.addSave()', 'POST', 1, 'admin', '建筑学院', '/system/dept/add', '127.0.0.1', '内网IP', '{\"parentId\":[\"130\"],\"deptName\":[\"移动设备应用开发\"],\"orderNum\":[\"1\"],\"leader\":[\"\"],\"phone\":[\"\"],\"email\":[\"\"],\"status\":[\"0\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-08-29 21:10:08');
INSERT INTO `sys_oper_log` VALUES (41, '部门管理', 1, 'com.eden.web.controller.system.SysDeptController.addSave()', 'POST', 1, 'admin', '建筑学院', '/system/dept/add', '127.0.0.1', '内网IP', '{\"parentId\":[\"130\"],\"deptName\":[\"云计算\"],\"orderNum\":[\"2\"],\"leader\":[\"\"],\"phone\":[\"\"],\"email\":[\"\"],\"status\":[\"0\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-08-29 21:10:24');
INSERT INTO `sys_oper_log` VALUES (42, '部门管理', 1, 'com.eden.web.controller.system.SysDeptController.addSave()', 'POST', 1, 'admin', '建筑学院', '/system/dept/add', '127.0.0.1', '内网IP', '{\"parentId\":[\"130\"],\"deptName\":[\"智能软件开发\"],\"orderNum\":[\"3\"],\"leader\":[\"\"],\"phone\":[\"\"],\"email\":[\"\"],\"status\":[\"0\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-08-29 21:10:39');
INSERT INTO `sys_oper_log` VALUES (43, '部门管理', 1, 'com.eden.web.controller.system.SysDeptController.addSave()', 'POST', 1, 'admin', '建筑学院', '/system/dept/add', '127.0.0.1', '内网IP', '{\"parentId\":[\"130\"],\"deptName\":[\"数据库技术\"],\"orderNum\":[\"4\"],\"leader\":[\"\"],\"phone\":[\"\"],\"email\":[\"\"],\"status\":[\"0\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-08-29 21:10:50');
INSERT INTO `sys_oper_log` VALUES (44, '部门管理', 1, 'com.eden.web.controller.system.SysDeptController.addSave()', 'POST', 1, 'admin', '建筑学院', '/system/dept/add', '127.0.0.1', '内网IP', '{\"parentId\":[\"130\"],\"deptName\":[\"游戏开发技术\"],\"orderNum\":[\"5\"],\"leader\":[\"\"],\"phone\":[\"\"],\"email\":[\"\"],\"status\":[\"0\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-08-29 21:11:00');
INSERT INTO `sys_oper_log` VALUES (45, '部门管理', 1, 'com.eden.web.controller.system.SysDeptController.addSave()', 'POST', 1, 'admin', '建筑学院', '/system/dept/add', '127.0.0.1', '内网IP', '{\"parentId\":[\"130\"],\"deptName\":[\"渗透与测试\"],\"orderNum\":[\"6\"],\"leader\":[\"\"],\"phone\":[\"\"],\"email\":[\"\"],\"status\":[\"0\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-08-29 21:11:09');
INSERT INTO `sys_oper_log` VALUES (46, '部门管理', 1, 'com.eden.web.controller.system.SysDeptController.addSave()', 'POST', 1, 'admin', '建筑学院', '/system/dept/add', '127.0.0.1', '内网IP', '{\"parentId\":[\"103\"],\"deptName\":[\"建筑学\"],\"orderNum\":[\"1\"],\"leader\":[\"\"],\"phone\":[\"\"],\"email\":[\"\"],\"status\":[\"0\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-08-29 21:12:33');
INSERT INTO `sys_oper_log` VALUES (47, '部门管理', 1, 'com.eden.web.controller.system.SysDeptController.addSave()', 'POST', 1, 'admin', '建筑学院', '/system/dept/add', '127.0.0.1', '内网IP', '{\"parentId\":[\"103\"],\"deptName\":[\"城乡规划\"],\"orderNum\":[\"2\"],\"leader\":[\"\"],\"phone\":[\"\"],\"email\":[\"\"],\"status\":[\"0\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-08-29 21:12:43');
INSERT INTO `sys_oper_log` VALUES (48, '部门管理', 1, 'com.eden.web.controller.system.SysDeptController.addSave()', 'POST', 1, 'admin', '建筑学院', '/system/dept/add', '127.0.0.1', '内网IP', '{\"parentId\":[\"103\"],\"deptName\":[\"历史建筑保护工程\"],\"orderNum\":[\"3\"],\"leader\":[\"\"],\"phone\":[\"\"],\"email\":[\"\"],\"status\":[\"0\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-08-29 21:12:51');
INSERT INTO `sys_oper_log` VALUES (49, '部门管理', 1, 'com.eden.web.controller.system.SysDeptController.addSave()', 'POST', 1, 'admin', '建筑学院', '/system/dept/add', '127.0.0.1', '内网IP', '{\"parentId\":[\"103\"],\"deptName\":[\"环境设计\"],\"orderNum\":[\"4\"],\"leader\":[\"\"],\"phone\":[\"\"],\"email\":[\"\"],\"status\":[\"0\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-08-29 21:12:59');
INSERT INTO `sys_oper_log` VALUES (50, '部门管理', 1, 'com.eden.web.controller.system.SysDeptController.addSave()', 'POST', 1, 'admin', '建筑学院', '/system/dept/add', '127.0.0.1', '内网IP', '{\"parentId\":[\"108\"],\"deptName\":[\"生物工程\"],\"orderNum\":[\"1\"],\"leader\":[\"\"],\"phone\":[\"\"],\"email\":[\"\"],\"status\":[\"0\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-08-29 21:13:43');
INSERT INTO `sys_oper_log` VALUES (51, '部门管理', 1, 'com.eden.web.controller.system.SysDeptController.addSave()', 'POST', 1, 'admin', '建筑学院', '/system/dept/add', '127.0.0.1', '内网IP', '{\"parentId\":[\"108\"],\"deptName\":[\"化学工程与工艺\"],\"orderNum\":[\"2\"],\"leader\":[\"\"],\"phone\":[\"\"],\"email\":[\"\"],\"status\":[\"0\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-08-29 21:13:59');
INSERT INTO `sys_oper_log` VALUES (52, '部门管理', 1, 'com.eden.web.controller.system.SysDeptController.addSave()', 'POST', 1, 'admin', '建筑学院', '/system/dept/add', '127.0.0.1', '内网IP', '{\"parentId\":[\"108\"],\"deptName\":[\"应用化学\"],\"orderNum\":[\"3\"],\"leader\":[\"\"],\"phone\":[\"\"],\"email\":[\"\"],\"status\":[\"0\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-08-29 21:14:15');
INSERT INTO `sys_oper_log` VALUES (53, '部门管理', 1, 'com.eden.web.controller.system.SysDeptController.addSave()', 'POST', 1, 'admin', '建筑学院', '/system/dept/add', '127.0.0.1', '内网IP', '{\"parentId\":[\"107\"],\"deptName\":[\"英语\"],\"orderNum\":[\"1\"],\"leader\":[\"\"],\"phone\":[\"\"],\"email\":[\"\"],\"status\":[\"0\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-08-29 21:14:55');
INSERT INTO `sys_oper_log` VALUES (54, '部门管理', 1, 'com.eden.web.controller.system.SysDeptController.addSave()', 'POST', 1, 'admin', '建筑学院', '/system/dept/add', '127.0.0.1', '内网IP', '{\"parentId\":[\"107\"],\"deptName\":[\"商务英语\"],\"orderNum\":[\"2\"],\"leader\":[\"\"],\"phone\":[\"\"],\"email\":[\"\"],\"status\":[\"0\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-08-29 21:15:10');
INSERT INTO `sys_oper_log` VALUES (55, '部门管理', 1, 'com.eden.web.controller.system.SysDeptController.addSave()', 'POST', 1, 'admin', '建筑学院', '/system/dept/add', '127.0.0.1', '内网IP', '{\"parentId\":[\"107\"],\"deptName\":[\"日语\"],\"orderNum\":[\"3\"],\"leader\":[\"\"],\"phone\":[\"\"],\"email\":[\"\"],\"status\":[\"0\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-08-29 21:15:24');
INSERT INTO `sys_oper_log` VALUES (56, '部门管理', 1, 'com.eden.web.controller.system.SysDeptController.addSave()', 'POST', 1, 'admin', '建筑学院', '/system/dept/add', '127.0.0.1', '内网IP', '{\"parentId\":[\"105\"],\"deptName\":[\"数学与应用数学\"],\"orderNum\":[\"1\"],\"leader\":[\"\"],\"phone\":[\"\"],\"email\":[\"\"],\"status\":[\"0\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-08-29 21:15:58');
INSERT INTO `sys_oper_log` VALUES (57, '部门管理', 1, 'com.eden.web.controller.system.SysDeptController.addSave()', 'POST', 1, 'admin', '建筑学院', '/system/dept/add', '127.0.0.1', '内网IP', '{\"parentId\":[\"105\"],\"deptName\":[\"应用统计学\"],\"orderNum\":[\"2\"],\"leader\":[\"\"],\"phone\":[\"\"],\"email\":[\"\"],\"status\":[\"0\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-08-29 21:16:11');
INSERT INTO `sys_oper_log` VALUES (58, '部门管理', 1, 'com.eden.web.controller.system.SysDeptController.addSave()', 'POST', 1, 'admin', '建筑学院', '/system/dept/add', '127.0.0.1', '内网IP', '{\"parentId\":[\"106\"],\"deptName\":[\"学前教育\"],\"orderNum\":[\"1\"],\"leader\":[\"\"],\"phone\":[\"\"],\"email\":[\"\"],\"status\":[\"0\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-08-29 21:16:48');
INSERT INTO `sys_oper_log` VALUES (59, '部门管理', 1, 'com.eden.web.controller.system.SysDeptController.addSave()', 'POST', 1, 'admin', '建筑学院', '/system/dept/add', '127.0.0.1', '内网IP', '{\"parentId\":[\"106\"],\"deptName\":[\"小学教育\"],\"orderNum\":[\"2\"],\"leader\":[\"\"],\"phone\":[\"\"],\"email\":[\"\"],\"status\":[\"0\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-08-29 21:16:59');
INSERT INTO `sys_oper_log` VALUES (60, '部门管理', 1, 'com.eden.web.controller.system.SysDeptController.addSave()', 'POST', 1, 'admin', '建筑学院', '/system/dept/add', '127.0.0.1', '内网IP', '{\"parentId\":[\"106\"],\"deptName\":[\"音乐学\"],\"orderNum\":[\"3\"],\"leader\":[\"\"],\"phone\":[\"\"],\"email\":[\"\"],\"status\":[\"0\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-08-29 21:17:11');
INSERT INTO `sys_oper_log` VALUES (61, '部门管理', 1, 'com.eden.web.controller.system.SysDeptController.addSave()', 'POST', 1, 'admin', '建筑学院', '/system/dept/add', '127.0.0.1', '内网IP', '{\"parentId\":[\"106\"],\"deptName\":[\"音乐表演\"],\"orderNum\":[\"4\"],\"leader\":[\"\"],\"phone\":[\"\"],\"email\":[\"\"],\"status\":[\"0\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-08-29 21:17:35');
INSERT INTO `sys_oper_log` VALUES (62, '部门管理', 3, 'com.eden.web.controller.system.SysDeptController.remove()', 'GET', 1, 'admin', '建筑学院', '/system/dept/remove/111', '127.0.0.1', '内网IP', NULL, '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-08-29 21:18:40');
INSERT INTO `sys_oper_log` VALUES (63, '部门管理', 3, 'com.eden.web.controller.system.SysDeptController.remove()', 'GET', 1, 'admin', '建筑学院', '/system/dept/remove/112', '127.0.0.1', '内网IP', NULL, '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-08-29 21:20:17');
INSERT INTO `sys_oper_log` VALUES (64, '部门管理', 1, 'com.eden.web.controller.system.SysDeptController.addSave()', 'POST', 1, 'admin', '建筑学院', '/system/dept/add', '127.0.0.1', '内网IP', '{\"parentId\":[\"113\"],\"deptName\":[\"数字媒体技术\"],\"orderNum\":[\"1\"],\"leader\":[\"\"],\"phone\":[\"\"],\"email\":[\"\"],\"status\":[\"0\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-08-29 21:20:58');
INSERT INTO `sys_oper_log` VALUES (65, '部门管理', 1, 'com.eden.web.controller.system.SysDeptController.addSave()', 'POST', 1, 'admin', '建筑学院', '/system/dept/add', '127.0.0.1', '内网IP', '{\"parentId\":[\"113\"],\"deptName\":[\"视觉传达设计\"],\"orderNum\":[\"2\"],\"leader\":[\"\"],\"phone\":[\"\"],\"email\":[\"\"],\"status\":[\"0\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-08-29 21:21:09');
INSERT INTO `sys_oper_log` VALUES (66, '部门管理', 1, 'com.eden.web.controller.system.SysDeptController.addSave()', 'POST', 1, 'admin', '建筑学院', '/system/dept/add', '127.0.0.1', '内网IP', '{\"parentId\":[\"113\"],\"deptName\":[\"动画\"],\"orderNum\":[\"3\"],\"leader\":[\"\"],\"phone\":[\"\"],\"email\":[\"\"],\"status\":[\"0\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-08-29 21:21:24');
INSERT INTO `sys_oper_log` VALUES (67, '部门管理', 1, 'com.eden.web.controller.system.SysDeptController.addSave()', 'POST', 1, 'admin', '建筑学院', '/system/dept/add', '127.0.0.1', '内网IP', '{\"parentId\":[\"113\"],\"deptName\":[\"数字媒体艺术\"],\"orderNum\":[\"4\"],\"leader\":[\"\"],\"phone\":[\"\"],\"email\":[\"\"],\"status\":[\"0\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-08-29 21:21:38');
INSERT INTO `sys_oper_log` VALUES (68, '部门管理', 1, 'com.eden.web.controller.system.SysDeptController.addSave()', 'POST', 1, 'admin', '建筑学院', '/system/dept/add', '127.0.0.1', '内网IP', '{\"parentId\":[\"114\"],\"deptName\":[\"土木工程\"],\"orderNum\":[\"1\"],\"leader\":[\"\"],\"phone\":[\"\"],\"email\":[\"\"],\"status\":[\"0\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-08-29 21:22:05');
INSERT INTO `sys_oper_log` VALUES (69, '部门管理', 1, 'com.eden.web.controller.system.SysDeptController.addSave()', 'POST', 1, 'admin', '建筑学院', '/system/dept/add', '127.0.0.1', '内网IP', '{\"parentId\":[\"157\"],\"deptName\":[\"建筑工程\"],\"orderNum\":[\"1\"],\"leader\":[\"\"],\"phone\":[\"\"],\"email\":[\"\"],\"status\":[\"0\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-08-29 21:22:15');
INSERT INTO `sys_oper_log` VALUES (70, '部门管理', 1, 'com.eden.web.controller.system.SysDeptController.addSave()', 'POST', 1, 'admin', '建筑学院', '/system/dept/add', '127.0.0.1', '内网IP', '{\"parentId\":[\"157\"],\"deptName\":[\"地下工程\"],\"orderNum\":[\"2\"],\"leader\":[\"\"],\"phone\":[\"\"],\"email\":[\"\"],\"status\":[\"0\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-08-29 21:22:24');
INSERT INTO `sys_oper_log` VALUES (71, '部门管理', 1, 'com.eden.web.controller.system.SysDeptController.addSave()', 'POST', 1, 'admin', '建筑学院', '/system/dept/add', '127.0.0.1', '内网IP', '{\"parentId\":[\"114\"],\"deptName\":[\"工程管理\"],\"orderNum\":[\"2\"],\"leader\":[\"\"],\"phone\":[\"\"],\"email\":[\"\"],\"status\":[\"0\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-08-29 21:22:36');
INSERT INTO `sys_oper_log` VALUES (72, '部门管理', 1, 'com.eden.web.controller.system.SysDeptController.addSave()', 'POST', 1, 'admin', '建筑学院', '/system/dept/add', '127.0.0.1', '内网IP', '{\"parentId\":[\"114\"],\"deptName\":[\"给排水科学与工程\"],\"orderNum\":[\"4\"],\"leader\":[\"\"],\"phone\":[\"\"],\"email\":[\"\"],\"status\":[\"0\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-08-29 21:22:59');
INSERT INTO `sys_oper_log` VALUES (73, '部门管理', 2, 'com.eden.web.controller.system.SysDeptController.editSave()', 'POST', 1, 'admin', '建筑学院', '/system/dept/edit', '127.0.0.1', '内网IP', '{\"deptId\":[\"161\"],\"parentId\":[\"114\"],\"parentName\":[\"土木工程学院\"],\"deptName\":[\"给排水科学与工程\"],\"orderNum\":[\"3\"],\"leader\":[\"\"],\"phone\":[\"\"],\"email\":[\"\"],\"status\":[\"0\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-08-29 21:23:09');
INSERT INTO `sys_oper_log` VALUES (74, '部门管理', 1, 'com.eden.web.controller.system.SysDeptController.addSave()', 'POST', 1, 'admin', '建筑学院', '/system/dept/add', '127.0.0.1', '内网IP', '{\"parentId\":[\"157\"],\"deptName\":[\"道路桥梁与渡河工程\"],\"orderNum\":[\"4\"],\"leader\":[\"\"],\"phone\":[\"\"],\"email\":[\"\"],\"status\":[\"0\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-08-29 21:23:26');
INSERT INTO `sys_oper_log` VALUES (75, '部门管理', 2, 'com.eden.web.controller.system.SysDeptController.editSave()', 'POST', 1, 'admin', '建筑学院', '/system/dept/edit', '127.0.0.1', '内网IP', '{\"deptId\":[\"162\"],\"parentId\":[\"114\"],\"parentName\":[\"土木工程学院\"],\"deptName\":[\"道路桥梁与渡河工程\"],\"orderNum\":[\"4\"],\"leader\":[\"\"],\"phone\":[\"\"],\"email\":[\"\"],\"status\":[\"0\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-08-29 21:23:56');
INSERT INTO `sys_oper_log` VALUES (76, '部门管理', 1, 'com.eden.web.controller.system.SysDeptController.addSave()', 'POST', 1, 'admin', '建筑学院', '/system/dept/add', '127.0.0.1', '内网IP', '{\"parentId\":[\"115\"],\"deptName\":[\"中医学\"],\"orderNum\":[\"1\"],\"leader\":[\"\"],\"phone\":[\"\"],\"email\":[\"\"],\"status\":[\"0\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-08-29 21:26:09');
INSERT INTO `sys_oper_log` VALUES (77, '部门管理', 1, 'com.eden.web.controller.system.SysDeptController.addSave()', 'POST', 1, 'admin', '建筑学院', '/system/dept/add', '127.0.0.1', '内网IP', '{\"parentId\":[\"115\"],\"deptName\":[\"中药学\"],\"orderNum\":[\"2\"],\"leader\":[\"\"],\"phone\":[\"\"],\"email\":[\"\"],\"status\":[\"0\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-08-29 21:26:19');
INSERT INTO `sys_oper_log` VALUES (78, '部门管理', 1, 'com.eden.web.controller.system.SysDeptController.addSave()', 'POST', 1, 'admin', '建筑学院', '/system/dept/add', '127.0.0.1', '内网IP', '{\"parentId\":[\"115\"],\"deptName\":[\"护理学\"],\"orderNum\":[\"3\"],\"leader\":[\"\"],\"phone\":[\"\"],\"email\":[\"\"],\"status\":[\"0\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-08-29 21:26:31');
INSERT INTO `sys_oper_log` VALUES (79, '部门管理', 1, 'com.eden.web.controller.system.SysDeptController.addSave()', 'POST', 1, 'admin', '建筑学院', '/system/dept/add', '127.0.0.1', '内网IP', '{\"parentId\":[\"115\"],\"deptName\":[\"食品科学与工程\"],\"orderNum\":[\"4\"],\"leader\":[\"\"],\"phone\":[\"\"],\"email\":[\"\"],\"status\":[\"0\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-08-29 21:26:44');
INSERT INTO `sys_oper_log` VALUES (80, '部门管理', 1, 'com.eden.web.controller.system.SysDeptController.addSave()', 'POST', 1, 'admin', '建筑学院', '/system/dept/add', '127.0.0.1', '内网IP', '{\"parentId\":[\"116\"],\"deptName\":[\"工商管理\"],\"orderNum\":[\"1\"],\"leader\":[\"\"],\"phone\":[\"\"],\"email\":[\"\"],\"status\":[\"0\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-08-29 21:27:12');
INSERT INTO `sys_oper_log` VALUES (81, '部门管理', 1, 'com.eden.web.controller.system.SysDeptController.addSave()', 'POST', 1, 'admin', '建筑学院', '/system/dept/add', '127.0.0.1', '内网IP', '{\"parentId\":[\"116\"],\"deptName\":[\"财务管理\"],\"orderNum\":[\"2\"],\"leader\":[\"\"],\"phone\":[\"\"],\"email\":[\"\"],\"status\":[\"0\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-08-29 21:27:20');
INSERT INTO `sys_oper_log` VALUES (82, '部门管理', 1, 'com.eden.web.controller.system.SysDeptController.addSave()', 'POST', 1, 'admin', '建筑学院', '/system/dept/add', '127.0.0.1', '内网IP', '{\"parentId\":[\"116\"],\"deptName\":[\"国际经济与贸易\"],\"orderNum\":[\"3\"],\"leader\":[\"\"],\"phone\":[\"\"],\"email\":[\"\"],\"status\":[\"0\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-08-29 21:27:31');
INSERT INTO `sys_oper_log` VALUES (83, '部门管理', 1, 'com.eden.web.controller.system.SysDeptController.addSave()', 'POST', 1, 'admin', '建筑学院', '/system/dept/add', '127.0.0.1', '内网IP', '{\"parentId\":[\"116\"],\"deptName\":[\"市场营销\"],\"orderNum\":[\"4\"],\"leader\":[\"\"],\"phone\":[\"\"],\"email\":[\"\"],\"status\":[\"0\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-08-29 21:27:43');
INSERT INTO `sys_oper_log` VALUES (84, '部门管理', 1, 'com.eden.web.controller.system.SysDeptController.addSave()', 'POST', 1, 'admin', '建筑学院', '/system/dept/add', '127.0.0.1', '内网IP', '{\"parentId\":[\"116\"],\"deptName\":[\"电子商务\"],\"orderNum\":[\"5\"],\"leader\":[\"\"],\"phone\":[\"\"],\"email\":[\"\"],\"status\":[\"0\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-08-29 21:27:53');
INSERT INTO `sys_oper_log` VALUES (85, '部门管理', 1, 'com.eden.web.controller.system.SysDeptController.addSave()', 'POST', 1, 'admin', '建筑学院', '/system/dept/add', '127.0.0.1', '内网IP', '{\"parentId\":[\"116\"],\"deptName\":[\"国际商务\"],\"orderNum\":[\"6\"],\"leader\":[\"\"],\"phone\":[\"\"],\"email\":[\"\"],\"status\":[\"0\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-08-29 21:28:04');
INSERT INTO `sys_oper_log` VALUES (86, '部门管理', 1, 'com.eden.web.controller.system.SysDeptController.addSave()', 'POST', 1, 'admin', '建筑学院', '/system/dept/add', '127.0.0.1', '内网IP', '{\"parentId\":[\"117\"],\"deptName\":[\"播音与主持艺术\"],\"orderNum\":[\"1\"],\"leader\":[\"\"],\"phone\":[\"\"],\"email\":[\"\"],\"status\":[\"0\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-08-29 21:28:35');
INSERT INTO `sys_oper_log` VALUES (87, '部门管理', 1, 'com.eden.web.controller.system.SysDeptController.addSave()', 'POST', 1, 'admin', '建筑学院', '/system/dept/add', '127.0.0.1', '内网IP', '{\"parentId\":[\"117\"],\"deptName\":[\"广播电视编导\"],\"orderNum\":[\"2\"],\"leader\":[\"\"],\"phone\":[\"\"],\"email\":[\"\"],\"status\":[\"0\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-08-29 21:28:45');
INSERT INTO `sys_oper_log` VALUES (88, '部门管理', 1, 'com.eden.web.controller.system.SysDeptController.addSave()', 'POST', 1, 'admin', '建筑学院', '/system/dept/add', '127.0.0.1', '内网IP', '{\"parentId\":[\"117\"],\"deptName\":[\"汉语言文学\"],\"orderNum\":[\"3\"],\"leader\":[\"\"],\"phone\":[\"\"],\"email\":[\"\"],\"status\":[\"0\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-08-29 21:28:59');
INSERT INTO `sys_oper_log` VALUES (89, '部门管理', 1, 'com.eden.web.controller.system.SysDeptController.addSave()', 'POST', 1, 'admin', '建筑学院', '/system/dept/add', '127.0.0.1', '内网IP', '{\"parentId\":[\"117\"],\"deptName\":[\"法学\"],\"orderNum\":[\"4\"],\"leader\":[\"\"],\"phone\":[\"\"],\"email\":[\"\"],\"status\":[\"0\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-08-29 21:29:07');
INSERT INTO `sys_oper_log` VALUES (90, '部门管理', 3, 'com.eden.web.controller.system.SysDeptController.remove()', 'GET', 1, 'admin', '建筑学院', '/system/dept/remove/118', '127.0.0.1', '内网IP', NULL, '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-08-29 21:29:31');
INSERT INTO `sys_oper_log` VALUES (91, '用户管理', 1, 'com.eden.web.controller.system.SysUserController.addSave()', 'POST', 1, 'admin', '建筑学院', '/system/user/add', '127.0.0.1', '内网IP', '{\"deptId\":[\"129\"],\"userName\":[\"焦洋\"],\"deptName\":[\"数据科学与大数据技术\"],\"phonenumber\":[\"17761628992\"],\"email\":[\"838855804@qq.com\"],\"loginName\":[\"1715925168\"],\"sex\":[\"0\"],\"remark\":[\"\"],\"status\":[\"0\"],\"roleIds\":[\"\"],\"postIds\":[\"\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-08-29 21:33:26');
INSERT INTO `sys_oper_log` VALUES (92, '角色管理', 1, 'com.eden.web.controller.system.SysRoleController.addSave()', 'POST', 1, 'admin', '建筑学院', '/system/role/add', '127.0.0.1', '内网IP', '{\"roleName\":[\"系统管理员\"],\"roleKey\":[\"systemadmin\"],\"roleSort\":[\"3\"],\"status\":[\"0\"],\"remark\":[\"系统管理员\"],\"menuIds\":[\"1,100,1000,1001,1002,1003,1004,1005,1006,101,1007,1008,1009,1010,1011,102,1012,1013,1014,1015,103,1016,1017,1018,1019,104,1020,1021,1022,1023,1024,105,1025,1026,1027,1028,1029,106,1030,1031,1032,1033,1034,107,1035,1036,1037,1038,108,500,1039,1040,1041,1042,501,1043,1044,1045,1046,2,109,1047,1048,1049,110,1050,1051,1052,1053,1054,1055,1056,111,112,3,113,114,1057,1058,1059,1060,1061,115\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-08-29 23:00:20');
INSERT INTO `sys_oper_log` VALUES (93, '角色管理', 2, 'com.eden.web.controller.system.SysRoleController.editSave()', 'POST', 1, 'admin', '建筑学院', '/system/role/edit', '127.0.0.1', '内网IP', '{\"roleId\":[\"3\"],\"roleName\":[\"系统管理员\"],\"roleKey\":[\"systemadmin\"],\"roleSort\":[\"3\"],\"status\":[\"0\"],\"remark\":[\"系统管理员\"],\"menuIds\":[\"2,109,1047,1048,1049,110,1050,1051,1052,1053,1054,1055,1056,111,112\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-08-29 23:02:37');
INSERT INTO `sys_oper_log` VALUES (94, '菜单管理', 3, 'com.eden.web.controller.system.SysMenuController.remove()', 'GET', 1, 'admin', '建筑学院', '/system/menu/remove/4', '127.0.0.1', '内网IP', NULL, '{\r\n  \"msg\" : \"菜单已分配,不允许删除\",\r\n  \"code\" : 301\r\n}', 0, NULL, '2020-08-29 23:04:54');
INSERT INTO `sys_oper_log` VALUES (95, '角色管理', 2, 'com.eden.web.controller.system.SysRoleController.editSave()', 'POST', 1, 'admin', '建筑学院', '/system/role/edit', '127.0.0.1', '内网IP', '{\"roleId\":[\"2\"],\"roleName\":[\"普通角色\"],\"roleKey\":[\"common\"],\"roleSort\":[\"2\"],\"status\":[\"0\"],\"remark\":[\"普通角色\"],\"menuIds\":[\"1,100,1000,1001,1002,1003,1004,1005,1006,101,1007,1008,1009,1010,1011,102,1012,1013,1014,1015,103,1016,1017,1018,1019,104,1020,1021,1022,1023,1024,105,1025,1026,1027,1028,1029,106,1030,1031,1032,1033,1034,107,1035,1036,1037,1038,108,500,1039,1040,1041,1042,501,1043,1044,1045,1046,2,109,1047,1048,1049,110,1050,1051,1052,1053,1054,1055,1056,111,112,3,113,114,1057,1058,1059,1060,1061,115\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-08-29 23:05:42');
INSERT INTO `sys_oper_log` VALUES (96, '菜单管理', 3, 'com.eden.web.controller.system.SysMenuController.remove()', 'GET', 1, 'admin', '建筑学院', '/system/menu/remove/4', '127.0.0.1', '内网IP', NULL, '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-08-29 23:05:48');
INSERT INTO `sys_oper_log` VALUES (97, '个人信息', 2, 'com.eden.web.controller.system.SysProfileController.update()', 'POST', 1, 'admin', '建筑学院', '/system/user/profile/update', '127.0.0.1', '内网IP', '{\"id\":[\"\"],\"userName\":[\"起源\"],\"phonenumber\":[\"17761628991\"],\"email\":[\"eden@163.com\"],\"sex\":[\"1\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-08-29 23:46:08');
INSERT INTO `sys_oper_log` VALUES (98, '用户管理', 2, 'com.eden.web.controller.system.SysUserController.editSave()', 'POST', 1, 'admin', '建筑学院', '/system/user/edit', '127.0.0.1', '内网IP', '{\"userId\":[\"1\"],\"deptId\":[\"103\"],\"userName\":[\"起源\"],\"dept.deptName\":[\"建筑学院\"],\"phonenumber\":[\"17761628991\"],\"email\":[\"eden@163.com\"],\"loginName\":[\"admin\"],\"sex\":[\"1\"],\"role\":[\"1\"],\"remark\":[\"管理员\"],\"status\":[\"0\"],\"roleIds\":[\"1\"],\"postIds\":[\"\"]}', 'null', 1, '不允许操作超级管理员用户', '2020-08-29 23:46:40');
INSERT INTO `sys_oper_log` VALUES (99, '角色管理', 1, 'com.eden.web.controller.system.SysRoleController.addSave()', 'POST', 1, 'admin', '建筑学院', '/system/role/add', '127.0.0.1', '内网IP', '{\"roleName\":[\"校长\"],\"roleKey\":[\"rector\"],\"roleSort\":[\"4\"],\"status\":[\"0\"],\"remark\":[\"\"],\"menuIds\":[\"1,100,1000,1001,1002,1003,1004,1005,1006,101,1007,1008,1009,1010,1011,102,1012,1013,1014,1015,103,1016,1017,1018,1019,104,1020,1021,1022,1023,1024\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-08-30 14:08:55');
INSERT INTO `sys_oper_log` VALUES (100, '角色管理', 1, 'com.eden.web.controller.system.SysRoleController.addSave()', 'POST', 1, 'admin', '建筑学院', '/system/role/add', '127.0.0.1', '内网IP', '{\"roleName\":[\"院长\"],\"roleKey\":[\"dean\"],\"roleSort\":[\"5\"],\"status\":[\"0\"],\"remark\":[\"\"],\"menuIds\":[\"1,100,1000,1001,1002,1003,1004,1005,1006,101,1007,1008,1009,1010,1011,103,1016,1017,1018,1019,104,1020,1021,1022,1023,1024\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-08-30 14:09:38');
INSERT INTO `sys_oper_log` VALUES (101, '角色管理', 1, 'com.eden.web.controller.system.SysRoleController.addSave()', 'POST', 1, 'admin', '建筑学院', '/system/role/add', '127.0.0.1', '内网IP', '{\"roleName\":[\"辅导员\"],\"roleKey\":[\"instructor\"],\"roleSort\":[\"6\"],\"status\":[\"0\"],\"remark\":[\"\"],\"menuIds\":[\"1,100,1000,1001,1002,1003,1004,1005,1006,101,1007,1008,1009,1010,1011\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-08-30 14:10:21');
INSERT INTO `sys_oper_log` VALUES (102, '角色管理', 1, 'com.eden.web.controller.system.SysRoleController.addSave()', 'POST', 1, 'admin', '建筑学院', '/system/role/add', '127.0.0.1', '内网IP', '{\"roleName\":[\"学生会主席\"],\"roleKey\":[\"studentleader\"],\"roleSort\":[\"7\"],\"status\":[\"0\"],\"remark\":[\"\"],\"menuIds\":[\"1,100,1000,1001,1002,1003,1004,1005,1006\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-08-30 14:11:11');
INSERT INTO `sys_oper_log` VALUES (103, '角色管理', 1, 'com.eden.web.controller.system.SysRoleController.addSave()', 'POST', 1, 'admin', '建筑学院', '/system/role/add', '127.0.0.1', '内网IP', '{\"roleName\":[\"学生会人员\"],\"roleKey\":[\"studentunionstaff\"],\"roleSort\":[\"8\"],\"status\":[\"0\"],\"remark\":[\"\"],\"menuIds\":[\"1,100,1000\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-08-30 14:14:16');
INSERT INTO `sys_oper_log` VALUES (104, '角色管理', 1, 'com.eden.web.controller.system.SysRoleController.addSave()', 'POST', 1, 'admin', '建筑学院', '/system/role/add', '127.0.0.1', '内网IP', '{\"roleName\":[\"学生\"],\"roleKey\":[\"student\"],\"roleSort\":[\"9\"],\"status\":[\"0\"],\"remark\":[\"\"],\"menuIds\":[\"\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-08-30 17:04:22');
INSERT INTO `sys_oper_log` VALUES (105, '角色管理', 1, 'com.eden.web.controller.system.SysRoleController.addSave()', 'POST', 1, 'admin', '建筑学院', '/system/role/add', '127.0.0.1', '内网IP', '{\"roleName\":[\"教师\"],\"roleKey\":[\"teacher \"],\"roleSort\":[\"10\"],\"status\":[\"0\"],\"remark\":[\"\"],\"menuIds\":[\"\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-08-30 17:05:01');
INSERT INTO `sys_oper_log` VALUES (106, '岗位管理', 2, 'com.eden.web.controller.system.SysPostController.editSave()', 'POST', 1, 'admin', '建筑学院', '/system/post/edit', '127.0.0.1', '内网IP', '{\"postId\":[\"1\"],\"postName\":[\"校长\"],\"postCode\":[\"rector\"],\"postSort\":[\"1\"],\"status\":[\"0\"],\"remark\":[\"\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-08-30 17:08:37');
INSERT INTO `sys_oper_log` VALUES (107, '岗位管理', 2, 'com.eden.web.controller.system.SysPostController.editSave()', 'POST', 1, 'admin', '建筑学院', '/system/post/edit', '127.0.0.1', '内网IP', '{\"postId\":[\"2\"],\"postName\":[\"院长\"],\"postCode\":[\"dean\"],\"postSort\":[\"2\"],\"status\":[\"0\"],\"remark\":[\"\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-08-30 17:09:21');
INSERT INTO `sys_oper_log` VALUES (108, '岗位管理', 2, 'com.eden.web.controller.system.SysPostController.editSave()', 'POST', 1, 'admin', '建筑学院', '/system/post/edit', '127.0.0.1', '内网IP', '{\"postId\":[\"3\"],\"postName\":[\"教师\"],\"postCode\":[\"teacher\"],\"postSort\":[\"3\"],\"status\":[\"0\"],\"remark\":[\"\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-08-30 17:10:08');
INSERT INTO `sys_oper_log` VALUES (109, '岗位管理', 2, 'com.eden.web.controller.system.SysPostController.editSave()', 'POST', 1, 'admin', '建筑学院', '/system/post/edit', '127.0.0.1', '内网IP', '{\"postId\":[\"4\"],\"postName\":[\"学生\"],\"postCode\":[\"student\"],\"postSort\":[\"4\"],\"status\":[\"0\"],\"remark\":[\"\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-08-30 17:10:37');
INSERT INTO `sys_oper_log` VALUES (110, '角色管理', 3, 'com.eden.web.controller.system.SysRoleController.remove()', 'POST', 1, 'admin', '建筑学院', '/system/role/remove', '127.0.0.1', '内网IP', '{\"ids\":[\"10\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-08-30 17:12:33');
INSERT INTO `sys_oper_log` VALUES (111, '角色管理', 3, 'com.eden.web.controller.system.SysRoleController.remove()', 'POST', 1, 'admin', '建筑学院', '/system/role/remove', '127.0.0.1', '内网IP', '{\"ids\":[\"9\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-08-30 17:12:36');
INSERT INTO `sys_oper_log` VALUES (112, '代码生成', 6, 'com.eden.generator.controller.GenController.importTableSave()', 'POST', 1, 'admin', '建筑学院', '/tool/gen/importTable', '127.0.0.1', '内网IP', '{\"tables\":[\"user_task,task_picture,user_task_picture\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-08-30 19:14:38');
INSERT INTO `sys_oper_log` VALUES (113, '代码生成', 8, 'com.eden.generator.controller.GenController.download()', 'GET', 1, 'admin', '建筑学院', '/tool/gen/download/user_task', '127.0.0.1', '内网IP', NULL, 'null', 0, NULL, '2020-08-30 19:22:00');
INSERT INTO `sys_oper_log` VALUES (114, '菜单管理', 1, 'com.eden.web.controller.system.SysMenuController.addSave()', 'POST', 1, 'admin', '建筑学院', '/system/menu/add', '127.0.0.1', '内网IP', '{\"parentId\":[\"0\"],\"menuType\":[\"M\"],\"menuName\":[\"用户管理\"],\"url\":[\"\"],\"target\":[\"menuItem\"],\"perms\":[\"\"],\"orderNum\":[\"4\"],\"icon\":[\"fa fa-user\"],\"visible\":[\"0\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-08-31 10:22:44');
INSERT INTO `sys_oper_log` VALUES (115, '菜单管理', 1, 'com.eden.web.controller.system.SysMenuController.addSave()', 'POST', 1, 'admin', '建筑学院', '/system/menu/add', '127.0.0.1', '内网IP', '{\"parentId\":[\"0\"],\"menuType\":[\"M\"],\"menuName\":[\"任务管理\"],\"url\":[\"\"],\"target\":[\"menuItem\"],\"perms\":[\"\"],\"orderNum\":[\"5\"],\"icon\":[\"fa fa-calendar-check-o\"],\"visible\":[\"0\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-08-31 10:58:41');
INSERT INTO `sys_oper_log` VALUES (116, '角色管理', 3, 'com.eden.web.controller.system.SysRoleController.remove()', 'POST', 1, 'admin', '建筑学院', '/system/role/remove', '127.0.0.1', '内网IP', '{\"ids\":[\"2\"]}', '{\r\n  \"msg\" : \"普通角色已分配,不能删除\",\r\n  \"code\" : 500\r\n}', 0, NULL, '2020-08-31 11:03:37');
INSERT INTO `sys_oper_log` VALUES (117, '用户管理', 3, 'com.eden.web.controller.system.SysUserController.remove()', 'POST', 1, 'admin', '建筑学院', '/system/user/remove', '127.0.0.1', '内网IP', '{\"ids\":[\"2\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-08-31 11:07:45');
INSERT INTO `sys_oper_log` VALUES (118, '角色管理', 3, 'com.eden.web.controller.system.SysRoleController.remove()', 'POST', 1, 'admin', '建筑学院', '/system/role/remove', '127.0.0.1', '内网IP', '{\"ids\":[\"2\"]}', '{\r\n  \"msg\" : \"普通角色已分配,不能删除\",\r\n  \"code\" : 500\r\n}', 0, NULL, '2020-08-31 11:07:57');
INSERT INTO `sys_oper_log` VALUES (119, '菜单管理', 3, 'com.eden.web.controller.system.SysMenuController.remove()', 'GET', 1, 'admin', '建筑学院', '/system/menu/remove/1062', '127.0.0.1', '内网IP', NULL, '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-08-31 15:33:11');
INSERT INTO `sys_oper_log` VALUES (120, '菜单管理', 3, 'com.eden.web.controller.system.SysMenuController.remove()', 'GET', 1, 'admin', '建筑学院', '/system/menu/remove/1063', '127.0.0.1', '内网IP', NULL, '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-08-31 15:33:15');
INSERT INTO `sys_oper_log` VALUES (121, '菜单管理', 1, 'com.eden.web.controller.system.SysMenuController.addSave()', 'POST', 1, 'admin', '建筑学院', '/system/menu/add', '127.0.0.1', '内网IP', '{\"parentId\":[\"0\"],\"menuType\":[\"M\"],\"menuName\":[\"校园管理\"],\"url\":[\"\"],\"target\":[\"menuItem\"],\"perms\":[\"\"],\"orderNum\":[\"4\"],\"icon\":[\"fa fa-university\"],\"visible\":[\"0\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-08-31 15:34:11');
INSERT INTO `sys_oper_log` VALUES (122, '菜单管理', 1, 'com.eden.web.controller.system.SysMenuController.addSave()', 'POST', 1, 'admin', '建筑学院', '/system/menu/add', '127.0.0.1', '内网IP', '{\"parentId\":[\"1064\"],\"menuType\":[\"C\"],\"menuName\":[\"用户管理\"],\"url\":[\"/system/user/schoolUser\"],\"target\":[\"menuItem\"],\"perms\":[\"\"],\"orderNum\":[\"1\"],\"icon\":[\"\"],\"visible\":[\"0\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-08-31 15:44:26');
INSERT INTO `sys_oper_log` VALUES (123, '菜单管理', 2, 'com.eden.web.controller.system.SysMenuController.editSave()', 'POST', 1, 'admin', '建筑学院', '/system/menu/edit', '127.0.0.1', '内网IP', '{\"menuId\":[\"1065\"],\"parentId\":[\"1064\"],\"menuType\":[\"C\"],\"menuName\":[\"用户管理\"],\"url\":[\"/system/user/schoolUser\"],\"target\":[\"menuItem\"],\"perms\":[\"system:user:view\"],\"orderNum\":[\"1\"],\"icon\":[\"#\"],\"visible\":[\"0\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-08-31 15:45:08');
INSERT INTO `sys_oper_log` VALUES (124, '菜单管理', 2, 'com.eden.web.controller.system.SysMenuController.editSave()', 'POST', 1, 'admin', '建筑学院', '/system/menu/edit', '127.0.0.1', '内网IP', '{\"menuId\":[\"1065\"],\"parentId\":[\"1064\"],\"menuType\":[\"C\"],\"menuName\":[\"校园用户管理\"],\"url\":[\"/system/user/schoolUser\"],\"target\":[\"menuItem\"],\"perms\":[\"system:user:view\"],\"orderNum\":[\"1\"],\"icon\":[\"#\"],\"visible\":[\"0\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-08-31 15:58:04');
INSERT INTO `sys_oper_log` VALUES (125, '菜单管理', 1, 'com.eden.web.controller.system.SysMenuController.addSave()', 'POST', 1, 'admin', '建筑学院', '/system/menu/add', '127.0.0.1', '内网IP', '{\"parentId\":[\"1064\"],\"menuType\":[\"C\"],\"menuName\":[\"任务管理\"],\"url\":[\"\"],\"target\":[\"menuItem\"],\"perms\":[\"\"],\"orderNum\":[\"2\"],\"icon\":[\"\"],\"visible\":[\"0\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-08-31 15:59:28');
INSERT INTO `sys_oper_log` VALUES (126, '菜单管理', 2, 'com.eden.web.controller.system.SysMenuController.editSave()', 'POST', 1, 'admin', '建筑学院', '/system/menu/edit', '127.0.0.1', '内网IP', '{\"menuId\":[\"1066\"],\"parentId\":[\"1064\"],\"menuType\":[\"C\"],\"menuName\":[\"校园任务管理\"],\"url\":[\"#\"],\"target\":[\"menuItem\"],\"perms\":[\"\"],\"orderNum\":[\"2\"],\"icon\":[\"#\"],\"visible\":[\"0\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-08-31 16:10:54');
INSERT INTO `sys_oper_log` VALUES (127, '代码生成', 2, 'com.eden.generator.controller.GenController.editSave()', 'POST', 1, 'admin', '建筑学院', '/tool/gen/edit', '127.0.0.1', '内网IP', '{\"tableId\":[\"3\"],\"tableName\":[\"user_task\"],\"tableComment\":[\"用户发布任务表\"],\"className\":[\"UserTask\"],\"functionAuthor\":[\"jiaoyang\"],\"remark\":[\"\"],\"columns[0].columnId\":[\"18\"],\"columns[0].sort\":[\"1\"],\"columns[0].columnComment\":[\"任务id\"],\"columns[0].javaType\":[\"String\"],\"columns[0].javaField\":[\"taskId\"],\"columns[0].isInsert\":[\"1\"],\"columns[0].queryType\":[\"EQ\"],\"columns[0].htmlType\":[\"input\"],\"columns[0].dictType\":[\"\"],\"columns[1].columnId\":[\"19\"],\"columns[1].sort\":[\"2\"],\"columns[1].columnComment\":[\"任务名称\"],\"columns[1].javaType\":[\"String\"],\"columns[1].javaField\":[\"taskName\"],\"columns[1].isInsert\":[\"1\"],\"columns[1].isEdit\":[\"1\"],\"columns[1].isList\":[\"1\"],\"columns[1].isQuery\":[\"1\"],\"columns[1].queryType\":[\"LIKE\"],\"columns[1].htmlType\":[\"input\"],\"columns[1].dictType\":[\"\"],\"columns[2].columnId\":[\"20\"],\"columns[2].sort\":[\"3\"],\"columns[2].columnComment\":[\"任务描述\"],\"columns[2].javaType\":[\"String\"],\"columns[2].javaField\":[\"taskDescription\"],\"columns[2].isInsert\":[\"1\"],\"columns[2].isEdit\":[\"1\"],\"columns[2].isList\":[\"1\"],\"columns[2].isQuery\":[\"1\"],\"columns[2].queryType\":[\"EQ\"],\"columns[2].htmlType\":[\"input\"],\"columns[2].dictType\":[\"\"],\"columns[3].columnId\":[\"21\"],\"columns[3].sort\":[\"4\"],\"columns[3].columnComment\":[\"任务状态 0：初始状态 1：待审核 2：审核通过 3：审核不通过 4：发布成功 5：发布失败\"],\"columns[3].javaType\":[\"String\"],\"columns[3].javaField\":[\"taskStatus\"],\"columns[3].isInsert\":[\"1\"],\"columns[3].isEdit\":[\"1\"],\"columns[3].isList\":[\"1\"],\"columns[3].isQuery\":[\"1\"],\"columns[3].queryType\":[\"EQ\"],\"columns[3].htmlType\":[\"radio\"],\"columns[3].dictType\":[\"\"],\"columns[4].columnId\":[\"22\"],\"columns[4].sort\":[\"5\"],\"columns[4].columnComment\":[\"赏金\"],\"columns[4].javaType\":[\"String\"],\"columns[4].javaField\":[\"money\"],\"columns[4].isInsert\":[\"1\"],\"columns[4].isEdit\":[\"1\"],\"columns[4].isList\":[\"1\"],\"columns[4].isQuery\":[\"1\"],\"columns[4].queryType\":[\"EQ\"],\"columns[4].htmlType\":[\"input\"],\"columns[4].dictType\":[\"\"],\"columns[5].columnId\":[\"23\"],\"columns[5].sort\":[\"6\"],\"columns[5].columnComment\":[\"电话号码\"],\"columns[5].javaType\":[\"String\"],\"c', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-08-31 16:41:18');
INSERT INTO `sys_oper_log` VALUES (128, '代码生成', 8, 'com.eden.generator.controller.GenController.download()', 'GET', 1, 'admin', '建筑学院', '/tool/gen/download/user_task', '127.0.0.1', '内网IP', NULL, 'null', 0, NULL, '2020-08-31 16:41:42');
INSERT INTO `sys_oper_log` VALUES (129, '代码生成', 8, 'com.eden.generator.controller.GenController.download()', 'GET', 1, 'admin', '建筑学院', '/tool/gen/download/user_task_picture', '127.0.0.1', '内网IP', NULL, 'null', 0, NULL, '2020-08-31 16:48:29');
INSERT INTO `sys_oper_log` VALUES (130, '代码生成', 2, 'com.eden.generator.controller.GenController.editSave()', 'POST', 1, 'admin', '建筑学院', '/tool/gen/edit', '127.0.0.1', '内网IP', '{\"tableId\":[\"3\"],\"tableName\":[\"user_task\"],\"tableComment\":[\"用户发布任务表\"],\"className\":[\"UserTask\"],\"functionAuthor\":[\"jiaoyang\"],\"remark\":[\"\"],\"columns[0].columnId\":[\"18\"],\"columns[0].sort\":[\"1\"],\"columns[0].columnComment\":[\"任务id\"],\"columns[0].javaType\":[\"String\"],\"columns[0].javaField\":[\"taskId\"],\"columns[0].isInsert\":[\"1\"],\"columns[0].queryType\":[\"EQ\"],\"columns[0].htmlType\":[\"input\"],\"columns[0].dictType\":[\"\"],\"columns[1].columnId\":[\"19\"],\"columns[1].sort\":[\"2\"],\"columns[1].columnComment\":[\"任务名称\"],\"columns[1].javaType\":[\"String\"],\"columns[1].javaField\":[\"taskName\"],\"columns[1].isInsert\":[\"1\"],\"columns[1].isEdit\":[\"1\"],\"columns[1].isList\":[\"1\"],\"columns[1].isQuery\":[\"1\"],\"columns[1].queryType\":[\"LIKE\"],\"columns[1].htmlType\":[\"input\"],\"columns[1].dictType\":[\"\"],\"columns[2].columnId\":[\"20\"],\"columns[2].sort\":[\"3\"],\"columns[2].columnComment\":[\"任务描述\"],\"columns[2].javaType\":[\"String\"],\"columns[2].javaField\":[\"taskDescription\"],\"columns[2].isInsert\":[\"1\"],\"columns[2].isEdit\":[\"1\"],\"columns[2].isList\":[\"1\"],\"columns[2].isQuery\":[\"1\"],\"columns[2].queryType\":[\"EQ\"],\"columns[2].htmlType\":[\"input\"],\"columns[2].dictType\":[\"\"],\"columns[3].columnId\":[\"21\"],\"columns[3].sort\":[\"4\"],\"columns[3].columnComment\":[\"任务状态 0：初始状态 1：待审核 2：审核通过 3：审核不通过 4：发布成功 5：发布失败\"],\"columns[3].javaType\":[\"String\"],\"columns[3].javaField\":[\"taskStatus\"],\"columns[3].isInsert\":[\"1\"],\"columns[3].isEdit\":[\"1\"],\"columns[3].isList\":[\"1\"],\"columns[3].isQuery\":[\"1\"],\"columns[3].queryType\":[\"EQ\"],\"columns[3].htmlType\":[\"radio\"],\"columns[3].dictType\":[\"\"],\"columns[4].columnId\":[\"22\"],\"columns[4].sort\":[\"5\"],\"columns[4].columnComment\":[\"赏金\"],\"columns[4].javaType\":[\"String\"],\"columns[4].javaField\":[\"money\"],\"columns[4].isInsert\":[\"1\"],\"columns[4].isEdit\":[\"1\"],\"columns[4].isList\":[\"1\"],\"columns[4].isQuery\":[\"1\"],\"columns[4].queryType\":[\"EQ\"],\"columns[4].htmlType\":[\"input\"],\"columns[4].dictType\":[\"\"],\"columns[5].columnId\":[\"23\"],\"columns[5].sort\":[\"6\"],\"columns[5].columnComment\":[\"电话号码\"],\"columns[5].javaType\":[\"String\"],\"c', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-08-31 17:05:07');
INSERT INTO `sys_oper_log` VALUES (131, '代码生成', 2, 'com.eden.generator.controller.GenController.editSave()', 'POST', 1, 'admin', '建筑学院', '/tool/gen/edit', '127.0.0.1', '内网IP', '{\"tableId\":[\"3\"],\"tableName\":[\"user_task\"],\"tableComment\":[\"用户发布任务表\"],\"className\":[\"UserTask\"],\"functionAuthor\":[\"jiaoyang\"],\"remark\":[\"\"],\"columns[0].columnId\":[\"18\"],\"columns[0].sort\":[\"1\"],\"columns[0].columnComment\":[\"任务id\"],\"columns[0].javaType\":[\"String\"],\"columns[0].javaField\":[\"taskId\"],\"columns[0].isInsert\":[\"1\"],\"columns[0].queryType\":[\"EQ\"],\"columns[0].htmlType\":[\"input\"],\"columns[0].dictType\":[\"\"],\"columns[1].columnId\":[\"19\"],\"columns[1].sort\":[\"2\"],\"columns[1].columnComment\":[\"任务名称\"],\"columns[1].javaType\":[\"String\"],\"columns[1].javaField\":[\"taskName\"],\"columns[1].isInsert\":[\"1\"],\"columns[1].isEdit\":[\"1\"],\"columns[1].isList\":[\"1\"],\"columns[1].isQuery\":[\"1\"],\"columns[1].queryType\":[\"LIKE\"],\"columns[1].htmlType\":[\"input\"],\"columns[1].dictType\":[\"\"],\"columns[2].columnId\":[\"20\"],\"columns[2].sort\":[\"3\"],\"columns[2].columnComment\":[\"任务描述\"],\"columns[2].javaType\":[\"String\"],\"columns[2].javaField\":[\"taskDescription\"],\"columns[2].isInsert\":[\"1\"],\"columns[2].isEdit\":[\"1\"],\"columns[2].isList\":[\"1\"],\"columns[2].isQuery\":[\"1\"],\"columns[2].queryType\":[\"EQ\"],\"columns[2].htmlType\":[\"input\"],\"columns[2].dictType\":[\"\"],\"columns[3].columnId\":[\"21\"],\"columns[3].sort\":[\"4\"],\"columns[3].columnComment\":[\"任务状态 0：初始状态 1：待审核 2：审核通过 3：审核不通过 4：发布成功 5：发布失败\"],\"columns[3].javaType\":[\"String\"],\"columns[3].javaField\":[\"taskStatus\"],\"columns[3].isInsert\":[\"1\"],\"columns[3].isEdit\":[\"1\"],\"columns[3].isList\":[\"1\"],\"columns[3].isQuery\":[\"1\"],\"columns[3].queryType\":[\"EQ\"],\"columns[3].htmlType\":[\"radio\"],\"columns[3].dictType\":[\"\"],\"columns[4].columnId\":[\"22\"],\"columns[4].sort\":[\"5\"],\"columns[4].columnComment\":[\"赏金\"],\"columns[4].javaType\":[\"String\"],\"columns[4].javaField\":[\"money\"],\"columns[4].isInsert\":[\"1\"],\"columns[4].isEdit\":[\"1\"],\"columns[4].isList\":[\"1\"],\"columns[4].isQuery\":[\"1\"],\"columns[4].queryType\":[\"EQ\"],\"columns[4].htmlType\":[\"input\"],\"columns[4].dictType\":[\"\"],\"columns[5].columnId\":[\"23\"],\"columns[5].sort\":[\"6\"],\"columns[5].columnComment\":[\"电话号码\"],\"columns[5].javaType\":[\"String\"],\"c', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-08-31 17:05:57');
INSERT INTO `sys_oper_log` VALUES (132, '代码生成', 8, 'com.eden.generator.controller.GenController.download()', 'GET', 1, 'admin', '建筑学院', '/tool/gen/download/user_task', '127.0.0.1', '内网IP', NULL, 'null', 0, NULL, '2020-08-31 17:06:02');
INSERT INTO `sys_oper_log` VALUES (133, '代码生成', 2, 'com.eden.generator.controller.GenController.editSave()', 'POST', 1, 'admin', '建筑学院', '/tool/gen/edit', '127.0.0.1', '内网IP', '{\"tableId\":[\"3\"],\"tableName\":[\"user_task\"],\"tableComment\":[\"用户发布任务表\"],\"className\":[\"UserTask\"],\"functionAuthor\":[\"jiaoyang\"],\"remark\":[\"\"],\"columns[0].columnId\":[\"18\"],\"columns[0].sort\":[\"1\"],\"columns[0].columnComment\":[\"任务id\"],\"columns[0].javaType\":[\"String\"],\"columns[0].javaField\":[\"taskId\"],\"columns[0].isInsert\":[\"1\"],\"columns[0].queryType\":[\"EQ\"],\"columns[0].htmlType\":[\"input\"],\"columns[0].dictType\":[\"\"],\"columns[1].columnId\":[\"19\"],\"columns[1].sort\":[\"2\"],\"columns[1].columnComment\":[\"任务名称\"],\"columns[1].javaType\":[\"String\"],\"columns[1].javaField\":[\"taskName\"],\"columns[1].isInsert\":[\"1\"],\"columns[1].isEdit\":[\"1\"],\"columns[1].isList\":[\"1\"],\"columns[1].isQuery\":[\"1\"],\"columns[1].queryType\":[\"LIKE\"],\"columns[1].htmlType\":[\"input\"],\"columns[1].dictType\":[\"\"],\"columns[2].columnId\":[\"20\"],\"columns[2].sort\":[\"3\"],\"columns[2].columnComment\":[\"任务描述\"],\"columns[2].javaType\":[\"String\"],\"columns[2].javaField\":[\"taskDescription\"],\"columns[2].isInsert\":[\"1\"],\"columns[2].isEdit\":[\"1\"],\"columns[2].isList\":[\"1\"],\"columns[2].isQuery\":[\"1\"],\"columns[2].queryType\":[\"EQ\"],\"columns[2].htmlType\":[\"input\"],\"columns[2].dictType\":[\"\"],\"columns[3].columnId\":[\"21\"],\"columns[3].sort\":[\"4\"],\"columns[3].columnComment\":[\"任务状态 0：初始状态 1：待审核 2：审核通过 3：审核不通过 4：发布成功 5：发布失败\"],\"columns[3].javaType\":[\"String\"],\"columns[3].javaField\":[\"taskStatus\"],\"columns[3].isInsert\":[\"1\"],\"columns[3].isEdit\":[\"1\"],\"columns[3].isList\":[\"1\"],\"columns[3].isQuery\":[\"1\"],\"columns[3].queryType\":[\"EQ\"],\"columns[3].htmlType\":[\"radio\"],\"columns[3].dictType\":[\"\"],\"columns[4].columnId\":[\"22\"],\"columns[4].sort\":[\"5\"],\"columns[4].columnComment\":[\"赏金\"],\"columns[4].javaType\":[\"String\"],\"columns[4].javaField\":[\"money\"],\"columns[4].isInsert\":[\"1\"],\"columns[4].isEdit\":[\"1\"],\"columns[4].isList\":[\"1\"],\"columns[4].isQuery\":[\"1\"],\"columns[4].queryType\":[\"EQ\"],\"columns[4].htmlType\":[\"input\"],\"columns[4].dictType\":[\"\"],\"columns[5].columnId\":[\"23\"],\"columns[5].sort\":[\"6\"],\"columns[5].columnComment\":[\"电话号码\"],\"columns[5].javaType\":[\"String\"],\"c', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-08-31 17:13:19');
INSERT INTO `sys_oper_log` VALUES (134, '代码生成', 8, 'com.eden.generator.controller.GenController.download()', 'GET', 1, 'admin', '建筑学院', '/tool/gen/download/user_task', '127.0.0.1', '内网IP', NULL, 'null', 0, NULL, '2020-08-31 17:13:39');
INSERT INTO `sys_oper_log` VALUES (135, '菜单管理', 2, 'com.eden.web.controller.system.SysMenuController.editSave()', 'POST', 1, 'admin', '建筑学院', '/system/menu/edit', '127.0.0.1', '内网IP', '{\"menuId\":[\"1066\"],\"parentId\":[\"1064\"],\"menuType\":[\"C\"],\"menuName\":[\"校园任务管理\"],\"url\":[\"/task/usertask\"],\"target\":[\"menuItem\"],\"perms\":[\"task:usertask:view\"],\"orderNum\":[\"2\"],\"icon\":[\"#\"],\"visible\":[\"0\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-08-31 17:18:18');
INSERT INTO `sys_oper_log` VALUES (136, '用户发布任务', 1, 'com.eden.task.controller.UserTaskController.addSave()', 'POST', 1, 'admin', '建筑学院', '/task/usertask/add', '127.0.0.1', '内网IP', '{\"taskName\":[\"\"],\"taskDescription\":[\"\"],\"money\":[\"\"],\"phonenumber\":[\"\"],\"address\":[\"\"]}', 'null', 1, '\r\n### Error updating database.  Cause: java.sql.SQLException: Field \'task_id\' doesn\'t have a default value\r\n### The error may involve com.eden.task.mapper.UserTaskMapper.insertUserTask-Inline\r\n### The error occurred while setting parameters\r\n### SQL: insert into user_task          ( task_name,             task_description,                          money,             phonenumber,             address,                          create_time )           values ( ?,             ?,                          ?,             ?,             ?,                          ? )\r\n### Cause: java.sql.SQLException: Field \'task_id\' doesn\'t have a default value\n; Field \'task_id\' doesn\'t have a default value; nested exception is java.sql.SQLException: Field \'task_id\' doesn\'t have a default value', '2020-08-31 17:43:15');
INSERT INTO `sys_oper_log` VALUES (137, '菜单管理', 1, 'com.eden.web.controller.system.SysMenuController.addSave()', 'POST', 1, 'admin', '建筑学院', '/system/menu/add', '127.0.0.1', '内网IP', '{\"parentId\":[\"0\"],\"menuType\":[\"C\"],\"menuName\":[\"校园任务审核\"],\"url\":[\"/task/usertask/check\"],\"target\":[\"menuItem\"],\"perms\":[\"task:usertask:view\"],\"orderNum\":[\"3\"],\"icon\":[\"\"],\"visible\":[\"0\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-08-31 17:56:01');
INSERT INTO `sys_oper_log` VALUES (138, '菜单管理', 3, 'com.eden.web.controller.system.SysMenuController.remove()', 'GET', 1, 'admin', '建筑学院', '/system/menu/remove/1067', '127.0.0.1', '内网IP', NULL, '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-08-31 17:56:57');
INSERT INTO `sys_oper_log` VALUES (139, '菜单管理', 1, 'com.eden.web.controller.system.SysMenuController.addSave()', 'POST', 1, 'admin', '建筑学院', '/system/menu/add', '127.0.0.1', '内网IP', '{\"parentId\":[\"1064\"],\"menuType\":[\"C\"],\"menuName\":[\"校园任务审查\"],\"url\":[\"task/usertask/check\"],\"target\":[\"menuItem\"],\"perms\":[\"task:usertask:view\"],\"orderNum\":[\"3\"],\"icon\":[\"\"],\"visible\":[\"0\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-08-31 17:58:01');
INSERT INTO `sys_oper_log` VALUES (140, '代码生成', 6, 'com.eden.generator.controller.GenController.importTableSave()', 'POST', 1, 'admin', '建筑学院', '/tool/gen/importTable', '127.0.0.1', '内网IP', '{\"tables\":[\"school_user\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-09-01 09:24:45');
INSERT INTO `sys_oper_log` VALUES (141, '代码生成', 2, 'com.eden.generator.controller.GenController.editSave()', 'POST', 1, 'admin', '建筑学院', '/tool/gen/edit', '127.0.0.1', '内网IP', '{\"tableId\":[\"5\"],\"tableName\":[\"school_user\"],\"tableComment\":[\"学校用户信息表\"],\"className\":[\"SchoolUser\"],\"functionAuthor\":[\"jiaoyang\"],\"remark\":[\"\"],\"columns[0].columnId\":[\"33\"],\"columns[0].sort\":[\"1\"],\"columns[0].columnComment\":[\"用户ID\"],\"columns[0].javaType\":[\"Long\"],\"columns[0].javaField\":[\"userId\"],\"columns[0].isInsert\":[\"1\"],\"columns[0].queryType\":[\"EQ\"],\"columns[0].htmlType\":[\"input\"],\"columns[0].dictType\":[\"\"],\"columns[1].columnId\":[\"34\"],\"columns[1].sort\":[\"2\"],\"columns[1].columnComment\":[\"专业ID\"],\"columns[1].javaType\":[\"Long\"],\"columns[1].javaField\":[\"deptId\"],\"columns[1].isInsert\":[\"1\"],\"columns[1].isEdit\":[\"1\"],\"columns[1].isList\":[\"1\"],\"columns[1].isQuery\":[\"1\"],\"columns[1].queryType\":[\"EQ\"],\"columns[1].htmlType\":[\"input\"],\"columns[1].dictType\":[\"\"],\"columns[2].columnId\":[\"35\"],\"columns[2].sort\":[\"3\"],\"columns[2].columnComment\":[\"登录账号\"],\"columns[2].javaType\":[\"String\"],\"columns[2].javaField\":[\"loginName\"],\"columns[2].isInsert\":[\"1\"],\"columns[2].isEdit\":[\"1\"],\"columns[2].isList\":[\"1\"],\"columns[2].isQuery\":[\"1\"],\"columns[2].queryType\":[\"LIKE\"],\"columns[2].isRequired\":[\"1\"],\"columns[2].htmlType\":[\"input\"],\"columns[2].dictType\":[\"\"],\"columns[3].columnId\":[\"36\"],\"columns[3].sort\":[\"4\"],\"columns[3].columnComment\":[\"用户昵称\"],\"columns[3].javaType\":[\"String\"],\"columns[3].javaField\":[\"userName\"],\"columns[3].isInsert\":[\"1\"],\"columns[3].isEdit\":[\"1\"],\"columns[3].isList\":[\"1\"],\"columns[3].isQuery\":[\"1\"],\"columns[3].queryType\":[\"LIKE\"],\"columns[3].htmlType\":[\"input\"],\"columns[3].dictType\":[\"\"],\"columns[4].columnId\":[\"37\"],\"columns[4].sort\":[\"5\"],\"columns[4].columnComment\":[\"用户类型（00系统用户 01注册用户）\"],\"columns[4].javaType\":[\"String\"],\"columns[4].javaField\":[\"userType\"],\"columns[4].isInsert\":[\"1\"],\"columns[4].isEdit\":[\"1\"],\"columns[4].isList\":[\"1\"],\"columns[4].isQuery\":[\"1\"],\"columns[4].queryType\":[\"EQ\"],\"columns[4].htmlType\":[\"select\"],\"columns[4].dictType\":[\"\"],\"columns[5].columnId\":[\"38\"],\"columns[5].sort\":[\"6\"],\"columns[5].columnComment\":[\"用户邮箱\"],\"columns[5].javaType\":[\"String\"],\"', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-09-01 09:29:33');
INSERT INTO `sys_oper_log` VALUES (142, '代码生成', 8, 'com.eden.generator.controller.GenController.download()', 'GET', 1, 'admin', '建筑学院', '/tool/gen/download/school_user', '127.0.0.1', '内网IP', NULL, 'null', 0, NULL, '2020-09-01 09:30:19');
INSERT INTO `sys_oper_log` VALUES (143, '菜单管理', 2, 'com.eden.web.controller.system.SysMenuController.editSave()', 'POST', 1, 'admin', '建筑学院', '/system/menu/edit', '127.0.0.1', '内网IP', '{\"menuId\":[\"1065\"],\"parentId\":[\"1064\"],\"menuType\":[\"C\"],\"menuName\":[\"校园用户管理\"],\"url\":[\"/school/schoolUser\"],\"target\":[\"menuItem\"],\"perms\":[\"school:schoolUser:view\"],\"orderNum\":[\"1\"],\"icon\":[\"#\"],\"visible\":[\"0\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-09-01 09:37:02');
INSERT INTO `sys_oper_log` VALUES (144, '学校用户信息', 1, 'com.eden.school.controller.SchoolUserController.addSave()', 'POST', 1, 'admin', '建筑学院', '/school/schoolUser/add', '127.0.0.1', '内网IP', '{\"deptId\":[\"129\"],\"userName\":[\"焦洋\"],\"deptName\":[\"数据科学与大数据技术\"],\"phonenumber\":[\"17761628992\"],\"email\":[\"838855804@qq.com\"],\"loginName\":[\"1715925168\"],\"sex\":[\"0\"],\"remark\":[\"\"],\"status\":[\"0\"],\"postIds\":[\"\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-09-01 12:04:04');
INSERT INTO `sys_oper_log` VALUES (145, '代码生成', 6, 'com.eden.generator.controller.GenController.importTableSave()', 'POST', 1, 'admin', '建筑学院', '/tool/gen/importTable', '127.0.0.1', '内网IP', '{\"tables\":[\"sys_region\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-09-01 13:54:11');
INSERT INTO `sys_oper_log` VALUES (146, '代码生成', 8, 'com.eden.generator.controller.GenController.download()', 'GET', 1, 'admin', '建筑学院', '/tool/gen/download/sys_region', '127.0.0.1', '内网IP', NULL, 'null', 0, NULL, '2020-09-01 13:54:38');
INSERT INTO `sys_oper_log` VALUES (147, '代码生成', 2, 'com.eden.generator.controller.GenController.editSave()', 'POST', 1, 'admin', '建筑学院', '/tool/gen/edit', '127.0.0.1', '内网IP', '{\"tableId\":[\"6\"],\"tableName\":[\"sys_region\"],\"tableComment\":[\"地区表\"],\"className\":[\"SysRegion\"],\"functionAuthor\":[\"jiaoyang\"],\"remark\":[\"\"],\"columns[0].columnId\":[\"54\"],\"columns[0].sort\":[\"1\"],\"columns[0].columnComment\":[\"地区主键编号\"],\"columns[0].javaType\":[\"String\"],\"columns[0].javaField\":[\"regionId\"],\"columns[0].isInsert\":[\"1\"],\"columns[0].queryType\":[\"EQ\"],\"columns[0].htmlType\":[\"input\"],\"columns[0].dictType\":[\"\"],\"columns[1].columnId\":[\"55\"],\"columns[1].sort\":[\"2\"],\"columns[1].columnComment\":[\"地区名称\"],\"columns[1].javaType\":[\"String\"],\"columns[1].javaField\":[\"regionName\"],\"columns[1].isInsert\":[\"1\"],\"columns[1].isEdit\":[\"1\"],\"columns[1].isList\":[\"1\"],\"columns[1].isQuery\":[\"1\"],\"columns[1].queryType\":[\"LIKE\"],\"columns[1].isRequired\":[\"1\"],\"columns[1].htmlType\":[\"input\"],\"columns[1].dictType\":[\"\"],\"columns[2].columnId\":[\"56\"],\"columns[2].sort\":[\"3\"],\"columns[2].columnComment\":[\"地区缩写\"],\"columns[2].javaType\":[\"String\"],\"columns[2].javaField\":[\"regionShortName\"],\"columns[2].isInsert\":[\"1\"],\"columns[2].isEdit\":[\"1\"],\"columns[2].isList\":[\"1\"],\"columns[2].isQuery\":[\"1\"],\"columns[2].queryType\":[\"LIKE\"],\"columns[2].htmlType\":[\"input\"],\"columns[2].dictType\":[\"\"],\"columns[3].columnId\":[\"57\"],\"columns[3].sort\":[\"4\"],\"columns[3].columnComment\":[\"行政地区编号\"],\"columns[3].javaType\":[\"String\"],\"columns[3].javaField\":[\"regionCode\"],\"columns[3].isInsert\":[\"1\"],\"columns[3].isEdit\":[\"1\"],\"columns[3].isList\":[\"1\"],\"columns[3].isQuery\":[\"1\"],\"columns[3].queryType\":[\"EQ\"],\"columns[3].htmlType\":[\"input\"],\"columns[3].dictType\":[\"\"],\"columns[4].columnId\":[\"58\"],\"columns[4].sort\":[\"5\"],\"columns[4].columnComment\":[\"地区父id\"],\"columns[4].javaType\":[\"String\"],\"columns[4].javaField\":[\"regionParentId\"],\"columns[4].isInsert\":[\"1\"],\"columns[4].isEdit\":[\"1\"],\"columns[4].isList\":[\"1\"],\"columns[4].isQuery\":[\"1\"],\"columns[4].queryType\":[\"EQ\"],\"columns[4].htmlType\":[\"input\"],\"columns[4].dictType\":[\"\"],\"columns[5].columnId\":[\"59\"],\"columns[5].sort\":[\"6\"],\"columns[5].columnComment\":[\"地区级别\"],\"columns[5].javaType\":[\"Int', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-09-01 13:55:58');
INSERT INTO `sys_oper_log` VALUES (148, '代码生成', 8, 'com.eden.generator.controller.GenController.download()', 'GET', 1, 'admin', '建筑学院', '/tool/gen/download/sys_region', '127.0.0.1', '内网IP', NULL, 'null', 0, NULL, '2020-09-01 13:56:17');
INSERT INTO `sys_oper_log` VALUES (149, '学校用户信息', 1, 'com.eden.school.controller.SchoolUserController.addSave()', 'POST', 1, 'admin', '建筑学院', '/school/schoolUser/add', '127.0.0.1', '内网IP', '{\"deptId\":[\"119\"],\"userName\":[\"小红\"],\"deptName\":[\"机械设计制造及其自动化\"],\"phonenumber\":[\"17761628997\"],\"email\":[\"838855805@qq.com\"],\"loginName\":[\"1715925169\"],\"sex\":[\"1\"],\"remark\":[\"\"],\"status\":[\"0\"],\"postIds\":[\"\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-09-03 13:59:11');
INSERT INTO `sys_oper_log` VALUES (150, '学校用户信息', 2, 'com.eden.school.controller.SchoolUserController.editSave()', 'POST', 1, 'admin', '建筑学院', '/school/schoolUser/edit', '127.0.0.1', '内网IP', '{\"userId\":[\"5\"],\"deptId\":[\"121\"],\"userName\":[\"小红\"],\"dept.deptName\":[\"测控技术与仪器\"],\"phonenumber\":[\"17761628997\"],\"email\":[\"838855805@qq.com\"],\"loginName\":[\"1715925169\"],\"sex\":[\"1\"],\"remark\":[\"测试用户\"],\"status\":[\"0\"],\"postIds\":[\"\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-09-03 14:33:17');
INSERT INTO `sys_oper_log` VALUES (151, '重置密码', 2, 'com.eden.web.controller.school.SchoolUserController.resetPwdSave()', 'POST', 1, 'admin', '建筑学院', '/school/schoolUser/resetPwd', '127.0.0.1', '内网IP', '{\"userId\":[\"5\"],\"loginName\":[\"1715925169\"]}', 'null', 1, '不允许操作超级管理员用户', '2020-09-03 15:38:33');
INSERT INTO `sys_oper_log` VALUES (152, '重置密码', 2, 'com.eden.web.controller.school.SchoolUserController.resetPwdSave()', 'POST', 1, 'admin', '建筑学院', '/school/schoolUser/resetPwd', '127.0.0.1', '内网IP', '{\"userId\":[\"5\"],\"loginName\":[\"1715925169\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-09-03 15:41:00');
INSERT INTO `sys_oper_log` VALUES (153, '学校用户信息', 1, 'com.eden.web.controller.school.SchoolUserController.addSave()', 'POST', 1, 'admin', '建筑学院', '/school/schoolUser/add', '127.0.0.1', '内网IP', '{\"deptId\":[\"120\"],\"userName\":[\"小强\"],\"deptName\":[\"电气工程及其自动化\"],\"phonenumber\":[\"17761628994\"],\"email\":[\"83885580@qq.com\"],\"loginName\":[\"1715925160\"],\"sex\":[\"0\"],\"remark\":[\"\"],\"status\":[\"0\"],\"postIds\":[\"\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-09-03 15:43:16');
INSERT INTO `sys_oper_log` VALUES (154, '学校用户信息', 3, 'com.eden.web.controller.school.SchoolUserController.remove()', 'POST', 1, 'admin', '建筑学院', '/school/schoolUser/remove', '127.0.0.1', '内网IP', '{\"ids\":[\"6,5\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-09-03 15:56:56');
INSERT INTO `sys_oper_log` VALUES (155, '学校用户信息', 1, 'com.eden.web.controller.school.SchoolUserController.addSave()', 'POST', 1, 'admin', '建筑学院', '/school/schoolUser/add', '127.0.0.1', '内网IP', '{\"deptId\":[\"119\"],\"userName\":[\"kk\"],\"deptName\":[\"机械设计制造及其自动化\"],\"phonenumber\":[\"17761628999\"],\"email\":[\"838855806@qq.com\"],\"loginName\":[\"1715925167\"],\"sex\":[\"0\"],\"remark\":[\"\"],\"status\":[\"0\"],\"postIds\":[\"\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-09-03 15:57:40');
INSERT INTO `sys_oper_log` VALUES (156, '重置密码', 2, 'com.eden.web.controller.school.SchoolUserController.resetPwdSave()', 'POST', 1, 'admin', '建筑学院', '/school/schoolUser/resetPwd', '127.0.0.1', '内网IP', '{\"userId\":[\"7\"],\"loginName\":[\"1715925167\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-09-03 15:58:14');
INSERT INTO `sys_oper_log` VALUES (157, '重置密码', 2, 'com.eden.web.controller.school.SchoolUserController.resetPwdSave()', 'POST', 1, 'admin', '建筑学院', '/school/schoolUser/resetPwd', '127.0.0.1', '内网IP', '{\"userId\":[\"7\"],\"loginName\":[\"1715925167\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-09-03 15:59:00');
INSERT INTO `sys_oper_log` VALUES (158, '代码生成', 8, 'com.eden.generator.controller.GenController.download()', 'GET', 1, 'admin', '建筑学院', '/tool/gen/download/user_task_picture', '127.0.0.1', '内网IP', NULL, 'null', 0, NULL, '2020-09-03 16:15:57');
INSERT INTO `sys_oper_log` VALUES (159, '代码生成', 8, 'com.eden.generator.controller.GenController.download()', 'GET', 1, 'admin', '建筑学院', '/tool/gen/download/task_picture', '127.0.0.1', '内网IP', NULL, 'null', 0, NULL, '2020-09-03 16:16:22');
INSERT INTO `sys_oper_log` VALUES (160, '代码生成', 2, 'com.eden.generator.controller.GenController.synchDb()', 'GET', 1, 'admin', '建筑学院', '/tool/gen/synchDb/task_picture', '127.0.0.1', '内网IP', NULL, '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-09-03 16:28:49');
INSERT INTO `sys_oper_log` VALUES (161, '代码生成', 2, 'com.eden.generator.controller.GenController.editSave()', 'POST', 1, 'admin', '建筑学院', '/tool/gen/edit', '127.0.0.1', '内网IP', '{\"tableId\":[\"2\"],\"tableName\":[\"task_picture\"],\"tableComment\":[\"任务图片表\"],\"className\":[\"TaskPicture\"],\"functionAuthor\":[\"jiaoyang\"],\"remark\":[\"\"],\"columns[0].columnId\":[\"16\"],\"columns[0].sort\":[\"1\"],\"columns[0].columnComment\":[\"图片Id\"],\"columns[0].javaType\":[\"String\"],\"columns[0].javaField\":[\"pictureId\"],\"columns[0].isInsert\":[\"1\"],\"columns[0].queryType\":[\"EQ\"],\"columns[0].htmlType\":[\"input\"],\"columns[0].dictType\":[\"\"],\"columns[1].columnId\":[\"17\"],\"columns[1].sort\":[\"2\"],\"columns[1].columnComment\":[\"图片地址\"],\"columns[1].javaType\":[\"String\"],\"columns[1].javaField\":[\"pictureAddress\"],\"columns[1].isInsert\":[\"1\"],\"columns[1].isEdit\":[\"1\"],\"columns[1].isList\":[\"1\"],\"columns[1].isQuery\":[\"1\"],\"columns[1].queryType\":[\"EQ\"],\"columns[1].htmlType\":[\"input\"],\"columns[1].dictType\":[\"\"],\"columns[2].columnId\":[\"60\"],\"columns[2].sort\":[\"3\"],\"columns[2].columnComment\":[\"创建者\"],\"columns[2].javaType\":[\"String\"],\"columns[2].javaField\":[\"createBy\"],\"columns[2].isInsert\":[\"1\"],\"columns[2].queryType\":[\"EQ\"],\"columns[2].htmlType\":[\"input\"],\"columns[2].dictType\":[\"\"],\"columns[3].columnId\":[\"61\"],\"columns[3].sort\":[\"4\"],\"columns[3].columnComment\":[\"创建时间\"],\"columns[3].javaType\":[\"Date\"],\"columns[3].javaField\":[\"createTime\"],\"columns[3].isInsert\":[\"1\"],\"columns[3].queryType\":[\"EQ\"],\"columns[3].htmlType\":[\"datetime\"],\"columns[3].dictType\":[\"\"],\"columns[4].columnId\":[\"62\"],\"columns[4].sort\":[\"5\"],\"columns[4].columnComment\":[\"更新者\"],\"columns[4].javaType\":[\"String\"],\"columns[4].javaField\":[\"updateBy\"],\"columns[4].isInsert\":[\"1\"],\"columns[4].isEdit\":[\"1\"],\"columns[4].queryType\":[\"EQ\"],\"columns[4].htmlType\":[\"input\"],\"columns[4].dictType\":[\"\"],\"columns[5].columnId\":[\"63\"],\"columns[5].sort\":[\"6\"],\"columns[5].columnComment\":[\"更新时间\"],\"columns[5].javaType\":[\"Date\"],\"columns[5].javaField\":[\"updateTime\"],\"columns[5].isInsert\":[\"1\"],\"columns[5].isEdit\":[\"1\"],\"columns[5].queryType\":[\"EQ\"],\"columns[5].htmlType\":[\"datetime\"],\"columns[5].dictType\":[\"\"],\"columns[6].columnId\":[\"64\"],\"columns[6].sort\":[\"7\"],\"columns[6].', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-09-03 16:29:59');
INSERT INTO `sys_oper_log` VALUES (162, '代码生成', 8, 'com.eden.generator.controller.GenController.download()', 'GET', 1, 'admin', '建筑学院', '/tool/gen/download/task_picture', '127.0.0.1', '内网IP', NULL, 'null', 0, NULL, '2020-09-03 16:30:14');
INSERT INTO `sys_oper_log` VALUES (163, '用户发布任务', 1, 'com.eden.task.controller.UserTaskController.addSave()', 'POST', 1, 'admin', '建筑学院', '/task/usertask/add', '127.0.0.1', '内网IP', '{\"taskName\":[\"21\"],\"taskDescription\":[\"21\"],\"money\":[\"21\"],\"phonenumber\":[\"21\"],\"address\":[\"21\"]}', 'null', 1, 'Invalid bound statement (not found): com.eden.task.mapper.UserTaskMapper.insertUserTask', '2020-09-03 17:00:04');
INSERT INTO `sys_oper_log` VALUES (164, '代码生成', 2, 'com.eden.generator.controller.GenController.editSave()', 'POST', 1, 'admin', '建筑学院', '/tool/gen/edit', '127.0.0.1', '内网IP', '{\"tableId\":[\"3\"],\"tableName\":[\"user_task\"],\"tableComment\":[\"用户发布任务表\"],\"className\":[\"UserTask\"],\"functionAuthor\":[\"jiaoyang\"],\"remark\":[\"\"],\"columns[0].columnId\":[\"18\"],\"columns[0].sort\":[\"1\"],\"columns[0].columnComment\":[\"任务id\"],\"columns[0].javaType\":[\"String\"],\"columns[0].javaField\":[\"taskId\"],\"columns[0].isInsert\":[\"1\"],\"columns[0].queryType\":[\"EQ\"],\"columns[0].htmlType\":[\"input\"],\"columns[0].dictType\":[\"\"],\"columns[1].columnId\":[\"19\"],\"columns[1].sort\":[\"2\"],\"columns[1].columnComment\":[\"任务名称\"],\"columns[1].javaType\":[\"String\"],\"columns[1].javaField\":[\"taskName\"],\"columns[1].isInsert\":[\"1\"],\"columns[1].isEdit\":[\"1\"],\"columns[1].isList\":[\"1\"],\"columns[1].isQuery\":[\"1\"],\"columns[1].queryType\":[\"LIKE\"],\"columns[1].htmlType\":[\"input\"],\"columns[1].dictType\":[\"\"],\"columns[2].columnId\":[\"20\"],\"columns[2].sort\":[\"3\"],\"columns[2].columnComment\":[\"任务描述\"],\"columns[2].javaType\":[\"String\"],\"columns[2].javaField\":[\"taskDescription\"],\"columns[2].isInsert\":[\"1\"],\"columns[2].isEdit\":[\"1\"],\"columns[2].isList\":[\"1\"],\"columns[2].isQuery\":[\"1\"],\"columns[2].queryType\":[\"EQ\"],\"columns[2].htmlType\":[\"input\"],\"columns[2].dictType\":[\"\"],\"columns[3].columnId\":[\"21\"],\"columns[3].sort\":[\"4\"],\"columns[3].columnComment\":[\"任务状态 0：初始状态 1：待审核 2：审核通过 3：审核不通过 4：发布成功 5：发布失败\"],\"columns[3].javaType\":[\"String\"],\"columns[3].javaField\":[\"taskStatus\"],\"columns[3].isInsert\":[\"1\"],\"columns[3].isEdit\":[\"1\"],\"columns[3].isList\":[\"1\"],\"columns[3].isQuery\":[\"1\"],\"columns[3].queryType\":[\"EQ\"],\"columns[3].htmlType\":[\"radio\"],\"columns[3].dictType\":[\"\"],\"columns[4].columnId\":[\"22\"],\"columns[4].sort\":[\"5\"],\"columns[4].columnComment\":[\"赏金\"],\"columns[4].javaType\":[\"String\"],\"columns[4].javaField\":[\"money\"],\"columns[4].isInsert\":[\"1\"],\"columns[4].isEdit\":[\"1\"],\"columns[4].isList\":[\"1\"],\"columns[4].isQuery\":[\"1\"],\"columns[4].queryType\":[\"EQ\"],\"columns[4].htmlType\":[\"input\"],\"columns[4].dictType\":[\"\"],\"columns[5].columnId\":[\"23\"],\"columns[5].sort\":[\"6\"],\"columns[5].columnComment\":[\"电话号码\"],\"columns[5].javaType\":[\"String\"],\"c', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-09-03 17:03:29');
INSERT INTO `sys_oper_log` VALUES (165, '代码生成', 2, 'com.eden.generator.controller.GenController.synchDb()', 'GET', 1, 'admin', '建筑学院', '/tool/gen/synchDb/user_task', '127.0.0.1', '内网IP', NULL, '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-09-03 17:03:43');
INSERT INTO `sys_oper_log` VALUES (166, '用户发布任务', 1, 'com.eden.task.controller.UserTaskController.addSave()', 'POST', 1, 'admin', '建筑学院', '/task/usertask/add', '127.0.0.1', '内网IP', '{\"taskName\":[\"11\"],\"taskDescription\":[\"11\"],\"money\":[\"11\"],\"phonenumber\":[\"11\"],\"address\":[\"111\"]}', 'null', 1, '\r\n### Error updating database.  Cause: java.sql.SQLException: Field \'task_id\' doesn\'t have a default value\r\n### The error may involve com.eden.task.mapper.UserTaskMapper.insertUserTask-Inline\r\n### The error occurred while setting parameters\r\n### SQL: insert into user_task          ( task_name,             task_description,                          money,             phonenumber,             address,                          create_time )           values ( ?,             ?,                          ?,             ?,             ?,                          ? )\r\n### Cause: java.sql.SQLException: Field \'task_id\' doesn\'t have a default value\n; Field \'task_id\' doesn\'t have a default value; nested exception is java.sql.SQLException: Field \'task_id\' doesn\'t have a default value', '2020-09-04 09:39:58');
INSERT INTO `sys_oper_log` VALUES (167, '用户发布任务', 1, 'com.eden.task.controller.UserTaskController.addSave()', 'POST', 1, 'admin', '建筑学院', '/task/usertask/add', '127.0.0.1', '内网IP', '{\"taskName\":[\"1\"],\"taskDescription\":[\"1\"],\"money\":[\"1\"],\"phonenumber\":[\"1\"],\"address\":[\"1\"]}', 'null', 1, 'Error selecting key or setting result to parameter object. Cause: java.sql.SQLException: The user specified as a definer (\'shnt\'@\'%\') does not exist\n; uncategorized SQLException; SQL state [HY000]; error code [1449]; The user specified as a definer (\'shnt\'@\'%\') does not exist; nested exception is java.sql.SQLException: The user specified as a definer (\'shnt\'@\'%\') does not exist', '2020-09-04 09:58:59');
INSERT INTO `sys_oper_log` VALUES (168, '用户发布任务', 1, 'com.eden.task.controller.UserTaskController.addSave()', 'POST', 1, 'admin', '建筑学院', '/task/usertask/add', '127.0.0.1', '内网IP', '{\"taskName\":[\"1\"],\"taskDescription\":[\"1\"],\"money\":[\"1\"],\"phonenumber\":[\"1\"],\"address\":[\"1\"]}', 'null', 1, 'Error selecting key or setting result to parameter object. Cause: java.sql.SQLSyntaxErrorException: execute command denied to user \'eden\'@\'localhost\' for routine \'edendb.next_prefix_seq_val\'\n; bad SQL grammar []; nested exception is java.sql.SQLSyntaxErrorException: execute command denied to user \'eden\'@\'localhost\' for routine \'edendb.next_prefix_seq_val\'', '2020-09-04 10:05:05');
INSERT INTO `sys_oper_log` VALUES (169, '用户发布任务', 1, 'com.eden.task.controller.UserTaskController.addSave()', 'POST', 1, 'admin', '建筑学院', '/task/usertask/add', '127.0.0.1', '内网IP', '{\"taskName\":[\"1\"],\"taskDescription\":[\"1\"],\"money\":[\"1\"],\"phonenumber\":[\"1\"],\"address\":[\"1\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-09-04 10:08:56');
INSERT INTO `sys_oper_log` VALUES (170, '字典类型', 1, 'com.eden.web.controller.system.SysDictTypeController.addSave()', 'POST', 1, 'admin', '建筑学院', '/system/dict/add', '127.0.0.1', '内网IP', '{\"dictName\":[\"用户任务\"],\"dictType\":[\"user_task_status\"],\"status\":[\"0\"],\"remark\":[\"用户发布任务状态\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-09-04 10:17:37');
INSERT INTO `sys_oper_log` VALUES (171, '字典数据', 1, 'com.eden.web.controller.system.SysDictDataController.addSave()', 'POST', 1, 'admin', '建筑学院', '/system/dict/data/add', '127.0.0.1', '内网IP', '{\"dictLabel\":[\"待审核\"],\"dictValue\":[\"0\"],\"dictType\":[\"user_task_status\"],\"cssClass\":[\"\"],\"dictSort\":[\"0\"],\"listClass\":[\"default\"],\"isDefault\":[\"Y\"],\"status\":[\"0\"],\"remark\":[\"用户任务待审核状态\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-09-04 10:19:20');
INSERT INTO `sys_oper_log` VALUES (172, '字典数据', 1, 'com.eden.web.controller.system.SysDictDataController.addSave()', 'POST', 1, 'admin', '建筑学院', '/system/dict/data/add', '127.0.0.1', '内网IP', '{\"dictLabel\":[\"审核通过\"],\"dictValue\":[\"1\"],\"dictType\":[\"user_task_status\"],\"cssClass\":[\"\"],\"dictSort\":[\"1\"],\"listClass\":[\"success\"],\"isDefault\":[\"Y\"],\"status\":[\"0\"],\"remark\":[\"用户任务审核通过状态\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-09-04 10:19:57');
INSERT INTO `sys_oper_log` VALUES (173, '字典数据', 2, 'com.eden.web.controller.system.SysDictDataController.editSave()', 'POST', 1, 'admin', '建筑学院', '/system/dict/data/edit', '127.0.0.1', '内网IP', '{\"dictCode\":[\"30\"],\"dictLabel\":[\"待审核\"],\"dictValue\":[\"0\"],\"dictType\":[\"user_task_status\"],\"cssClass\":[\"\"],\"dictSort\":[\"0\"],\"listClass\":[\"primary\"],\"isDefault\":[\"Y\"],\"status\":[\"0\"],\"remark\":[\"用户任务待审核状态\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-09-04 10:20:12');
INSERT INTO `sys_oper_log` VALUES (174, '字典数据', 1, 'com.eden.web.controller.system.SysDictDataController.addSave()', 'POST', 1, 'admin', '建筑学院', '/system/dict/data/add', '127.0.0.1', '内网IP', '{\"dictLabel\":[\"审核未通过\"],\"dictValue\":[\"2\"],\"dictType\":[\"user_task_status\"],\"cssClass\":[\"\"],\"dictSort\":[\"2\"],\"listClass\":[\"danger\"],\"isDefault\":[\"Y\"],\"status\":[\"0\"],\"remark\":[\"任务审核未通过状态\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-09-04 10:22:22');
INSERT INTO `sys_oper_log` VALUES (175, '代码生成', 2, 'com.eden.generator.controller.GenController.synchDb()', 'GET', 1, 'admin', '建筑学院', '/tool/gen/synchDb/user_task', '127.0.0.1', '内网IP', NULL, '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-09-04 10:27:57');
INSERT INTO `sys_oper_log` VALUES (176, '用户发布任务', 3, 'com.eden.task.controller.UserTaskController.remove()', 'POST', 1, 'admin', '建筑学院', '/task/usertask/remove', '127.0.0.1', '内网IP', '{\"ids\":[\"T0000000001\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-09-04 10:58:57');
INSERT INTO `sys_oper_log` VALUES (177, '用户发布任务', 1, 'com.eden.task.controller.UserTaskController.addSave()', 'POST', 1, 'admin', '建筑学院', '/task/usertask/add', '127.0.0.1', '内网IP', '{\"taskName\":[\"1\"],\"taskDescription\":[\"1\"],\"money\":[\"1\"],\"phonenumber\":[\"1\"],\"address\":[\"1\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-09-04 10:59:04');
INSERT INTO `sys_oper_log` VALUES (178, '代码生成', 6, 'com.eden.generator.controller.GenController.importTableSave()', 'POST', 1, 'admin', '建筑学院', '/tool/gen/importTable', '127.0.0.1', '内网IP', '{\"tables\":[\"task_video\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-09-05 11:54:23');
INSERT INTO `sys_oper_log` VALUES (179, '代码生成', 2, 'com.eden.generator.controller.GenController.editSave()', 'POST', 1, 'admin', '建筑学院', '/tool/gen/edit', '127.0.0.1', '内网IP', '{\"tableId\":[\"7\"],\"tableName\":[\"task_video\"],\"tableComment\":[\"用户发布视频表\"],\"className\":[\"TaskVideo\"],\"functionAuthor\":[\"jiaoyang\"],\"remark\":[\"\"],\"tplCategory\":[\"crud\"],\"packageName\":[\"com.eden.task\"],\"moduleName\":[\"task\"],\"businessName\":[\"userTaskVideo\"],\"functionName\":[\"用户发布视频\"],\"params[parentMenuId]\":[\"1068\"],\"params[parentMenuName]\":[\"校园任务审查\"],\"genType\":[\"0\"],\"genPath\":[\"/\"],\"subTableName\":[\"\"],\"params[treeCode]\":[\"\"],\"params[treeParentCode]\":[\"\"],\"params[treeName]\":[\"\"]}', 'null', 1, '', '2020-09-05 11:55:21');
INSERT INTO `sys_oper_log` VALUES (180, '代码生成', 2, 'com.eden.generator.controller.GenController.editSave()', 'POST', 1, 'admin', '建筑学院', '/tool/gen/edit', '127.0.0.1', '内网IP', '{\"tableId\":[\"7\"],\"tableName\":[\"task_video\"],\"tableComment\":[\"用户发布视频表\"],\"className\":[\"TaskVideo\"],\"functionAuthor\":[\"jiaoyang\"],\"remark\":[\"\"],\"columns[0].columnId\":[\"66\"],\"columns[0].sort\":[\"1\"],\"columns[0].columnComment\":[\"视频Id\"],\"columns[0].javaType\":[\"String\"],\"columns[0].javaField\":[\"videoId\"],\"columns[0].isInsert\":[\"1\"],\"columns[0].queryType\":[\"EQ\"],\"columns[0].htmlType\":[\"input\"],\"columns[0].dictType\":[\"\"],\"columns[1].columnId\":[\"67\"],\"columns[1].sort\":[\"2\"],\"columns[1].columnComment\":[\"视频上传路径\"],\"columns[1].javaType\":[\"String\"],\"columns[1].javaField\":[\"videoUrl\"],\"columns[1].isInsert\":[\"1\"],\"columns[1].isEdit\":[\"1\"],\"columns[1].isList\":[\"1\"],\"columns[1].isQuery\":[\"1\"],\"columns[1].queryType\":[\"EQ\"],\"columns[1].htmlType\":[\"input\"],\"columns[1].dictType\":[\"\"],\"columns[2].columnId\":[\"68\"],\"columns[2].sort\":[\"3\"],\"columns[2].columnComment\":[\"创建时间\"],\"columns[2].javaType\":[\"String\"],\"columns[2].javaField\":[\"createBy\"],\"columns[2].isInsert\":[\"1\"],\"columns[2].queryType\":[\"EQ\"],\"columns[2].htmlType\":[\"input\"],\"columns[2].dictType\":[\"\"],\"columns[3].columnId\":[\"69\"],\"columns[3].sort\":[\"4\"],\"columns[3].columnComment\":[\"创建时间\"],\"columns[3].javaType\":[\"Date\"],\"columns[3].javaField\":[\"createTime\"],\"columns[3].isInsert\":[\"1\"],\"columns[3].queryType\":[\"EQ\"],\"columns[3].htmlType\":[\"datetime\"],\"columns[3].dictType\":[\"\"],\"columns[4].columnId\":[\"70\"],\"columns[4].sort\":[\"5\"],\"columns[4].columnComment\":[\"更新者\"],\"columns[4].javaType\":[\"String\"],\"columns[4].javaField\":[\"updateBy\"],\"columns[4].isInsert\":[\"1\"],\"columns[4].isEdit\":[\"1\"],\"columns[4].queryType\":[\"EQ\"],\"columns[4].htmlType\":[\"input\"],\"columns[4].dictType\":[\"\"],\"columns[5].columnId\":[\"71\"],\"columns[5].sort\":[\"6\"],\"columns[5].columnComment\":[\"更新时间\"],\"columns[5].javaType\":[\"Date\"],\"columns[5].javaField\":[\"updateTime\"],\"columns[5].isInsert\":[\"1\"],\"columns[5].isEdit\":[\"1\"],\"columns[5].queryType\":[\"EQ\"],\"columns[5].htmlType\":[\"datetime\"],\"columns[5].dictType\":[\"\"],\"columns[6].columnId\":[\"72\"],\"columns[6].sort\":[\"7\"],\"columns[6].columnC', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-09-05 11:57:08');
INSERT INTO `sys_oper_log` VALUES (181, '代码生成', 8, 'com.eden.generator.controller.GenController.download()', 'GET', 1, 'admin', '建筑学院', '/tool/gen/download/task_video', '127.0.0.1', '内网IP', NULL, 'null', 0, NULL, '2020-09-05 11:57:39');
INSERT INTO `sys_oper_log` VALUES (182, '代码生成', 6, 'com.eden.generator.controller.GenController.importTableSave()', 'POST', 1, 'admin', '建筑学院', '/tool/gen/importTable', '127.0.0.1', '内网IP', '{\"tables\":[\"sys_file_info\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-09-07 11:30:33');
INSERT INTO `sys_oper_log` VALUES (183, '代码生成', 2, 'com.eden.generator.controller.GenController.editSave()', 'POST', 1, 'admin', '建筑学院', '/tool/gen/edit', '127.0.0.1', '内网IP', '{\"tableId\":[\"8\"],\"tableName\":[\"sys_file_info\"],\"tableComment\":[\"文件上传下载\"],\"className\":[\"SysFileInfo\"],\"functionAuthor\":[\"焦洋\"],\"remark\":[\"\"],\"columns[0].columnId\":[\"73\"],\"columns[0].sort\":[\"1\"],\"columns[0].columnComment\":[\"文件id\"],\"columns[0].javaType\":[\"Long\"],\"columns[0].javaField\":[\"fileId\"],\"columns[0].isInsert\":[\"1\"],\"columns[0].queryType\":[\"EQ\"],\"columns[0].htmlType\":[\"input\"],\"columns[0].dictType\":[\"\"],\"columns[1].columnId\":[\"74\"],\"columns[1].sort\":[\"2\"],\"columns[1].columnComment\":[\"文件名称\"],\"columns[1].javaType\":[\"String\"],\"columns[1].javaField\":[\"fileName\"],\"columns[1].isInsert\":[\"1\"],\"columns[1].isEdit\":[\"1\"],\"columns[1].isList\":[\"1\"],\"columns[1].isQuery\":[\"1\"],\"columns[1].queryType\":[\"LIKE\"],\"columns[1].htmlType\":[\"input\"],\"columns[1].dictType\":[\"\"],\"columns[2].columnId\":[\"75\"],\"columns[2].sort\":[\"3\"],\"columns[2].columnComment\":[\"文件路径\"],\"columns[2].javaType\":[\"String\"],\"columns[2].javaField\":[\"filePath\"],\"columns[2].isInsert\":[\"1\"],\"columns[2].isEdit\":[\"1\"],\"columns[2].isList\":[\"1\"],\"columns[2].isQuery\":[\"1\"],\"columns[2].queryType\":[\"EQ\"],\"columns[2].htmlType\":[\"input\"],\"columns[2].dictType\":[\"\"],\"tplCategory\":[\"crud\"],\"packageName\":[\"com.eden.system\"],\"moduleName\":[\"system\"],\"businessName\":[\"info\"],\"functionName\":[\"文件信息\"],\"params[parentMenuId]\":[\"1\"],\"params[parentMenuName]\":[\"系统管理\"],\"genType\":[\"0\"],\"genPath\":[\"/\"],\"subTableName\":[\"\"],\"params[treeCode]\":[\"\"],\"params[treeParentCode]\":[\"\"],\"params[treeName]\":[\"\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-09-07 11:31:28');
INSERT INTO `sys_oper_log` VALUES (184, '代码生成', 2, 'com.eden.generator.controller.GenController.editSave()', 'POST', 1, 'admin', '建筑学院', '/tool/gen/edit', '127.0.0.1', '内网IP', '{\"tableId\":[\"8\"],\"tableName\":[\"sys_file_info\"],\"tableComment\":[\"文件上传下载\"],\"className\":[\"SysFileInfo\"],\"functionAuthor\":[\"jiaoyang\"],\"remark\":[\"\"],\"columns[0].columnId\":[\"73\"],\"columns[0].sort\":[\"1\"],\"columns[0].columnComment\":[\"文件id\"],\"columns[0].javaType\":[\"Long\"],\"columns[0].javaField\":[\"fileId\"],\"columns[0].isInsert\":[\"1\"],\"columns[0].queryType\":[\"EQ\"],\"columns[0].htmlType\":[\"input\"],\"columns[0].dictType\":[\"\"],\"columns[1].columnId\":[\"74\"],\"columns[1].sort\":[\"2\"],\"columns[1].columnComment\":[\"文件名称\"],\"columns[1].javaType\":[\"String\"],\"columns[1].javaField\":[\"fileName\"],\"columns[1].isInsert\":[\"1\"],\"columns[1].isEdit\":[\"1\"],\"columns[1].isList\":[\"1\"],\"columns[1].isQuery\":[\"1\"],\"columns[1].queryType\":[\"LIKE\"],\"columns[1].htmlType\":[\"input\"],\"columns[1].dictType\":[\"\"],\"columns[2].columnId\":[\"75\"],\"columns[2].sort\":[\"3\"],\"columns[2].columnComment\":[\"文件路径\"],\"columns[2].javaType\":[\"String\"],\"columns[2].javaField\":[\"filePath\"],\"columns[2].isInsert\":[\"1\"],\"columns[2].isEdit\":[\"1\"],\"columns[2].isList\":[\"1\"],\"columns[2].isQuery\":[\"1\"],\"columns[2].queryType\":[\"EQ\"],\"columns[2].htmlType\":[\"input\"],\"columns[2].dictType\":[\"\"],\"tplCategory\":[\"crud\"],\"packageName\":[\"com.eden.system\"],\"moduleName\":[\"system\"],\"businessName\":[\"info\"],\"functionName\":[\"文件信息\"],\"params[parentMenuId]\":[\"1\"],\"params[parentMenuName]\":[\"系统管理\"],\"genType\":[\"0\"],\"genPath\":[\"/\"],\"subTableName\":[\"\"],\"params[treeCode]\":[\"\"],\"params[treeParentCode]\":[\"\"],\"params[treeName]\":[\"\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-09-07 11:31:52');
INSERT INTO `sys_oper_log` VALUES (185, '代码生成', 2, 'com.eden.generator.controller.GenController.editSave()', 'POST', 1, 'admin', '建筑学院', '/tool/gen/edit', '127.0.0.1', '内网IP', '{\"tableId\":[\"8\"],\"tableName\":[\"sys_file_info\"],\"tableComment\":[\"文件信息表\"],\"className\":[\"SysFileInfo\"],\"functionAuthor\":[\"jiaoyang\"],\"remark\":[\"\"],\"columns[0].columnId\":[\"73\"],\"columns[0].sort\":[\"1\"],\"columns[0].columnComment\":[\"文件id\"],\"columns[0].javaType\":[\"Long\"],\"columns[0].javaField\":[\"fileId\"],\"columns[0].isInsert\":[\"1\"],\"columns[0].queryType\":[\"EQ\"],\"columns[0].htmlType\":[\"input\"],\"columns[0].dictType\":[\"\"],\"columns[1].columnId\":[\"74\"],\"columns[1].sort\":[\"2\"],\"columns[1].columnComment\":[\"文件名称\"],\"columns[1].javaType\":[\"String\"],\"columns[1].javaField\":[\"fileName\"],\"columns[1].isInsert\":[\"1\"],\"columns[1].isEdit\":[\"1\"],\"columns[1].isList\":[\"1\"],\"columns[1].isQuery\":[\"1\"],\"columns[1].queryType\":[\"LIKE\"],\"columns[1].htmlType\":[\"input\"],\"columns[1].dictType\":[\"\"],\"columns[2].columnId\":[\"75\"],\"columns[2].sort\":[\"3\"],\"columns[2].columnComment\":[\"文件路径\"],\"columns[2].javaType\":[\"String\"],\"columns[2].javaField\":[\"filePath\"],\"columns[2].isInsert\":[\"1\"],\"columns[2].isEdit\":[\"1\"],\"columns[2].isList\":[\"1\"],\"columns[2].isQuery\":[\"1\"],\"columns[2].queryType\":[\"EQ\"],\"columns[2].htmlType\":[\"input\"],\"columns[2].dictType\":[\"\"],\"tplCategory\":[\"crud\"],\"packageName\":[\"com.eden.system\"],\"moduleName\":[\"system\"],\"businessName\":[\"info\"],\"functionName\":[\"文件信息\"],\"params[parentMenuId]\":[\"1\"],\"params[parentMenuName]\":[\"系统管理\"],\"genType\":[\"0\"],\"genPath\":[\"/\"],\"subTableName\":[\"\"],\"params[treeCode]\":[\"\"],\"params[treeParentCode]\":[\"\"],\"params[treeName]\":[\"\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-09-07 11:32:26');
INSERT INTO `sys_oper_log` VALUES (186, '代码生成', 8, 'com.eden.generator.controller.GenController.download()', 'GET', 1, 'admin', '建筑学院', '/tool/gen/download/sys_file_info', '127.0.0.1', '内网IP', NULL, 'null', 0, NULL, '2020-09-07 11:32:30');
INSERT INTO `sys_oper_log` VALUES (187, '代码生成', 2, 'com.eden.generator.controller.GenController.synchDb()', 'GET', 1, 'admin', '建筑学院', '/tool/gen/synchDb/task_picture', '127.0.0.1', '内网IP', NULL, '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-09-07 13:28:29');
INSERT INTO `sys_oper_log` VALUES (188, '代码生成', 2, 'com.eden.generator.controller.GenController.synchDb()', 'GET', 1, 'admin', '建筑学院', '/tool/gen/synchDb/task_video', '127.0.0.1', '内网IP', NULL, '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-09-07 13:31:56');
INSERT INTO `sys_oper_log` VALUES (189, '用户发布任务', 1, 'com.eden.task.controller.UserTaskController.addSave()', 'POST', 1, 'admin', '建筑学院', '/task/usertask/add', '127.0.0.1', '内网IP', '{\"fileId\":[\"10667_touxiang.jpg\"],\"initialPreview\":[\"[]\"],\"initialPreviewConfig\":[\"[]\"],\"initialPreviewThumbTags\":[\"[]\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-09-07 16:02:07');
INSERT INTO `sys_oper_log` VALUES (190, '用户发布任务', 1, 'com.eden.task.controller.UserTaskController.addSave()', 'POST', 1, 'admin', '建筑学院', '/task/usertask/add', '127.0.0.1', '内网IP', '{\"fileId\":[\"10667_touxiang.jpg\"],\"initialPreview\":[\"[]\"],\"initialPreviewConfig\":[\"[]\"],\"initialPreviewThumbTags\":[\"[]\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-09-08 08:57:16');
INSERT INTO `sys_oper_log` VALUES (191, '用户发布任务', 1, 'com.eden.task.controller.UserTaskController.addSave()', 'POST', 1, 'admin', '建筑学院', '/task/usertask/add', '127.0.0.1', '内网IP', '{\"fileId\":[\"10667_touxiang.jpg\"],\"initialPreview\":[\"[]\"],\"initialPreviewConfig\":[\"[]\"],\"initialPreviewThumbTags\":[\"[]\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-09-08 08:59:52');
INSERT INTO `sys_oper_log` VALUES (192, '用户发布任务', 1, 'com.eden.task.controller.UserTaskController.addSave()', 'POST', 1, 'admin', '建筑学院', '/task/usertask/add', '127.0.0.1', '内网IP', '{\"fileId\":[\"10667_touxiang.jpg\"],\"initialPreview\":[\"[]\"],\"initialPreviewConfig\":[\"[]\"],\"initialPreviewThumbTags\":[\"[]\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-09-08 09:01:03');
INSERT INTO `sys_oper_log` VALUES (193, '用户发布任务', 1, 'com.eden.task.controller.UserTaskController.addSave()', 'POST', 1, 'admin', '建筑学院', '/task/usertask/add', '127.0.0.1', '内网IP', '{\"fileId\":[\"10667_touxiang.jpg\"],\"initialPreview\":[\"[]\"],\"initialPreviewConfig\":[\"[]\"],\"initialPreviewThumbTags\":[\"[]\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-09-08 09:13:44');
INSERT INTO `sys_oper_log` VALUES (194, '用户发布任务', 1, 'com.eden.task.controller.UserTaskController.addSave()', 'POST', 1, 'admin', '建筑学院', '/task/usertask/add', '127.0.0.1', '内网IP', '{\"taskName\":[\"\"],\"taskDescription\":[\"\"],\"money\":[\"\"],\"phonenumber\":[\"\"],\"address\":[\"\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-09-08 09:13:49');
INSERT INTO `sys_oper_log` VALUES (195, '用户发布任务', 1, 'com.eden.task.controller.UserTaskController.addSave()', 'POST', 1, 'admin', '建筑学院', '/task/usertask/add', '127.0.0.1', '内网IP', '{\"taskName\":[\"2\"],\"taskDescription\":[\"2\"],\"money\":[\"2\"],\"phonenumber\":[\"2\"],\"address\":[\"2\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-09-08 10:22:00');
INSERT INTO `sys_oper_log` VALUES (196, '代码生成', 2, 'com.eden.generator.controller.GenController.synchDb()', 'GET', 1, 'admin', '建筑学院', '/tool/gen/synchDb/task_picture', '127.0.0.1', '内网IP', NULL, '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-09-08 11:25:40');
INSERT INTO `sys_oper_log` VALUES (197, '代码生成', 2, 'com.eden.generator.controller.GenController.synchDb()', 'GET', 1, 'admin', '建筑学院', '/tool/gen/synchDb/task_video', '127.0.0.1', '内网IP', NULL, '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-09-08 11:25:48');
INSERT INTO `sys_oper_log` VALUES (198, '代码生成', 2, 'com.eden.generator.controller.GenController.synchDb()', 'GET', 1, 'admin', '建筑学院', '/tool/gen/synchDb/task_video', '127.0.0.1', '内网IP', NULL, '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-09-08 13:07:46');
INSERT INTO `sys_oper_log` VALUES (199, '代码生成', 2, 'com.eden.generator.controller.GenController.synchDb()', 'GET', 1, 'admin', '建筑学院', '/tool/gen/synchDb/task_picture', '127.0.0.1', '内网IP', NULL, '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-09-08 13:07:50');
INSERT INTO `sys_oper_log` VALUES (200, '用户发布任务', 1, 'com.eden.task.controller.UserTaskController.addSave()', 'POST', 1, 'admin', '建筑学院', '/task/usertask/add', '127.0.0.1', '内网IP', '{\"taskName\":[\"\"],\"taskDescription\":[\"\"],\"money\":[\"\"],\"phonenumber\":[\"\"],\"address\":[\"\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-09-10 12:28:55');
INSERT INTO `sys_oper_log` VALUES (201, '用户发布任务', 1, 'com.eden.task.controller.UserTaskController.addSave()', 'POST', 1, 'admin', '建筑学院', '/task/usertask/add', '127.0.0.1', '内网IP', '{\"taskName\":[\"\"],\"taskDescription\":[\"\"],\"money\":[\"\"],\"phonenumber\":[\"\"],\"address\":[\"\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-09-10 12:31:48');
INSERT INTO `sys_oper_log` VALUES (202, '用户发布任务', 1, 'com.eden.task.controller.UserTaskController.addSave()', 'POST', 1, 'admin', '建筑学院', '/task/usertask/add', '127.0.0.1', '内网IP', '{\"taskName\":[\"\"],\"taskDescription\":[\"\"],\"money\":[\"\"],\"phonenumber\":[\"\"],\"address\":[\"\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-09-10 12:32:23');
INSERT INTO `sys_oper_log` VALUES (203, '用户发布任务', 1, 'com.eden.task.controller.UserTaskController.addSave()', 'POST', 1, 'admin', '建筑学院', '/task/usertask/add', '127.0.0.1', '内网IP', '{\"taskName\":[\"\"],\"taskDescription\":[\"\"],\"money\":[\"\"],\"phonenumber\":[\"\"],\"address\":[\"\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-09-10 12:33:55');
INSERT INTO `sys_oper_log` VALUES (204, '用户发布任务', 3, 'com.eden.task.controller.UserTaskController.remove()', 'POST', 1, 'admin', '建筑学院', '/task/usertask/remove', '127.0.0.1', '内网IP', '{\"ids\":[\"T0000000013\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-09-10 12:34:55');
INSERT INTO `sys_oper_log` VALUES (205, '用户发布任务', 3, 'com.eden.task.controller.UserTaskController.remove()', 'POST', 1, 'admin', '建筑学院', '/task/usertask/remove', '127.0.0.1', '内网IP', '{\"ids\":[\"T0000000012\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-09-10 12:34:57');
INSERT INTO `sys_oper_log` VALUES (206, '用户发布任务', 3, 'com.eden.task.controller.UserTaskController.remove()', 'POST', 1, 'admin', '建筑学院', '/task/usertask/remove', '127.0.0.1', '内网IP', '{\"ids\":[\"T0000000011\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-09-10 12:34:58');
INSERT INTO `sys_oper_log` VALUES (207, '用户发布任务', 3, 'com.eden.task.controller.UserTaskController.remove()', 'POST', 1, 'admin', '建筑学院', '/task/usertask/remove', '127.0.0.1', '内网IP', '{\"ids\":[\"T0000000010\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-09-10 12:35:00');
INSERT INTO `sys_oper_log` VALUES (208, '用户发布任务', 1, 'com.eden.task.controller.UserTaskController.addSave()', 'POST', 1, 'admin', '建筑学院', '/task/usertask/add', '127.0.0.1', '内网IP', '{\"taskName\":[\"\"],\"taskDescription\":[\"\"],\"money\":[\"\"],\"phonenumber\":[\"\"],\"address\":[\"\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-09-10 12:39:09');
INSERT INTO `sys_oper_log` VALUES (209, '用户发布任务', 1, 'com.eden.task.controller.UserTaskController.addSave()', 'POST', 1, 'admin', '建筑学院', '/task/usertask/add', '127.0.0.1', '内网IP', '{\"taskName\":[\"\"],\"taskDescription\":[\"\"],\"money\":[\"\"],\"phonenumber\":[\"\"],\"address\":[\"\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-09-10 12:40:48');
INSERT INTO `sys_oper_log` VALUES (210, '用户发布任务', 1, 'com.eden.task.controller.UserTaskController.addSave()', 'POST', 1, 'admin', '建筑学院', '/task/usertask/add', '127.0.0.1', '内网IP', '{\"taskName\":[\"\"],\"taskDescription\":[\"\"],\"money\":[\"\"],\"phonenumber\":[\"\"],\"address\":[\"\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-09-10 12:49:05');
INSERT INTO `sys_oper_log` VALUES (211, '用户发布任务', 1, 'com.eden.task.controller.UserTaskController.addSave()', 'POST', 1, 'admin', '建筑学院', '/task/usertask/add', '127.0.0.1', '内网IP', '{\"taskName\":[\"\"],\"taskDescription\":[\"\"],\"money\":[\"\"],\"phonenumber\":[\"\"],\"address\":[\"\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-09-10 13:01:14');
INSERT INTO `sys_oper_log` VALUES (212, '用户发布任务', 1, 'com.eden.task.controller.UserTaskController.addSave()', 'POST', 1, 'admin', '建筑学院', '/task/usertask/add', '127.0.0.1', '内网IP', '{\"taskName\":[\"\"],\"taskDescription\":[\"\"],\"money\":[\"\"],\"phonenumber\":[\"\"],\"address\":[\"\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-09-10 13:04:10');
INSERT INTO `sys_oper_log` VALUES (213, '用户发布任务', 1, 'com.eden.task.controller.UserTaskController.addSave()', 'POST', 1, 'admin', '建筑学院', '/task/usertask/add', '127.0.0.1', '内网IP', '{\"taskName\":[\"\"],\"taskDescription\":[\"\"],\"money\":[\"\"],\"phonenumber\":[\"\"],\"address\":[\"\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-09-10 13:43:47');
INSERT INTO `sys_oper_log` VALUES (214, '用户发布任务', 1, 'com.eden.task.controller.UserTaskController.addSave()', 'POST', 1, 'admin', '建筑学院', '/task/usertask/add', '127.0.0.1', '内网IP', '{\"taskName\":[\"\"],\"taskDescription\":[\"\"],\"money\":[\"\"],\"phonenumber\":[\"\"],\"address\":[\"\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-09-10 13:52:59');
INSERT INTO `sys_oper_log` VALUES (215, '用户发布任务', 1, 'com.eden.task.controller.UserTaskController.addSave()', 'POST', 1, 'admin', '建筑学院', '/task/usertask/add', '127.0.0.1', '内网IP', '{\"taskName\":[\"\"],\"taskDescription\":[\"\"],\"money\":[\"\"],\"phonenumber\":[\"\"],\"address\":[\"\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-09-10 13:53:48');
INSERT INTO `sys_oper_log` VALUES (216, '用户发布任务', 1, 'com.eden.task.controller.UserTaskController.addSave()', 'POST', 1, 'admin', '建筑学院', '/task/usertask/add', '127.0.0.1', '内网IP', '{\"taskName\":[\"3\"],\"money\":[\"3\"],\"phonenumber\":[\"3\"],\"address\":[\"3\"],\"taskDescription\":[\"3\"]}', 'null', 1, '\r\n### Error updating database.  Cause: java.sql.SQLException: Field \'task_id\' doesn\'t have a default value\r\n### The error may involve com.eden.task.mapper.UserTaskMapper.insertUserTask-Inline\r\n### The error occurred while setting parameters\r\n### SQL: insert into user_task          ( task_name,             task_description,             task_status,             money,             phonenumber,             address,             create_by,             create_time )           values ( ?,             ?,             ?,             ?,             ?,             ?,             ?,             ? )\r\n### Cause: java.sql.SQLException: Field \'task_id\' doesn\'t have a default value\n; Field \'task_id\' doesn\'t have a default value; nested exception is java.sql.SQLException: Field \'task_id\' doesn\'t have a default value', '2020-09-11 13:10:19');
INSERT INTO `sys_oper_log` VALUES (217, '用户发布任务', 1, 'com.eden.task.controller.UserTaskController.addSave()', 'POST', 1, 'admin', '建筑学院', '/task/usertask/add', '127.0.0.1', '内网IP', '{\"taskName\":[\"3\"],\"money\":[\"3\"],\"phonenumber\":[\"3\"],\"address\":[\"3\"],\"taskDescription\":[\"3\"]}', 'null', 1, '\r\n### Error updating database.  Cause: java.sql.SQLException: Field \'task_id\' doesn\'t have a default value\r\n### The error may involve com.eden.task.mapper.UserTaskMapper.insertUserTask-Inline\r\n### The error occurred while setting parameters\r\n### SQL: insert into user_task          ( task_name,             task_description,             task_status,             money,             phonenumber,             address,             create_by,             create_time )           values ( ?,             ?,             ?,             ?,             ?,             ?,             ?,             ? )\r\n### Cause: java.sql.SQLException: Field \'task_id\' doesn\'t have a default value\n; Field \'task_id\' doesn\'t have a default value; nested exception is java.sql.SQLException: Field \'task_id\' doesn\'t have a default value', '2020-09-11 13:15:51');
INSERT INTO `sys_oper_log` VALUES (218, '用户发布任务', 1, 'com.eden.task.controller.UserTaskController.addSave()', 'POST', 1, 'admin', '建筑学院', '/task/usertask/add', '127.0.0.1', '内网IP', '{\"taskName\":[\"3\"],\"money\":[\"3\"],\"phonenumber\":[\"3\"],\"address\":[\"3\"],\"taskDescription\":[\"3\"]}', 'null', 1, '\r\n### Error updating database.  Cause: java.sql.SQLException: Field \'task_id\' doesn\'t have a default value\r\n### The error may involve com.eden.task.mapper.UserTaskMapper.insertUserTask-Inline\r\n### The error occurred while setting parameters\r\n### SQL: insert into user_task          ( task_name,             task_description,             task_status,             money,             phonenumber,             address,             create_by,             create_time )           values ( ?,             ?,             ?,             ?,             ?,             ?,             ?,             ? )\r\n### Cause: java.sql.SQLException: Field \'task_id\' doesn\'t have a default value\n; Field \'task_id\' doesn\'t have a default value; nested exception is java.sql.SQLException: Field \'task_id\' doesn\'t have a default value', '2020-09-11 13:17:54');
INSERT INTO `sys_oper_log` VALUES (219, '用户发布任务', 1, 'com.eden.task.controller.UserTaskController.addSave()', 'POST', 1, 'admin', '建筑学院', '/task/usertask/add', '127.0.0.1', '内网IP', '{\"taskName\":[\"4\"],\"money\":[\"4\"],\"phonenumber\":[\"4\"],\"address\":[\"4\"],\"taskDescription\":[\"4\"]}', 'null', 1, '\r\n### Error updating database.  Cause: java.sql.SQLException: Field \'task_id\' doesn\'t have a default value\r\n### The error may involve com.eden.task.mapper.UserTaskMapper.insertUserTask-Inline\r\n### The error occurred while setting parameters\r\n### SQL: insert into user_task          ( task_name,             task_description,             task_status,             money,             phonenumber,             address,             create_by,             create_time )           values ( ?,             ?,             ?,             ?,             ?,             ?,             ?,             ? )\r\n### Cause: java.sql.SQLException: Field \'task_id\' doesn\'t have a default value\n; Field \'task_id\' doesn\'t have a default value; nested exception is java.sql.SQLException: Field \'task_id\' doesn\'t have a default value', '2020-09-11 13:19:36');
INSERT INTO `sys_oper_log` VALUES (220, '用户发布任务', 1, 'com.eden.task.controller.UserTaskController.addSave()', 'POST', 1, 'admin', '建筑学院', '/task/usertask/add', '127.0.0.1', '内网IP', '{\"taskName\":[\"2\"],\"money\":[\"2\"],\"phonenumber\":[\"2\"],\"address\":[\"2\"],\"taskDescription\":[\"2\"]}', 'null', 1, '\r\n### Error updating database.  Cause: java.sql.SQLException: Field \'task_id\' doesn\'t have a default value\r\n### The error may involve com.eden.task.mapper.UserTaskMapper.insertUserTask-Inline\r\n### The error occurred while setting parameters\r\n### SQL: insert into user_task          ( task_name,             task_description,             task_status,             money,             phonenumber,             address,             create_by,             create_time )           values ( ?,             ?,             ?,             ?,             ?,             ?,             ?,             ? )\r\n### Cause: java.sql.SQLException: Field \'task_id\' doesn\'t have a default value\n; Field \'task_id\' doesn\'t have a default value; nested exception is java.sql.SQLException: Field \'task_id\' doesn\'t have a default value', '2020-09-11 13:21:51');
INSERT INTO `sys_oper_log` VALUES (221, '用户发布任务', 1, 'com.eden.task.controller.UserTaskController.addSave()', 'POST', 1, 'admin', '建筑学院', '/task/usertask/add', '127.0.0.1', '内网IP', '{\"taskName\":[\"3\"],\"money\":[\"3\"],\"phonenumber\":[\"3\"],\"address\":[\"3\"],\"taskDescription\":[\"3\"]}', 'null', 1, '\r\n### Error updating database.  Cause: java.sql.SQLException: Field \'task_id\' doesn\'t have a default value\r\n### The error may involve com.eden.task.mapper.UserTaskMapper.insertUserTask-Inline\r\n### The error occurred while setting parameters\r\n### SQL: insert into user_task          ( task_name,             task_description,             task_status,             money,             phonenumber,             address,             create_by,             create_time )           values ( ?,             ?,             ?,             ?,             ?,             ?,             ?,             ? )\r\n### Cause: java.sql.SQLException: Field \'task_id\' doesn\'t have a default value\n; Field \'task_id\' doesn\'t have a default value; nested exception is java.sql.SQLException: Field \'task_id\' doesn\'t have a default value', '2020-09-11 13:22:46');
INSERT INTO `sys_oper_log` VALUES (222, '用户发布任务', 1, 'com.eden.task.controller.UserTaskController.addSave()', 'POST', 1, 'admin', '建筑学院', '/task/usertask/add', '127.0.0.1', '内网IP', '{\"taskName\":[\"2\"],\"money\":[\"2\"],\"phonenumber\":[\"2\"],\"address\":[\"2\"],\"taskDescription\":[\"2\"]}', 'null', 1, '\r\n### Error updating database.  Cause: java.sql.SQLException: Field \'task_id\' doesn\'t have a default value\r\n### The error may involve com.eden.task.mapper.UserTaskMapper.insertUserTask-Inline\r\n### The error occurred while setting parameters\r\n### SQL: insert into user_task          ( task_name,             task_description,             task_status,             money,             phonenumber,             address,             create_by,             create_time )           values ( ?,             ?,             ?,             ?,             ?,             ?,             ?,             ? )\r\n### Cause: java.sql.SQLException: Field \'task_id\' doesn\'t have a default value\n; Field \'task_id\' doesn\'t have a default value; nested exception is java.sql.SQLException: Field \'task_id\' doesn\'t have a default value', '2020-09-11 13:25:08');
INSERT INTO `sys_oper_log` VALUES (223, '用户发布任务', 1, 'com.eden.task.controller.UserTaskController.addSave()', 'POST', 1, 'admin', '建筑学院', '/task/usertask/add', '127.0.0.1', '内网IP', '{\"taskName\":[\"2\"],\"money\":[\"2\"],\"phonenumber\":[\"2\"],\"address\":[\"2\"],\"taskDescription\":[\"2\"]}', 'null', 1, '\r\n### Error updating database.  Cause: java.sql.SQLException: Field \'task_id\' doesn\'t have a default value\r\n### The error may involve com.eden.task.mapper.UserTaskMapper.insertUserTask-Inline\r\n### The error occurred while setting parameters\r\n### SQL: insert into user_task          ( task_name,             task_description,             task_status,             money,             phonenumber,             address,             create_by,             create_time )           values ( ?,             ?,             ?,             ?,             ?,             ?,             ?,             ? )\r\n### Cause: java.sql.SQLException: Field \'task_id\' doesn\'t have a default value\n; Field \'task_id\' doesn\'t have a default value; nested exception is java.sql.SQLException: Field \'task_id\' doesn\'t have a default value', '2020-09-11 13:26:37');
INSERT INTO `sys_oper_log` VALUES (224, '用户发布任务', 1, 'com.eden.task.controller.UserTaskController.addSave()', 'POST', 1, 'admin', '建筑学院', '/task/usertask/add', '127.0.0.1', '内网IP', '{\"taskName\":[\"2\"],\"money\":[\"2\"],\"phonenumber\":[\"2\"],\"address\":[\"2\"],\"taskDescription\":[\"2\"]}', 'null', 1, '\r\n### Error updating database.  Cause: java.sql.SQLException: Field \'task_id\' doesn\'t have a default value\r\n### The error may involve com.eden.task.mapper.UserTaskMapper.insertUserTask-Inline\r\n### The error occurred while setting parameters\r\n### SQL: insert into user_task          ( task_name,             task_description,             task_status,             money,             phonenumber,             address,             create_by,             create_time )           values ( ?,             ?,             ?,             ?,             ?,             ?,             ?,             ? )\r\n### Cause: java.sql.SQLException: Field \'task_id\' doesn\'t have a default value\n; Field \'task_id\' doesn\'t have a default value; nested exception is java.sql.SQLException: Field \'task_id\' doesn\'t have a default value', '2020-09-11 13:33:40');
INSERT INTO `sys_oper_log` VALUES (225, '用户发布任务', 1, 'com.eden.task.controller.UserTaskController.addSave()', 'POST', 1, 'admin', '建筑学院', '/task/usertask/add', '127.0.0.1', '内网IP', '{\"taskName\":[\"2\"],\"money\":[\"2\"],\"phonenumber\":[\"2\"],\"address\":[\"2\"],\"taskDescription\":[\"2\"]}', 'null', 1, '\r\n### Error updating database.  Cause: java.sql.SQLException: Field \'task_id\' doesn\'t have a default value\r\n### The error may involve com.eden.task.mapper.UserTaskMapper.insertUserTask-Inline\r\n### The error occurred while setting parameters\r\n### SQL: insert into user_task          ( task_name,             task_description,             task_status,             money,             phonenumber,             address,             create_by,             create_time )           values ( ?,             ?,             ?,             ?,             ?,             ?,             ?,             ? )\r\n### Cause: java.sql.SQLException: Field \'task_id\' doesn\'t have a default value\n; Field \'task_id\' doesn\'t have a default value; nested exception is java.sql.SQLException: Field \'task_id\' doesn\'t have a default value', '2020-09-11 13:36:26');
INSERT INTO `sys_oper_log` VALUES (226, '用户发布任务', 1, 'com.eden.task.controller.UserTaskController.addSave()', 'POST', 1, 'admin', '建筑学院', '/task/usertask/add', '127.0.0.1', '内网IP', '{\"taskId\":[\"Tcfd21599802641411\"],\"taskName\":[\"3\"],\"money\":[\"3\"],\"phonenumber\":[\"3\"],\"address\":[\"3\"],\"taskDescription\":[\"3\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-09-11 13:38:06');
INSERT INTO `sys_oper_log` VALUES (227, '用户发布任务', 1, 'com.eden.task.controller.UserTaskController.addSave()', 'POST', 1, 'admin', '建筑学院', '/task/usertask/add', '127.0.0.1', '内网IP', '{\"taskId\":[\"T337d1599802696940\"],\"taskName\":[\"4\"],\"money\":[\"4\"],\"phonenumber\":[\"4\"],\"address\":[\"4\"],\"taskDescription\":[\"4\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-09-11 13:38:48');
INSERT INTO `sys_oper_log` VALUES (228, '用户发布任务', 1, 'com.eden.task.controller.UserTaskController.addSave()', 'POST', 1, 'admin', '建筑学院', '/task/usertask/add', '127.0.0.1', '内网IP', '{\"taskId\":[\"Td4ac1599803463498\"],\"taskName\":[\"测试01\"],\"money\":[\"20\"],\"phonenumber\":[\"17761628992\"],\"address\":[\"河南省南阳市\"],\"taskDescription\":[\"用于测试\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-09-11 13:51:57');
INSERT INTO `sys_oper_log` VALUES (229, '代码生成', 8, 'com.eden.generator.controller.GenController.download()', 'GET', 1, 'admin', '建筑学院', '/tool/gen/download/task_picture', '127.0.0.1', '内网IP', NULL, 'null', 0, NULL, '2020-09-11 14:41:40');
INSERT INTO `sys_oper_log` VALUES (230, '用户发布任务', 1, 'com.eden.task.controller.UserTaskController.addSave()', 'POST', 1, 'admin', '建筑学院', '/task/usertask/add', '127.0.0.1', '内网IP', '{\"taskId\":[\"T5ec31599807415072\"],\"taskName\":[\"测试01\"],\"money\":[\"20\"],\"phonenumber\":[\"17761628992\"],\"address\":[\"河南省南阳市\"],\"taskDescription\":[\"用于测试\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-09-11 14:57:57');
INSERT INTO `sys_oper_log` VALUES (231, '用户发布任务', 1, 'com.eden.task.controller.UserTaskController.addSave()', 'POST', 1, 'admin', '建筑学院', '/task/usertask/add', '127.0.0.1', '内网IP', '{\"taskId\":[\"T95e11599811137876\"],\"taskName\":[\"测试01\"],\"money\":[\"21\"],\"phonenumber\":[\"17761628992\"],\"address\":[\"河南省南阳市\"],\"taskDescription\":[\"用于测试\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-09-11 16:00:00');
INSERT INTO `sys_oper_log` VALUES (232, '用户发布任务', 1, 'com.eden.task.controller.UserTaskController.addSave()', 'POST', 1, 'admin', '建筑学院', '/task/usertask/add', '127.0.0.1', '内网IP', '{\"taskId\":[\"T5ad41599812393783\"],\"taskName\":[\"测试01\"],\"money\":[\"20\"],\"phonenumber\":[\"17761628999\"],\"address\":[\"河南省南阳市\"],\"taskDescription\":[\"用于测试\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-09-11 16:20:52');
INSERT INTO `sys_oper_log` VALUES (233, '代码生成', 6, 'com.eden.generator.controller.GenController.importTableSave()', 'POST', 1, 'admin', '建筑学院', '/tool/gen/importTable', '127.0.0.1', '内网IP', '{\"tables\":[\"check_opinion\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-09-14 12:44:54');
INSERT INTO `sys_oper_log` VALUES (234, '代码生成', 2, 'com.eden.generator.controller.GenController.editSave()', 'POST', 1, 'admin', '建筑学院', '/tool/gen/edit', '127.0.0.1', '内网IP', '{\"tableId\":[\"9\"],\"tableName\":[\"check_opinion\"],\"tableComment\":[\"审核任务回退原因表\"],\"className\":[\"CheckOpinion\"],\"functionAuthor\":[\"jiaoyang\"],\"remark\":[\"\"],\"columns[0].columnId\":[\"82\"],\"columns[0].sort\":[\"1\"],\"columns[0].columnComment\":[\"质检退回ID\"],\"columns[0].javaType\":[\"String\"],\"columns[0].javaField\":[\"checkOpinionId\"],\"columns[0].isInsert\":[\"1\"],\"columns[0].queryType\":[\"EQ\"],\"columns[0].htmlType\":[\"input\"],\"columns[0].dictType\":[\"\"],\"columns[1].columnId\":[\"83\"],\"columns[1].sort\":[\"2\"],\"columns[1].columnComment\":[\"质检不通过原因\"],\"columns[1].javaType\":[\"String\"],\"columns[1].javaField\":[\"content\"],\"columns[1].isInsert\":[\"1\"],\"columns[1].isEdit\":[\"1\"],\"columns[1].isList\":[\"1\"],\"columns[1].isQuery\":[\"1\"],\"columns[1].queryType\":[\"EQ\"],\"columns[1].isRequired\":[\"1\"],\"columns[1].htmlType\":[\"summernote\"],\"columns[1].dictType\":[\"\"],\"columns[2].columnId\":[\"84\"],\"columns[2].sort\":[\"3\"],\"columns[2].columnComment\":[\"该订单的下一个流程\"],\"columns[2].javaType\":[\"String\"],\"columns[2].javaField\":[\"nextFlow\"],\"columns[2].isInsert\":[\"1\"],\"columns[2].isEdit\":[\"1\"],\"columns[2].isList\":[\"1\"],\"columns[2].isQuery\":[\"1\"],\"columns[2].queryType\":[\"EQ\"],\"columns[2].isRequired\":[\"1\"],\"columns[2].htmlType\":[\"input\"],\"columns[2].dictType\":[\"\"],\"columns[3].columnId\":[\"85\"],\"columns[3].sort\":[\"4\"],\"columns[3].columnComment\":[\"创建者\"],\"columns[3].javaType\":[\"String\"],\"columns[3].javaField\":[\"createBy\"],\"columns[3].isInsert\":[\"1\"],\"columns[3].queryType\":[\"EQ\"],\"columns[3].htmlType\":[\"input\"],\"columns[3].dictType\":[\"\"],\"columns[4].columnId\":[\"86\"],\"columns[4].sort\":[\"5\"],\"columns[4].columnComment\":[\"创建时间\"],\"columns[4].javaType\":[\"Date\"],\"columns[4].javaField\":[\"createTime\"],\"columns[4].isInsert\":[\"1\"],\"columns[4].queryType\":[\"EQ\"],\"columns[4].htmlType\":[\"datetime\"],\"columns[4].dictType\":[\"\"],\"columns[5].columnId\":[\"87\"],\"columns[5].sort\":[\"6\"],\"columns[5].columnComment\":[\"更新者\"],\"columns[5].javaType\":[\"String\"],\"columns[5].javaField\":[\"updateBy\"],\"columns[5].isInsert\":[\"1\"],\"columns[5].isEdit\":[\"1\"],\"columns[5].queryType\":[\"', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-09-14 12:46:21');
INSERT INTO `sys_oper_log` VALUES (235, '代码生成', 2, 'com.eden.generator.controller.GenController.editSave()', 'POST', 1, 'admin', '建筑学院', '/tool/gen/edit', '127.0.0.1', '内网IP', '{\"tableId\":[\"9\"],\"tableName\":[\"check_opinion\"],\"tableComment\":[\"审核任务回退原因表\"],\"className\":[\"CheckOpinion\"],\"functionAuthor\":[\"jiaoyang\"],\"remark\":[\"\"],\"columns[0].columnId\":[\"82\"],\"columns[0].sort\":[\"1\"],\"columns[0].columnComment\":[\"质检退回ID\"],\"columns[0].javaType\":[\"String\"],\"columns[0].javaField\":[\"checkOpinionId\"],\"columns[0].isInsert\":[\"1\"],\"columns[0].queryType\":[\"EQ\"],\"columns[0].htmlType\":[\"input\"],\"columns[0].dictType\":[\"\"],\"columns[1].columnId\":[\"83\"],\"columns[1].sort\":[\"2\"],\"columns[1].columnComment\":[\"质检不通过原因\"],\"columns[1].javaType\":[\"String\"],\"columns[1].javaField\":[\"content\"],\"columns[1].isInsert\":[\"1\"],\"columns[1].isEdit\":[\"1\"],\"columns[1].isList\":[\"1\"],\"columns[1].isQuery\":[\"1\"],\"columns[1].queryType\":[\"EQ\"],\"columns[1].isRequired\":[\"1\"],\"columns[1].htmlType\":[\"summernote\"],\"columns[1].dictType\":[\"\"],\"columns[2].columnId\":[\"84\"],\"columns[2].sort\":[\"3\"],\"columns[2].columnComment\":[\"该订单的下一个流程\"],\"columns[2].javaType\":[\"String\"],\"columns[2].javaField\":[\"nextFlow\"],\"columns[2].isInsert\":[\"1\"],\"columns[2].isEdit\":[\"1\"],\"columns[2].isList\":[\"1\"],\"columns[2].isQuery\":[\"1\"],\"columns[2].queryType\":[\"EQ\"],\"columns[2].isRequired\":[\"1\"],\"columns[2].htmlType\":[\"input\"],\"columns[2].dictType\":[\"\"],\"columns[3].columnId\":[\"85\"],\"columns[3].sort\":[\"4\"],\"columns[3].columnComment\":[\"创建者\"],\"columns[3].javaType\":[\"String\"],\"columns[3].javaField\":[\"createBy\"],\"columns[3].isInsert\":[\"1\"],\"columns[3].queryType\":[\"EQ\"],\"columns[3].htmlType\":[\"input\"],\"columns[3].dictType\":[\"\"],\"columns[4].columnId\":[\"86\"],\"columns[4].sort\":[\"5\"],\"columns[4].columnComment\":[\"创建时间\"],\"columns[4].javaType\":[\"Date\"],\"columns[4].javaField\":[\"createTime\"],\"columns[4].isInsert\":[\"1\"],\"columns[4].queryType\":[\"EQ\"],\"columns[4].htmlType\":[\"datetime\"],\"columns[4].dictType\":[\"\"],\"columns[5].columnId\":[\"87\"],\"columns[5].sort\":[\"6\"],\"columns[5].columnComment\":[\"更新者\"],\"columns[5].javaType\":[\"String\"],\"columns[5].javaField\":[\"updateBy\"],\"columns[5].isInsert\":[\"1\"],\"columns[5].isEdit\":[\"1\"],\"columns[5].queryType\":[\"', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-09-14 12:54:33');
INSERT INTO `sys_oper_log` VALUES (236, '代码生成', 2, 'com.eden.generator.controller.GenController.editSave()', 'POST', 1, 'admin', '建筑学院', '/tool/gen/edit', '127.0.0.1', '内网IP', '{\"tableId\":[\"9\"],\"tableName\":[\"check_opinion\"],\"tableComment\":[\"审核任务回退原因表\"],\"className\":[\"CheckOpinion\"],\"functionAuthor\":[\"jiaoyang\"],\"remark\":[\"\"],\"columns[0].columnId\":[\"82\"],\"columns[0].sort\":[\"1\"],\"columns[0].columnComment\":[\"质检退回ID\"],\"columns[0].javaType\":[\"String\"],\"columns[0].javaField\":[\"checkOpinionId\"],\"columns[0].isInsert\":[\"1\"],\"columns[0].queryType\":[\"EQ\"],\"columns[0].htmlType\":[\"input\"],\"columns[0].dictType\":[\"\"],\"columns[1].columnId\":[\"83\"],\"columns[1].sort\":[\"2\"],\"columns[1].columnComment\":[\"质检不通过原因\"],\"columns[1].javaType\":[\"String\"],\"columns[1].javaField\":[\"content\"],\"columns[1].isInsert\":[\"1\"],\"columns[1].isEdit\":[\"1\"],\"columns[1].isList\":[\"1\"],\"columns[1].isQuery\":[\"1\"],\"columns[1].queryType\":[\"EQ\"],\"columns[1].isRequired\":[\"1\"],\"columns[1].htmlType\":[\"summernote\"],\"columns[1].dictType\":[\"\"],\"columns[2].columnId\":[\"84\"],\"columns[2].sort\":[\"3\"],\"columns[2].columnComment\":[\"该订单的下一个流程\"],\"columns[2].javaType\":[\"String\"],\"columns[2].javaField\":[\"nextFlow\"],\"columns[2].isInsert\":[\"1\"],\"columns[2].isEdit\":[\"1\"],\"columns[2].isList\":[\"1\"],\"columns[2].isQuery\":[\"1\"],\"columns[2].queryType\":[\"EQ\"],\"columns[2].isRequired\":[\"1\"],\"columns[2].htmlType\":[\"input\"],\"columns[2].dictType\":[\"\"],\"columns[3].columnId\":[\"85\"],\"columns[3].sort\":[\"4\"],\"columns[3].columnComment\":[\"创建者\"],\"columns[3].javaType\":[\"String\"],\"columns[3].javaField\":[\"createBy\"],\"columns[3].isInsert\":[\"1\"],\"columns[3].queryType\":[\"EQ\"],\"columns[3].htmlType\":[\"input\"],\"columns[3].dictType\":[\"\"],\"columns[4].columnId\":[\"86\"],\"columns[4].sort\":[\"5\"],\"columns[4].columnComment\":[\"创建时间\"],\"columns[4].javaType\":[\"Date\"],\"columns[4].javaField\":[\"createTime\"],\"columns[4].isInsert\":[\"1\"],\"columns[4].queryType\":[\"EQ\"],\"columns[4].htmlType\":[\"datetime\"],\"columns[4].dictType\":[\"\"],\"columns[5].columnId\":[\"87\"],\"columns[5].sort\":[\"6\"],\"columns[5].columnComment\":[\"更新者\"],\"columns[5].javaType\":[\"String\"],\"columns[5].javaField\":[\"updateBy\"],\"columns[5].isInsert\":[\"1\"],\"columns[5].isEdit\":[\"1\"],\"columns[5].queryType\":[\"', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-09-14 12:55:22');
INSERT INTO `sys_oper_log` VALUES (237, '代码生成', 8, 'com.eden.generator.controller.GenController.download()', 'GET', 1, 'admin', '建筑学院', '/tool/gen/download/check_opinion', '127.0.0.1', '内网IP', NULL, 'null', 0, NULL, '2020-09-14 12:55:28');
INSERT INTO `sys_oper_log` VALUES (238, '用户发布任务', 3, 'com.eden.task.controller.UserTaskController.checkTaskNextFlow()', 'GET', 1, 'admin', '建筑学院', '/task/usertask/checkTaskNextFlow', '127.0.0.1', '内网IP', '{\"taskId\":[\"T5ad41599812393783\"]}', '\"task/usertask/checkTaskNextFlow\"', 0, NULL, '2020-09-14 14:17:37');
INSERT INTO `sys_oper_log` VALUES (239, '用户发布任务', 3, 'com.eden.task.controller.UserTaskController.checkTaskNextFlow()', 'GET', 1, 'admin', '建筑学院', '/task/usertask/checkTaskNextFlow', '127.0.0.1', '内网IP', '{\"taskId\":[\"T5ad41599812393783\"]}', '\"task/usertask/checkTaskNextFlow\"', 0, NULL, '2020-09-14 14:21:01');
INSERT INTO `sys_oper_log` VALUES (240, '用户发布任务', 3, 'com.eden.task.controller.UserTaskController.checkTaskNextFlow()', 'GET', 1, 'admin', '建筑学院', '/task/usertask/checkTaskNextFlow', '127.0.0.1', '内网IP', '{\"taskId\":[\"T5ad41599812393783\"]}', '\"task/usertask/checkTaskNextFlow\"', 0, NULL, '2020-09-14 14:22:51');
INSERT INTO `sys_oper_log` VALUES (241, '用户发布任务', 3, 'com.eden.task.controller.UserTaskController.checkUserTask()', 'POST', 1, 'admin', '建筑学院', '/task/usertask/checkUserTask', '127.0.0.1', '内网IP', '{\"taskId\":[\"T5ad41599812393783\"],\"taskStatus\":[\"1\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-09-14 14:24:21');
INSERT INTO `sys_oper_log` VALUES (242, '用户发布任务', 1, 'com.eden.task.controller.UserTaskController.addSave()', 'POST', 1, 'admin', '建筑学院', '/task/usertask/add', '127.0.0.1', '内网IP', '{\"taskId\":[\"Taf381600065254499\"],\"taskName\":[\"测试02\"],\"money\":[\"32\"],\"phonenumber\":[\"17761628997\"],\"address\":[\"南阳理工学院\"],\"taskDescription\":[\"用于测试02\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-09-14 14:35:40');
INSERT INTO `sys_oper_log` VALUES (243, '用户发布任务', 3, 'com.eden.task.controller.UserTaskController.checkTaskNextFlow()', 'GET', 1, 'admin', '建筑学院', '/task/usertask/checkTaskNextFlow', '127.0.0.1', '内网IP', '{\"taskId\":[\"Taf381600065254499\"]}', '\"task/usertask/checkTaskNextFlow\"', 0, NULL, '2020-09-14 14:38:49');
INSERT INTO `sys_oper_log` VALUES (244, '用户发布任务', 3, 'com.eden.task.controller.UserTaskController.checkTaskNextFlow()', 'GET', 1, 'admin', '建筑学院', '/task/usertask/checkTaskNextFlow', '127.0.0.1', '内网IP', '{\"taskId\":[\"Taf381600065254499\"]}', '\"task/usertask/checkTaskNextFlow\"', 0, NULL, '2020-09-14 14:48:24');
INSERT INTO `sys_oper_log` VALUES (245, '用户发布任务', 3, 'com.eden.task.controller.UserTaskController.checkTaskNextFlow()', 'GET', 1, 'admin', '建筑学院', '/task/usertask/checkTaskNextFlow', '127.0.0.1', '内网IP', '{\"taskId\":[\"Taf381600065254499\"]}', '\"task/usertask/checkTaskNextFlow\"', 0, NULL, '2020-09-14 14:49:54');
INSERT INTO `sys_oper_log` VALUES (246, '用户发布任务', 3, 'com.eden.task.controller.UserTaskController.checkUserTask()', 'POST', 1, 'admin', '建筑学院', '/task/usertask/checkUserTask', '127.0.0.1', '内网IP', '{\"taskId\":[\"Taf381600065254499\"],\"checkStatus\":[\"2\"],\"checkOpinion\":[\"C00000000001\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-09-14 14:49:56');
INSERT INTO `sys_oper_log` VALUES (247, '用户发布任务', 3, 'com.eden.task.controller.UserTaskController.checkTaskNextFlow()', 'GET', 1, 'admin', '建筑学院', '/task/usertask/checkTaskNextFlow', '127.0.0.1', '内网IP', '{\"taskId\":[\"Taf381600065254499\"]}', '\"task/usertask/checkTaskNextFlow\"', 0, NULL, '2020-09-14 14:50:38');
INSERT INTO `sys_oper_log` VALUES (248, '用户发布任务', 3, 'com.eden.task.controller.UserTaskController.checkUserTask()', 'POST', 1, 'admin', '建筑学院', '/task/usertask/checkUserTask', '127.0.0.1', '内网IP', '{\"taskId\":[\"Taf381600065254499\"],\"checkStatus\":[\"2\"],\"checkOpinion\":[\"C00000000001\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-09-14 14:54:41');
INSERT INTO `sys_oper_log` VALUES (249, '用户发布任务', 3, 'com.eden.task.controller.UserTaskController.checkTaskNextFlow()', 'GET', 1, 'admin', '建筑学院', '/task/usertask/checkTaskNextFlow', '127.0.0.1', '内网IP', '{\"taskId\":[\"Taf381600065254499\"]}', '\"task/usertask/checkTaskNextFlow\"', 0, NULL, '2020-09-14 14:55:06');
INSERT INTO `sys_oper_log` VALUES (250, '用户发布任务', 3, 'com.eden.task.controller.UserTaskController.checkUserTask()', 'POST', 1, 'admin', '建筑学院', '/task/usertask/checkUserTask', '127.0.0.1', '内网IP', '{\"taskId\":[\"Taf381600065254499\"],\"checkStatus\":[\"2\"],\"checkOpinion\":[\"C00000000001\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-09-14 14:55:28');
INSERT INTO `sys_oper_log` VALUES (251, '用户发布任务', 3, 'com.eden.task.controller.UserTaskController.checkTaskNextFlow()', 'GET', 1, 'admin', '建筑学院', '/task/usertask/checkTaskNextFlow', '127.0.0.1', '内网IP', '{\"taskId\":[\"Taf381600065254499\"]}', '\"task/usertask/checkTaskNextFlow\"', 0, NULL, '2020-09-14 14:55:59');
INSERT INTO `sys_oper_log` VALUES (252, '用户发布任务', 3, 'com.eden.task.controller.UserTaskController.checkUserTask()', 'POST', 1, 'admin', '建筑学院', '/task/usertask/checkUserTask', '127.0.0.1', '内网IP', '{\"taskId\":[\"Taf381600065254499\"],\"taskStatus\":[\"2\"],\"checkOpinion\":[\"C00000000001\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-09-14 14:56:13');
INSERT INTO `sys_oper_log` VALUES (253, '菜单管理', 1, 'com.eden.web.controller.system.SysMenuController.addSave()', 'POST', 1, 'admin', '建筑学院', '/system/menu/add', '127.0.0.1', '内网IP', '{\"parentId\":[\"3\"],\"menuType\":[\"C\"],\"menuName\":[\"客户端接口\"],\"url\":[\"/api/swagger\"],\"target\":[\"menuItem\"],\"perms\":[\"api:swagger:view\"],\"orderNum\":[\"4\"],\"icon\":[\"\"],\"visible\":[\"0\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-09-15 15:08:31');
INSERT INTO `sys_oper_log` VALUES (254, '菜单管理', 3, 'com.eden.web.controller.system.SysMenuController.remove()', 'GET', 1, 'admin', '建筑学院', '/system/menu/remove/1069', '127.0.0.1', '内网IP', NULL, '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-09-16 09:26:44');
INSERT INTO `sys_oper_log` VALUES (255, '菜单管理', 2, 'com.eden.web.controller.system.SysMenuController.editSave()', 'POST', 1, 'admin', '建筑学院', '/system/menu/edit', '127.0.0.1', '内网IP', '{\"menuId\":[\"115\"],\"parentId\":[\"3\"],\"menuType\":[\"C\"],\"menuName\":[\"客户端接口\"],\"url\":[\"/tool/swagger\"],\"target\":[\"menuItem\"],\"perms\":[\"tool:swagger:view\"],\"orderNum\":[\"3\"],\"icon\":[\"#\"],\"visible\":[\"0\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-09-16 09:26:55');
INSERT INTO `sys_oper_log` VALUES (256, '代码生成', 6, 'com.eden.generator.controller.GenController.importTableSave()', 'POST', 1, 'admin', '建筑学院', '/tool/gen/importTable', '127.0.0.1', '内网IP', '{\"tables\":[\"school_user_role\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-09-16 13:38:27');
INSERT INTO `sys_oper_log` VALUES (257, '代码生成', 2, 'com.eden.generator.controller.GenController.synchDb()', 'GET', 1, 'admin', '建筑学院', '/tool/gen/synchDb/school_user', '127.0.0.1', '内网IP', NULL, '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-09-16 14:00:09');
INSERT INTO `sys_oper_log` VALUES (258, '学校用户信息', 1, 'com.eden.web.controller.school.SchoolUserController.addSave()', 'POST', 1, 'admin', '建筑学院', '/school/schoolUser/add', '127.0.0.1', '内网IP', '{\"deptId\":[\"128\"],\"userName\":[\"小刚\"],\"deptName\":[\"计算机科学与技术\"],\"phonenumber\":[\"17761628993\"],\"email\":[\"838855801@qq.com\"],\"loginName\":[\"1715925161\"],\"sex\":[\"0\"],\"roleId\":[\"9\"],\"remark\":[\"\"],\"status\":[\"0\"],\"postIds\":[\"\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-09-17 11:54:26');
INSERT INTO `sys_oper_log` VALUES (259, '学校用户信息', 1, 'com.eden.web.controller.school.SchoolUserController.addSave()', 'POST', 1, 'admin', '建筑学院', '/school/schoolUser/add', '127.0.0.1', '内网IP', '{\"deptId\":[\"144\"],\"userName\":[\"小美\"],\"deptName\":[\"英语\"],\"phonenumber\":[\"17761628995\"],\"email\":[\"838855807@qq.com\"],\"loginName\":[\"1715925165\"],\"sex\":[\"1\"],\"roleId\":[\"9\"],\"remark\":[\"\"],\"status\":[\"0\"],\"postIds\":[\"\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-09-17 13:28:56');
INSERT INTO `sys_oper_log` VALUES (260, '学校用户信息', 1, 'com.eden.web.controller.school.SchoolUserController.addSave()', 'POST', 1, 'admin', '建筑学院', '/school/schoolUser/add', '127.0.0.1', '内网IP', '{\"deptId\":[\"120\"],\"userName\":[\"小明\"],\"deptName\":[\"电气工程及其自动化\"],\"phonenumber\":[\"17761628902\"],\"email\":[\"838855855@qq.com\"],\"loginName\":[\"1715925102\"],\"sex\":[\"0\"],\"roleId\":[\"9\"],\"remark\":[\"\"],\"status\":[\"0\"],\"postIds\":[\"\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-09-17 13:30:14');
INSERT INTO `sys_oper_log` VALUES (261, '用户管理', 2, 'com.eden.web.controller.system.SysUserController.editSave()', 'POST', 1, 'admin', '建筑学院', '/system/user/edit', '127.0.0.1', '内网IP', '{\"userId\":[\"3\"],\"deptId\":[\"129\"],\"userName\":[\"焦洋\"],\"dept.deptName\":[\"数据科学与大数据技术\"],\"phonenumber\":[\"17761628992\"],\"email\":[\"838855804@qq.com\"],\"loginName\":[\"1715925168\"],\"sex\":[\"0\"],\"role\":[\"4\"],\"remark\":[\"\"],\"status\":[\"0\"],\"roleIds\":[\"4\"],\"postIds\":[\"\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-09-17 13:37:52');
INSERT INTO `sys_oper_log` VALUES (262, '用户管理', 2, 'com.eden.web.controller.system.SysUserController.editSave()', 'POST', 1, '1715925168', '数据科学与大数据技术', '/system/user/edit', '127.0.0.1', '内网IP', '{\"userId\":[\"3\"],\"deptId\":[\"129\"],\"userName\":[\"焦洋\"],\"dept.deptName\":[\"数据科学与大数据技术\"],\"phonenumber\":[\"17761628992\"],\"email\":[\"838855804@qq.com\"],\"loginName\":[\"1715925168\"],\"sex\":[\"0\"],\"role\":[\"4\",\"3\"],\"remark\":[\"\"],\"status\":[\"0\"],\"roleIds\":[\"4,3\"],\"postIds\":[\"\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-09-17 13:38:40');
INSERT INTO `sys_oper_log` VALUES (263, '角色管理', 2, 'com.eden.web.controller.system.SysRoleController.editSave()', 'POST', 1, 'admin', '建筑学院', '/system/role/edit', '127.0.0.1', '内网IP', '{\"roleId\":[\"4\"],\"roleName\":[\"校长\"],\"roleKey\":[\"rector\"],\"roleSort\":[\"4\"],\"status\":[\"0\"],\"remark\":[\"\"],\"menuIds\":[\"1,100,1000,1001,1002,1003,1004,1005,1006,101,1007,1008,1009,1010,1011,103,1016,1017,1018,1019,1064,1065,1066,1068\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-09-17 13:40:10');
INSERT INTO `sys_oper_log` VALUES (264, '用户管理', 2, 'com.eden.web.controller.system.SysUserController.editSave()', 'POST', 1, 'admin', '建筑学院', '/system/user/edit', '127.0.0.1', '内网IP', '{\"userId\":[\"3\"],\"deptId\":[\"129\"],\"userName\":[\"焦洋\"],\"dept.deptName\":[\"数据科学与大数据技术\"],\"phonenumber\":[\"17761628992\"],\"email\":[\"838855804@qq.com\"],\"loginName\":[\"1715925168\"],\"sex\":[\"0\"],\"role\":[\"4\"],\"remark\":[\"\"],\"status\":[\"0\"],\"roleIds\":[\"4\"],\"postIds\":[\"\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-09-17 13:42:01');
INSERT INTO `sys_oper_log` VALUES (265, '用户管理', 4, 'com.eden.web.controller.system.SysUserController.insertAuthRole()', 'POST', 1, '1715925168', '数据科学与大数据技术', '/system/user/authRole/insertAuthRole', '127.0.0.1', '内网IP', '{\"userId\":[\"3\"],\"roleIds\":[\"4\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-09-17 13:43:39');
INSERT INTO `sys_oper_log` VALUES (266, '用户管理', 2, 'com.eden.web.controller.system.SysUserController.editSave()', 'POST', 1, 'admin', '建筑学院', '/system/user/edit', '127.0.0.1', '内网IP', '{\"userId\":[\"3\"],\"deptId\":[\"129\"],\"userName\":[\"焦洋\"],\"dept.deptName\":[\"数据科学与大数据技术\"],\"phonenumber\":[\"17761628992\"],\"email\":[\"838855804@qq.com\"],\"loginName\":[\"1715925168\"],\"sex\":[\"0\"],\"role\":[\"4\"],\"remark\":[\"\"],\"status\":[\"0\"],\"roleIds\":[\"4\"],\"postIds\":[\"\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-09-17 13:44:46');
INSERT INTO `sys_oper_log` VALUES (267, '角色管理', 2, 'com.eden.web.controller.system.SysRoleController.editSave()', 'POST', 1, 'admin', '建筑学院', '/system/role/edit', '127.0.0.1', '内网IP', '{\"roleId\":[\"4\"],\"roleName\":[\"校长\"],\"roleKey\":[\"rector\"],\"roleSort\":[\"4\"],\"status\":[\"0\"],\"remark\":[\"\"],\"menuIds\":[\"1,100,1000,1001,1002,1003,1004,1005,1006,101,1007,1008,1009,1010,1011,103,1016,1017,1018,1019,1064,1065,1066,1068\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-09-17 13:45:38');
INSERT INTO `sys_oper_log` VALUES (268, '菜单管理', 1, 'com.eden.web.controller.system.SysMenuController.addSave()', 'POST', 1, 'admin', '建筑学院', '/system/menu/add', '127.0.0.1', '内网IP', '{\"parentId\":[\"1065\"],\"menuType\":[\"F\"],\"menuName\":[\"校园用户查看权限\"],\"url\":[\"\"],\"target\":[\"menuItem\"],\"perms\":[\"school:schoolUser:list\"],\"orderNum\":[\"1\"],\"icon\":[\"\"],\"visible\":[\"0\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-09-17 13:47:19');
INSERT INTO `sys_oper_log` VALUES (269, '菜单管理', 1, 'com.eden.web.controller.system.SysMenuController.addSave()', 'POST', 1, 'admin', '建筑学院', '/system/menu/add', '127.0.0.1', '内网IP', '{\"parentId\":[\"1065\"],\"menuType\":[\"F\"],\"menuName\":[\"校园用户添加权限\"],\"url\":[\"\"],\"target\":[\"menuItem\"],\"perms\":[\"school:schoolUser:add\"],\"orderNum\":[\"2\"],\"icon\":[\"\"],\"visible\":[\"0\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-09-17 13:47:55');
INSERT INTO `sys_oper_log` VALUES (270, '菜单管理', 1, 'com.eden.web.controller.system.SysMenuController.addSave()', 'POST', 1, 'admin', '建筑学院', '/system/menu/add', '127.0.0.1', '内网IP', '{\"parentId\":[\"1065\"],\"menuType\":[\"F\"],\"menuName\":[\"校园用户删除权限\"],\"url\":[\"\"],\"target\":[\"menuItem\"],\"perms\":[\"school:schoolUser:remove\"],\"orderNum\":[\"3\"],\"icon\":[\"\"],\"visible\":[\"0\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-09-17 13:48:37');
INSERT INTO `sys_oper_log` VALUES (271, '菜单管理', 1, 'com.eden.web.controller.system.SysMenuController.addSave()', 'POST', 1, 'admin', '建筑学院', '/system/menu/add', '127.0.0.1', '内网IP', '{\"parentId\":[\"1065\"],\"menuType\":[\"F\"],\"menuName\":[\"校园用户修改权限\"],\"url\":[\"\"],\"target\":[\"menuItem\"],\"perms\":[\"school:schoolUser:edit\"],\"orderNum\":[\"4\"],\"icon\":[\"\"],\"visible\":[\"0\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-09-17 13:49:17');
INSERT INTO `sys_oper_log` VALUES (272, '菜单管理', 1, 'com.eden.web.controller.system.SysMenuController.addSave()', 'POST', 1, 'admin', '建筑学院', '/system/menu/add', '127.0.0.1', '内网IP', '{\"parentId\":[\"1065\"],\"menuType\":[\"F\"],\"menuName\":[\"校园用户导出权限\"],\"url\":[\"\"],\"target\":[\"menuItem\"],\"perms\":[\"school:schoolUser:export\"],\"orderNum\":[\"5\"],\"icon\":[\"\"],\"visible\":[\"0\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-09-17 13:50:02');
INSERT INTO `sys_oper_log` VALUES (273, '菜单管理', 1, 'com.eden.web.controller.system.SysMenuController.addSave()', 'POST', 1, 'admin', '建筑学院', '/system/menu/add', '127.0.0.1', '内网IP', '{\"parentId\":[\"1065\"],\"menuType\":[\"F\"],\"menuName\":[\"校园用户密码修改权限\"],\"url\":[\"\"],\"target\":[\"menuItem\"],\"perms\":[\"school:schoolUser:resetPwd\"],\"orderNum\":[\"6\"],\"icon\":[\"\"],\"visible\":[\"0\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-09-17 13:50:47');
INSERT INTO `sys_oper_log` VALUES (274, '菜单管理', 1, 'com.eden.web.controller.system.SysMenuController.addSave()', 'POST', 1, 'admin', '建筑学院', '/system/menu/add', '127.0.0.1', '内网IP', '{\"parentId\":[\"1066\"],\"menuType\":[\"F\"],\"menuName\":[\"校园任务查看权限\"],\"url\":[\"\"],\"target\":[\"menuItem\"],\"perms\":[\"task:usertask:list\"],\"orderNum\":[\"1\"],\"icon\":[\"\"],\"visible\":[\"0\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-09-17 13:52:13');
INSERT INTO `sys_oper_log` VALUES (275, '菜单管理', 1, 'com.eden.web.controller.system.SysMenuController.addSave()', 'POST', 1, 'admin', '建筑学院', '/system/menu/add', '127.0.0.1', '内网IP', '{\"parentId\":[\"1066\"],\"menuType\":[\"F\"],\"menuName\":[\"校园任务删除权限\"],\"url\":[\"\"],\"target\":[\"menuItem\"],\"perms\":[\"task:usertask:remove\"],\"orderNum\":[\"2\"],\"icon\":[\"\"],\"visible\":[\"0\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-09-17 13:53:01');
INSERT INTO `sys_oper_log` VALUES (276, '菜单管理', 1, 'com.eden.web.controller.system.SysMenuController.addSave()', 'POST', 1, 'admin', '建筑学院', '/system/menu/add', '127.0.0.1', '内网IP', '{\"parentId\":[\"1066\"],\"menuType\":[\"F\"],\"menuName\":[\"校园用户添加权限\"],\"url\":[\"\"],\"target\":[\"menuItem\"],\"perms\":[\"task:usertask:add\"],\"orderNum\":[\"3\"],\"icon\":[\"\"],\"visible\":[\"0\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-09-17 13:53:32');
INSERT INTO `sys_oper_log` VALUES (277, '菜单管理', 1, 'com.eden.web.controller.system.SysMenuController.addSave()', 'POST', 1, 'admin', '建筑学院', '/system/menu/add', '127.0.0.1', '内网IP', '{\"parentId\":[\"1066\"],\"menuType\":[\"F\"],\"menuName\":[\"校园任务导出权限\"],\"url\":[\"\"],\"target\":[\"menuItem\"],\"perms\":[\"task:usertask:export\"],\"orderNum\":[\"4\"],\"icon\":[\"\"],\"visible\":[\"0\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-09-17 13:54:48');
INSERT INTO `sys_oper_log` VALUES (278, '菜单管理', 1, 'com.eden.web.controller.system.SysMenuController.addSave()', 'POST', 1, 'admin', '建筑学院', '/system/menu/add', '127.0.0.1', '内网IP', '{\"parentId\":[\"1068\"],\"menuType\":[\"F\"],\"menuName\":[\"校园任务审核权限\"],\"url\":[\"\"],\"target\":[\"menuItem\"],\"perms\":[\"task:usertask:check\"],\"orderNum\":[\"1\"],\"icon\":[\"\"],\"visible\":[\"0\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-09-17 14:23:19');
INSERT INTO `sys_oper_log` VALUES (279, '菜单管理', 1, 'com.eden.web.controller.system.SysMenuController.addSave()', 'POST', 1, 'admin', '建筑学院', '/system/menu/add', '127.0.0.1', '内网IP', '{\"parentId\":[\"1068\"],\"menuType\":[\"F\"],\"menuName\":[\"校园任务导出权限\"],\"url\":[\"\"],\"target\":[\"menuItem\"],\"perms\":[\"task:usertask:export\"],\"orderNum\":[\"2\"],\"icon\":[\"\"],\"visible\":[\"0\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-09-17 14:24:48');
INSERT INTO `sys_oper_log` VALUES (280, '菜单管理', 1, 'com.eden.web.controller.system.SysMenuController.addSave()', 'POST', 1, 'admin', '建筑学院', '/system/menu/add', '127.0.0.1', '内网IP', '{\"parentId\":[\"1068\"],\"menuType\":[\"F\"],\"menuName\":[\"校园任务删除权限\"],\"url\":[\"\"],\"target\":[\"menuItem\"],\"perms\":[\"task:usertask:remove\"],\"orderNum\":[\"3\"],\"icon\":[\"\"],\"visible\":[\"0\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-09-17 14:25:20');
INSERT INTO `sys_oper_log` VALUES (281, '角色管理', 2, 'com.eden.web.controller.system.SysRoleController.editSave()', 'POST', 1, 'admin', '建筑学院', '/system/role/edit', '127.0.0.1', '内网IP', '{\"roleId\":[\"4\"],\"roleName\":[\"校长\"],\"roleKey\":[\"rector\"],\"roleSort\":[\"4\"],\"status\":[\"0\"],\"remark\":[\"\"],\"menuIds\":[\"1,100,1000,1001,1002,1003,1004,1005,1006,101,1007,1008,1009,1010,1011,103,1016,1017,1018,1019,1064,1065,1070,1071,1072,1073,1074,1075,1066,1076,1077,1078,1079,1068,1080,1081,1082\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-09-17 14:25:46');
INSERT INTO `sys_oper_log` VALUES (282, '审核用户发布任务', 1, 'com.eden.task.controller.UserTaskController.checkTask()', 'GET', 1, 'admin', '建筑学院', '/task/usertask/checkTask/Taf381600065254499', '127.0.0.1', '内网IP', NULL, '\"task/usertask/checkUserTask\"', 0, NULL, '2020-09-17 16:54:10');
INSERT INTO `sys_oper_log` VALUES (283, '学校用户信息', 2, 'com.eden.web.controller.school.SchoolUserController.editSave()', 'POST', 1, 'admin', '建筑学院', '/school/schoolUser/edit', '127.0.0.1', '内网IP', '{\"userId\":[\"10\"],\"deptId\":[\"120\"],\"userName\":[\"小明\"],\"dept.deptName\":[\"电气工程及其自动化\"],\"phonenumber\":[\"17761628902\"],\"email\":[\"838855855@qq.com\"],\"loginName\":[\"1715925102\"],\"sex\":[\"0\"],\"roleId\":[\"9\"],\"remark\":[\"\"],\"status\":[\"0\"],\"postIds\":[\"\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-09-17 17:06:56');
INSERT INTO `sys_oper_log` VALUES (284, '学校用户信息', 2, 'com.eden.web.controller.school.SchoolUserController.editSave()', 'POST', 1, 'admin', '建筑学院', '/school/schoolUser/edit', '127.0.0.1', '内网IP', '{\"userId\":[\"10\"],\"deptId\":[\"120\"],\"userName\":[\"小明\"],\"dept.deptName\":[\"电气工程及其自动化\"],\"phonenumber\":[\"17761628902\"],\"email\":[\"838855855@qq.com\"],\"loginName\":[\"1715925102\"],\"sex\":[\"0\"],\"roleId\":[\"9\"],\"remark\":[\"\"],\"status\":[\"0\"],\"postIds\":[\"\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-09-17 17:08:23');
INSERT INTO `sys_oper_log` VALUES (285, '角色管理', 2, 'com.eden.web.controller.system.SysRoleController.editSave()', 'POST', 1, 'admin', '建筑学院', '/system/role/edit', '127.0.0.1', '内网IP', '{\"roleId\":[\"10\"],\"roleName\":[\"教师\"],\"roleKey\":[\"teacher\"],\"roleSort\":[\"6\"],\"status\":[\"0\"],\"remark\":[\"\"],\"menuIds\":[\"\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-09-17 17:11:29');
INSERT INTO `sys_oper_log` VALUES (286, '角色管理', 3, 'com.eden.web.controller.system.SysRoleController.remove()', 'POST', 1, 'admin', '建筑学院', '/system/role/remove', '127.0.0.1', '内网IP', '{\"ids\":[\"6\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-09-17 17:11:48');
INSERT INTO `sys_oper_log` VALUES (287, '学校用户信息', 2, 'com.eden.web.controller.school.SchoolUserController.editSave()', 'POST', 1, 'admin', '建筑学院', '/school/schoolUser/edit', '127.0.0.1', '内网IP', '{\"userId\":[\"10\"],\"deptId\":[\"120\"],\"userName\":[\"小明\"],\"dept.deptName\":[\"电气工程及其自动化\"],\"phonenumber\":[\"17761628902\"],\"email\":[\"838855855@qq.com\"],\"loginName\":[\"1715925102\"],\"sex\":[\"0\"],\"roleId\":[\"6\"],\"remark\":[\"\"],\"status\":[\"0\"],\"postIds\":[\"\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-09-17 17:16:43');
INSERT INTO `sys_oper_log` VALUES (288, '学校用户信息', 2, 'com.eden.web.controller.school.SchoolUserController.editSave()', 'POST', 1, 'admin', '建筑学院', '/school/schoolUser/edit', '127.0.0.1', '内网IP', '{\"userId\":[\"10\"],\"deptId\":[\"120\"],\"userName\":[\"小明\"],\"dept.deptName\":[\"电气工程及其自动化\"],\"phonenumber\":[\"17761628902\"],\"email\":[\"838855855@qq.com\"],\"loginName\":[\"1715925102\"],\"sex\":[\"0\"],\"roleId\":[\"7\"],\"remark\":[\"\"],\"status\":[\"0\"],\"postIds\":[\"\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-09-17 17:16:52');
INSERT INTO `sys_oper_log` VALUES (289, '审核用户发布任务', 1, 'com.eden.task.controller.UserTaskController.checkTask()', 'GET', 1, 'admin', '建筑学院', '/task/usertask/checkTask/Taf381600065254499', '127.0.0.1', '内网IP', NULL, '\"task/usertask/checkUserTask\"', 0, NULL, '2020-09-17 17:17:41');
INSERT INTO `sys_oper_log` VALUES (290, '角色管理', 2, 'com.eden.web.controller.system.SysRoleController.editSave()', 'POST', 1, 'admin', '建筑学院', '/system/role/edit', '127.0.0.1', '内网IP', '{\"roleId\":[\"9\"],\"roleName\":[\"学生\"],\"roleKey\":[\"student\"],\"roleSort\":[\"9\"],\"status\":[\"0\"],\"remark\":[\"\"],\"menuIds\":[\"1064,1065,1072,1073,1075,1066,1076,1078\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-09-22 15:54:40');
INSERT INTO `sys_oper_log` VALUES (291, '角色管理', 2, 'com.eden.web.controller.system.SysRoleController.editSave()', 'POST', 1, 'admin', '建筑学院', '/system/role/edit', '127.0.0.1', '内网IP', '{\"roleId\":[\"6\"],\"roleName\":[\"教师\"],\"roleKey\":[\"teacher\"],\"roleSort\":[\"6\"],\"status\":[\"0\"],\"remark\":[\"\"],\"menuIds\":[\"1064,1065,1070,1071,1072,1073,1074,1075,1066,1076,1077,1078,1079,1068,1080,1081,1082\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2020-09-22 15:55:02');
INSERT INTO `sys_oper_log` VALUES (292, '审核用户发布任务', 1, 'com.eden.task.controller.UserTaskController.checkTask()', 'GET', 1, 'admin', '建筑学院', '/task/usertask/checkTask/Taf381600065254499', '127.0.0.1', '内网IP', NULL, '\"task/usertask/checkUserTask\"', 0, NULL, '2020-09-24 16:45:29');
INSERT INTO `sys_oper_log` VALUES (293, '审核用户发布任务', 1, 'com.eden.task.controller.UserTaskController.checkTask()', 'GET', 1, 'admin', '建筑学院', '/task/usertask/checkTask/Taf381600065254499', '127.0.0.1', '内网IP', NULL, '\"task/usertask/checkUserTask\"', 0, NULL, '2020-09-24 16:45:47');
INSERT INTO `sys_oper_log` VALUES (294, '审核用户发布任务', 1, 'com.eden.task.controller.UserTaskController.checkTask()', 'GET', 1, 'admin', '建筑学院', '/task/usertask/checkTask/T5ad41599812393783', '127.0.0.1', '内网IP', NULL, '\"task/usertask/checkUserTask\"', 0, NULL, '2020-09-24 16:45:51');
INSERT INTO `sys_oper_log` VALUES (295, '审核用户发布任务', 1, 'com.eden.task.controller.UserTaskController.checkTask()', 'GET', 1, 'admin', '建筑学院', '/task/usertask/checkTask/T5ad41599812393783', '127.0.0.1', '内网IP', NULL, '\"task/usertask/checkUserTask\"', 0, NULL, '2020-09-24 16:45:57');
INSERT INTO `sys_oper_log` VALUES (296, '审核用户发布任务', 1, 'com.eden.task.controller.UserTaskController.checkTask()', 'GET', 1, 'admin', '建筑学院', '/task/usertask/checkTask/Taf381600065254499', '127.0.0.1', '内网IP', NULL, '\"task/usertask/checkUserTask\"', 0, NULL, '2020-09-24 16:46:02');
INSERT INTO `sys_oper_log` VALUES (297, '审核用户发布任务', 1, 'com.eden.task.controller.UserTaskController.checkTask()', 'GET', 1, 'admin', '建筑学院', '/task/usertask/checkTask/T5ad41599812393783', '127.0.0.1', '内网IP', NULL, '\"task/usertask/checkUserTask\"', 0, NULL, '2020-10-30 16:27:00');
INSERT INTO `sys_oper_log` VALUES (298, '审核用户发布任务', 1, 'com.eden.task.controller.UserTaskController.checkTask()', 'GET', 1, 'admin', '建筑学院', '/task/usertask/checkTask/T5ad41599812393783', '127.0.0.1', '内网IP', NULL, '\"task/usertask/checkUserTask\"', 0, NULL, '2020-10-30 16:27:08');
INSERT INTO `sys_oper_log` VALUES (299, '审核用户发布任务', 1, 'com.eden.task.controller.UserTaskController.checkTask()', 'GET', 1, 'admin', '建筑学院', '/task/usertask/checkTask/T5ad41599812393783', '127.0.0.1', '内网IP', NULL, '\"task/usertask/checkUserTask\"', 0, NULL, '2020-10-30 16:27:55');
COMMIT;

-- ----------------------------
-- Table structure for sys_post
-- ----------------------------
DROP TABLE IF EXISTS `sys_post`;
CREATE TABLE `sys_post` (
  `post_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '岗位ID',
  `post_code` varchar(64) NOT NULL COMMENT '岗位编码',
  `post_name` varchar(50) NOT NULL COMMENT '岗位名称',
  `post_sort` int(4) NOT NULL COMMENT '显示顺序',
  `status` char(1) NOT NULL COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`post_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='岗位信息表';

-- ----------------------------
-- Records of sys_post
-- ----------------------------
BEGIN;
INSERT INTO `sys_post` VALUES (1, 'rector', '校长', 1, '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-08-30 17:08:37', '');
INSERT INTO `sys_post` VALUES (2, 'dean', '院长', 2, '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-08-30 17:09:21', '');
INSERT INTO `sys_post` VALUES (3, 'teacher', '教师', 3, '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-08-30 17:10:08', '');
INSERT INTO `sys_post` VALUES (4, 'student', '学生', 4, '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-08-30 17:10:37', '');
COMMIT;

-- ----------------------------
-- Table structure for sys_region
-- ----------------------------
DROP TABLE IF EXISTS `sys_region`;
CREATE TABLE `sys_region` (
  `region_code` varchar(20) CHARACTER SET utf8mb4 NOT NULL COMMENT '行政地区编号',
  `region_name` varchar(50) CHARACTER SET utf8mb4 NOT NULL COMMENT '地区名称',
  `region_parent_id` varchar(10) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '地区父id',
  `region_level` int(2) DEFAULT NULL COMMENT '地区级别 1-省、自治区、直辖市 2-地级市、地区、自治州、盟 3-市辖区、县级市、县'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='地区表';

-- ----------------------------
-- Records of sys_region
-- ----------------------------
BEGIN;
INSERT INTO `sys_region` VALUES ('110000', '北京市', '0', 1);
INSERT INTO `sys_region` VALUES ('110000', '北京市', '110000', 2);
INSERT INTO `sys_region` VALUES ('110101', '东城区', '110000', 3);
INSERT INTO `sys_region` VALUES ('110102', '西城区', '110000', 3);
INSERT INTO `sys_region` VALUES ('110105', '朝阳区', '110000', 3);
INSERT INTO `sys_region` VALUES ('110106', '丰台区', '110000', 3);
INSERT INTO `sys_region` VALUES ('110107', '石景山区', '110000', 3);
INSERT INTO `sys_region` VALUES ('110108', '海淀区', '110000', 3);
INSERT INTO `sys_region` VALUES ('110109', '门头沟区', '110000', 3);
INSERT INTO `sys_region` VALUES ('110111', '房山区', '110000', 3);
INSERT INTO `sys_region` VALUES ('110112', '通州区', '110000', 3);
INSERT INTO `sys_region` VALUES ('110113', '顺义区', '110000', 3);
INSERT INTO `sys_region` VALUES ('110114', '昌平区', '110000', 3);
INSERT INTO `sys_region` VALUES ('110115', '大兴区', '110000', 3);
INSERT INTO `sys_region` VALUES ('110116', '怀柔区', '110000', 3);
INSERT INTO `sys_region` VALUES ('110117', '平谷区', '110000', 3);
INSERT INTO `sys_region` VALUES ('110118', '密云区', '110000', 3);
INSERT INTO `sys_region` VALUES ('110119', '延庆区', '110000', 3);
INSERT INTO `sys_region` VALUES ('120000', '天津市', '0', 1);
INSERT INTO `sys_region` VALUES ('120000', '天津市', '120000', 2);
INSERT INTO `sys_region` VALUES ('120101', '和平区', '120000', 3);
INSERT INTO `sys_region` VALUES ('120102', '河东区', '120000', 3);
INSERT INTO `sys_region` VALUES ('120103', '河西区', '120000', 3);
INSERT INTO `sys_region` VALUES ('120104', '南开区', '120000', 3);
INSERT INTO `sys_region` VALUES ('120105', '河北区', '120000', 3);
INSERT INTO `sys_region` VALUES ('120106', '红桥区', '120000', 3);
INSERT INTO `sys_region` VALUES ('120110', '东丽区', '120000', 3);
INSERT INTO `sys_region` VALUES ('120111', '西青区', '120000', 3);
INSERT INTO `sys_region` VALUES ('120112', '津南区', '120000', 3);
INSERT INTO `sys_region` VALUES ('120113', '北辰区', '120000', 3);
INSERT INTO `sys_region` VALUES ('120114', '武清区', '120000', 3);
INSERT INTO `sys_region` VALUES ('120115', '宝坻区', '120000', 3);
INSERT INTO `sys_region` VALUES ('120116', '滨海新区', '120000', 3);
INSERT INTO `sys_region` VALUES ('120117', '宁河区', '120000', 3);
INSERT INTO `sys_region` VALUES ('120118', '静海区', '120000', 3);
INSERT INTO `sys_region` VALUES ('120119', '蓟州区', '120000', 3);
INSERT INTO `sys_region` VALUES ('130000', '河北省', '0', 1);
INSERT INTO `sys_region` VALUES ('130100', '石家庄市', '130000', 2);
INSERT INTO `sys_region` VALUES ('130102', '长安区', '130100', 3);
INSERT INTO `sys_region` VALUES ('130104', '桥西区', '130100', 3);
INSERT INTO `sys_region` VALUES ('130105', '新华区', '130100', 3);
INSERT INTO `sys_region` VALUES ('130107', '井陉矿区', '130100', 3);
INSERT INTO `sys_region` VALUES ('130108', '裕华区', '130100', 3);
INSERT INTO `sys_region` VALUES ('130109', '藁城区', '130100', 3);
INSERT INTO `sys_region` VALUES ('130110', '鹿泉区', '130100', 3);
INSERT INTO `sys_region` VALUES ('130111', '栾城区', '130100', 3);
INSERT INTO `sys_region` VALUES ('130121', '井陉县', '130100', 3);
INSERT INTO `sys_region` VALUES ('130123', '正定县', '130100', 3);
INSERT INTO `sys_region` VALUES ('130125', '行唐县', '130100', 3);
INSERT INTO `sys_region` VALUES ('130126', '灵寿县', '130100', 3);
INSERT INTO `sys_region` VALUES ('130127', '高邑县', '130100', 3);
INSERT INTO `sys_region` VALUES ('130128', '深泽县', '130100', 3);
INSERT INTO `sys_region` VALUES ('130129', '赞皇县', '130100', 3);
INSERT INTO `sys_region` VALUES ('130130', '无极县', '130100', 3);
INSERT INTO `sys_region` VALUES ('130131', '平山县', '130100', 3);
INSERT INTO `sys_region` VALUES ('130132', '元氏县', '130100', 3);
INSERT INTO `sys_region` VALUES ('130133', '赵县', '130100', 3);
INSERT INTO `sys_region` VALUES ('130181', '辛集市', '130100', 3);
INSERT INTO `sys_region` VALUES ('130183', '晋州市', '130100', 3);
INSERT INTO `sys_region` VALUES ('130184', '新乐市', '130100', 3);
INSERT INTO `sys_region` VALUES ('130200', '唐山市', '130000', 2);
INSERT INTO `sys_region` VALUES ('130202', '路南区', '130200', 3);
INSERT INTO `sys_region` VALUES ('130203', '路北区', '130200', 3);
INSERT INTO `sys_region` VALUES ('130204', '古冶区', '130200', 3);
INSERT INTO `sys_region` VALUES ('130205', '开平区', '130200', 3);
INSERT INTO `sys_region` VALUES ('130207', '丰南区', '130200', 3);
INSERT INTO `sys_region` VALUES ('130208', '丰润区', '130200', 3);
INSERT INTO `sys_region` VALUES ('130209', '曹妃甸区', '130200', 3);
INSERT INTO `sys_region` VALUES ('130224', '滦南县', '130200', 3);
INSERT INTO `sys_region` VALUES ('130225', '乐亭县', '130200', 3);
INSERT INTO `sys_region` VALUES ('130227', '迁西县', '130200', 3);
INSERT INTO `sys_region` VALUES ('130229', '玉田县', '130200', 3);
INSERT INTO `sys_region` VALUES ('130281', '遵化市', '130200', 3);
INSERT INTO `sys_region` VALUES ('130283', '迁安市', '130200', 3);
INSERT INTO `sys_region` VALUES ('130284', '滦州市', '130200', 3);
INSERT INTO `sys_region` VALUES ('130300', '秦皇岛市', '130000', 2);
INSERT INTO `sys_region` VALUES ('130302', '海港区', '130300', 3);
INSERT INTO `sys_region` VALUES ('130303', '山海关区', '130300', 3);
INSERT INTO `sys_region` VALUES ('130304', '北戴河区', '130300', 3);
INSERT INTO `sys_region` VALUES ('130306', '抚宁区', '130300', 3);
INSERT INTO `sys_region` VALUES ('130321', '青龙满族自治县', '130300', 3);
INSERT INTO `sys_region` VALUES ('130322', '昌黎县', '130300', 3);
INSERT INTO `sys_region` VALUES ('130324', '卢龙县', '130300', 3);
INSERT INTO `sys_region` VALUES ('130400', '邯郸市', '130000', 2);
INSERT INTO `sys_region` VALUES ('130402', '邯山区', '130400', 3);
INSERT INTO `sys_region` VALUES ('130403', '丛台区', '130400', 3);
INSERT INTO `sys_region` VALUES ('130404', '复兴区', '130400', 3);
INSERT INTO `sys_region` VALUES ('130406', '峰峰矿区', '130400', 3);
INSERT INTO `sys_region` VALUES ('130407', '肥乡区', '130400', 3);
INSERT INTO `sys_region` VALUES ('130408', '永年区', '130400', 3);
INSERT INTO `sys_region` VALUES ('130423', '临漳县', '130400', 3);
INSERT INTO `sys_region` VALUES ('130424', '成安县', '130400', 3);
INSERT INTO `sys_region` VALUES ('130425', '大名县', '130400', 3);
INSERT INTO `sys_region` VALUES ('130426', '涉县', '130400', 3);
INSERT INTO `sys_region` VALUES ('130427', '磁县', '130400', 3);
INSERT INTO `sys_region` VALUES ('130430', '邱县', '130400', 3);
INSERT INTO `sys_region` VALUES ('130431', '鸡泽县', '130400', 3);
INSERT INTO `sys_region` VALUES ('130432', '广平县', '130400', 3);
INSERT INTO `sys_region` VALUES ('130433', '馆陶县', '130400', 3);
INSERT INTO `sys_region` VALUES ('130434', '魏县', '130400', 3);
INSERT INTO `sys_region` VALUES ('130435', '曲周县', '130400', 3);
INSERT INTO `sys_region` VALUES ('130481', '武安市', '130400', 3);
INSERT INTO `sys_region` VALUES ('130500', '邢台市', '130000', 2);
INSERT INTO `sys_region` VALUES ('130502', '桥东区', '130500', 3);
INSERT INTO `sys_region` VALUES ('130503', '桥西区', '130500', 3);
INSERT INTO `sys_region` VALUES ('130521', '邢台县', '130500', 3);
INSERT INTO `sys_region` VALUES ('130522', '临城县', '130500', 3);
INSERT INTO `sys_region` VALUES ('130523', '内丘县', '130500', 3);
INSERT INTO `sys_region` VALUES ('130524', '柏乡县', '130500', 3);
INSERT INTO `sys_region` VALUES ('130525', '隆尧县', '130500', 3);
INSERT INTO `sys_region` VALUES ('130526', '任县', '130500', 3);
INSERT INTO `sys_region` VALUES ('130527', '南和县', '130500', 3);
INSERT INTO `sys_region` VALUES ('130528', '宁晋县', '130500', 3);
INSERT INTO `sys_region` VALUES ('130529', '巨鹿县', '130500', 3);
INSERT INTO `sys_region` VALUES ('130530', '新河县', '130500', 3);
INSERT INTO `sys_region` VALUES ('130531', '广宗县', '130500', 3);
INSERT INTO `sys_region` VALUES ('130532', '平乡县', '130500', 3);
INSERT INTO `sys_region` VALUES ('130533', '威县', '130500', 3);
INSERT INTO `sys_region` VALUES ('130534', '清河县', '130500', 3);
INSERT INTO `sys_region` VALUES ('130535', '临西县', '130500', 3);
INSERT INTO `sys_region` VALUES ('130581', '南宫市', '130500', 3);
INSERT INTO `sys_region` VALUES ('130582', '沙河市', '130500', 3);
INSERT INTO `sys_region` VALUES ('130600', '保定市', '130000', 2);
INSERT INTO `sys_region` VALUES ('130602', '竞秀区', '130600', 3);
INSERT INTO `sys_region` VALUES ('130606', '莲池区', '130600', 3);
INSERT INTO `sys_region` VALUES ('130607', '满城区', '130600', 3);
INSERT INTO `sys_region` VALUES ('130608', '清苑区', '130600', 3);
INSERT INTO `sys_region` VALUES ('130609', '徐水区', '130600', 3);
INSERT INTO `sys_region` VALUES ('130623', '涞水县', '130600', 3);
INSERT INTO `sys_region` VALUES ('130624', '阜平县', '130600', 3);
INSERT INTO `sys_region` VALUES ('130626', '定兴县', '130600', 3);
INSERT INTO `sys_region` VALUES ('130627', '唐县', '130600', 3);
INSERT INTO `sys_region` VALUES ('130628', '高阳县', '130600', 3);
INSERT INTO `sys_region` VALUES ('130629', '容城县', '130600', 3);
INSERT INTO `sys_region` VALUES ('130630', '涞源县', '130600', 3);
INSERT INTO `sys_region` VALUES ('130631', '望都县', '130600', 3);
INSERT INTO `sys_region` VALUES ('130632', '安新县', '130600', 3);
INSERT INTO `sys_region` VALUES ('130633', '易县', '130600', 3);
INSERT INTO `sys_region` VALUES ('130634', '曲阳县', '130600', 3);
INSERT INTO `sys_region` VALUES ('130635', '蠡县', '130600', 3);
INSERT INTO `sys_region` VALUES ('130636', '顺平县', '130600', 3);
INSERT INTO `sys_region` VALUES ('130637', '博野县', '130600', 3);
INSERT INTO `sys_region` VALUES ('130638', '雄县', '130600', 3);
INSERT INTO `sys_region` VALUES ('130681', '涿州市', '130600', 3);
INSERT INTO `sys_region` VALUES ('130682', '定州市', '130600', 3);
INSERT INTO `sys_region` VALUES ('130683', '安国市', '130600', 3);
INSERT INTO `sys_region` VALUES ('130684', '高碑店市', '130600', 3);
INSERT INTO `sys_region` VALUES ('130700', '张家口市', '130000', 2);
INSERT INTO `sys_region` VALUES ('130702', '桥东区', '130700', 3);
INSERT INTO `sys_region` VALUES ('130703', '桥西区', '130700', 3);
INSERT INTO `sys_region` VALUES ('130705', '宣化区', '130700', 3);
INSERT INTO `sys_region` VALUES ('130706', '下花园区', '130700', 3);
INSERT INTO `sys_region` VALUES ('130708', '万全区', '130700', 3);
INSERT INTO `sys_region` VALUES ('130709', '崇礼区', '130700', 3);
INSERT INTO `sys_region` VALUES ('130722', '张北县', '130700', 3);
INSERT INTO `sys_region` VALUES ('130723', '康保县', '130700', 3);
INSERT INTO `sys_region` VALUES ('130724', '沽源县', '130700', 3);
INSERT INTO `sys_region` VALUES ('130725', '尚义县', '130700', 3);
INSERT INTO `sys_region` VALUES ('130726', '蔚县', '130700', 3);
INSERT INTO `sys_region` VALUES ('130727', '阳原县', '130700', 3);
INSERT INTO `sys_region` VALUES ('130728', '怀安县', '130700', 3);
INSERT INTO `sys_region` VALUES ('130730', '怀来县', '130700', 3);
INSERT INTO `sys_region` VALUES ('130731', '涿鹿县', '130700', 3);
INSERT INTO `sys_region` VALUES ('130732', '赤城县', '130700', 3);
INSERT INTO `sys_region` VALUES ('130800', '承德市', '130000', 2);
INSERT INTO `sys_region` VALUES ('130802', '双桥区', '130800', 3);
INSERT INTO `sys_region` VALUES ('130803', '双滦区', '130800', 3);
INSERT INTO `sys_region` VALUES ('130804', '鹰手营子矿区', '130800', 3);
INSERT INTO `sys_region` VALUES ('130821', '承德县', '130800', 3);
INSERT INTO `sys_region` VALUES ('130822', '兴隆县', '130800', 3);
INSERT INTO `sys_region` VALUES ('130824', '滦平县', '130800', 3);
INSERT INTO `sys_region` VALUES ('130825', '隆化县', '130800', 3);
INSERT INTO `sys_region` VALUES ('130826', '丰宁满族自治县', '130800', 3);
INSERT INTO `sys_region` VALUES ('130827', '宽城满族自治县', '130800', 3);
INSERT INTO `sys_region` VALUES ('130828', '围场满族蒙古族自治县', '130800', 3);
INSERT INTO `sys_region` VALUES ('130881', '平泉市', '130800', 3);
INSERT INTO `sys_region` VALUES ('130900', '沧州市', '130000', 2);
INSERT INTO `sys_region` VALUES ('130902', '新华区', '130900', 3);
INSERT INTO `sys_region` VALUES ('130903', '运河区', '130900', 3);
INSERT INTO `sys_region` VALUES ('130921', '沧县', '130900', 3);
INSERT INTO `sys_region` VALUES ('130922', '青县', '130900', 3);
INSERT INTO `sys_region` VALUES ('130923', '东光县', '130900', 3);
INSERT INTO `sys_region` VALUES ('130924', '海兴县', '130900', 3);
INSERT INTO `sys_region` VALUES ('130925', '盐山县', '130900', 3);
INSERT INTO `sys_region` VALUES ('130926', '肃宁县', '130900', 3);
INSERT INTO `sys_region` VALUES ('130927', '南皮县', '130900', 3);
INSERT INTO `sys_region` VALUES ('130928', '吴桥县', '130900', 3);
INSERT INTO `sys_region` VALUES ('130929', '献县', '130900', 3);
INSERT INTO `sys_region` VALUES ('130930', '孟村回族自治县', '130900', 3);
INSERT INTO `sys_region` VALUES ('130981', '泊头市', '130900', 3);
INSERT INTO `sys_region` VALUES ('130982', '任丘市', '130900', 3);
INSERT INTO `sys_region` VALUES ('130983', '黄骅市', '130900', 3);
INSERT INTO `sys_region` VALUES ('130984', '河间市', '130900', 3);
INSERT INTO `sys_region` VALUES ('131000', '廊坊市', '130000', 2);
INSERT INTO `sys_region` VALUES ('131002', '安次区', '131000', 3);
INSERT INTO `sys_region` VALUES ('131003', '广阳区', '131000', 3);
INSERT INTO `sys_region` VALUES ('131022', '固安县', '131000', 3);
INSERT INTO `sys_region` VALUES ('131023', '永清县', '131000', 3);
INSERT INTO `sys_region` VALUES ('131024', '香河县', '131000', 3);
INSERT INTO `sys_region` VALUES ('131025', '大城县', '131000', 3);
INSERT INTO `sys_region` VALUES ('131026', '文安县', '131000', 3);
INSERT INTO `sys_region` VALUES ('131028', '大厂回族自治县', '131000', 3);
INSERT INTO `sys_region` VALUES ('131081', '霸州市', '131000', 3);
INSERT INTO `sys_region` VALUES ('131082', '三河市', '131000', 3);
INSERT INTO `sys_region` VALUES ('131100', '衡水市', '130000', 2);
INSERT INTO `sys_region` VALUES ('131102', '桃城区', '131100', 3);
INSERT INTO `sys_region` VALUES ('131103', '冀州区', '131100', 3);
INSERT INTO `sys_region` VALUES ('131121', '枣强县', '131100', 3);
INSERT INTO `sys_region` VALUES ('131122', '武邑县', '131100', 3);
INSERT INTO `sys_region` VALUES ('131123', '武强县', '131100', 3);
INSERT INTO `sys_region` VALUES ('131124', '饶阳县', '131100', 3);
INSERT INTO `sys_region` VALUES ('131125', '安平县', '131100', 3);
INSERT INTO `sys_region` VALUES ('131126', '故城县', '131100', 3);
INSERT INTO `sys_region` VALUES ('131127', '景县', '131100', 3);
INSERT INTO `sys_region` VALUES ('131128', '阜城县', '131100', 3);
INSERT INTO `sys_region` VALUES ('131182', '深州市', '131100', 3);
INSERT INTO `sys_region` VALUES ('140000', '山西省', '0', 1);
INSERT INTO `sys_region` VALUES ('140100', '太原市', '140000', 2);
INSERT INTO `sys_region` VALUES ('140105', '小店区', '140100', 3);
INSERT INTO `sys_region` VALUES ('140106', '迎泽区', '140100', 3);
INSERT INTO `sys_region` VALUES ('140107', '杏花岭区', '140100', 3);
INSERT INTO `sys_region` VALUES ('140108', '尖草坪区', '140100', 3);
INSERT INTO `sys_region` VALUES ('140109', '万柏林区', '140100', 3);
INSERT INTO `sys_region` VALUES ('140110', '晋源区', '140100', 3);
INSERT INTO `sys_region` VALUES ('140121', '清徐县', '140100', 3);
INSERT INTO `sys_region` VALUES ('140122', '阳曲县', '140100', 3);
INSERT INTO `sys_region` VALUES ('140123', '娄烦县', '140100', 3);
INSERT INTO `sys_region` VALUES ('140181', '古交市', '140100', 3);
INSERT INTO `sys_region` VALUES ('140200', '大同市', '140000', 2);
INSERT INTO `sys_region` VALUES ('140212', '新荣区', '140200', 3);
INSERT INTO `sys_region` VALUES ('140213', '平城区', '140200', 3);
INSERT INTO `sys_region` VALUES ('140214', '云冈区', '140200', 3);
INSERT INTO `sys_region` VALUES ('140215', '云州区', '140200', 3);
INSERT INTO `sys_region` VALUES ('140221', '阳高县', '140200', 3);
INSERT INTO `sys_region` VALUES ('140222', '天镇县', '140200', 3);
INSERT INTO `sys_region` VALUES ('140223', '广灵县', '140200', 3);
INSERT INTO `sys_region` VALUES ('140224', '灵丘县', '140200', 3);
INSERT INTO `sys_region` VALUES ('140225', '浑源县', '140200', 3);
INSERT INTO `sys_region` VALUES ('140226', '左云县', '140200', 3);
INSERT INTO `sys_region` VALUES ('140300', '阳泉市', '140000', 2);
INSERT INTO `sys_region` VALUES ('140302', '城区', '140300', 3);
INSERT INTO `sys_region` VALUES ('140303', '矿区', '140300', 3);
INSERT INTO `sys_region` VALUES ('140311', '郊区', '140300', 3);
INSERT INTO `sys_region` VALUES ('140321', '平定县', '140300', 3);
INSERT INTO `sys_region` VALUES ('140322', '盂县', '140300', 3);
INSERT INTO `sys_region` VALUES ('140400', '长治市', '140000', 2);
INSERT INTO `sys_region` VALUES ('140403', '潞州区', '140400', 3);
INSERT INTO `sys_region` VALUES ('140404', '上党区', '140400', 3);
INSERT INTO `sys_region` VALUES ('140405', '屯留区', '140400', 3);
INSERT INTO `sys_region` VALUES ('140406', '潞城区', '140400', 3);
INSERT INTO `sys_region` VALUES ('140423', '襄垣县', '140400', 3);
INSERT INTO `sys_region` VALUES ('140425', '平顺县', '140400', 3);
INSERT INTO `sys_region` VALUES ('140426', '黎城县', '140400', 3);
INSERT INTO `sys_region` VALUES ('140427', '壶关县', '140400', 3);
INSERT INTO `sys_region` VALUES ('140428', '长子县', '140400', 3);
INSERT INTO `sys_region` VALUES ('140429', '武乡县', '140400', 3);
INSERT INTO `sys_region` VALUES ('140430', '沁县', '140400', 3);
INSERT INTO `sys_region` VALUES ('140431', '沁源县', '140400', 3);
INSERT INTO `sys_region` VALUES ('140500', '晋城市', '140000', 2);
INSERT INTO `sys_region` VALUES ('140502', '城区', '140500', 3);
INSERT INTO `sys_region` VALUES ('140521', '沁水县', '140500', 3);
INSERT INTO `sys_region` VALUES ('140522', '阳城县', '140500', 3);
INSERT INTO `sys_region` VALUES ('140524', '陵川县', '140500', 3);
INSERT INTO `sys_region` VALUES ('140525', '泽州县', '140500', 3);
INSERT INTO `sys_region` VALUES ('140581', '高平市', '140500', 3);
INSERT INTO `sys_region` VALUES ('140600', '朔州市', '140000', 2);
INSERT INTO `sys_region` VALUES ('140602', '朔城区', '140600', 3);
INSERT INTO `sys_region` VALUES ('140603', '平鲁区', '140600', 3);
INSERT INTO `sys_region` VALUES ('140621', '山阴县', '140600', 3);
INSERT INTO `sys_region` VALUES ('140622', '应县', '140600', 3);
INSERT INTO `sys_region` VALUES ('140623', '右玉县', '140600', 3);
INSERT INTO `sys_region` VALUES ('140681', '怀仁市', '140600', 3);
INSERT INTO `sys_region` VALUES ('140700', '晋中市', '140000', 2);
INSERT INTO `sys_region` VALUES ('140702', '榆次区', '140700', 3);
INSERT INTO `sys_region` VALUES ('140703', '太谷区', '140700', 3);
INSERT INTO `sys_region` VALUES ('140721', '榆社县', '140700', 3);
INSERT INTO `sys_region` VALUES ('140722', '左权县', '140700', 3);
INSERT INTO `sys_region` VALUES ('140723', '和顺县', '140700', 3);
INSERT INTO `sys_region` VALUES ('140724', '昔阳县', '140700', 3);
INSERT INTO `sys_region` VALUES ('140725', '寿阳县', '140700', 3);
INSERT INTO `sys_region` VALUES ('140727', '祁县', '140700', 3);
INSERT INTO `sys_region` VALUES ('140728', '平遥县', '140700', 3);
INSERT INTO `sys_region` VALUES ('140729', '灵石县', '140700', 3);
INSERT INTO `sys_region` VALUES ('140781', '介休市', '140700', 3);
INSERT INTO `sys_region` VALUES ('140800', '运城市', '140000', 2);
INSERT INTO `sys_region` VALUES ('140802', '盐湖区', '140800', 3);
INSERT INTO `sys_region` VALUES ('140821', '临猗县', '140800', 3);
INSERT INTO `sys_region` VALUES ('140822', '万荣县', '140800', 3);
INSERT INTO `sys_region` VALUES ('140823', '闻喜县', '140800', 3);
INSERT INTO `sys_region` VALUES ('140824', '稷山县', '140800', 3);
INSERT INTO `sys_region` VALUES ('140825', '新绛县', '140800', 3);
INSERT INTO `sys_region` VALUES ('140826', '绛县', '140800', 3);
INSERT INTO `sys_region` VALUES ('140827', '垣曲县', '140800', 3);
INSERT INTO `sys_region` VALUES ('140828', '夏县', '140800', 3);
INSERT INTO `sys_region` VALUES ('140829', '平陆县', '140800', 3);
INSERT INTO `sys_region` VALUES ('140830', '芮城县', '140800', 3);
INSERT INTO `sys_region` VALUES ('140881', '永济市', '140800', 3);
INSERT INTO `sys_region` VALUES ('140882', '河津市', '140800', 3);
INSERT INTO `sys_region` VALUES ('140900', '忻州市', '140000', 2);
INSERT INTO `sys_region` VALUES ('140902', '忻府区', '140900', 3);
INSERT INTO `sys_region` VALUES ('140921', '定襄县', '140900', 3);
INSERT INTO `sys_region` VALUES ('140922', '五台县', '140900', 3);
INSERT INTO `sys_region` VALUES ('140923', '代县', '140900', 3);
INSERT INTO `sys_region` VALUES ('140924', '繁峙县', '140900', 3);
INSERT INTO `sys_region` VALUES ('140925', '宁武县', '140900', 3);
INSERT INTO `sys_region` VALUES ('140926', '静乐县', '140900', 3);
INSERT INTO `sys_region` VALUES ('140927', '神池县', '140900', 3);
INSERT INTO `sys_region` VALUES ('140928', '五寨县', '140900', 3);
INSERT INTO `sys_region` VALUES ('140929', '岢岚县', '140900', 3);
INSERT INTO `sys_region` VALUES ('140930', '河曲县', '140900', 3);
INSERT INTO `sys_region` VALUES ('140931', '保德县', '140900', 3);
INSERT INTO `sys_region` VALUES ('140932', '偏关县', '140900', 3);
INSERT INTO `sys_region` VALUES ('140981', '原平市', '140900', 3);
INSERT INTO `sys_region` VALUES ('141000', '临汾市', '140000', 2);
INSERT INTO `sys_region` VALUES ('141002', '尧都区', '141000', 3);
INSERT INTO `sys_region` VALUES ('141021', '曲沃县', '141000', 3);
INSERT INTO `sys_region` VALUES ('141022', '翼城县', '141000', 3);
INSERT INTO `sys_region` VALUES ('141023', '襄汾县', '141000', 3);
INSERT INTO `sys_region` VALUES ('141024', '洪洞县', '141000', 3);
INSERT INTO `sys_region` VALUES ('141025', '古县', '141000', 3);
INSERT INTO `sys_region` VALUES ('141026', '安泽县', '141000', 3);
INSERT INTO `sys_region` VALUES ('141027', '浮山县', '141000', 3);
INSERT INTO `sys_region` VALUES ('141028', '吉县', '141000', 3);
INSERT INTO `sys_region` VALUES ('141029', '乡宁县', '141000', 3);
INSERT INTO `sys_region` VALUES ('141030', '大宁县', '141000', 3);
INSERT INTO `sys_region` VALUES ('141031', '隰县', '141000', 3);
INSERT INTO `sys_region` VALUES ('141032', '永和县', '141000', 3);
INSERT INTO `sys_region` VALUES ('141033', '蒲县', '141000', 3);
INSERT INTO `sys_region` VALUES ('141034', '汾西县', '141000', 3);
INSERT INTO `sys_region` VALUES ('141081', '侯马市', '141000', 3);
INSERT INTO `sys_region` VALUES ('141082', '霍州市', '141000', 3);
INSERT INTO `sys_region` VALUES ('141100', '吕梁市', '140000', 2);
INSERT INTO `sys_region` VALUES ('141102', '离石区', '141100', 3);
INSERT INTO `sys_region` VALUES ('141121', '文水县', '141100', 3);
INSERT INTO `sys_region` VALUES ('141122', '交城县', '141100', 3);
INSERT INTO `sys_region` VALUES ('141123', '兴县', '141100', 3);
INSERT INTO `sys_region` VALUES ('141124', '临县', '141100', 3);
INSERT INTO `sys_region` VALUES ('141125', '柳林县', '141100', 3);
INSERT INTO `sys_region` VALUES ('141126', '石楼县', '141100', 3);
INSERT INTO `sys_region` VALUES ('141127', '岚县', '141100', 3);
INSERT INTO `sys_region` VALUES ('141128', '方山县', '141100', 3);
INSERT INTO `sys_region` VALUES ('141129', '中阳县', '141100', 3);
INSERT INTO `sys_region` VALUES ('141130', '交口县', '141100', 3);
INSERT INTO `sys_region` VALUES ('141181', '孝义市', '141100', 3);
INSERT INTO `sys_region` VALUES ('141182', '汾阳市', '141100', 3);
INSERT INTO `sys_region` VALUES ('150000', '内蒙古自治区', '0', 1);
INSERT INTO `sys_region` VALUES ('150100', '呼和浩特市', '150000', 2);
INSERT INTO `sys_region` VALUES ('150102', '新城区', '150100', 3);
INSERT INTO `sys_region` VALUES ('150103', '回民区', '150100', 3);
INSERT INTO `sys_region` VALUES ('150104', '玉泉区', '150100', 3);
INSERT INTO `sys_region` VALUES ('150105', '赛罕区', '150100', 3);
INSERT INTO `sys_region` VALUES ('150121', '土默特左旗', '150100', 3);
INSERT INTO `sys_region` VALUES ('150122', '托克托县', '150100', 3);
INSERT INTO `sys_region` VALUES ('150123', '和林格尔县', '150100', 3);
INSERT INTO `sys_region` VALUES ('150124', '清水河县', '150100', 3);
INSERT INTO `sys_region` VALUES ('150125', '武川县', '150100', 3);
INSERT INTO `sys_region` VALUES ('150200', '包头市', '150000', 2);
INSERT INTO `sys_region` VALUES ('150202', '东河区', '150200', 3);
INSERT INTO `sys_region` VALUES ('150203', '昆都仑区', '150200', 3);
INSERT INTO `sys_region` VALUES ('150204', '青山区', '150200', 3);
INSERT INTO `sys_region` VALUES ('150205', '石拐区', '150200', 3);
INSERT INTO `sys_region` VALUES ('150206', '白云鄂博矿区', '150200', 3);
INSERT INTO `sys_region` VALUES ('150207', '九原区', '150200', 3);
INSERT INTO `sys_region` VALUES ('150221', '土默特右旗', '150200', 3);
INSERT INTO `sys_region` VALUES ('150222', '固阳县', '150200', 3);
INSERT INTO `sys_region` VALUES ('150223', '达尔罕茂明安联合旗', '150200', 3);
INSERT INTO `sys_region` VALUES ('150300', '乌海市', '150000', 2);
INSERT INTO `sys_region` VALUES ('150302', '海勃湾区', '150300', 3);
INSERT INTO `sys_region` VALUES ('150303', '海南区', '150300', 3);
INSERT INTO `sys_region` VALUES ('150304', '乌达区', '150300', 3);
INSERT INTO `sys_region` VALUES ('150400', '赤峰市', '150000', 2);
INSERT INTO `sys_region` VALUES ('150402', '红山区', '150400', 3);
INSERT INTO `sys_region` VALUES ('150403', '元宝山区', '150400', 3);
INSERT INTO `sys_region` VALUES ('150404', '松山区', '150400', 3);
INSERT INTO `sys_region` VALUES ('150421', '阿鲁科尔沁旗', '150400', 3);
INSERT INTO `sys_region` VALUES ('150422', '巴林左旗', '150400', 3);
INSERT INTO `sys_region` VALUES ('150423', '巴林右旗', '150400', 3);
INSERT INTO `sys_region` VALUES ('150424', '林西县', '150400', 3);
INSERT INTO `sys_region` VALUES ('150425', '克什克腾旗', '150400', 3);
INSERT INTO `sys_region` VALUES ('150426', '翁牛特旗', '150400', 3);
INSERT INTO `sys_region` VALUES ('150428', '喀喇沁旗', '150400', 3);
INSERT INTO `sys_region` VALUES ('150429', '宁城县', '150400', 3);
INSERT INTO `sys_region` VALUES ('150430', '敖汉旗', '150400', 3);
INSERT INTO `sys_region` VALUES ('150500', '通辽市', '150000', 2);
INSERT INTO `sys_region` VALUES ('150502', '科尔沁区', '150500', 3);
INSERT INTO `sys_region` VALUES ('150521', '科尔沁左翼中旗', '150500', 3);
INSERT INTO `sys_region` VALUES ('150522', '科尔沁左翼后旗', '150500', 3);
INSERT INTO `sys_region` VALUES ('150523', '开鲁县', '150500', 3);
INSERT INTO `sys_region` VALUES ('150524', '库伦旗', '150500', 3);
INSERT INTO `sys_region` VALUES ('150525', '奈曼旗', '150500', 3);
INSERT INTO `sys_region` VALUES ('150526', '扎鲁特旗', '150500', 3);
INSERT INTO `sys_region` VALUES ('150581', '霍林郭勒市', '150500', 3);
INSERT INTO `sys_region` VALUES ('150600', '鄂尔多斯市', '150000', 2);
INSERT INTO `sys_region` VALUES ('150602', '东胜区', '150600', 3);
INSERT INTO `sys_region` VALUES ('150603', '康巴什区', '150600', 3);
INSERT INTO `sys_region` VALUES ('150621', '达拉特旗', '150600', 3);
INSERT INTO `sys_region` VALUES ('150622', '准格尔旗', '150600', 3);
INSERT INTO `sys_region` VALUES ('150623', '鄂托克前旗', '150600', 3);
INSERT INTO `sys_region` VALUES ('150624', '鄂托克旗', '150600', 3);
INSERT INTO `sys_region` VALUES ('150625', '杭锦旗', '150600', 3);
INSERT INTO `sys_region` VALUES ('150626', '乌审旗', '150600', 3);
INSERT INTO `sys_region` VALUES ('150627', '伊金霍洛旗', '150600', 3);
INSERT INTO `sys_region` VALUES ('150700', '呼伦贝尔市', '150000', 2);
INSERT INTO `sys_region` VALUES ('150702', '海拉尔区', '150700', 3);
INSERT INTO `sys_region` VALUES ('150703', '扎赉诺尔区', '150700', 3);
INSERT INTO `sys_region` VALUES ('150721', '阿荣旗', '150700', 3);
INSERT INTO `sys_region` VALUES ('150722', '莫力达瓦达斡尔族自治旗', '150700', 3);
INSERT INTO `sys_region` VALUES ('150723', '鄂伦春自治旗', '150700', 3);
INSERT INTO `sys_region` VALUES ('150724', '鄂温克族自治旗', '150700', 3);
INSERT INTO `sys_region` VALUES ('150725', '陈巴尔虎旗', '150700', 3);
INSERT INTO `sys_region` VALUES ('150726', '新巴尔虎左旗', '150700', 3);
INSERT INTO `sys_region` VALUES ('150727', '新巴尔虎右旗', '150700', 3);
INSERT INTO `sys_region` VALUES ('150781', '满洲里市', '150700', 3);
INSERT INTO `sys_region` VALUES ('150782', '牙克石市', '150700', 3);
INSERT INTO `sys_region` VALUES ('150783', '扎兰屯市', '150700', 3);
INSERT INTO `sys_region` VALUES ('150784', '额尔古纳市', '150700', 3);
INSERT INTO `sys_region` VALUES ('150785', '根河市', '150700', 3);
INSERT INTO `sys_region` VALUES ('150800', '巴彦淖尔市', '150000', 2);
INSERT INTO `sys_region` VALUES ('150802', '临河区', '150800', 3);
INSERT INTO `sys_region` VALUES ('150821', '五原县', '150800', 3);
INSERT INTO `sys_region` VALUES ('150822', '磴口县', '150800', 3);
INSERT INTO `sys_region` VALUES ('150823', '乌拉特前旗', '150800', 3);
INSERT INTO `sys_region` VALUES ('150824', '乌拉特中旗', '150800', 3);
INSERT INTO `sys_region` VALUES ('150825', '乌拉特后旗', '150800', 3);
INSERT INTO `sys_region` VALUES ('150826', '杭锦后旗', '150800', 3);
INSERT INTO `sys_region` VALUES ('150900', '乌兰察布市', '150000', 2);
INSERT INTO `sys_region` VALUES ('150902', '集宁区', '150900', 3);
INSERT INTO `sys_region` VALUES ('150921', '卓资县', '150900', 3);
INSERT INTO `sys_region` VALUES ('150922', '化德县', '150900', 3);
INSERT INTO `sys_region` VALUES ('150923', '商都县', '150900', 3);
INSERT INTO `sys_region` VALUES ('150924', '兴和县', '150900', 3);
INSERT INTO `sys_region` VALUES ('150925', '凉城县', '150900', 3);
INSERT INTO `sys_region` VALUES ('150926', '察哈尔右翼前旗', '150900', 3);
INSERT INTO `sys_region` VALUES ('150927', '察哈尔右翼中旗', '150900', 3);
INSERT INTO `sys_region` VALUES ('150928', '察哈尔右翼后旗', '150900', 3);
INSERT INTO `sys_region` VALUES ('150929', '四子王旗', '150900', 3);
INSERT INTO `sys_region` VALUES ('150981', '丰镇市', '150900', 3);
INSERT INTO `sys_region` VALUES ('152200', '兴安盟', '150000', 2);
INSERT INTO `sys_region` VALUES ('152201', '乌兰浩特市', '152200', 3);
INSERT INTO `sys_region` VALUES ('152202', '阿尔山市', '152200', 3);
INSERT INTO `sys_region` VALUES ('152221', '科尔沁右翼前旗', '152200', 3);
INSERT INTO `sys_region` VALUES ('152222', '科尔沁右翼中旗', '152200', 3);
INSERT INTO `sys_region` VALUES ('152223', '扎赉特旗', '152200', 3);
INSERT INTO `sys_region` VALUES ('152224', '突泉县', '152200', 3);
INSERT INTO `sys_region` VALUES ('152500', '锡林郭勒盟', '150000', 2);
INSERT INTO `sys_region` VALUES ('152501', '二连浩特市', '152500', 3);
INSERT INTO `sys_region` VALUES ('152502', '锡林浩特市', '152500', 3);
INSERT INTO `sys_region` VALUES ('152522', '阿巴嘎旗', '152500', 3);
INSERT INTO `sys_region` VALUES ('152523', '苏尼特左旗', '152500', 3);
INSERT INTO `sys_region` VALUES ('152524', '苏尼特右旗', '152500', 3);
INSERT INTO `sys_region` VALUES ('152525', '东乌珠穆沁旗', '152500', 3);
INSERT INTO `sys_region` VALUES ('152526', '西乌珠穆沁旗', '152500', 3);
INSERT INTO `sys_region` VALUES ('152527', '太仆寺旗', '152500', 3);
INSERT INTO `sys_region` VALUES ('152528', '镶黄旗', '152500', 3);
INSERT INTO `sys_region` VALUES ('152529', '正镶白旗', '152500', 3);
INSERT INTO `sys_region` VALUES ('152530', '正蓝旗', '152500', 3);
INSERT INTO `sys_region` VALUES ('152531', '多伦县', '152500', 3);
INSERT INTO `sys_region` VALUES ('152900', '阿拉善盟', '150000', 2);
INSERT INTO `sys_region` VALUES ('152921', '阿拉善左旗', '152900', 3);
INSERT INTO `sys_region` VALUES ('152922', '阿拉善右旗', '152900', 3);
INSERT INTO `sys_region` VALUES ('152923', '额济纳旗', '152900', 3);
INSERT INTO `sys_region` VALUES ('210000', '辽宁省', '0', 1);
INSERT INTO `sys_region` VALUES ('210100', '沈阳市', '210000', 2);
INSERT INTO `sys_region` VALUES ('210102', '和平区', '210100', 3);
INSERT INTO `sys_region` VALUES ('210103', '沈河区', '210100', 3);
INSERT INTO `sys_region` VALUES ('210104', '大东区', '210100', 3);
INSERT INTO `sys_region` VALUES ('210105', '皇姑区', '210100', 3);
INSERT INTO `sys_region` VALUES ('210106', '铁西区', '210100', 3);
INSERT INTO `sys_region` VALUES ('210111', '苏家屯区', '210100', 3);
INSERT INTO `sys_region` VALUES ('210112', '浑南区', '210100', 3);
INSERT INTO `sys_region` VALUES ('210113', '沈北新区', '210100', 3);
INSERT INTO `sys_region` VALUES ('210114', '于洪区', '210100', 3);
INSERT INTO `sys_region` VALUES ('210115', '辽中区', '210100', 3);
INSERT INTO `sys_region` VALUES ('210123', '康平县', '210100', 3);
INSERT INTO `sys_region` VALUES ('210124', '法库县', '210100', 3);
INSERT INTO `sys_region` VALUES ('210181', '新民市', '210100', 3);
INSERT INTO `sys_region` VALUES ('210200', '大连市', '210000', 2);
INSERT INTO `sys_region` VALUES ('210202', '中山区', '210200', 3);
INSERT INTO `sys_region` VALUES ('210203', '西岗区', '210200', 3);
INSERT INTO `sys_region` VALUES ('210204', '沙河口区', '210200', 3);
INSERT INTO `sys_region` VALUES ('210211', '甘井子区', '210200', 3);
INSERT INTO `sys_region` VALUES ('210212', '旅顺口区', '210200', 3);
INSERT INTO `sys_region` VALUES ('210213', '金州区', '210200', 3);
INSERT INTO `sys_region` VALUES ('210214', '普兰店区', '210200', 3);
INSERT INTO `sys_region` VALUES ('210224', '长海县', '210200', 3);
INSERT INTO `sys_region` VALUES ('210281', '瓦房店市', '210200', 3);
INSERT INTO `sys_region` VALUES ('210283', '庄河市', '210200', 3);
INSERT INTO `sys_region` VALUES ('210300', '鞍山市', '210000', 2);
INSERT INTO `sys_region` VALUES ('210302', '铁东区', '210300', 3);
INSERT INTO `sys_region` VALUES ('210303', '铁西区', '210300', 3);
INSERT INTO `sys_region` VALUES ('210304', '立山区', '210300', 3);
INSERT INTO `sys_region` VALUES ('210311', '千山区', '210300', 3);
INSERT INTO `sys_region` VALUES ('210321', '台安县', '210300', 3);
INSERT INTO `sys_region` VALUES ('210323', '岫岩满族自治县', '210300', 3);
INSERT INTO `sys_region` VALUES ('210381', '海城市', '210300', 3);
INSERT INTO `sys_region` VALUES ('210400', '抚顺市', '210000', 2);
INSERT INTO `sys_region` VALUES ('210402', '新抚区', '210400', 3);
INSERT INTO `sys_region` VALUES ('210403', '东洲区', '210400', 3);
INSERT INTO `sys_region` VALUES ('210404', '望花区', '210400', 3);
INSERT INTO `sys_region` VALUES ('210411', '顺城区', '210400', 3);
INSERT INTO `sys_region` VALUES ('210421', '抚顺县', '210400', 3);
INSERT INTO `sys_region` VALUES ('210422', '新宾满族自治县', '210400', 3);
INSERT INTO `sys_region` VALUES ('210423', '清原满族自治县', '210400', 3);
INSERT INTO `sys_region` VALUES ('210500', '本溪市', '210000', 2);
INSERT INTO `sys_region` VALUES ('210502', '平山区', '210500', 3);
INSERT INTO `sys_region` VALUES ('210503', '溪湖区', '210500', 3);
INSERT INTO `sys_region` VALUES ('210504', '明山区', '210500', 3);
INSERT INTO `sys_region` VALUES ('210505', '南芬区', '210500', 3);
INSERT INTO `sys_region` VALUES ('210521', '本溪满族自治县', '210500', 3);
INSERT INTO `sys_region` VALUES ('210522', '桓仁满族自治县', '210500', 3);
INSERT INTO `sys_region` VALUES ('210600', '丹东市', '210000', 2);
INSERT INTO `sys_region` VALUES ('210602', '元宝区', '210600', 3);
INSERT INTO `sys_region` VALUES ('210603', '振兴区', '210600', 3);
INSERT INTO `sys_region` VALUES ('210604', '振安区', '210600', 3);
INSERT INTO `sys_region` VALUES ('210624', '宽甸满族自治县', '210600', 3);
INSERT INTO `sys_region` VALUES ('210681', '东港市', '210600', 3);
INSERT INTO `sys_region` VALUES ('210682', '凤城市', '210600', 3);
INSERT INTO `sys_region` VALUES ('210700', '锦州市', '210000', 2);
INSERT INTO `sys_region` VALUES ('210702', '古塔区', '210700', 3);
INSERT INTO `sys_region` VALUES ('210703', '凌河区', '210700', 3);
INSERT INTO `sys_region` VALUES ('210711', '太和区', '210700', 3);
INSERT INTO `sys_region` VALUES ('210726', '黑山县', '210700', 3);
INSERT INTO `sys_region` VALUES ('210727', '义县', '210700', 3);
INSERT INTO `sys_region` VALUES ('210781', '凌海市', '210700', 3);
INSERT INTO `sys_region` VALUES ('210782', '北镇市', '210700', 3);
INSERT INTO `sys_region` VALUES ('210800', '营口市', '210000', 2);
INSERT INTO `sys_region` VALUES ('210802', '站前区', '210800', 3);
INSERT INTO `sys_region` VALUES ('210803', '西市区', '210800', 3);
INSERT INTO `sys_region` VALUES ('210804', '鲅鱼圈区', '210800', 3);
INSERT INTO `sys_region` VALUES ('210811', '老边区', '210800', 3);
INSERT INTO `sys_region` VALUES ('210881', '盖州市', '210800', 3);
INSERT INTO `sys_region` VALUES ('210882', '大石桥市', '210800', 3);
INSERT INTO `sys_region` VALUES ('210900', '阜新市', '210000', 2);
INSERT INTO `sys_region` VALUES ('210902', '海州区', '210900', 3);
INSERT INTO `sys_region` VALUES ('210903', '新邱区', '210900', 3);
INSERT INTO `sys_region` VALUES ('210904', '太平区', '210900', 3);
INSERT INTO `sys_region` VALUES ('210905', '清河门区', '210900', 3);
INSERT INTO `sys_region` VALUES ('210911', '细河区', '210900', 3);
INSERT INTO `sys_region` VALUES ('210921', '阜新蒙古族自治县', '210900', 3);
INSERT INTO `sys_region` VALUES ('210922', '彰武县', '210900', 3);
INSERT INTO `sys_region` VALUES ('211000', '辽阳市', '210000', 2);
INSERT INTO `sys_region` VALUES ('211002', '白塔区', '211000', 3);
INSERT INTO `sys_region` VALUES ('211003', '文圣区', '211000', 3);
INSERT INTO `sys_region` VALUES ('211004', '宏伟区', '211000', 3);
INSERT INTO `sys_region` VALUES ('211005', '弓长岭区', '211000', 3);
INSERT INTO `sys_region` VALUES ('211011', '太子河区', '211000', 3);
INSERT INTO `sys_region` VALUES ('211021', '辽阳县', '211000', 3);
INSERT INTO `sys_region` VALUES ('211081', '灯塔市', '211000', 3);
INSERT INTO `sys_region` VALUES ('211100', '盘锦市', '210000', 2);
INSERT INTO `sys_region` VALUES ('211102', '双台子区', '211100', 3);
INSERT INTO `sys_region` VALUES ('211103', '兴隆台区', '211100', 3);
INSERT INTO `sys_region` VALUES ('211104', '大洼区', '211100', 3);
INSERT INTO `sys_region` VALUES ('211122', '盘山县', '211100', 3);
INSERT INTO `sys_region` VALUES ('211200', '铁岭市', '210000', 2);
INSERT INTO `sys_region` VALUES ('211202', '银州区', '211200', 3);
INSERT INTO `sys_region` VALUES ('211204', '清河区', '211200', 3);
INSERT INTO `sys_region` VALUES ('211221', '铁岭县', '211200', 3);
INSERT INTO `sys_region` VALUES ('211223', '西丰县', '211200', 3);
INSERT INTO `sys_region` VALUES ('211224', '昌图县', '211200', 3);
INSERT INTO `sys_region` VALUES ('211281', '调兵山市', '211200', 3);
INSERT INTO `sys_region` VALUES ('211282', '开原市', '211200', 3);
INSERT INTO `sys_region` VALUES ('211300', '朝阳市', '210000', 2);
INSERT INTO `sys_region` VALUES ('211302', '双塔区', '211300', 3);
INSERT INTO `sys_region` VALUES ('211303', '龙城区', '211300', 3);
INSERT INTO `sys_region` VALUES ('211321', '朝阳县', '211300', 3);
INSERT INTO `sys_region` VALUES ('211322', '建平县', '211300', 3);
INSERT INTO `sys_region` VALUES ('211324', '喀喇沁左翼蒙古族自治县', '211300', 3);
INSERT INTO `sys_region` VALUES ('211381', '北票市', '211300', 3);
INSERT INTO `sys_region` VALUES ('211382', '凌源市', '211300', 3);
INSERT INTO `sys_region` VALUES ('211400', '葫芦岛市', '210000', 2);
INSERT INTO `sys_region` VALUES ('211402', '连山区', '211400', 3);
INSERT INTO `sys_region` VALUES ('211403', '龙港区', '211400', 3);
INSERT INTO `sys_region` VALUES ('211404', '南票区', '211400', 3);
INSERT INTO `sys_region` VALUES ('211421', '绥中县', '211400', 3);
INSERT INTO `sys_region` VALUES ('211422', '建昌县', '211400', 3);
INSERT INTO `sys_region` VALUES ('211481', '兴城市', '211400', 3);
INSERT INTO `sys_region` VALUES ('220000', '吉林省', '0', 1);
INSERT INTO `sys_region` VALUES ('220100', '长春市', '220000', 2);
INSERT INTO `sys_region` VALUES ('220102', '南关区', '220100', 3);
INSERT INTO `sys_region` VALUES ('220103', '宽城区', '220100', 3);
INSERT INTO `sys_region` VALUES ('220104', '朝阳区', '220100', 3);
INSERT INTO `sys_region` VALUES ('220105', '二道区', '220100', 3);
INSERT INTO `sys_region` VALUES ('220106', '绿园区', '220100', 3);
INSERT INTO `sys_region` VALUES ('220112', '双阳区', '220100', 3);
INSERT INTO `sys_region` VALUES ('220113', '九台区', '220100', 3);
INSERT INTO `sys_region` VALUES ('220122', '农安县', '220100', 3);
INSERT INTO `sys_region` VALUES ('220182', '榆树市', '220100', 3);
INSERT INTO `sys_region` VALUES ('220183', '德惠市', '220100', 3);
INSERT INTO `sys_region` VALUES ('220200', '吉林市', '220000', 2);
INSERT INTO `sys_region` VALUES ('220202', '昌邑区', '220200', 3);
INSERT INTO `sys_region` VALUES ('220203', '龙潭区', '220200', 3);
INSERT INTO `sys_region` VALUES ('220204', '船营区', '220200', 3);
INSERT INTO `sys_region` VALUES ('220211', '丰满区', '220200', 3);
INSERT INTO `sys_region` VALUES ('220221', '永吉县', '220200', 3);
INSERT INTO `sys_region` VALUES ('220281', '蛟河市', '220200', 3);
INSERT INTO `sys_region` VALUES ('220282', '桦甸市', '220200', 3);
INSERT INTO `sys_region` VALUES ('220283', '舒兰市', '220200', 3);
INSERT INTO `sys_region` VALUES ('220284', '磐石市', '220200', 3);
INSERT INTO `sys_region` VALUES ('220300', '四平市', '220000', 2);
INSERT INTO `sys_region` VALUES ('220302', '铁西区', '220300', 3);
INSERT INTO `sys_region` VALUES ('220303', '铁东区', '220300', 3);
INSERT INTO `sys_region` VALUES ('220322', '梨树县', '220300', 3);
INSERT INTO `sys_region` VALUES ('220323', '伊通满族自治县', '220300', 3);
INSERT INTO `sys_region` VALUES ('220381', '公主岭市', '220300', 3);
INSERT INTO `sys_region` VALUES ('220382', '双辽市', '220300', 3);
INSERT INTO `sys_region` VALUES ('220400', '辽源市', '220000', 2);
INSERT INTO `sys_region` VALUES ('220402', '龙山区', '220400', 3);
INSERT INTO `sys_region` VALUES ('220403', '西安区', '220400', 3);
INSERT INTO `sys_region` VALUES ('220421', '东丰县', '220400', 3);
INSERT INTO `sys_region` VALUES ('220422', '东辽县', '220400', 3);
INSERT INTO `sys_region` VALUES ('220500', '通化市', '220000', 2);
INSERT INTO `sys_region` VALUES ('220502', '东昌区', '220500', 3);
INSERT INTO `sys_region` VALUES ('220503', '二道江区', '220500', 3);
INSERT INTO `sys_region` VALUES ('220521', '通化县', '220500', 3);
INSERT INTO `sys_region` VALUES ('220523', '辉南县', '220500', 3);
INSERT INTO `sys_region` VALUES ('220524', '柳河县', '220500', 3);
INSERT INTO `sys_region` VALUES ('220581', '梅河口市', '220500', 3);
INSERT INTO `sys_region` VALUES ('220582', '集安市', '220500', 3);
INSERT INTO `sys_region` VALUES ('220600', '白山市', '220000', 2);
INSERT INTO `sys_region` VALUES ('220602', '浑江区', '220600', 3);
INSERT INTO `sys_region` VALUES ('220605', '江源区', '220600', 3);
INSERT INTO `sys_region` VALUES ('220621', '抚松县', '220600', 3);
INSERT INTO `sys_region` VALUES ('220622', '靖宇县', '220600', 3);
INSERT INTO `sys_region` VALUES ('220623', '长白朝鲜族自治县', '220600', 3);
INSERT INTO `sys_region` VALUES ('220681', '临江市', '220600', 3);
INSERT INTO `sys_region` VALUES ('220700', '松原市', '220000', 2);
INSERT INTO `sys_region` VALUES ('220702', '宁江区', '220700', 3);
INSERT INTO `sys_region` VALUES ('220721', '前郭尔罗斯蒙古族自治县', '220700', 3);
INSERT INTO `sys_region` VALUES ('220722', '长岭县', '220700', 3);
INSERT INTO `sys_region` VALUES ('220723', '乾安县', '220700', 3);
INSERT INTO `sys_region` VALUES ('220781', '扶余市', '220700', 3);
INSERT INTO `sys_region` VALUES ('220800', '白城市', '220000', 2);
INSERT INTO `sys_region` VALUES ('220802', '洮北区', '220800', 3);
INSERT INTO `sys_region` VALUES ('220821', '镇赉县', '220800', 3);
INSERT INTO `sys_region` VALUES ('220822', '通榆县', '220800', 3);
INSERT INTO `sys_region` VALUES ('220881', '洮南市', '220800', 3);
INSERT INTO `sys_region` VALUES ('220882', '大安市', '220800', 3);
INSERT INTO `sys_region` VALUES ('222400', '延边朝鲜族自治州', '220000', 2);
INSERT INTO `sys_region` VALUES ('222401', '延吉市', '222400', 3);
INSERT INTO `sys_region` VALUES ('222402', '图们市', '222400', 3);
INSERT INTO `sys_region` VALUES ('222403', '敦化市', '222400', 3);
INSERT INTO `sys_region` VALUES ('222404', '珲春市', '222400', 3);
INSERT INTO `sys_region` VALUES ('222405', '龙井市', '222400', 3);
INSERT INTO `sys_region` VALUES ('222406', '和龙市', '222400', 3);
INSERT INTO `sys_region` VALUES ('222424', '汪清县', '222400', 3);
INSERT INTO `sys_region` VALUES ('222426', '安图县', '222400', 3);
INSERT INTO `sys_region` VALUES ('230000', '黑龙江省', '0', 1);
INSERT INTO `sys_region` VALUES ('230100', '哈尔滨市', '230000', 2);
INSERT INTO `sys_region` VALUES ('230102', '道里区', '230100', 3);
INSERT INTO `sys_region` VALUES ('230103', '南岗区', '230100', 3);
INSERT INTO `sys_region` VALUES ('230104', '道外区', '230100', 3);
INSERT INTO `sys_region` VALUES ('230108', '平房区', '230100', 3);
INSERT INTO `sys_region` VALUES ('230109', '松北区', '230100', 3);
INSERT INTO `sys_region` VALUES ('230110', '香坊区', '230100', 3);
INSERT INTO `sys_region` VALUES ('230111', '呼兰区', '230100', 3);
INSERT INTO `sys_region` VALUES ('230112', '阿城区', '230100', 3);
INSERT INTO `sys_region` VALUES ('230113', '双城区', '230100', 3);
INSERT INTO `sys_region` VALUES ('230123', '依兰县', '230100', 3);
INSERT INTO `sys_region` VALUES ('230124', '方正县', '230100', 3);
INSERT INTO `sys_region` VALUES ('230125', '宾县', '230100', 3);
INSERT INTO `sys_region` VALUES ('230126', '巴彦县', '230100', 3);
INSERT INTO `sys_region` VALUES ('230127', '木兰县', '230100', 3);
INSERT INTO `sys_region` VALUES ('230128', '通河县', '230100', 3);
INSERT INTO `sys_region` VALUES ('230129', '延寿县', '230100', 3);
INSERT INTO `sys_region` VALUES ('230183', '尚志市', '230100', 3);
INSERT INTO `sys_region` VALUES ('230184', '五常市', '230100', 3);
INSERT INTO `sys_region` VALUES ('230200', '齐齐哈尔市', '230000', 2);
INSERT INTO `sys_region` VALUES ('230202', '龙沙区', '230200', 3);
INSERT INTO `sys_region` VALUES ('230203', '建华区', '230200', 3);
INSERT INTO `sys_region` VALUES ('230204', '铁锋区', '230200', 3);
INSERT INTO `sys_region` VALUES ('230205', '昂昂溪区', '230200', 3);
INSERT INTO `sys_region` VALUES ('230206', '富拉尔基区', '230200', 3);
INSERT INTO `sys_region` VALUES ('230207', '碾子山区', '230200', 3);
INSERT INTO `sys_region` VALUES ('230208', '梅里斯达斡尔族区', '230200', 3);
INSERT INTO `sys_region` VALUES ('230221', '龙江县', '230200', 3);
INSERT INTO `sys_region` VALUES ('230223', '依安县', '230200', 3);
INSERT INTO `sys_region` VALUES ('230224', '泰来县', '230200', 3);
INSERT INTO `sys_region` VALUES ('230225', '甘南县', '230200', 3);
INSERT INTO `sys_region` VALUES ('230227', '富裕县', '230200', 3);
INSERT INTO `sys_region` VALUES ('230229', '克山县', '230200', 3);
INSERT INTO `sys_region` VALUES ('230230', '克东县', '230200', 3);
INSERT INTO `sys_region` VALUES ('230231', '拜泉县', '230200', 3);
INSERT INTO `sys_region` VALUES ('230281', '讷河市', '230200', 3);
INSERT INTO `sys_region` VALUES ('230300', '鸡西市', '230000', 2);
INSERT INTO `sys_region` VALUES ('230302', '鸡冠区', '230300', 3);
INSERT INTO `sys_region` VALUES ('230303', '恒山区', '230300', 3);
INSERT INTO `sys_region` VALUES ('230304', '滴道区', '230300', 3);
INSERT INTO `sys_region` VALUES ('230305', '梨树区', '230300', 3);
INSERT INTO `sys_region` VALUES ('230306', '城子河区', '230300', 3);
INSERT INTO `sys_region` VALUES ('230307', '麻山区', '230300', 3);
INSERT INTO `sys_region` VALUES ('230321', '鸡东县', '230300', 3);
INSERT INTO `sys_region` VALUES ('230381', '虎林市', '230300', 3);
INSERT INTO `sys_region` VALUES ('230382', '密山市', '230300', 3);
INSERT INTO `sys_region` VALUES ('230400', '鹤岗市', '230000', 2);
INSERT INTO `sys_region` VALUES ('230402', '向阳区', '230400', 3);
INSERT INTO `sys_region` VALUES ('230403', '工农区', '230400', 3);
INSERT INTO `sys_region` VALUES ('230404', '南山区', '230400', 3);
INSERT INTO `sys_region` VALUES ('230405', '兴安区', '230400', 3);
INSERT INTO `sys_region` VALUES ('230406', '东山区', '230400', 3);
INSERT INTO `sys_region` VALUES ('230407', '兴山区', '230400', 3);
INSERT INTO `sys_region` VALUES ('230421', '萝北县', '230400', 3);
INSERT INTO `sys_region` VALUES ('230422', '绥滨县', '230400', 3);
INSERT INTO `sys_region` VALUES ('230500', '双鸭山市', '230000', 2);
INSERT INTO `sys_region` VALUES ('230502', '尖山区', '230500', 3);
INSERT INTO `sys_region` VALUES ('230503', '岭东区', '230500', 3);
INSERT INTO `sys_region` VALUES ('230505', '四方台区', '230500', 3);
INSERT INTO `sys_region` VALUES ('230506', '宝山区', '230500', 3);
INSERT INTO `sys_region` VALUES ('230521', '集贤县', '230500', 3);
INSERT INTO `sys_region` VALUES ('230522', '友谊县', '230500', 3);
INSERT INTO `sys_region` VALUES ('230523', '宝清县', '230500', 3);
INSERT INTO `sys_region` VALUES ('230524', '饶河县', '230500', 3);
INSERT INTO `sys_region` VALUES ('230600', '大庆市', '230000', 2);
INSERT INTO `sys_region` VALUES ('230602', '萨尔图区', '230600', 3);
INSERT INTO `sys_region` VALUES ('230603', '龙凤区', '230600', 3);
INSERT INTO `sys_region` VALUES ('230604', '让胡路区', '230600', 3);
INSERT INTO `sys_region` VALUES ('230605', '红岗区', '230600', 3);
INSERT INTO `sys_region` VALUES ('230606', '大同区', '230600', 3);
INSERT INTO `sys_region` VALUES ('230621', '肇州县', '230600', 3);
INSERT INTO `sys_region` VALUES ('230622', '肇源县', '230600', 3);
INSERT INTO `sys_region` VALUES ('230623', '林甸县', '230600', 3);
INSERT INTO `sys_region` VALUES ('230624', '杜尔伯特蒙古族自治县', '230600', 3);
INSERT INTO `sys_region` VALUES ('230700', '伊春市', '230000', 2);
INSERT INTO `sys_region` VALUES ('230717', '伊美区', '230700', 3);
INSERT INTO `sys_region` VALUES ('230718', '乌翠区', '230700', 3);
INSERT INTO `sys_region` VALUES ('230719', '友好区', '230700', 3);
INSERT INTO `sys_region` VALUES ('230722', '嘉荫县', '230700', 3);
INSERT INTO `sys_region` VALUES ('230723', '汤旺县', '230700', 3);
INSERT INTO `sys_region` VALUES ('230724', '丰林县', '230700', 3);
INSERT INTO `sys_region` VALUES ('230725', '大箐山县', '230700', 3);
INSERT INTO `sys_region` VALUES ('230726', '南岔县', '230700', 3);
INSERT INTO `sys_region` VALUES ('230751', '金林区', '230700', 3);
INSERT INTO `sys_region` VALUES ('230781', '铁力市', '230700', 3);
INSERT INTO `sys_region` VALUES ('230800', '佳木斯市', '230000', 2);
INSERT INTO `sys_region` VALUES ('230803', '向阳区', '230800', 3);
INSERT INTO `sys_region` VALUES ('230804', '前进区', '230800', 3);
INSERT INTO `sys_region` VALUES ('230805', '东风区', '230800', 3);
INSERT INTO `sys_region` VALUES ('230811', '郊区', '230800', 3);
INSERT INTO `sys_region` VALUES ('230822', '桦南县', '230800', 3);
INSERT INTO `sys_region` VALUES ('230826', '桦川县', '230800', 3);
INSERT INTO `sys_region` VALUES ('230828', '汤原县', '230800', 3);
INSERT INTO `sys_region` VALUES ('230881', '同江市', '230800', 3);
INSERT INTO `sys_region` VALUES ('230882', '富锦市', '230800', 3);
INSERT INTO `sys_region` VALUES ('230883', '抚远市', '230800', 3);
INSERT INTO `sys_region` VALUES ('230900', '七台河市', '230000', 2);
INSERT INTO `sys_region` VALUES ('230902', '新兴区', '230900', 3);
INSERT INTO `sys_region` VALUES ('230903', '桃山区', '230900', 3);
INSERT INTO `sys_region` VALUES ('230904', '茄子河区', '230900', 3);
INSERT INTO `sys_region` VALUES ('230921', '勃利县', '230900', 3);
INSERT INTO `sys_region` VALUES ('231000', '牡丹江市', '230000', 2);
INSERT INTO `sys_region` VALUES ('231002', '东安区', '231000', 3);
INSERT INTO `sys_region` VALUES ('231003', '阳明区', '231000', 3);
INSERT INTO `sys_region` VALUES ('231004', '爱民区', '231000', 3);
INSERT INTO `sys_region` VALUES ('231005', '西安区', '231000', 3);
INSERT INTO `sys_region` VALUES ('231025', '林口县', '231000', 3);
INSERT INTO `sys_region` VALUES ('231081', '绥芬河市', '231000', 3);
INSERT INTO `sys_region` VALUES ('231083', '海林市', '231000', 3);
INSERT INTO `sys_region` VALUES ('231084', '宁安市', '231000', 3);
INSERT INTO `sys_region` VALUES ('231085', '穆棱市', '231000', 3);
INSERT INTO `sys_region` VALUES ('231086', '东宁市', '231000', 3);
INSERT INTO `sys_region` VALUES ('231100', '黑河市', '230000', 2);
INSERT INTO `sys_region` VALUES ('231102', '爱辉区', '231100', 3);
INSERT INTO `sys_region` VALUES ('231123', '逊克县', '231100', 3);
INSERT INTO `sys_region` VALUES ('231124', '孙吴县', '231100', 3);
INSERT INTO `sys_region` VALUES ('231181', '北安市', '231100', 3);
INSERT INTO `sys_region` VALUES ('231182', '五大连池市', '231100', 3);
INSERT INTO `sys_region` VALUES ('231183', '嫩江市', '231100', 3);
INSERT INTO `sys_region` VALUES ('231200', '绥化市', '230000', 2);
INSERT INTO `sys_region` VALUES ('231202', '北林区', '231200', 3);
INSERT INTO `sys_region` VALUES ('231221', '望奎县', '231200', 3);
INSERT INTO `sys_region` VALUES ('231222', '兰西县', '231200', 3);
INSERT INTO `sys_region` VALUES ('231223', '青冈县', '231200', 3);
INSERT INTO `sys_region` VALUES ('231224', '庆安县', '231200', 3);
INSERT INTO `sys_region` VALUES ('231225', '明水县', '231200', 3);
INSERT INTO `sys_region` VALUES ('231226', '绥棱县', '231200', 3);
INSERT INTO `sys_region` VALUES ('231281', '安达市', '231200', 3);
INSERT INTO `sys_region` VALUES ('231282', '肇东市', '231200', 3);
INSERT INTO `sys_region` VALUES ('231283', '海伦市', '231200', 3);
INSERT INTO `sys_region` VALUES ('232700', '大兴安岭地区', '230000', 2);
INSERT INTO `sys_region` VALUES ('232701', '漠河市', '232700', 3);
INSERT INTO `sys_region` VALUES ('232721', '呼玛县', '232700', 3);
INSERT INTO `sys_region` VALUES ('232722', '塔河县', '232700', 3);
INSERT INTO `sys_region` VALUES ('310000', '上海市', '0', 1);
INSERT INTO `sys_region` VALUES ('310000', '上海市', '310000', 2);
INSERT INTO `sys_region` VALUES ('310101', '黄浦区', '310000', 3);
INSERT INTO `sys_region` VALUES ('310104', '徐汇区', '310000', 3);
INSERT INTO `sys_region` VALUES ('310105', '长宁区', '310000', 3);
INSERT INTO `sys_region` VALUES ('310106', '静安区', '310000', 3);
INSERT INTO `sys_region` VALUES ('310107', '普陀区', '310000', 3);
INSERT INTO `sys_region` VALUES ('310109', '虹口区', '310000', 3);
INSERT INTO `sys_region` VALUES ('310110', '杨浦区', '310000', 3);
INSERT INTO `sys_region` VALUES ('310112', '闵行区', '310000', 3);
INSERT INTO `sys_region` VALUES ('310113', '宝山区', '310000', 3);
INSERT INTO `sys_region` VALUES ('310114', '嘉定区', '310000', 3);
INSERT INTO `sys_region` VALUES ('310115', '浦东新区', '310000', 3);
INSERT INTO `sys_region` VALUES ('310116', '金山区', '310000', 3);
INSERT INTO `sys_region` VALUES ('310117', '松江区', '310000', 3);
INSERT INTO `sys_region` VALUES ('310118', '青浦区', '310000', 3);
INSERT INTO `sys_region` VALUES ('310120', '奉贤区', '310000', 3);
INSERT INTO `sys_region` VALUES ('310151', '崇明区', '310000', 3);
INSERT INTO `sys_region` VALUES ('320000', '江苏省', '0', 1);
INSERT INTO `sys_region` VALUES ('320100', '南京市', '320000', 2);
INSERT INTO `sys_region` VALUES ('320102', '玄武区', '320100', 3);
INSERT INTO `sys_region` VALUES ('320104', '秦淮区', '320100', 3);
INSERT INTO `sys_region` VALUES ('320105', '建邺区', '320100', 3);
INSERT INTO `sys_region` VALUES ('320106', '鼓楼区', '320100', 3);
INSERT INTO `sys_region` VALUES ('320111', '浦口区', '320100', 3);
INSERT INTO `sys_region` VALUES ('320113', '栖霞区', '320100', 3);
INSERT INTO `sys_region` VALUES ('320114', '雨花台区', '320100', 3);
INSERT INTO `sys_region` VALUES ('320115', '江宁区', '320100', 3);
INSERT INTO `sys_region` VALUES ('320116', '六合区', '320100', 3);
INSERT INTO `sys_region` VALUES ('320117', '溧水区', '320100', 3);
INSERT INTO `sys_region` VALUES ('320118', '高淳区', '320100', 3);
INSERT INTO `sys_region` VALUES ('320200', '无锡市', '320000', 2);
INSERT INTO `sys_region` VALUES ('320205', '锡山区', '320200', 3);
INSERT INTO `sys_region` VALUES ('320206', '惠山区', '320200', 3);
INSERT INTO `sys_region` VALUES ('320211', '滨湖区', '320200', 3);
INSERT INTO `sys_region` VALUES ('320213', '梁溪区', '320200', 3);
INSERT INTO `sys_region` VALUES ('320214', '新吴区', '320200', 3);
INSERT INTO `sys_region` VALUES ('320281', '江阴市', '320200', 3);
INSERT INTO `sys_region` VALUES ('320282', '宜兴市', '320200', 3);
INSERT INTO `sys_region` VALUES ('320300', '徐州市', '320000', 2);
INSERT INTO `sys_region` VALUES ('320302', '鼓楼区', '320300', 3);
INSERT INTO `sys_region` VALUES ('320303', '云龙区', '320300', 3);
INSERT INTO `sys_region` VALUES ('320305', '贾汪区', '320300', 3);
INSERT INTO `sys_region` VALUES ('320311', '泉山区', '320300', 3);
INSERT INTO `sys_region` VALUES ('320312', '铜山区', '320300', 3);
INSERT INTO `sys_region` VALUES ('320321', '丰县', '320300', 3);
INSERT INTO `sys_region` VALUES ('320322', '沛县', '320300', 3);
INSERT INTO `sys_region` VALUES ('320324', '睢宁县', '320300', 3);
INSERT INTO `sys_region` VALUES ('320381', '新沂市', '320300', 3);
INSERT INTO `sys_region` VALUES ('320382', '邳州市', '320300', 3);
INSERT INTO `sys_region` VALUES ('320400', '常州市', '320000', 2);
INSERT INTO `sys_region` VALUES ('320402', '天宁区', '320400', 3);
INSERT INTO `sys_region` VALUES ('320404', '钟楼区', '320400', 3);
INSERT INTO `sys_region` VALUES ('320411', '新北区', '320400', 3);
INSERT INTO `sys_region` VALUES ('320412', '武进区', '320400', 3);
INSERT INTO `sys_region` VALUES ('320413', '金坛区', '320400', 3);
INSERT INTO `sys_region` VALUES ('320481', '溧阳市', '320400', 3);
INSERT INTO `sys_region` VALUES ('320500', '苏州市', '320000', 2);
INSERT INTO `sys_region` VALUES ('320505', '虎丘区', '320500', 3);
INSERT INTO `sys_region` VALUES ('320506', '吴中区', '320500', 3);
INSERT INTO `sys_region` VALUES ('320507', '相城区', '320500', 3);
INSERT INTO `sys_region` VALUES ('320508', '姑苏区', '320500', 3);
INSERT INTO `sys_region` VALUES ('320509', '吴江区', '320500', 3);
INSERT INTO `sys_region` VALUES ('320581', '常熟市', '320500', 3);
INSERT INTO `sys_region` VALUES ('320582', '张家港市', '320500', 3);
INSERT INTO `sys_region` VALUES ('320583', '昆山市', '320500', 3);
INSERT INTO `sys_region` VALUES ('320585', '太仓市', '320500', 3);
INSERT INTO `sys_region` VALUES ('320600', '南通市', '320000', 2);
INSERT INTO `sys_region` VALUES ('320602', '崇川区', '320600', 3);
INSERT INTO `sys_region` VALUES ('320611', '港闸区', '320600', 3);
INSERT INTO `sys_region` VALUES ('320612', '通州区', '320600', 3);
INSERT INTO `sys_region` VALUES ('320623', '如东县', '320600', 3);
INSERT INTO `sys_region` VALUES ('320681', '启东市', '320600', 3);
INSERT INTO `sys_region` VALUES ('320682', '如皋市', '320600', 3);
INSERT INTO `sys_region` VALUES ('320684', '海门市', '320600', 3);
INSERT INTO `sys_region` VALUES ('320685', '海安市', '320600', 3);
INSERT INTO `sys_region` VALUES ('320700', '连云港市', '320000', 2);
INSERT INTO `sys_region` VALUES ('320703', '连云区', '320700', 3);
INSERT INTO `sys_region` VALUES ('320706', '海州区', '320700', 3);
INSERT INTO `sys_region` VALUES ('320707', '赣榆区', '320700', 3);
INSERT INTO `sys_region` VALUES ('320722', '东海县', '320700', 3);
INSERT INTO `sys_region` VALUES ('320723', '灌云县', '320700', 3);
INSERT INTO `sys_region` VALUES ('320724', '灌南县', '320700', 3);
INSERT INTO `sys_region` VALUES ('320800', '淮安市', '320000', 2);
INSERT INTO `sys_region` VALUES ('320803', '淮安区', '320800', 3);
INSERT INTO `sys_region` VALUES ('320804', '淮阴区', '320800', 3);
INSERT INTO `sys_region` VALUES ('320812', '清江浦区', '320800', 3);
INSERT INTO `sys_region` VALUES ('320813', '洪泽区', '320800', 3);
INSERT INTO `sys_region` VALUES ('320826', '涟水县', '320800', 3);
INSERT INTO `sys_region` VALUES ('320830', '盱眙县', '320800', 3);
INSERT INTO `sys_region` VALUES ('320831', '金湖县', '320800', 3);
INSERT INTO `sys_region` VALUES ('320900', '盐城市', '320000', 2);
INSERT INTO `sys_region` VALUES ('320902', '亭湖区', '320900', 3);
INSERT INTO `sys_region` VALUES ('320903', '盐都区', '320900', 3);
INSERT INTO `sys_region` VALUES ('320904', '大丰区', '320900', 3);
INSERT INTO `sys_region` VALUES ('320921', '响水县', '320900', 3);
INSERT INTO `sys_region` VALUES ('320922', '滨海县', '320900', 3);
INSERT INTO `sys_region` VALUES ('320923', '阜宁县', '320900', 3);
INSERT INTO `sys_region` VALUES ('320924', '射阳县', '320900', 3);
INSERT INTO `sys_region` VALUES ('320925', '建湖县', '320900', 3);
INSERT INTO `sys_region` VALUES ('320981', '东台市', '320900', 3);
INSERT INTO `sys_region` VALUES ('321000', '扬州市', '320000', 2);
INSERT INTO `sys_region` VALUES ('321002', '广陵区', '321000', 3);
INSERT INTO `sys_region` VALUES ('321003', '邗江区', '321000', 3);
INSERT INTO `sys_region` VALUES ('321012', '江都区', '321000', 3);
INSERT INTO `sys_region` VALUES ('321023', '宝应县', '321000', 3);
INSERT INTO `sys_region` VALUES ('321081', '仪征市', '321000', 3);
INSERT INTO `sys_region` VALUES ('321084', '高邮市', '321000', 3);
INSERT INTO `sys_region` VALUES ('321100', '镇江市', '320000', 2);
INSERT INTO `sys_region` VALUES ('321102', '京口区', '321100', 3);
INSERT INTO `sys_region` VALUES ('321111', '润州区', '321100', 3);
INSERT INTO `sys_region` VALUES ('321112', '丹徒区', '321100', 3);
INSERT INTO `sys_region` VALUES ('321181', '丹阳市', '321100', 3);
INSERT INTO `sys_region` VALUES ('321182', '扬中市', '321100', 3);
INSERT INTO `sys_region` VALUES ('321183', '句容市', '321100', 3);
INSERT INTO `sys_region` VALUES ('321200', '泰州市', '320000', 2);
INSERT INTO `sys_region` VALUES ('321202', '海陵区', '321200', 3);
INSERT INTO `sys_region` VALUES ('321203', '高港区', '321200', 3);
INSERT INTO `sys_region` VALUES ('321204', '姜堰区', '321200', 3);
INSERT INTO `sys_region` VALUES ('321281', '兴化市', '321200', 3);
INSERT INTO `sys_region` VALUES ('321282', '靖江市', '321200', 3);
INSERT INTO `sys_region` VALUES ('321283', '泰兴市', '321200', 3);
INSERT INTO `sys_region` VALUES ('321300', '宿迁市', '320000', 2);
INSERT INTO `sys_region` VALUES ('321302', '宿城区', '321300', 3);
INSERT INTO `sys_region` VALUES ('321311', '宿豫区', '321300', 3);
INSERT INTO `sys_region` VALUES ('321322', '沭阳县', '321300', 3);
INSERT INTO `sys_region` VALUES ('321323', '泗阳县', '321300', 3);
INSERT INTO `sys_region` VALUES ('321324', '泗洪县', '321300', 3);
INSERT INTO `sys_region` VALUES ('330000', '浙江省', '0', 1);
INSERT INTO `sys_region` VALUES ('330100', '杭州市', '330000', 2);
INSERT INTO `sys_region` VALUES ('330102', '上城区', '330100', 3);
INSERT INTO `sys_region` VALUES ('330103', '下城区', '330100', 3);
INSERT INTO `sys_region` VALUES ('330104', '江干区', '330100', 3);
INSERT INTO `sys_region` VALUES ('330105', '拱墅区', '330100', 3);
INSERT INTO `sys_region` VALUES ('330106', '西湖区', '330100', 3);
INSERT INTO `sys_region` VALUES ('330108', '滨江区', '330100', 3);
INSERT INTO `sys_region` VALUES ('330109', '萧山区', '330100', 3);
INSERT INTO `sys_region` VALUES ('330110', '余杭区', '330100', 3);
INSERT INTO `sys_region` VALUES ('330111', '富阳区', '330100', 3);
INSERT INTO `sys_region` VALUES ('330112', '临安区', '330100', 3);
INSERT INTO `sys_region` VALUES ('330122', '桐庐县', '330100', 3);
INSERT INTO `sys_region` VALUES ('330127', '淳安县', '330100', 3);
INSERT INTO `sys_region` VALUES ('330182', '建德市', '330100', 3);
INSERT INTO `sys_region` VALUES ('330200', '宁波市', '330000', 2);
INSERT INTO `sys_region` VALUES ('330203', '海曙区', '330200', 3);
INSERT INTO `sys_region` VALUES ('330205', '江北区', '330200', 3);
INSERT INTO `sys_region` VALUES ('330206', '北仑区', '330200', 3);
INSERT INTO `sys_region` VALUES ('330211', '镇海区', '330200', 3);
INSERT INTO `sys_region` VALUES ('330212', '鄞州区', '330200', 3);
INSERT INTO `sys_region` VALUES ('330213', '奉化区', '330200', 3);
INSERT INTO `sys_region` VALUES ('330225', '象山县', '330200', 3);
INSERT INTO `sys_region` VALUES ('330226', '宁海县', '330200', 3);
INSERT INTO `sys_region` VALUES ('330281', '余姚市', '330200', 3);
INSERT INTO `sys_region` VALUES ('330282', '慈溪市', '330200', 3);
INSERT INTO `sys_region` VALUES ('330300', '温州市', '330000', 2);
INSERT INTO `sys_region` VALUES ('330302', '鹿城区', '330300', 3);
INSERT INTO `sys_region` VALUES ('330303', '龙湾区', '330300', 3);
INSERT INTO `sys_region` VALUES ('330304', '瓯海区', '330300', 3);
INSERT INTO `sys_region` VALUES ('330305', '洞头区', '330300', 3);
INSERT INTO `sys_region` VALUES ('330324', '永嘉县', '330300', 3);
INSERT INTO `sys_region` VALUES ('330326', '平阳县', '330300', 3);
INSERT INTO `sys_region` VALUES ('330327', '苍南县', '330300', 3);
INSERT INTO `sys_region` VALUES ('330328', '文成县', '330300', 3);
INSERT INTO `sys_region` VALUES ('330329', '泰顺县', '330300', 3);
INSERT INTO `sys_region` VALUES ('330381', '瑞安市', '330300', 3);
INSERT INTO `sys_region` VALUES ('330382', '乐清市', '330300', 3);
INSERT INTO `sys_region` VALUES ('330383', '龙港市', '330300', 3);
INSERT INTO `sys_region` VALUES ('330400', '嘉兴市', '330000', 2);
INSERT INTO `sys_region` VALUES ('330402', '南湖区', '330400', 3);
INSERT INTO `sys_region` VALUES ('330411', '秀洲区', '330400', 3);
INSERT INTO `sys_region` VALUES ('330421', '嘉善县', '330400', 3);
INSERT INTO `sys_region` VALUES ('330424', '海盐县', '330400', 3);
INSERT INTO `sys_region` VALUES ('330481', '海宁市', '330400', 3);
INSERT INTO `sys_region` VALUES ('330482', '平湖市', '330400', 3);
INSERT INTO `sys_region` VALUES ('330483', '桐乡市', '330400', 3);
INSERT INTO `sys_region` VALUES ('330500', '湖州市', '330000', 2);
INSERT INTO `sys_region` VALUES ('330502', '吴兴区', '330500', 3);
INSERT INTO `sys_region` VALUES ('330503', '南浔区', '330500', 3);
INSERT INTO `sys_region` VALUES ('330521', '德清县', '330500', 3);
INSERT INTO `sys_region` VALUES ('330522', '长兴县', '330500', 3);
INSERT INTO `sys_region` VALUES ('330523', '安吉县', '330500', 3);
INSERT INTO `sys_region` VALUES ('330600', '绍兴市', '330000', 2);
INSERT INTO `sys_region` VALUES ('330602', '越城区', '330600', 3);
INSERT INTO `sys_region` VALUES ('330603', '柯桥区', '330600', 3);
INSERT INTO `sys_region` VALUES ('330604', '上虞区', '330600', 3);
INSERT INTO `sys_region` VALUES ('330624', '新昌县', '330600', 3);
INSERT INTO `sys_region` VALUES ('330681', '诸暨市', '330600', 3);
INSERT INTO `sys_region` VALUES ('330683', '嵊州市', '330600', 3);
INSERT INTO `sys_region` VALUES ('330700', '金华市', '330000', 2);
INSERT INTO `sys_region` VALUES ('330702', '婺城区', '330700', 3);
INSERT INTO `sys_region` VALUES ('330703', '金东区', '330700', 3);
INSERT INTO `sys_region` VALUES ('330723', '武义县', '330700', 3);
INSERT INTO `sys_region` VALUES ('330726', '浦江县', '330700', 3);
INSERT INTO `sys_region` VALUES ('330727', '磐安县', '330700', 3);
INSERT INTO `sys_region` VALUES ('330781', '兰溪市', '330700', 3);
INSERT INTO `sys_region` VALUES ('330782', '义乌市', '330700', 3);
INSERT INTO `sys_region` VALUES ('330783', '东阳市', '330700', 3);
INSERT INTO `sys_region` VALUES ('330784', '永康市', '330700', 3);
INSERT INTO `sys_region` VALUES ('330800', '衢州市', '330000', 2);
INSERT INTO `sys_region` VALUES ('330802', '柯城区', '330800', 3);
INSERT INTO `sys_region` VALUES ('330803', '衢江区', '330800', 3);
INSERT INTO `sys_region` VALUES ('330822', '常山县', '330800', 3);
INSERT INTO `sys_region` VALUES ('330824', '开化县', '330800', 3);
INSERT INTO `sys_region` VALUES ('330825', '龙游县', '330800', 3);
INSERT INTO `sys_region` VALUES ('330881', '江山市', '330800', 3);
INSERT INTO `sys_region` VALUES ('330900', '舟山市', '330000', 2);
INSERT INTO `sys_region` VALUES ('330902', '定海区', '330900', 3);
INSERT INTO `sys_region` VALUES ('330903', '普陀区', '330900', 3);
INSERT INTO `sys_region` VALUES ('330921', '岱山县', '330900', 3);
INSERT INTO `sys_region` VALUES ('330922', '嵊泗县', '330900', 3);
INSERT INTO `sys_region` VALUES ('331000', '台州市', '330000', 2);
INSERT INTO `sys_region` VALUES ('331002', '椒江区', '331000', 3);
INSERT INTO `sys_region` VALUES ('331003', '黄岩区', '331000', 3);
INSERT INTO `sys_region` VALUES ('331004', '路桥区', '331000', 3);
INSERT INTO `sys_region` VALUES ('331022', '三门县', '331000', 3);
INSERT INTO `sys_region` VALUES ('331023', '天台县', '331000', 3);
INSERT INTO `sys_region` VALUES ('331024', '仙居县', '331000', 3);
INSERT INTO `sys_region` VALUES ('331081', '温岭市', '331000', 3);
INSERT INTO `sys_region` VALUES ('331082', '临海市', '331000', 3);
INSERT INTO `sys_region` VALUES ('331083', '玉环市', '331000', 3);
INSERT INTO `sys_region` VALUES ('331100', '丽水市', '330000', 2);
INSERT INTO `sys_region` VALUES ('331102', '莲都区', '331100', 3);
INSERT INTO `sys_region` VALUES ('331121', '青田县', '331100', 3);
INSERT INTO `sys_region` VALUES ('331122', '缙云县', '331100', 3);
INSERT INTO `sys_region` VALUES ('331123', '遂昌县', '331100', 3);
INSERT INTO `sys_region` VALUES ('331124', '松阳县', '331100', 3);
INSERT INTO `sys_region` VALUES ('331125', '云和县', '331100', 3);
INSERT INTO `sys_region` VALUES ('331126', '庆元县', '331100', 3);
INSERT INTO `sys_region` VALUES ('331127', '景宁畲族自治县', '331100', 3);
INSERT INTO `sys_region` VALUES ('331181', '龙泉市', '331100', 3);
INSERT INTO `sys_region` VALUES ('340000', '安徽省', '0', 1);
INSERT INTO `sys_region` VALUES ('340100', '合肥市', '340000', 2);
INSERT INTO `sys_region` VALUES ('340102', '瑶海区', '340100', 3);
INSERT INTO `sys_region` VALUES ('340103', '庐阳区', '340100', 3);
INSERT INTO `sys_region` VALUES ('340104', '蜀山区', '340100', 3);
INSERT INTO `sys_region` VALUES ('340111', '包河区', '340100', 3);
INSERT INTO `sys_region` VALUES ('340121', '长丰县', '340100', 3);
INSERT INTO `sys_region` VALUES ('340122', '肥东县', '340100', 3);
INSERT INTO `sys_region` VALUES ('340123', '肥西县', '340100', 3);
INSERT INTO `sys_region` VALUES ('340124', '庐江县', '340100', 3);
INSERT INTO `sys_region` VALUES ('340181', '巢湖市', '340100', 3);
INSERT INTO `sys_region` VALUES ('340200', '芜湖市', '340000', 2);
INSERT INTO `sys_region` VALUES ('340202', '镜湖区', '340200', 3);
INSERT INTO `sys_region` VALUES ('340203', '弋江区', '340200', 3);
INSERT INTO `sys_region` VALUES ('340207', '鸠江区', '340200', 3);
INSERT INTO `sys_region` VALUES ('340208', '三山区', '340200', 3);
INSERT INTO `sys_region` VALUES ('340221', '芜湖县', '340200', 3);
INSERT INTO `sys_region` VALUES ('340222', '繁昌县', '340200', 3);
INSERT INTO `sys_region` VALUES ('340223', '南陵县', '340200', 3);
INSERT INTO `sys_region` VALUES ('340281', '无为市', '340200', 3);
INSERT INTO `sys_region` VALUES ('340300', '蚌埠市', '340000', 2);
INSERT INTO `sys_region` VALUES ('340302', '龙子湖区', '340300', 3);
INSERT INTO `sys_region` VALUES ('340303', '蚌山区', '340300', 3);
INSERT INTO `sys_region` VALUES ('340304', '禹会区', '340300', 3);
INSERT INTO `sys_region` VALUES ('340311', '淮上区', '340300', 3);
INSERT INTO `sys_region` VALUES ('340321', '怀远县', '340300', 3);
INSERT INTO `sys_region` VALUES ('340322', '五河县', '340300', 3);
INSERT INTO `sys_region` VALUES ('340323', '固镇县', '340300', 3);
INSERT INTO `sys_region` VALUES ('340400', '淮南市', '340000', 2);
INSERT INTO `sys_region` VALUES ('340402', '大通区', '340400', 3);
INSERT INTO `sys_region` VALUES ('340403', '田家庵区', '340400', 3);
INSERT INTO `sys_region` VALUES ('340404', '谢家集区', '340400', 3);
INSERT INTO `sys_region` VALUES ('340405', '八公山区', '340400', 3);
INSERT INTO `sys_region` VALUES ('340406', '潘集区', '340400', 3);
INSERT INTO `sys_region` VALUES ('340421', '凤台县', '340400', 3);
INSERT INTO `sys_region` VALUES ('340422', '寿县', '340400', 3);
INSERT INTO `sys_region` VALUES ('340500', '马鞍山市', '340000', 2);
INSERT INTO `sys_region` VALUES ('340503', '花山区', '340500', 3);
INSERT INTO `sys_region` VALUES ('340504', '雨山区', '340500', 3);
INSERT INTO `sys_region` VALUES ('340506', '博望区', '340500', 3);
INSERT INTO `sys_region` VALUES ('340521', '当涂县', '340500', 3);
INSERT INTO `sys_region` VALUES ('340522', '含山县', '340500', 3);
INSERT INTO `sys_region` VALUES ('340523', '和县', '340500', 3);
INSERT INTO `sys_region` VALUES ('340600', '淮北市', '340000', 2);
INSERT INTO `sys_region` VALUES ('340602', '杜集区', '340600', 3);
INSERT INTO `sys_region` VALUES ('340603', '相山区', '340600', 3);
INSERT INTO `sys_region` VALUES ('340604', '烈山区', '340600', 3);
INSERT INTO `sys_region` VALUES ('340621', '濉溪县', '340600', 3);
INSERT INTO `sys_region` VALUES ('340700', '铜陵市', '340000', 2);
INSERT INTO `sys_region` VALUES ('340705', '铜官区', '340700', 3);
INSERT INTO `sys_region` VALUES ('340706', '义安区', '340700', 3);
INSERT INTO `sys_region` VALUES ('340711', '郊区', '340700', 3);
INSERT INTO `sys_region` VALUES ('340722', '枞阳县', '340700', 3);
INSERT INTO `sys_region` VALUES ('340800', '安庆市', '340000', 2);
INSERT INTO `sys_region` VALUES ('340802', '迎江区', '340800', 3);
INSERT INTO `sys_region` VALUES ('340803', '大观区', '340800', 3);
INSERT INTO `sys_region` VALUES ('340811', '宜秀区', '340800', 3);
INSERT INTO `sys_region` VALUES ('340822', '怀宁县', '340800', 3);
INSERT INTO `sys_region` VALUES ('340825', '太湖县', '340800', 3);
INSERT INTO `sys_region` VALUES ('340826', '宿松县', '340800', 3);
INSERT INTO `sys_region` VALUES ('340827', '望江县', '340800', 3);
INSERT INTO `sys_region` VALUES ('340828', '岳西县', '340800', 3);
INSERT INTO `sys_region` VALUES ('340881', '桐城市', '340800', 3);
INSERT INTO `sys_region` VALUES ('340882', '潜山市', '340800', 3);
INSERT INTO `sys_region` VALUES ('341000', '黄山市', '340000', 2);
INSERT INTO `sys_region` VALUES ('341002', '屯溪区', '341000', 3);
INSERT INTO `sys_region` VALUES ('341003', '黄山区', '341000', 3);
INSERT INTO `sys_region` VALUES ('341004', '徽州区', '341000', 3);
INSERT INTO `sys_region` VALUES ('341021', '歙县', '341000', 3);
INSERT INTO `sys_region` VALUES ('341022', '休宁县', '341000', 3);
INSERT INTO `sys_region` VALUES ('341023', '黟县', '341000', 3);
INSERT INTO `sys_region` VALUES ('341024', '祁门县', '341000', 3);
INSERT INTO `sys_region` VALUES ('341100', '滁州市', '340000', 2);
INSERT INTO `sys_region` VALUES ('341102', '琅琊区', '341100', 3);
INSERT INTO `sys_region` VALUES ('341103', '南谯区', '341100', 3);
INSERT INTO `sys_region` VALUES ('341122', '来安县', '341100', 3);
INSERT INTO `sys_region` VALUES ('341124', '全椒县', '341100', 3);
INSERT INTO `sys_region` VALUES ('341125', '定远县', '341100', 3);
INSERT INTO `sys_region` VALUES ('341126', '凤阳县', '341100', 3);
INSERT INTO `sys_region` VALUES ('341181', '天长市', '341100', 3);
INSERT INTO `sys_region` VALUES ('341182', '明光市', '341100', 3);
INSERT INTO `sys_region` VALUES ('341200', '阜阳市', '340000', 2);
INSERT INTO `sys_region` VALUES ('341202', '颍州区', '341200', 3);
INSERT INTO `sys_region` VALUES ('341203', '颍东区', '341200', 3);
INSERT INTO `sys_region` VALUES ('341204', '颍泉区', '341200', 3);
INSERT INTO `sys_region` VALUES ('341221', '临泉县', '341200', 3);
INSERT INTO `sys_region` VALUES ('341222', '太和县', '341200', 3);
INSERT INTO `sys_region` VALUES ('341225', '阜南县', '341200', 3);
INSERT INTO `sys_region` VALUES ('341226', '颍上县', '341200', 3);
INSERT INTO `sys_region` VALUES ('341282', '界首市', '341200', 3);
INSERT INTO `sys_region` VALUES ('341300', '宿州市', '340000', 2);
INSERT INTO `sys_region` VALUES ('341302', '埇桥区', '341300', 3);
INSERT INTO `sys_region` VALUES ('341321', '砀山县', '341300', 3);
INSERT INTO `sys_region` VALUES ('341322', '萧县', '341300', 3);
INSERT INTO `sys_region` VALUES ('341323', '灵璧县', '341300', 3);
INSERT INTO `sys_region` VALUES ('341324', '泗县', '341300', 3);
INSERT INTO `sys_region` VALUES ('341500', '六安市', '340000', 2);
INSERT INTO `sys_region` VALUES ('341502', '金安区', '341500', 3);
INSERT INTO `sys_region` VALUES ('341503', '裕安区', '341500', 3);
INSERT INTO `sys_region` VALUES ('341504', '叶集区', '341500', 3);
INSERT INTO `sys_region` VALUES ('341522', '霍邱县', '341500', 3);
INSERT INTO `sys_region` VALUES ('341523', '舒城县', '341500', 3);
INSERT INTO `sys_region` VALUES ('341524', '金寨县', '341500', 3);
INSERT INTO `sys_region` VALUES ('341525', '霍山县', '341500', 3);
INSERT INTO `sys_region` VALUES ('341600', '亳州市', '340000', 2);
INSERT INTO `sys_region` VALUES ('341602', '谯城区', '341600', 3);
INSERT INTO `sys_region` VALUES ('341621', '涡阳县', '341600', 3);
INSERT INTO `sys_region` VALUES ('341622', '蒙城县', '341600', 3);
INSERT INTO `sys_region` VALUES ('341623', '利辛县', '341600', 3);
INSERT INTO `sys_region` VALUES ('341700', '池州市', '340000', 2);
INSERT INTO `sys_region` VALUES ('341702', '贵池区', '341700', 3);
INSERT INTO `sys_region` VALUES ('341721', '东至县', '341700', 3);
INSERT INTO `sys_region` VALUES ('341722', '石台县', '341700', 3);
INSERT INTO `sys_region` VALUES ('341723', '青阳县', '341700', 3);
INSERT INTO `sys_region` VALUES ('341800', '宣城市', '340000', 2);
INSERT INTO `sys_region` VALUES ('341802', '宣州区', '341800', 3);
INSERT INTO `sys_region` VALUES ('341821', '郎溪县', '341800', 3);
INSERT INTO `sys_region` VALUES ('341823', '泾县', '341800', 3);
INSERT INTO `sys_region` VALUES ('341824', '绩溪县', '341800', 3);
INSERT INTO `sys_region` VALUES ('341825', '旌德县', '341800', 3);
INSERT INTO `sys_region` VALUES ('341881', '宁国市', '341800', 3);
INSERT INTO `sys_region` VALUES ('341882', '广德市', '341800', 3);
INSERT INTO `sys_region` VALUES ('350000', '福建省', '0', 1);
INSERT INTO `sys_region` VALUES ('350100', '福州市', '350000', 2);
INSERT INTO `sys_region` VALUES ('350102', '鼓楼区', '350100', 3);
INSERT INTO `sys_region` VALUES ('350103', '台江区', '350100', 3);
INSERT INTO `sys_region` VALUES ('350104', '仓山区', '350100', 3);
INSERT INTO `sys_region` VALUES ('350105', '马尾区', '350100', 3);
INSERT INTO `sys_region` VALUES ('350111', '晋安区', '350100', 3);
INSERT INTO `sys_region` VALUES ('350112', '长乐区', '350100', 3);
INSERT INTO `sys_region` VALUES ('350121', '闽侯县', '350100', 3);
INSERT INTO `sys_region` VALUES ('350122', '连江县', '350100', 3);
INSERT INTO `sys_region` VALUES ('350123', '罗源县', '350100', 3);
INSERT INTO `sys_region` VALUES ('350124', '闽清县', '350100', 3);
INSERT INTO `sys_region` VALUES ('350125', '永泰县', '350100', 3);
INSERT INTO `sys_region` VALUES ('350128', '平潭县', '350100', 3);
INSERT INTO `sys_region` VALUES ('350181', '福清市', '350100', 3);
INSERT INTO `sys_region` VALUES ('350200', '厦门市', '350000', 2);
INSERT INTO `sys_region` VALUES ('350203', '思明区', '350200', 3);
INSERT INTO `sys_region` VALUES ('350205', '海沧区', '350200', 3);
INSERT INTO `sys_region` VALUES ('350206', '湖里区', '350200', 3);
INSERT INTO `sys_region` VALUES ('350211', '集美区', '350200', 3);
INSERT INTO `sys_region` VALUES ('350212', '同安区', '350200', 3);
INSERT INTO `sys_region` VALUES ('350213', '翔安区', '350200', 3);
INSERT INTO `sys_region` VALUES ('350300', '莆田市', '350000', 2);
INSERT INTO `sys_region` VALUES ('350302', '城厢区', '350300', 3);
INSERT INTO `sys_region` VALUES ('350303', '涵江区', '350300', 3);
INSERT INTO `sys_region` VALUES ('350304', '荔城区', '350300', 3);
INSERT INTO `sys_region` VALUES ('350305', '秀屿区', '350300', 3);
INSERT INTO `sys_region` VALUES ('350322', '仙游县', '350300', 3);
INSERT INTO `sys_region` VALUES ('350400', '三明市', '350000', 2);
INSERT INTO `sys_region` VALUES ('350402', '梅列区', '350400', 3);
INSERT INTO `sys_region` VALUES ('350403', '三元区', '350400', 3);
INSERT INTO `sys_region` VALUES ('350421', '明溪县', '350400', 3);
INSERT INTO `sys_region` VALUES ('350423', '清流县', '350400', 3);
INSERT INTO `sys_region` VALUES ('350424', '宁化县', '350400', 3);
INSERT INTO `sys_region` VALUES ('350425', '大田县', '350400', 3);
INSERT INTO `sys_region` VALUES ('350426', '尤溪县', '350400', 3);
INSERT INTO `sys_region` VALUES ('350427', '沙县', '350400', 3);
INSERT INTO `sys_region` VALUES ('350428', '将乐县', '350400', 3);
INSERT INTO `sys_region` VALUES ('350429', '泰宁县', '350400', 3);
INSERT INTO `sys_region` VALUES ('350430', '建宁县', '350400', 3);
INSERT INTO `sys_region` VALUES ('350481', '永安市', '350400', 3);
INSERT INTO `sys_region` VALUES ('350500', '泉州市', '350000', 2);
INSERT INTO `sys_region` VALUES ('350502', '鲤城区', '350500', 3);
INSERT INTO `sys_region` VALUES ('350503', '丰泽区', '350500', 3);
INSERT INTO `sys_region` VALUES ('350504', '洛江区', '350500', 3);
INSERT INTO `sys_region` VALUES ('350505', '泉港区', '350500', 3);
INSERT INTO `sys_region` VALUES ('350521', '惠安县', '350500', 3);
INSERT INTO `sys_region` VALUES ('350524', '安溪县', '350500', 3);
INSERT INTO `sys_region` VALUES ('350525', '永春县', '350500', 3);
INSERT INTO `sys_region` VALUES ('350526', '德化县', '350500', 3);
INSERT INTO `sys_region` VALUES ('350527', '金门县', '350500', 3);
INSERT INTO `sys_region` VALUES ('350581', '石狮市', '350500', 3);
INSERT INTO `sys_region` VALUES ('350582', '晋江市', '350500', 3);
INSERT INTO `sys_region` VALUES ('350583', '南安市', '350500', 3);
INSERT INTO `sys_region` VALUES ('350600', '漳州市', '350000', 2);
INSERT INTO `sys_region` VALUES ('350602', '芗城区', '350600', 3);
INSERT INTO `sys_region` VALUES ('350603', '龙文区', '350600', 3);
INSERT INTO `sys_region` VALUES ('350622', '云霄县', '350600', 3);
INSERT INTO `sys_region` VALUES ('350623', '漳浦县', '350600', 3);
INSERT INTO `sys_region` VALUES ('350624', '诏安县', '350600', 3);
INSERT INTO `sys_region` VALUES ('350625', '长泰县', '350600', 3);
INSERT INTO `sys_region` VALUES ('350626', '东山县', '350600', 3);
INSERT INTO `sys_region` VALUES ('350627', '南靖县', '350600', 3);
INSERT INTO `sys_region` VALUES ('350628', '平和县', '350600', 3);
INSERT INTO `sys_region` VALUES ('350629', '华安县', '350600', 3);
INSERT INTO `sys_region` VALUES ('350681', '龙海市', '350600', 3);
INSERT INTO `sys_region` VALUES ('350700', '南平市', '350000', 2);
INSERT INTO `sys_region` VALUES ('350702', '延平区', '350700', 3);
INSERT INTO `sys_region` VALUES ('350703', '建阳区', '350700', 3);
INSERT INTO `sys_region` VALUES ('350721', '顺昌县', '350700', 3);
INSERT INTO `sys_region` VALUES ('350722', '浦城县', '350700', 3);
INSERT INTO `sys_region` VALUES ('350723', '光泽县', '350700', 3);
INSERT INTO `sys_region` VALUES ('350724', '松溪县', '350700', 3);
INSERT INTO `sys_region` VALUES ('350725', '政和县', '350700', 3);
INSERT INTO `sys_region` VALUES ('350781', '邵武市', '350700', 3);
INSERT INTO `sys_region` VALUES ('350782', '武夷山市', '350700', 3);
INSERT INTO `sys_region` VALUES ('350783', '建瓯市', '350700', 3);
INSERT INTO `sys_region` VALUES ('350800', '龙岩市', '350000', 2);
INSERT INTO `sys_region` VALUES ('350802', '新罗区', '350800', 3);
INSERT INTO `sys_region` VALUES ('350803', '永定区', '350800', 3);
INSERT INTO `sys_region` VALUES ('350821', '长汀县', '350800', 3);
INSERT INTO `sys_region` VALUES ('350823', '上杭县', '350800', 3);
INSERT INTO `sys_region` VALUES ('350824', '武平县', '350800', 3);
INSERT INTO `sys_region` VALUES ('350825', '连城县', '350800', 3);
INSERT INTO `sys_region` VALUES ('350881', '漳平市', '350800', 3);
INSERT INTO `sys_region` VALUES ('350900', '宁德市', '350000', 2);
INSERT INTO `sys_region` VALUES ('350902', '蕉城区', '350900', 3);
INSERT INTO `sys_region` VALUES ('350921', '霞浦县', '350900', 3);
INSERT INTO `sys_region` VALUES ('350922', '古田县', '350900', 3);
INSERT INTO `sys_region` VALUES ('350923', '屏南县', '350900', 3);
INSERT INTO `sys_region` VALUES ('350924', '寿宁县', '350900', 3);
INSERT INTO `sys_region` VALUES ('350925', '周宁县', '350900', 3);
INSERT INTO `sys_region` VALUES ('350926', '柘荣县', '350900', 3);
INSERT INTO `sys_region` VALUES ('350981', '福安市', '350900', 3);
INSERT INTO `sys_region` VALUES ('350982', '福鼎市', '350900', 3);
INSERT INTO `sys_region` VALUES ('360000', '江西省', '0', 1);
INSERT INTO `sys_region` VALUES ('360100', '南昌市', '360000', 2);
INSERT INTO `sys_region` VALUES ('360102', '东湖区', '360100', 3);
INSERT INTO `sys_region` VALUES ('360103', '西湖区', '360100', 3);
INSERT INTO `sys_region` VALUES ('360104', '青云谱区', '360100', 3);
INSERT INTO `sys_region` VALUES ('360111', '青山湖区', '360100', 3);
INSERT INTO `sys_region` VALUES ('360112', '新建区', '360100', 3);
INSERT INTO `sys_region` VALUES ('360113', '红谷滩区', '360100', 3);
INSERT INTO `sys_region` VALUES ('360121', '南昌县', '360100', 3);
INSERT INTO `sys_region` VALUES ('360123', '安义县', '360100', 3);
INSERT INTO `sys_region` VALUES ('360124', '进贤县', '360100', 3);
INSERT INTO `sys_region` VALUES ('360200', '景德镇市', '360000', 2);
INSERT INTO `sys_region` VALUES ('360202', '昌江区', '360200', 3);
INSERT INTO `sys_region` VALUES ('360203', '珠山区', '360200', 3);
INSERT INTO `sys_region` VALUES ('360222', '浮梁县', '360200', 3);
INSERT INTO `sys_region` VALUES ('360281', '乐平市', '360200', 3);
INSERT INTO `sys_region` VALUES ('360300', '萍乡市', '360000', 2);
INSERT INTO `sys_region` VALUES ('360302', '安源区', '360300', 3);
INSERT INTO `sys_region` VALUES ('360313', '湘东区', '360300', 3);
INSERT INTO `sys_region` VALUES ('360321', '莲花县', '360300', 3);
INSERT INTO `sys_region` VALUES ('360322', '上栗县', '360300', 3);
INSERT INTO `sys_region` VALUES ('360323', '芦溪县', '360300', 3);
INSERT INTO `sys_region` VALUES ('360400', '九江市', '360000', 2);
INSERT INTO `sys_region` VALUES ('360402', '濂溪区', '360400', 3);
INSERT INTO `sys_region` VALUES ('360403', '浔阳区', '360400', 3);
INSERT INTO `sys_region` VALUES ('360404', '柴桑区', '360400', 3);
INSERT INTO `sys_region` VALUES ('360423', '武宁县', '360400', 3);
INSERT INTO `sys_region` VALUES ('360424', '修水县', '360400', 3);
INSERT INTO `sys_region` VALUES ('360425', '永修县', '360400', 3);
INSERT INTO `sys_region` VALUES ('360426', '德安县', '360400', 3);
INSERT INTO `sys_region` VALUES ('360428', '都昌县', '360400', 3);
INSERT INTO `sys_region` VALUES ('360429', '湖口县', '360400', 3);
INSERT INTO `sys_region` VALUES ('360430', '彭泽县', '360400', 3);
INSERT INTO `sys_region` VALUES ('360481', '瑞昌市', '360400', 3);
INSERT INTO `sys_region` VALUES ('360482', '共青城市', '360400', 3);
INSERT INTO `sys_region` VALUES ('360483', '庐山市', '360400', 3);
INSERT INTO `sys_region` VALUES ('360500', '新余市', '360000', 2);
INSERT INTO `sys_region` VALUES ('360502', '渝水区', '360500', 3);
INSERT INTO `sys_region` VALUES ('360521', '分宜县', '360500', 3);
INSERT INTO `sys_region` VALUES ('360600', '鹰潭市', '360000', 2);
INSERT INTO `sys_region` VALUES ('360602', '月湖区', '360600', 3);
INSERT INTO `sys_region` VALUES ('360603', '余江区', '360600', 3);
INSERT INTO `sys_region` VALUES ('360681', '贵溪市', '360600', 3);
INSERT INTO `sys_region` VALUES ('360700', '赣州市', '360000', 2);
INSERT INTO `sys_region` VALUES ('360702', '章贡区', '360700', 3);
INSERT INTO `sys_region` VALUES ('360703', '南康区', '360700', 3);
INSERT INTO `sys_region` VALUES ('360704', '赣县区', '360700', 3);
INSERT INTO `sys_region` VALUES ('360722', '信丰县', '360700', 3);
INSERT INTO `sys_region` VALUES ('360723', '大余县', '360700', 3);
INSERT INTO `sys_region` VALUES ('360724', '上犹县', '360700', 3);
INSERT INTO `sys_region` VALUES ('360725', '崇义县', '360700', 3);
INSERT INTO `sys_region` VALUES ('360726', '安远县', '360700', 3);
INSERT INTO `sys_region` VALUES ('360727', '龙南县', '360700', 3);
INSERT INTO `sys_region` VALUES ('360728', '定南县', '360700', 3);
INSERT INTO `sys_region` VALUES ('360729', '全南县', '360700', 3);
INSERT INTO `sys_region` VALUES ('360730', '宁都县', '360700', 3);
INSERT INTO `sys_region` VALUES ('360731', '于都县', '360700', 3);
INSERT INTO `sys_region` VALUES ('360732', '兴国县', '360700', 3);
INSERT INTO `sys_region` VALUES ('360733', '会昌县', '360700', 3);
INSERT INTO `sys_region` VALUES ('360734', '寻乌县', '360700', 3);
INSERT INTO `sys_region` VALUES ('360735', '石城县', '360700', 3);
INSERT INTO `sys_region` VALUES ('360781', '瑞金市', '360700', 3);
INSERT INTO `sys_region` VALUES ('360800', '吉安市', '360000', 2);
INSERT INTO `sys_region` VALUES ('360802', '吉州区', '360800', 3);
INSERT INTO `sys_region` VALUES ('360803', '青原区', '360800', 3);
INSERT INTO `sys_region` VALUES ('360821', '吉安县', '360800', 3);
INSERT INTO `sys_region` VALUES ('360822', '吉水县', '360800', 3);
INSERT INTO `sys_region` VALUES ('360823', '峡江县', '360800', 3);
INSERT INTO `sys_region` VALUES ('360824', '新干县', '360800', 3);
INSERT INTO `sys_region` VALUES ('360825', '永丰县', '360800', 3);
INSERT INTO `sys_region` VALUES ('360826', '泰和县', '360800', 3);
INSERT INTO `sys_region` VALUES ('360827', '遂川县', '360800', 3);
INSERT INTO `sys_region` VALUES ('360828', '万安县', '360800', 3);
INSERT INTO `sys_region` VALUES ('360829', '安福县', '360800', 3);
INSERT INTO `sys_region` VALUES ('360830', '永新县', '360800', 3);
INSERT INTO `sys_region` VALUES ('360881', '井冈山市', '360800', 3);
INSERT INTO `sys_region` VALUES ('360900', '宜春市', '360000', 2);
INSERT INTO `sys_region` VALUES ('360902', '袁州区', '360900', 3);
INSERT INTO `sys_region` VALUES ('360921', '奉新县', '360900', 3);
INSERT INTO `sys_region` VALUES ('360922', '万载县', '360900', 3);
INSERT INTO `sys_region` VALUES ('360923', '上高县', '360900', 3);
INSERT INTO `sys_region` VALUES ('360924', '宜丰县', '360900', 3);
INSERT INTO `sys_region` VALUES ('360925', '靖安县', '360900', 3);
INSERT INTO `sys_region` VALUES ('360926', '铜鼓县', '360900', 3);
INSERT INTO `sys_region` VALUES ('360981', '丰城市', '360900', 3);
INSERT INTO `sys_region` VALUES ('360982', '樟树市', '360900', 3);
INSERT INTO `sys_region` VALUES ('360983', '高安市', '360900', 3);
INSERT INTO `sys_region` VALUES ('361000', '抚州市', '360000', 2);
INSERT INTO `sys_region` VALUES ('361002', '临川区', '361000', 3);
INSERT INTO `sys_region` VALUES ('361003', '东乡区', '361000', 3);
INSERT INTO `sys_region` VALUES ('361021', '南城县', '361000', 3);
INSERT INTO `sys_region` VALUES ('361022', '黎川县', '361000', 3);
INSERT INTO `sys_region` VALUES ('361023', '南丰县', '361000', 3);
INSERT INTO `sys_region` VALUES ('361024', '崇仁县', '361000', 3);
INSERT INTO `sys_region` VALUES ('361025', '乐安县', '361000', 3);
INSERT INTO `sys_region` VALUES ('361026', '宜黄县', '361000', 3);
INSERT INTO `sys_region` VALUES ('361027', '金溪县', '361000', 3);
INSERT INTO `sys_region` VALUES ('361028', '资溪县', '361000', 3);
INSERT INTO `sys_region` VALUES ('361030', '广昌县', '361000', 3);
INSERT INTO `sys_region` VALUES ('361100', '上饶市', '360000', 2);
INSERT INTO `sys_region` VALUES ('361102', '信州区', '361100', 3);
INSERT INTO `sys_region` VALUES ('361103', '广丰区', '361100', 3);
INSERT INTO `sys_region` VALUES ('361104', '广信区', '361100', 3);
INSERT INTO `sys_region` VALUES ('361123', '玉山县', '361100', 3);
INSERT INTO `sys_region` VALUES ('361124', '铅山县', '361100', 3);
INSERT INTO `sys_region` VALUES ('361125', '横峰县', '361100', 3);
INSERT INTO `sys_region` VALUES ('361126', '弋阳县', '361100', 3);
INSERT INTO `sys_region` VALUES ('361127', '余干县', '361100', 3);
INSERT INTO `sys_region` VALUES ('361128', '鄱阳县', '361100', 3);
INSERT INTO `sys_region` VALUES ('361129', '万年县', '361100', 3);
INSERT INTO `sys_region` VALUES ('361130', '婺源县', '361100', 3);
INSERT INTO `sys_region` VALUES ('361181', '德兴市', '361100', 3);
INSERT INTO `sys_region` VALUES ('370000', '山东省', '0', 1);
INSERT INTO `sys_region` VALUES ('370100', '济南市', '370000', 2);
INSERT INTO `sys_region` VALUES ('370102', '历下区', '370100', 3);
INSERT INTO `sys_region` VALUES ('370103', '市中区', '370100', 3);
INSERT INTO `sys_region` VALUES ('370104', '槐荫区', '370100', 3);
INSERT INTO `sys_region` VALUES ('370105', '天桥区', '370100', 3);
INSERT INTO `sys_region` VALUES ('370112', '历城区', '370100', 3);
INSERT INTO `sys_region` VALUES ('370113', '长清区', '370100', 3);
INSERT INTO `sys_region` VALUES ('370114', '章丘区', '370100', 3);
INSERT INTO `sys_region` VALUES ('370115', '济阳区', '370100', 3);
INSERT INTO `sys_region` VALUES ('370116', '莱芜区', '370100', 3);
INSERT INTO `sys_region` VALUES ('370117', '钢城区', '370100', 3);
INSERT INTO `sys_region` VALUES ('370124', '平阴县', '370100', 3);
INSERT INTO `sys_region` VALUES ('370126', '商河县', '370100', 3);
INSERT INTO `sys_region` VALUES ('370200', '青岛市', '370000', 2);
INSERT INTO `sys_region` VALUES ('370202', '市南区', '370200', 3);
INSERT INTO `sys_region` VALUES ('370203', '市北区', '370200', 3);
INSERT INTO `sys_region` VALUES ('370211', '黄岛区', '370200', 3);
INSERT INTO `sys_region` VALUES ('370212', '崂山区', '370200', 3);
INSERT INTO `sys_region` VALUES ('370213', '李沧区', '370200', 3);
INSERT INTO `sys_region` VALUES ('370214', '城阳区', '370200', 3);
INSERT INTO `sys_region` VALUES ('370215', '即墨区', '370200', 3);
INSERT INTO `sys_region` VALUES ('370281', '胶州市', '370200', 3);
INSERT INTO `sys_region` VALUES ('370283', '平度市', '370200', 3);
INSERT INTO `sys_region` VALUES ('370285', '莱西市', '370200', 3);
INSERT INTO `sys_region` VALUES ('370300', '淄博市', '370000', 2);
INSERT INTO `sys_region` VALUES ('370302', '淄川区', '370300', 3);
INSERT INTO `sys_region` VALUES ('370303', '张店区', '370300', 3);
INSERT INTO `sys_region` VALUES ('370304', '博山区', '370300', 3);
INSERT INTO `sys_region` VALUES ('370305', '临淄区', '370300', 3);
INSERT INTO `sys_region` VALUES ('370306', '周村区', '370300', 3);
INSERT INTO `sys_region` VALUES ('370321', '桓台县', '370300', 3);
INSERT INTO `sys_region` VALUES ('370322', '高青县', '370300', 3);
INSERT INTO `sys_region` VALUES ('370323', '沂源县', '370300', 3);
INSERT INTO `sys_region` VALUES ('370400', '枣庄市', '370000', 2);
INSERT INTO `sys_region` VALUES ('370402', '市中区', '370400', 3);
INSERT INTO `sys_region` VALUES ('370403', '薛城区', '370400', 3);
INSERT INTO `sys_region` VALUES ('370404', '峄城区', '370400', 3);
INSERT INTO `sys_region` VALUES ('370405', '台儿庄区', '370400', 3);
INSERT INTO `sys_region` VALUES ('370406', '山亭区', '370400', 3);
INSERT INTO `sys_region` VALUES ('370481', '滕州市', '370400', 3);
INSERT INTO `sys_region` VALUES ('370500', '东营市', '370000', 2);
INSERT INTO `sys_region` VALUES ('370502', '东营区', '370500', 3);
INSERT INTO `sys_region` VALUES ('370503', '河口区', '370500', 3);
INSERT INTO `sys_region` VALUES ('370505', '垦利区', '370500', 3);
INSERT INTO `sys_region` VALUES ('370522', '利津县', '370500', 3);
INSERT INTO `sys_region` VALUES ('370523', '广饶县', '370500', 3);
INSERT INTO `sys_region` VALUES ('370600', '烟台市', '370000', 2);
INSERT INTO `sys_region` VALUES ('370602', '芝罘区', '370600', 3);
INSERT INTO `sys_region` VALUES ('370611', '福山区', '370600', 3);
INSERT INTO `sys_region` VALUES ('370612', '牟平区', '370600', 3);
INSERT INTO `sys_region` VALUES ('370613', '莱山区', '370600', 3);
INSERT INTO `sys_region` VALUES ('370634', '长岛县', '370600', 3);
INSERT INTO `sys_region` VALUES ('370681', '龙口市', '370600', 3);
INSERT INTO `sys_region` VALUES ('370682', '莱阳市', '370600', 3);
INSERT INTO `sys_region` VALUES ('370683', '莱州市', '370600', 3);
INSERT INTO `sys_region` VALUES ('370684', '蓬莱市', '370600', 3);
INSERT INTO `sys_region` VALUES ('370685', '招远市', '370600', 3);
INSERT INTO `sys_region` VALUES ('370686', '栖霞市', '370600', 3);
INSERT INTO `sys_region` VALUES ('370687', '海阳市', '370600', 3);
INSERT INTO `sys_region` VALUES ('370700', '潍坊市', '370000', 2);
INSERT INTO `sys_region` VALUES ('370702', '潍城区', '370700', 3);
INSERT INTO `sys_region` VALUES ('370703', '寒亭区', '370700', 3);
INSERT INTO `sys_region` VALUES ('370704', '坊子区', '370700', 3);
INSERT INTO `sys_region` VALUES ('370705', '奎文区', '370700', 3);
INSERT INTO `sys_region` VALUES ('370724', '临朐县', '370700', 3);
INSERT INTO `sys_region` VALUES ('370725', '昌乐县', '370700', 3);
INSERT INTO `sys_region` VALUES ('370781', '青州市', '370700', 3);
INSERT INTO `sys_region` VALUES ('370782', '诸城市', '370700', 3);
INSERT INTO `sys_region` VALUES ('370783', '寿光市', '370700', 3);
INSERT INTO `sys_region` VALUES ('370784', '安丘市', '370700', 3);
INSERT INTO `sys_region` VALUES ('370785', '高密市', '370700', 3);
INSERT INTO `sys_region` VALUES ('370786', '昌邑市', '370700', 3);
INSERT INTO `sys_region` VALUES ('370800', '济宁市', '370000', 2);
INSERT INTO `sys_region` VALUES ('370811', '任城区', '370800', 3);
INSERT INTO `sys_region` VALUES ('370812', '兖州区', '370800', 3);
INSERT INTO `sys_region` VALUES ('370826', '微山县', '370800', 3);
INSERT INTO `sys_region` VALUES ('370827', '鱼台县', '370800', 3);
INSERT INTO `sys_region` VALUES ('370828', '金乡县', '370800', 3);
INSERT INTO `sys_region` VALUES ('370829', '嘉祥县', '370800', 3);
INSERT INTO `sys_region` VALUES ('370830', '汶上县', '370800', 3);
INSERT INTO `sys_region` VALUES ('370831', '泗水县', '370800', 3);
INSERT INTO `sys_region` VALUES ('370832', '梁山县', '370800', 3);
INSERT INTO `sys_region` VALUES ('370881', '曲阜市', '370800', 3);
INSERT INTO `sys_region` VALUES ('370883', '邹城市', '370800', 3);
INSERT INTO `sys_region` VALUES ('370900', '泰安市', '370000', 2);
INSERT INTO `sys_region` VALUES ('370902', '泰山区', '370900', 3);
INSERT INTO `sys_region` VALUES ('370911', '岱岳区', '370900', 3);
INSERT INTO `sys_region` VALUES ('370921', '宁阳县', '370900', 3);
INSERT INTO `sys_region` VALUES ('370923', '东平县', '370900', 3);
INSERT INTO `sys_region` VALUES ('370982', '新泰市', '370900', 3);
INSERT INTO `sys_region` VALUES ('370983', '肥城市', '370900', 3);
INSERT INTO `sys_region` VALUES ('371000', '威海市', '370000', 2);
INSERT INTO `sys_region` VALUES ('371002', '环翠区', '371000', 3);
INSERT INTO `sys_region` VALUES ('371003', '文登区', '371000', 3);
INSERT INTO `sys_region` VALUES ('371082', '荣成市', '371000', 3);
INSERT INTO `sys_region` VALUES ('371083', '乳山市', '371000', 3);
INSERT INTO `sys_region` VALUES ('371100', '日照市', '370000', 2);
INSERT INTO `sys_region` VALUES ('371102', '东港区', '371100', 3);
INSERT INTO `sys_region` VALUES ('371103', '岚山区', '371100', 3);
INSERT INTO `sys_region` VALUES ('371121', '五莲县', '371100', 3);
INSERT INTO `sys_region` VALUES ('371122', '莒县', '371100', 3);
INSERT INTO `sys_region` VALUES ('371300', '临沂市', '370000', 2);
INSERT INTO `sys_region` VALUES ('371302', '兰山区', '371300', 3);
INSERT INTO `sys_region` VALUES ('371311', '罗庄区', '371300', 3);
INSERT INTO `sys_region` VALUES ('371312', '河东区', '371300', 3);
INSERT INTO `sys_region` VALUES ('371321', '沂南县', '371300', 3);
INSERT INTO `sys_region` VALUES ('371322', '郯城县', '371300', 3);
INSERT INTO `sys_region` VALUES ('371323', '沂水县', '371300', 3);
INSERT INTO `sys_region` VALUES ('371324', '兰陵县', '371300', 3);
INSERT INTO `sys_region` VALUES ('371325', '费县', '371300', 3);
INSERT INTO `sys_region` VALUES ('371326', '平邑县', '371300', 3);
INSERT INTO `sys_region` VALUES ('371327', '莒南县', '371300', 3);
INSERT INTO `sys_region` VALUES ('371328', '蒙阴县', '371300', 3);
INSERT INTO `sys_region` VALUES ('371329', '临沭县', '371300', 3);
INSERT INTO `sys_region` VALUES ('371400', '德州市', '370000', 2);
INSERT INTO `sys_region` VALUES ('371402', '德城区', '371400', 3);
INSERT INTO `sys_region` VALUES ('371403', '陵城区', '371400', 3);
INSERT INTO `sys_region` VALUES ('371422', '宁津县', '371400', 3);
INSERT INTO `sys_region` VALUES ('371423', '庆云县', '371400', 3);
INSERT INTO `sys_region` VALUES ('371424', '临邑县', '371400', 3);
INSERT INTO `sys_region` VALUES ('371425', '齐河县', '371400', 3);
INSERT INTO `sys_region` VALUES ('371426', '平原县', '371400', 3);
INSERT INTO `sys_region` VALUES ('371427', '夏津县', '371400', 3);
INSERT INTO `sys_region` VALUES ('371428', '武城县', '371400', 3);
INSERT INTO `sys_region` VALUES ('371481', '乐陵市', '371400', 3);
INSERT INTO `sys_region` VALUES ('371482', '禹城市', '371400', 3);
INSERT INTO `sys_region` VALUES ('371500', '聊城市', '370000', 2);
INSERT INTO `sys_region` VALUES ('371502', '东昌府区', '371500', 3);
INSERT INTO `sys_region` VALUES ('371503', '茌平区', '371500', 3);
INSERT INTO `sys_region` VALUES ('371521', '阳谷县', '371500', 3);
INSERT INTO `sys_region` VALUES ('371522', '莘县', '371500', 3);
INSERT INTO `sys_region` VALUES ('371524', '东阿县', '371500', 3);
INSERT INTO `sys_region` VALUES ('371525', '冠县', '371500', 3);
INSERT INTO `sys_region` VALUES ('371526', '高唐县', '371500', 3);
INSERT INTO `sys_region` VALUES ('371581', '临清市', '371500', 3);
INSERT INTO `sys_region` VALUES ('371600', '滨州市', '370000', 2);
INSERT INTO `sys_region` VALUES ('371602', '滨城区', '371600', 3);
INSERT INTO `sys_region` VALUES ('371603', '沾化区', '371600', 3);
INSERT INTO `sys_region` VALUES ('371621', '惠民县', '371600', 3);
INSERT INTO `sys_region` VALUES ('371622', '阳信县', '371600', 3);
INSERT INTO `sys_region` VALUES ('371623', '无棣县', '371600', 3);
INSERT INTO `sys_region` VALUES ('371625', '博兴县', '371600', 3);
INSERT INTO `sys_region` VALUES ('371681', '邹平市', '371600', 3);
INSERT INTO `sys_region` VALUES ('371700', '菏泽市', '370000', 2);
INSERT INTO `sys_region` VALUES ('371702', '牡丹区', '371700', 3);
INSERT INTO `sys_region` VALUES ('371703', '定陶区', '371700', 3);
INSERT INTO `sys_region` VALUES ('371721', '曹县', '371700', 3);
INSERT INTO `sys_region` VALUES ('371722', '单县', '371700', 3);
INSERT INTO `sys_region` VALUES ('371723', '成武县', '371700', 3);
INSERT INTO `sys_region` VALUES ('371724', '巨野县', '371700', 3);
INSERT INTO `sys_region` VALUES ('371725', '郓城县', '371700', 3);
INSERT INTO `sys_region` VALUES ('371726', '鄄城县', '371700', 3);
INSERT INTO `sys_region` VALUES ('371728', '东明县', '371700', 3);
INSERT INTO `sys_region` VALUES ('410000', '河南省', '0', 1);
INSERT INTO `sys_region` VALUES ('410100', '郑州市', '410000', 2);
INSERT INTO `sys_region` VALUES ('410102', '中原区', '410100', 3);
INSERT INTO `sys_region` VALUES ('410103', '二七区', '410100', 3);
INSERT INTO `sys_region` VALUES ('410104', '管城回族区', '410100', 3);
INSERT INTO `sys_region` VALUES ('410105', '金水区', '410100', 3);
INSERT INTO `sys_region` VALUES ('410106', '上街区', '410100', 3);
INSERT INTO `sys_region` VALUES ('410108', '惠济区', '410100', 3);
INSERT INTO `sys_region` VALUES ('410122', '中牟县', '410100', 3);
INSERT INTO `sys_region` VALUES ('410181', '巩义市', '410100', 3);
INSERT INTO `sys_region` VALUES ('410182', '荥阳市', '410100', 3);
INSERT INTO `sys_region` VALUES ('410183', '新密市', '410100', 3);
INSERT INTO `sys_region` VALUES ('410184', '新郑市', '410100', 3);
INSERT INTO `sys_region` VALUES ('410185', '登封市', '410100', 3);
INSERT INTO `sys_region` VALUES ('410200', '开封市', '410000', 2);
INSERT INTO `sys_region` VALUES ('410202', '龙亭区', '410200', 3);
INSERT INTO `sys_region` VALUES ('410203', '顺河回族区', '410200', 3);
INSERT INTO `sys_region` VALUES ('410204', '鼓楼区', '410200', 3);
INSERT INTO `sys_region` VALUES ('410205', '禹王台区', '410200', 3);
INSERT INTO `sys_region` VALUES ('410212', '祥符区', '410200', 3);
INSERT INTO `sys_region` VALUES ('410221', '杞县', '410200', 3);
INSERT INTO `sys_region` VALUES ('410222', '通许县', '410200', 3);
INSERT INTO `sys_region` VALUES ('410223', '尉氏县', '410200', 3);
INSERT INTO `sys_region` VALUES ('410225', '兰考县', '410200', 3);
INSERT INTO `sys_region` VALUES ('410300', '洛阳市', '410000', 2);
INSERT INTO `sys_region` VALUES ('410302', '老城区', '410300', 3);
INSERT INTO `sys_region` VALUES ('410303', '西工区', '410300', 3);
INSERT INTO `sys_region` VALUES ('410304', '瀍河回族区', '410300', 3);
INSERT INTO `sys_region` VALUES ('410305', '涧西区', '410300', 3);
INSERT INTO `sys_region` VALUES ('410306', '吉利区', '410300', 3);
INSERT INTO `sys_region` VALUES ('410311', '洛龙区', '410300', 3);
INSERT INTO `sys_region` VALUES ('410322', '孟津县', '410300', 3);
INSERT INTO `sys_region` VALUES ('410323', '新安县', '410300', 3);
INSERT INTO `sys_region` VALUES ('410324', '栾川县', '410300', 3);
INSERT INTO `sys_region` VALUES ('410325', '嵩县', '410300', 3);
INSERT INTO `sys_region` VALUES ('410326', '汝阳县', '410300', 3);
INSERT INTO `sys_region` VALUES ('410327', '宜阳县', '410300', 3);
INSERT INTO `sys_region` VALUES ('410328', '洛宁县', '410300', 3);
INSERT INTO `sys_region` VALUES ('410329', '伊川县', '410300', 3);
INSERT INTO `sys_region` VALUES ('410381', '偃师市', '410300', 3);
INSERT INTO `sys_region` VALUES ('410400', '平顶山市', '410000', 2);
INSERT INTO `sys_region` VALUES ('410402', '新华区', '410400', 3);
INSERT INTO `sys_region` VALUES ('410403', '卫东区', '410400', 3);
INSERT INTO `sys_region` VALUES ('410404', '石龙区', '410400', 3);
INSERT INTO `sys_region` VALUES ('410411', '湛河区', '410400', 3);
INSERT INTO `sys_region` VALUES ('410421', '宝丰县', '410400', 3);
INSERT INTO `sys_region` VALUES ('410422', '叶县', '410400', 3);
INSERT INTO `sys_region` VALUES ('410423', '鲁山县', '410400', 3);
INSERT INTO `sys_region` VALUES ('410425', '郏县', '410400', 3);
INSERT INTO `sys_region` VALUES ('410481', '舞钢市', '410400', 3);
INSERT INTO `sys_region` VALUES ('410482', '汝州市', '410400', 3);
INSERT INTO `sys_region` VALUES ('410500', '安阳市', '410000', 2);
INSERT INTO `sys_region` VALUES ('410502', '文峰区', '410500', 3);
INSERT INTO `sys_region` VALUES ('410503', '北关区', '410500', 3);
INSERT INTO `sys_region` VALUES ('410505', '殷都区', '410500', 3);
INSERT INTO `sys_region` VALUES ('410506', '龙安区', '410500', 3);
INSERT INTO `sys_region` VALUES ('410522', '安阳县', '410500', 3);
INSERT INTO `sys_region` VALUES ('410523', '汤阴县', '410500', 3);
INSERT INTO `sys_region` VALUES ('410526', '滑县', '410500', 3);
INSERT INTO `sys_region` VALUES ('410527', '内黄县', '410500', 3);
INSERT INTO `sys_region` VALUES ('410581', '林州市', '410500', 3);
INSERT INTO `sys_region` VALUES ('410600', '鹤壁市', '410000', 2);
INSERT INTO `sys_region` VALUES ('410602', '鹤山区', '410600', 3);
INSERT INTO `sys_region` VALUES ('410603', '山城区', '410600', 3);
INSERT INTO `sys_region` VALUES ('410611', '淇滨区', '410600', 3);
INSERT INTO `sys_region` VALUES ('410621', '浚县', '410600', 3);
INSERT INTO `sys_region` VALUES ('410622', '淇县', '410600', 3);
INSERT INTO `sys_region` VALUES ('410700', '新乡市', '410000', 2);
INSERT INTO `sys_region` VALUES ('410702', '红旗区', '410700', 3);
INSERT INTO `sys_region` VALUES ('410703', '卫滨区', '410700', 3);
INSERT INTO `sys_region` VALUES ('410704', '凤泉区', '410700', 3);
INSERT INTO `sys_region` VALUES ('410711', '牧野区', '410700', 3);
INSERT INTO `sys_region` VALUES ('410721', '新乡县', '410700', 3);
INSERT INTO `sys_region` VALUES ('410724', '获嘉县', '410700', 3);
INSERT INTO `sys_region` VALUES ('410725', '原阳县', '410700', 3);
INSERT INTO `sys_region` VALUES ('410726', '延津县', '410700', 3);
INSERT INTO `sys_region` VALUES ('410727', '封丘县', '410700', 3);
INSERT INTO `sys_region` VALUES ('410781', '卫辉市', '410700', 3);
INSERT INTO `sys_region` VALUES ('410782', '辉县市', '410700', 3);
INSERT INTO `sys_region` VALUES ('410783', '长垣市', '410700', 3);
INSERT INTO `sys_region` VALUES ('410800', '焦作市', '410000', 2);
INSERT INTO `sys_region` VALUES ('410802', '解放区', '410800', 3);
INSERT INTO `sys_region` VALUES ('410803', '中站区', '410800', 3);
INSERT INTO `sys_region` VALUES ('410804', '马村区', '410800', 3);
INSERT INTO `sys_region` VALUES ('410811', '山阳区', '410800', 3);
INSERT INTO `sys_region` VALUES ('410821', '修武县', '410800', 3);
INSERT INTO `sys_region` VALUES ('410822', '博爱县', '410800', 3);
INSERT INTO `sys_region` VALUES ('410823', '武陟县', '410800', 3);
INSERT INTO `sys_region` VALUES ('410825', '温县', '410800', 3);
INSERT INTO `sys_region` VALUES ('410882', '沁阳市', '410800', 3);
INSERT INTO `sys_region` VALUES ('410883', '孟州市', '410800', 3);
INSERT INTO `sys_region` VALUES ('410900', '濮阳市', '410000', 2);
INSERT INTO `sys_region` VALUES ('410902', '华龙区', '410900', 3);
INSERT INTO `sys_region` VALUES ('410922', '清丰县', '410900', 3);
INSERT INTO `sys_region` VALUES ('410923', '南乐县', '410900', 3);
INSERT INTO `sys_region` VALUES ('410926', '范县', '410900', 3);
INSERT INTO `sys_region` VALUES ('410927', '台前县', '410900', 3);
INSERT INTO `sys_region` VALUES ('410928', '濮阳县', '410900', 3);
INSERT INTO `sys_region` VALUES ('411000', '许昌市', '410000', 2);
INSERT INTO `sys_region` VALUES ('411002', '魏都区', '411000', 3);
INSERT INTO `sys_region` VALUES ('411003', '建安区', '411000', 3);
INSERT INTO `sys_region` VALUES ('411024', '鄢陵县', '411000', 3);
INSERT INTO `sys_region` VALUES ('411025', '襄城县', '411000', 3);
INSERT INTO `sys_region` VALUES ('411081', '禹州市', '411000', 3);
INSERT INTO `sys_region` VALUES ('411082', '长葛市', '411000', 3);
INSERT INTO `sys_region` VALUES ('411100', '漯河市', '410000', 2);
INSERT INTO `sys_region` VALUES ('411102', '源汇区', '411100', 3);
INSERT INTO `sys_region` VALUES ('411103', '郾城区', '411100', 3);
INSERT INTO `sys_region` VALUES ('411104', '召陵区', '411100', 3);
INSERT INTO `sys_region` VALUES ('411121', '舞阳县', '411100', 3);
INSERT INTO `sys_region` VALUES ('411122', '临颍县', '411100', 3);
INSERT INTO `sys_region` VALUES ('411200', '三门峡市', '410000', 2);
INSERT INTO `sys_region` VALUES ('411202', '湖滨区', '411200', 3);
INSERT INTO `sys_region` VALUES ('411203', '陕州区', '411200', 3);
INSERT INTO `sys_region` VALUES ('411221', '渑池县', '411200', 3);
INSERT INTO `sys_region` VALUES ('411224', '卢氏县', '411200', 3);
INSERT INTO `sys_region` VALUES ('411281', '义马市', '411200', 3);
INSERT INTO `sys_region` VALUES ('411282', '灵宝市', '411200', 3);
INSERT INTO `sys_region` VALUES ('411300', '南阳市', '410000', 2);
INSERT INTO `sys_region` VALUES ('411302', '宛城区', '411300', 3);
INSERT INTO `sys_region` VALUES ('411303', '卧龙区', '411300', 3);
INSERT INTO `sys_region` VALUES ('411321', '南召县', '411300', 3);
INSERT INTO `sys_region` VALUES ('411322', '方城县', '411300', 3);
INSERT INTO `sys_region` VALUES ('411323', '西峡县', '411300', 3);
INSERT INTO `sys_region` VALUES ('411324', '镇平县', '411300', 3);
INSERT INTO `sys_region` VALUES ('411325', '内乡县', '411300', 3);
INSERT INTO `sys_region` VALUES ('411326', '淅川县', '411300', 3);
INSERT INTO `sys_region` VALUES ('411327', '社旗县', '411300', 3);
INSERT INTO `sys_region` VALUES ('411328', '唐河县', '411300', 3);
INSERT INTO `sys_region` VALUES ('411329', '新野县', '411300', 3);
INSERT INTO `sys_region` VALUES ('411330', '桐柏县', '411300', 3);
INSERT INTO `sys_region` VALUES ('411381', '邓州市', '411300', 3);
INSERT INTO `sys_region` VALUES ('411400', '商丘市', '410000', 2);
INSERT INTO `sys_region` VALUES ('411402', '梁园区', '411400', 3);
INSERT INTO `sys_region` VALUES ('411403', '睢阳区', '411400', 3);
INSERT INTO `sys_region` VALUES ('411421', '民权县', '411400', 3);
INSERT INTO `sys_region` VALUES ('411422', '睢县', '411400', 3);
INSERT INTO `sys_region` VALUES ('411423', '宁陵县', '411400', 3);
INSERT INTO `sys_region` VALUES ('411424', '柘城县', '411400', 3);
INSERT INTO `sys_region` VALUES ('411425', '虞城县', '411400', 3);
INSERT INTO `sys_region` VALUES ('411426', '夏邑县', '411400', 3);
INSERT INTO `sys_region` VALUES ('411481', '永城市', '411400', 3);
INSERT INTO `sys_region` VALUES ('411500', '信阳市', '410000', 2);
INSERT INTO `sys_region` VALUES ('411502', '浉河区', '411500', 3);
INSERT INTO `sys_region` VALUES ('411503', '平桥区', '411500', 3);
INSERT INTO `sys_region` VALUES ('411521', '罗山县', '411500', 3);
INSERT INTO `sys_region` VALUES ('411522', '光山县', '411500', 3);
INSERT INTO `sys_region` VALUES ('411523', '新县', '411500', 3);
INSERT INTO `sys_region` VALUES ('411524', '商城县', '411500', 3);
INSERT INTO `sys_region` VALUES ('411525', '固始县', '411500', 3);
INSERT INTO `sys_region` VALUES ('411526', '潢川县', '411500', 3);
INSERT INTO `sys_region` VALUES ('411527', '淮滨县', '411500', 3);
INSERT INTO `sys_region` VALUES ('411528', '息县', '411500', 3);
INSERT INTO `sys_region` VALUES ('411600', '周口市', '410000', 2);
INSERT INTO `sys_region` VALUES ('411602', '川汇区', '411600', 3);
INSERT INTO `sys_region` VALUES ('411603', '淮阳区', '411600', 3);
INSERT INTO `sys_region` VALUES ('411621', '扶沟县', '411600', 3);
INSERT INTO `sys_region` VALUES ('411622', '西华县', '411600', 3);
INSERT INTO `sys_region` VALUES ('411623', '商水县', '411600', 3);
INSERT INTO `sys_region` VALUES ('411624', '沈丘县', '411600', 3);
INSERT INTO `sys_region` VALUES ('411625', '郸城县', '411600', 3);
INSERT INTO `sys_region` VALUES ('411627', '太康县', '411600', 3);
INSERT INTO `sys_region` VALUES ('411628', '鹿邑县', '411600', 3);
INSERT INTO `sys_region` VALUES ('411681', '项城市', '411600', 3);
INSERT INTO `sys_region` VALUES ('411700', '驻马店市', '410000', 2);
INSERT INTO `sys_region` VALUES ('411702', '驿城区', '411700', 3);
INSERT INTO `sys_region` VALUES ('411721', '西平县', '411700', 3);
INSERT INTO `sys_region` VALUES ('411722', '上蔡县', '411700', 3);
INSERT INTO `sys_region` VALUES ('411723', '平舆县', '411700', 3);
INSERT INTO `sys_region` VALUES ('411724', '正阳县', '411700', 3);
INSERT INTO `sys_region` VALUES ('411725', '确山县', '411700', 3);
INSERT INTO `sys_region` VALUES ('411726', '泌阳县', '411700', 3);
INSERT INTO `sys_region` VALUES ('411727', '汝南县', '411700', 3);
INSERT INTO `sys_region` VALUES ('411728', '遂平县', '411700', 3);
INSERT INTO `sys_region` VALUES ('411729', '新蔡县', '411700', 3);
INSERT INTO `sys_region` VALUES ('419001', '济源市', '410000', 2);
INSERT INTO `sys_region` VALUES ('420000', '湖北省', '0', 1);
INSERT INTO `sys_region` VALUES ('420100', '武汉市', '420000', 2);
INSERT INTO `sys_region` VALUES ('420102', '江岸区', '420100', 3);
INSERT INTO `sys_region` VALUES ('420103', '江汉区', '420100', 3);
INSERT INTO `sys_region` VALUES ('420104', '硚口区', '420100', 3);
INSERT INTO `sys_region` VALUES ('420105', '汉阳区', '420100', 3);
INSERT INTO `sys_region` VALUES ('420106', '武昌区', '420100', 3);
INSERT INTO `sys_region` VALUES ('420107', '青山区', '420100', 3);
INSERT INTO `sys_region` VALUES ('420111', '洪山区', '420100', 3);
INSERT INTO `sys_region` VALUES ('420112', '东西湖区', '420100', 3);
INSERT INTO `sys_region` VALUES ('420113', '汉南区', '420100', 3);
INSERT INTO `sys_region` VALUES ('420114', '蔡甸区', '420100', 3);
INSERT INTO `sys_region` VALUES ('420115', '江夏区', '420100', 3);
INSERT INTO `sys_region` VALUES ('420116', '黄陂区', '420100', 3);
INSERT INTO `sys_region` VALUES ('420117', '新洲区', '420100', 3);
INSERT INTO `sys_region` VALUES ('420200', '黄石市', '420000', 2);
INSERT INTO `sys_region` VALUES ('420202', '黄石港区', '420200', 3);
INSERT INTO `sys_region` VALUES ('420203', '西塞山区', '420200', 3);
INSERT INTO `sys_region` VALUES ('420204', '下陆区', '420200', 3);
INSERT INTO `sys_region` VALUES ('420205', '铁山区', '420200', 3);
INSERT INTO `sys_region` VALUES ('420222', '阳新县', '420200', 3);
INSERT INTO `sys_region` VALUES ('420281', '大冶市', '420200', 3);
INSERT INTO `sys_region` VALUES ('420300', '十堰市', '420000', 2);
INSERT INTO `sys_region` VALUES ('420302', '茅箭区', '420300', 3);
INSERT INTO `sys_region` VALUES ('420303', '张湾区', '420300', 3);
INSERT INTO `sys_region` VALUES ('420304', '郧阳区', '420300', 3);
INSERT INTO `sys_region` VALUES ('420322', '郧西县', '420300', 3);
INSERT INTO `sys_region` VALUES ('420323', '竹山县', '420300', 3);
INSERT INTO `sys_region` VALUES ('420324', '竹溪县', '420300', 3);
INSERT INTO `sys_region` VALUES ('420325', '房县', '420300', 3);
INSERT INTO `sys_region` VALUES ('420381', '丹江口市', '420300', 3);
INSERT INTO `sys_region` VALUES ('420500', '宜昌市', '420000', 2);
INSERT INTO `sys_region` VALUES ('420502', '西陵区', '420500', 3);
INSERT INTO `sys_region` VALUES ('420503', '伍家岗区', '420500', 3);
INSERT INTO `sys_region` VALUES ('420504', '点军区', '420500', 3);
INSERT INTO `sys_region` VALUES ('420505', '猇亭区', '420500', 3);
INSERT INTO `sys_region` VALUES ('420506', '夷陵区', '420500', 3);
INSERT INTO `sys_region` VALUES ('420525', '远安县', '420500', 3);
INSERT INTO `sys_region` VALUES ('420526', '兴山县', '420500', 3);
INSERT INTO `sys_region` VALUES ('420527', '秭归县', '420500', 3);
INSERT INTO `sys_region` VALUES ('420528', '长阳土家族自治县', '420500', 3);
INSERT INTO `sys_region` VALUES ('420529', '五峰土家族自治县', '420500', 3);
INSERT INTO `sys_region` VALUES ('420581', '宜都市', '420500', 3);
INSERT INTO `sys_region` VALUES ('420582', '当阳市', '420500', 3);
INSERT INTO `sys_region` VALUES ('420583', '枝江市', '420500', 3);
INSERT INTO `sys_region` VALUES ('420600', '襄阳市', '420000', 2);
INSERT INTO `sys_region` VALUES ('420602', '襄城区', '420600', 3);
INSERT INTO `sys_region` VALUES ('420606', '樊城区', '420600', 3);
INSERT INTO `sys_region` VALUES ('420607', '襄州区', '420600', 3);
INSERT INTO `sys_region` VALUES ('420624', '南漳县', '420600', 3);
INSERT INTO `sys_region` VALUES ('420625', '谷城县', '420600', 3);
INSERT INTO `sys_region` VALUES ('420626', '保康县', '420600', 3);
INSERT INTO `sys_region` VALUES ('420682', '老河口市', '420600', 3);
INSERT INTO `sys_region` VALUES ('420683', '枣阳市', '420600', 3);
INSERT INTO `sys_region` VALUES ('420684', '宜城市', '420600', 3);
INSERT INTO `sys_region` VALUES ('420700', '鄂州市', '420000', 2);
INSERT INTO `sys_region` VALUES ('420702', '梁子湖区', '420700', 3);
INSERT INTO `sys_region` VALUES ('420703', '华容区', '420700', 3);
INSERT INTO `sys_region` VALUES ('420704', '鄂城区', '420700', 3);
INSERT INTO `sys_region` VALUES ('420800', '荆门市', '420000', 2);
INSERT INTO `sys_region` VALUES ('420802', '东宝区', '420800', 3);
INSERT INTO `sys_region` VALUES ('420804', '掇刀区', '420800', 3);
INSERT INTO `sys_region` VALUES ('420822', '沙洋县', '420800', 3);
INSERT INTO `sys_region` VALUES ('420881', '钟祥市', '420800', 3);
INSERT INTO `sys_region` VALUES ('420882', '京山市', '420800', 3);
INSERT INTO `sys_region` VALUES ('420900', '孝感市', '420000', 2);
INSERT INTO `sys_region` VALUES ('420902', '孝南区', '420900', 3);
INSERT INTO `sys_region` VALUES ('420921', '孝昌县', '420900', 3);
INSERT INTO `sys_region` VALUES ('420922', '大悟县', '420900', 3);
INSERT INTO `sys_region` VALUES ('420923', '云梦县', '420900', 3);
INSERT INTO `sys_region` VALUES ('420981', '应城市', '420900', 3);
INSERT INTO `sys_region` VALUES ('420982', '安陆市', '420900', 3);
INSERT INTO `sys_region` VALUES ('420984', '汉川市', '420900', 3);
INSERT INTO `sys_region` VALUES ('421000', '荆州市', '420000', 2);
INSERT INTO `sys_region` VALUES ('421002', '沙市区', '421000', 3);
INSERT INTO `sys_region` VALUES ('421003', '荆州区', '421000', 3);
INSERT INTO `sys_region` VALUES ('421022', '公安县', '421000', 3);
INSERT INTO `sys_region` VALUES ('421023', '监利县', '421000', 3);
INSERT INTO `sys_region` VALUES ('421024', '江陵县', '421000', 3);
INSERT INTO `sys_region` VALUES ('421081', '石首市', '421000', 3);
INSERT INTO `sys_region` VALUES ('421083', '洪湖市', '421000', 3);
INSERT INTO `sys_region` VALUES ('421087', '松滋市', '421000', 3);
INSERT INTO `sys_region` VALUES ('421100', '黄冈市', '420000', 2);
INSERT INTO `sys_region` VALUES ('421102', '黄州区', '421100', 3);
INSERT INTO `sys_region` VALUES ('421121', '团风县', '421100', 3);
INSERT INTO `sys_region` VALUES ('421122', '红安县', '421100', 3);
INSERT INTO `sys_region` VALUES ('421123', '罗田县', '421100', 3);
INSERT INTO `sys_region` VALUES ('421124', '英山县', '421100', 3);
INSERT INTO `sys_region` VALUES ('421125', '浠水县', '421100', 3);
INSERT INTO `sys_region` VALUES ('421126', '蕲春县', '421100', 3);
INSERT INTO `sys_region` VALUES ('421127', '黄梅县', '421100', 3);
INSERT INTO `sys_region` VALUES ('421181', '麻城市', '421100', 3);
INSERT INTO `sys_region` VALUES ('421182', '武穴市', '421100', 3);
INSERT INTO `sys_region` VALUES ('421200', '咸宁市', '420000', 2);
INSERT INTO `sys_region` VALUES ('421202', '咸安区', '421200', 3);
INSERT INTO `sys_region` VALUES ('421221', '嘉鱼县', '421200', 3);
INSERT INTO `sys_region` VALUES ('421222', '通城县', '421200', 3);
INSERT INTO `sys_region` VALUES ('421223', '崇阳县', '421200', 3);
INSERT INTO `sys_region` VALUES ('421224', '通山县', '421200', 3);
INSERT INTO `sys_region` VALUES ('421281', '赤壁市', '421200', 3);
INSERT INTO `sys_region` VALUES ('421300', '随州市', '420000', 2);
INSERT INTO `sys_region` VALUES ('421303', '曾都区', '421300', 3);
INSERT INTO `sys_region` VALUES ('421321', '随县', '421300', 3);
INSERT INTO `sys_region` VALUES ('421381', '广水市', '421300', 3);
INSERT INTO `sys_region` VALUES ('422800', '恩施土家族苗族自治州', '420000', 2);
INSERT INTO `sys_region` VALUES ('422801', '恩施市', '422800', 3);
INSERT INTO `sys_region` VALUES ('422802', '利川市', '422800', 3);
INSERT INTO `sys_region` VALUES ('422822', '建始县', '422800', 3);
INSERT INTO `sys_region` VALUES ('422823', '巴东县', '422800', 3);
INSERT INTO `sys_region` VALUES ('422825', '宣恩县', '422800', 3);
INSERT INTO `sys_region` VALUES ('422826', '咸丰县', '422800', 3);
INSERT INTO `sys_region` VALUES ('422827', '来凤县', '422800', 3);
INSERT INTO `sys_region` VALUES ('422828', '鹤峰县', '422800', 3);
INSERT INTO `sys_region` VALUES ('429004', '仙桃市', '420000', 2);
INSERT INTO `sys_region` VALUES ('429005', '潜江市', '420000', 2);
INSERT INTO `sys_region` VALUES ('429006', '天门市', '420000', 2);
INSERT INTO `sys_region` VALUES ('429021', '神农架林区', '420000', 2);
INSERT INTO `sys_region` VALUES ('430000', '湖南省', '0', 1);
INSERT INTO `sys_region` VALUES ('430100', '长沙市', '430000', 2);
INSERT INTO `sys_region` VALUES ('430102', '芙蓉区', '430100', 3);
INSERT INTO `sys_region` VALUES ('430103', '天心区', '430100', 3);
INSERT INTO `sys_region` VALUES ('430104', '岳麓区', '430100', 3);
INSERT INTO `sys_region` VALUES ('430105', '开福区', '430100', 3);
INSERT INTO `sys_region` VALUES ('430111', '雨花区', '430100', 3);
INSERT INTO `sys_region` VALUES ('430112', '望城区', '430100', 3);
INSERT INTO `sys_region` VALUES ('430121', '长沙县', '430100', 3);
INSERT INTO `sys_region` VALUES ('430181', '浏阳市', '430100', 3);
INSERT INTO `sys_region` VALUES ('430182', '宁乡市', '430100', 3);
INSERT INTO `sys_region` VALUES ('430200', '株洲市', '430000', 2);
INSERT INTO `sys_region` VALUES ('430202', '荷塘区', '430200', 3);
INSERT INTO `sys_region` VALUES ('430203', '芦淞区', '430200', 3);
INSERT INTO `sys_region` VALUES ('430204', '石峰区', '430200', 3);
INSERT INTO `sys_region` VALUES ('430211', '天元区', '430200', 3);
INSERT INTO `sys_region` VALUES ('430212', '渌口区', '430200', 3);
INSERT INTO `sys_region` VALUES ('430223', '攸县', '430200', 3);
INSERT INTO `sys_region` VALUES ('430224', '茶陵县', '430200', 3);
INSERT INTO `sys_region` VALUES ('430225', '炎陵县', '430200', 3);
INSERT INTO `sys_region` VALUES ('430281', '醴陵市', '430200', 3);
INSERT INTO `sys_region` VALUES ('430300', '湘潭市', '430000', 2);
INSERT INTO `sys_region` VALUES ('430302', '雨湖区', '430300', 3);
INSERT INTO `sys_region` VALUES ('430304', '岳塘区', '430300', 3);
INSERT INTO `sys_region` VALUES ('430321', '湘潭县', '430300', 3);
INSERT INTO `sys_region` VALUES ('430381', '湘乡市', '430300', 3);
INSERT INTO `sys_region` VALUES ('430382', '韶山市', '430300', 3);
INSERT INTO `sys_region` VALUES ('430400', '衡阳市', '430000', 2);
INSERT INTO `sys_region` VALUES ('430405', '珠晖区', '430400', 3);
INSERT INTO `sys_region` VALUES ('430406', '雁峰区', '430400', 3);
INSERT INTO `sys_region` VALUES ('430407', '石鼓区', '430400', 3);
INSERT INTO `sys_region` VALUES ('430408', '蒸湘区', '430400', 3);
INSERT INTO `sys_region` VALUES ('430412', '南岳区', '430400', 3);
INSERT INTO `sys_region` VALUES ('430421', '衡阳县', '430400', 3);
INSERT INTO `sys_region` VALUES ('430422', '衡南县', '430400', 3);
INSERT INTO `sys_region` VALUES ('430423', '衡山县', '430400', 3);
INSERT INTO `sys_region` VALUES ('430424', '衡东县', '430400', 3);
INSERT INTO `sys_region` VALUES ('430426', '祁东县', '430400', 3);
INSERT INTO `sys_region` VALUES ('430481', '耒阳市', '430400', 3);
INSERT INTO `sys_region` VALUES ('430482', '常宁市', '430400', 3);
INSERT INTO `sys_region` VALUES ('430500', '邵阳市', '430000', 2);
INSERT INTO `sys_region` VALUES ('430502', '双清区', '430500', 3);
INSERT INTO `sys_region` VALUES ('430503', '大祥区', '430500', 3);
INSERT INTO `sys_region` VALUES ('430511', '北塔区', '430500', 3);
INSERT INTO `sys_region` VALUES ('430522', '新邵县', '430500', 3);
INSERT INTO `sys_region` VALUES ('430523', '邵阳县', '430500', 3);
INSERT INTO `sys_region` VALUES ('430524', '隆回县', '430500', 3);
INSERT INTO `sys_region` VALUES ('430525', '洞口县', '430500', 3);
INSERT INTO `sys_region` VALUES ('430527', '绥宁县', '430500', 3);
INSERT INTO `sys_region` VALUES ('430528', '新宁县', '430500', 3);
INSERT INTO `sys_region` VALUES ('430529', '城步苗族自治县', '430500', 3);
INSERT INTO `sys_region` VALUES ('430581', '武冈市', '430500', 3);
INSERT INTO `sys_region` VALUES ('430582', '邵东市', '430500', 3);
INSERT INTO `sys_region` VALUES ('430600', '岳阳市', '430000', 2);
INSERT INTO `sys_region` VALUES ('430602', '岳阳楼区', '430600', 3);
INSERT INTO `sys_region` VALUES ('430603', '云溪区', '430600', 3);
INSERT INTO `sys_region` VALUES ('430611', '君山区', '430600', 3);
INSERT INTO `sys_region` VALUES ('430621', '岳阳县', '430600', 3);
INSERT INTO `sys_region` VALUES ('430623', '华容县', '430600', 3);
INSERT INTO `sys_region` VALUES ('430624', '湘阴县', '430600', 3);
INSERT INTO `sys_region` VALUES ('430626', '平江县', '430600', 3);
INSERT INTO `sys_region` VALUES ('430681', '汨罗市', '430600', 3);
INSERT INTO `sys_region` VALUES ('430682', '临湘市', '430600', 3);
INSERT INTO `sys_region` VALUES ('430700', '常德市', '430000', 2);
INSERT INTO `sys_region` VALUES ('430702', '武陵区', '430700', 3);
INSERT INTO `sys_region` VALUES ('430703', '鼎城区', '430700', 3);
INSERT INTO `sys_region` VALUES ('430721', '安乡县', '430700', 3);
INSERT INTO `sys_region` VALUES ('430722', '汉寿县', '430700', 3);
INSERT INTO `sys_region` VALUES ('430723', '澧县', '430700', 3);
INSERT INTO `sys_region` VALUES ('430724', '临澧县', '430700', 3);
INSERT INTO `sys_region` VALUES ('430725', '桃源县', '430700', 3);
INSERT INTO `sys_region` VALUES ('430726', '石门县', '430700', 3);
INSERT INTO `sys_region` VALUES ('430781', '津市市', '430700', 3);
INSERT INTO `sys_region` VALUES ('430800', '张家界市', '430000', 2);
INSERT INTO `sys_region` VALUES ('430802', '永定区', '430800', 3);
INSERT INTO `sys_region` VALUES ('430811', '武陵源区', '430800', 3);
INSERT INTO `sys_region` VALUES ('430821', '慈利县', '430800', 3);
INSERT INTO `sys_region` VALUES ('430822', '桑植县', '430800', 3);
INSERT INTO `sys_region` VALUES ('430900', '益阳市', '430000', 2);
INSERT INTO `sys_region` VALUES ('430902', '资阳区', '430900', 3);
INSERT INTO `sys_region` VALUES ('430903', '赫山区', '430900', 3);
INSERT INTO `sys_region` VALUES ('430921', '南县', '430900', 3);
INSERT INTO `sys_region` VALUES ('430922', '桃江县', '430900', 3);
INSERT INTO `sys_region` VALUES ('430923', '安化县', '430900', 3);
INSERT INTO `sys_region` VALUES ('430981', '沅江市', '430900', 3);
INSERT INTO `sys_region` VALUES ('431000', '郴州市', '430000', 2);
INSERT INTO `sys_region` VALUES ('431002', '北湖区', '431000', 3);
INSERT INTO `sys_region` VALUES ('431003', '苏仙区', '431000', 3);
INSERT INTO `sys_region` VALUES ('431021', '桂阳县', '431000', 3);
INSERT INTO `sys_region` VALUES ('431022', '宜章县', '431000', 3);
INSERT INTO `sys_region` VALUES ('431023', '永兴县', '431000', 3);
INSERT INTO `sys_region` VALUES ('431024', '嘉禾县', '431000', 3);
INSERT INTO `sys_region` VALUES ('431025', '临武县', '431000', 3);
INSERT INTO `sys_region` VALUES ('431026', '汝城县', '431000', 3);
INSERT INTO `sys_region` VALUES ('431027', '桂东县', '431000', 3);
INSERT INTO `sys_region` VALUES ('431028', '安仁县', '431000', 3);
INSERT INTO `sys_region` VALUES ('431081', '资兴市', '431000', 3);
INSERT INTO `sys_region` VALUES ('431100', '永州市', '430000', 2);
INSERT INTO `sys_region` VALUES ('431102', '零陵区', '431100', 3);
INSERT INTO `sys_region` VALUES ('431103', '冷水滩区', '431100', 3);
INSERT INTO `sys_region` VALUES ('431121', '祁阳县', '431100', 3);
INSERT INTO `sys_region` VALUES ('431122', '东安县', '431100', 3);
INSERT INTO `sys_region` VALUES ('431123', '双牌县', '431100', 3);
INSERT INTO `sys_region` VALUES ('431124', '道县', '431100', 3);
INSERT INTO `sys_region` VALUES ('431125', '江永县', '431100', 3);
INSERT INTO `sys_region` VALUES ('431126', '宁远县', '431100', 3);
INSERT INTO `sys_region` VALUES ('431127', '蓝山县', '431100', 3);
INSERT INTO `sys_region` VALUES ('431128', '新田县', '431100', 3);
INSERT INTO `sys_region` VALUES ('431129', '江华瑶族自治县', '431100', 3);
INSERT INTO `sys_region` VALUES ('431200', '怀化市', '430000', 2);
INSERT INTO `sys_region` VALUES ('431202', '鹤城区', '431200', 3);
INSERT INTO `sys_region` VALUES ('431221', '中方县', '431200', 3);
INSERT INTO `sys_region` VALUES ('431222', '沅陵县', '431200', 3);
INSERT INTO `sys_region` VALUES ('431223', '辰溪县', '431200', 3);
INSERT INTO `sys_region` VALUES ('431224', '溆浦县', '431200', 3);
INSERT INTO `sys_region` VALUES ('431225', '会同县', '431200', 3);
INSERT INTO `sys_region` VALUES ('431226', '麻阳苗族自治县', '431200', 3);
INSERT INTO `sys_region` VALUES ('431227', '新晃侗族自治县', '431200', 3);
INSERT INTO `sys_region` VALUES ('431228', '芷江侗族自治县', '431200', 3);
INSERT INTO `sys_region` VALUES ('431229', '靖州苗族侗族自治县', '431200', 3);
INSERT INTO `sys_region` VALUES ('431230', '通道侗族自治县', '431200', 3);
INSERT INTO `sys_region` VALUES ('431281', '洪江市', '431200', 3);
INSERT INTO `sys_region` VALUES ('431300', '娄底市', '430000', 2);
INSERT INTO `sys_region` VALUES ('431302', '娄星区', '431300', 3);
INSERT INTO `sys_region` VALUES ('431321', '双峰县', '431300', 3);
INSERT INTO `sys_region` VALUES ('431322', '新化县', '431300', 3);
INSERT INTO `sys_region` VALUES ('431381', '冷水江市', '431300', 3);
INSERT INTO `sys_region` VALUES ('431382', '涟源市', '431300', 3);
INSERT INTO `sys_region` VALUES ('433100', '湘西土家族苗族自治州', '430000', 2);
INSERT INTO `sys_region` VALUES ('433101', '吉首市', '433100', 3);
INSERT INTO `sys_region` VALUES ('433122', '泸溪县', '433100', 3);
INSERT INTO `sys_region` VALUES ('433123', '凤凰县', '433100', 3);
INSERT INTO `sys_region` VALUES ('433124', '花垣县', '433100', 3);
INSERT INTO `sys_region` VALUES ('433125', '保靖县', '433100', 3);
INSERT INTO `sys_region` VALUES ('433126', '古丈县', '433100', 3);
INSERT INTO `sys_region` VALUES ('433127', '永顺县', '433100', 3);
INSERT INTO `sys_region` VALUES ('433130', '龙山县', '433100', 3);
INSERT INTO `sys_region` VALUES ('440000', '广东省', '0', 1);
INSERT INTO `sys_region` VALUES ('440100', '广州市', '440000', 2);
INSERT INTO `sys_region` VALUES ('440103', '荔湾区', '440100', 3);
INSERT INTO `sys_region` VALUES ('440104', '越秀区', '440100', 3);
INSERT INTO `sys_region` VALUES ('440105', '海珠区', '440100', 3);
INSERT INTO `sys_region` VALUES ('440106', '天河区', '440100', 3);
INSERT INTO `sys_region` VALUES ('440111', '白云区', '440100', 3);
INSERT INTO `sys_region` VALUES ('440112', '黄埔区', '440100', 3);
INSERT INTO `sys_region` VALUES ('440113', '番禺区', '440100', 3);
INSERT INTO `sys_region` VALUES ('440114', '花都区', '440100', 3);
INSERT INTO `sys_region` VALUES ('440115', '南沙区', '440100', 3);
INSERT INTO `sys_region` VALUES ('440117', '从化区', '440100', 3);
INSERT INTO `sys_region` VALUES ('440118', '增城区', '440100', 3);
INSERT INTO `sys_region` VALUES ('440200', '韶关市', '440000', 2);
INSERT INTO `sys_region` VALUES ('440203', '武江区', '440200', 3);
INSERT INTO `sys_region` VALUES ('440204', '浈江区', '440200', 3);
INSERT INTO `sys_region` VALUES ('440205', '曲江区', '440200', 3);
INSERT INTO `sys_region` VALUES ('440222', '始兴县', '440200', 3);
INSERT INTO `sys_region` VALUES ('440224', '仁化县', '440200', 3);
INSERT INTO `sys_region` VALUES ('440229', '翁源县', '440200', 3);
INSERT INTO `sys_region` VALUES ('440232', '乳源瑶族自治县', '440200', 3);
INSERT INTO `sys_region` VALUES ('440233', '新丰县', '440200', 3);
INSERT INTO `sys_region` VALUES ('440281', '乐昌市', '440200', 3);
INSERT INTO `sys_region` VALUES ('440282', '南雄市', '440200', 3);
INSERT INTO `sys_region` VALUES ('440300', '深圳市', '440000', 2);
INSERT INTO `sys_region` VALUES ('440303', '罗湖区', '440300', 3);
INSERT INTO `sys_region` VALUES ('440304', '福田区', '440300', 3);
INSERT INTO `sys_region` VALUES ('440305', '南山区', '440300', 3);
INSERT INTO `sys_region` VALUES ('440306', '宝安区', '440300', 3);
INSERT INTO `sys_region` VALUES ('440307', '龙岗区', '440300', 3);
INSERT INTO `sys_region` VALUES ('440308', '盐田区', '440300', 3);
INSERT INTO `sys_region` VALUES ('440309', '龙华区', '440300', 3);
INSERT INTO `sys_region` VALUES ('440310', '坪山区', '440300', 3);
INSERT INTO `sys_region` VALUES ('440311', '光明区', '440300', 3);
INSERT INTO `sys_region` VALUES ('440400', '珠海市', '440000', 2);
INSERT INTO `sys_region` VALUES ('440402', '香洲区', '440400', 3);
INSERT INTO `sys_region` VALUES ('440403', '斗门区', '440400', 3);
INSERT INTO `sys_region` VALUES ('440404', '金湾区', '440400', 3);
INSERT INTO `sys_region` VALUES ('440500', '汕头市', '440000', 2);
INSERT INTO `sys_region` VALUES ('440507', '龙湖区', '440500', 3);
INSERT INTO `sys_region` VALUES ('440511', '金平区', '440500', 3);
INSERT INTO `sys_region` VALUES ('440512', '濠江区', '440500', 3);
INSERT INTO `sys_region` VALUES ('440513', '潮阳区', '440500', 3);
INSERT INTO `sys_region` VALUES ('440514', '潮南区', '440500', 3);
INSERT INTO `sys_region` VALUES ('440515', '澄海区', '440500', 3);
INSERT INTO `sys_region` VALUES ('440523', '南澳县', '440500', 3);
INSERT INTO `sys_region` VALUES ('440600', '佛山市', '440000', 2);
INSERT INTO `sys_region` VALUES ('440604', '禅城区', '440600', 3);
INSERT INTO `sys_region` VALUES ('440605', '南海区', '440600', 3);
INSERT INTO `sys_region` VALUES ('440606', '顺德区', '440600', 3);
INSERT INTO `sys_region` VALUES ('440607', '三水区', '440600', 3);
INSERT INTO `sys_region` VALUES ('440608', '高明区', '440600', 3);
INSERT INTO `sys_region` VALUES ('440700', '江门市', '440000', 2);
INSERT INTO `sys_region` VALUES ('440703', '蓬江区', '440700', 3);
INSERT INTO `sys_region` VALUES ('440704', '江海区', '440700', 3);
INSERT INTO `sys_region` VALUES ('440705', '新会区', '440700', 3);
INSERT INTO `sys_region` VALUES ('440781', '台山市', '440700', 3);
INSERT INTO `sys_region` VALUES ('440783', '开平市', '440700', 3);
INSERT INTO `sys_region` VALUES ('440784', '鹤山市', '440700', 3);
INSERT INTO `sys_region` VALUES ('440785', '恩平市', '440700', 3);
INSERT INTO `sys_region` VALUES ('440800', '湛江市', '440000', 2);
INSERT INTO `sys_region` VALUES ('440802', '赤坎区', '440800', 3);
INSERT INTO `sys_region` VALUES ('440803', '霞山区', '440800', 3);
INSERT INTO `sys_region` VALUES ('440804', '坡头区', '440800', 3);
INSERT INTO `sys_region` VALUES ('440811', '麻章区', '440800', 3);
INSERT INTO `sys_region` VALUES ('440823', '遂溪县', '440800', 3);
INSERT INTO `sys_region` VALUES ('440825', '徐闻县', '440800', 3);
INSERT INTO `sys_region` VALUES ('440881', '廉江市', '440800', 3);
INSERT INTO `sys_region` VALUES ('440882', '雷州市', '440800', 3);
INSERT INTO `sys_region` VALUES ('440883', '吴川市', '440800', 3);
INSERT INTO `sys_region` VALUES ('440900', '茂名市', '440000', 2);
INSERT INTO `sys_region` VALUES ('440902', '茂南区', '440900', 3);
INSERT INTO `sys_region` VALUES ('440904', '电白区', '440900', 3);
INSERT INTO `sys_region` VALUES ('440981', '高州市', '440900', 3);
INSERT INTO `sys_region` VALUES ('440982', '化州市', '440900', 3);
INSERT INTO `sys_region` VALUES ('440983', '信宜市', '440900', 3);
INSERT INTO `sys_region` VALUES ('441200', '肇庆市', '440000', 2);
INSERT INTO `sys_region` VALUES ('441202', '端州区', '441200', 3);
INSERT INTO `sys_region` VALUES ('441203', '鼎湖区', '441200', 3);
INSERT INTO `sys_region` VALUES ('441204', '高要区', '441200', 3);
INSERT INTO `sys_region` VALUES ('441223', '广宁县', '441200', 3);
INSERT INTO `sys_region` VALUES ('441224', '怀集县', '441200', 3);
INSERT INTO `sys_region` VALUES ('441225', '封开县', '441200', 3);
INSERT INTO `sys_region` VALUES ('441226', '德庆县', '441200', 3);
INSERT INTO `sys_region` VALUES ('441284', '四会市', '441200', 3);
INSERT INTO `sys_region` VALUES ('441300', '惠州市', '440000', 2);
INSERT INTO `sys_region` VALUES ('441302', '惠城区', '441300', 3);
INSERT INTO `sys_region` VALUES ('441303', '惠阳区', '441300', 3);
INSERT INTO `sys_region` VALUES ('441322', '博罗县', '441300', 3);
INSERT INTO `sys_region` VALUES ('441323', '惠东县', '441300', 3);
INSERT INTO `sys_region` VALUES ('441324', '龙门县', '441300', 3);
INSERT INTO `sys_region` VALUES ('441400', '梅州市', '440000', 2);
INSERT INTO `sys_region` VALUES ('441402', '梅江区', '441400', 3);
INSERT INTO `sys_region` VALUES ('441403', '梅县区', '441400', 3);
INSERT INTO `sys_region` VALUES ('441422', '大埔县', '441400', 3);
INSERT INTO `sys_region` VALUES ('441423', '丰顺县', '441400', 3);
INSERT INTO `sys_region` VALUES ('441424', '五华县', '441400', 3);
INSERT INTO `sys_region` VALUES ('441426', '平远县', '441400', 3);
INSERT INTO `sys_region` VALUES ('441427', '蕉岭县', '441400', 3);
INSERT INTO `sys_region` VALUES ('441481', '兴宁市', '441400', 3);
INSERT INTO `sys_region` VALUES ('441500', '汕尾市', '440000', 2);
INSERT INTO `sys_region` VALUES ('441502', '城区', '441500', 3);
INSERT INTO `sys_region` VALUES ('441521', '海丰县', '441500', 3);
INSERT INTO `sys_region` VALUES ('441523', '陆河县', '441500', 3);
INSERT INTO `sys_region` VALUES ('441581', '陆丰市', '441500', 3);
INSERT INTO `sys_region` VALUES ('441600', '河源市', '440000', 2);
INSERT INTO `sys_region` VALUES ('441602', '源城区', '441600', 3);
INSERT INTO `sys_region` VALUES ('441621', '紫金县', '441600', 3);
INSERT INTO `sys_region` VALUES ('441622', '龙川县', '441600', 3);
INSERT INTO `sys_region` VALUES ('441623', '连平县', '441600', 3);
INSERT INTO `sys_region` VALUES ('441624', '和平县', '441600', 3);
INSERT INTO `sys_region` VALUES ('441625', '东源县', '441600', 3);
INSERT INTO `sys_region` VALUES ('441700', '阳江市', '440000', 2);
INSERT INTO `sys_region` VALUES ('441702', '江城区', '441700', 3);
INSERT INTO `sys_region` VALUES ('441704', '阳东区', '441700', 3);
INSERT INTO `sys_region` VALUES ('441721', '阳西县', '441700', 3);
INSERT INTO `sys_region` VALUES ('441781', '阳春市', '441700', 3);
INSERT INTO `sys_region` VALUES ('441800', '清远市', '440000', 2);
INSERT INTO `sys_region` VALUES ('441802', '清城区', '441800', 3);
INSERT INTO `sys_region` VALUES ('441803', '清新区', '441800', 3);
INSERT INTO `sys_region` VALUES ('441821', '佛冈县', '441800', 3);
INSERT INTO `sys_region` VALUES ('441823', '阳山县', '441800', 3);
INSERT INTO `sys_region` VALUES ('441825', '连山壮族瑶族自治县', '441800', 3);
INSERT INTO `sys_region` VALUES ('441826', '连南瑶族自治县', '441800', 3);
INSERT INTO `sys_region` VALUES ('441881', '英德市', '441800', 3);
INSERT INTO `sys_region` VALUES ('441882', '连州市', '441800', 3);
INSERT INTO `sys_region` VALUES ('441900', '东莞市', '440000', 2);
INSERT INTO `sys_region` VALUES ('442000', '中山市', '440000', 2);
INSERT INTO `sys_region` VALUES ('445100', '潮州市', '440000', 2);
INSERT INTO `sys_region` VALUES ('445102', '湘桥区', '445100', 3);
INSERT INTO `sys_region` VALUES ('445103', '潮安区', '445100', 3);
INSERT INTO `sys_region` VALUES ('445122', '饶平县', '445100', 3);
INSERT INTO `sys_region` VALUES ('445200', '揭阳市', '440000', 2);
INSERT INTO `sys_region` VALUES ('445202', '榕城区', '445200', 3);
INSERT INTO `sys_region` VALUES ('445203', '揭东区', '445200', 3);
INSERT INTO `sys_region` VALUES ('445222', '揭西县', '445200', 3);
INSERT INTO `sys_region` VALUES ('445224', '惠来县', '445200', 3);
INSERT INTO `sys_region` VALUES ('445281', '普宁市', '445200', 3);
INSERT INTO `sys_region` VALUES ('445300', '云浮市', '440000', 2);
INSERT INTO `sys_region` VALUES ('445302', '云城区', '445300', 3);
INSERT INTO `sys_region` VALUES ('445303', '云安区', '445300', 3);
INSERT INTO `sys_region` VALUES ('445321', '新兴县', '445300', 3);
INSERT INTO `sys_region` VALUES ('445322', '郁南县', '445300', 3);
INSERT INTO `sys_region` VALUES ('445381', '罗定市', '445300', 3);
INSERT INTO `sys_region` VALUES ('450000', '广西壮族自治区', '0', 1);
INSERT INTO `sys_region` VALUES ('450100', '南宁市', '450000', 2);
INSERT INTO `sys_region` VALUES ('450102', '兴宁区', '450100', 3);
INSERT INTO `sys_region` VALUES ('450103', '青秀区', '450100', 3);
INSERT INTO `sys_region` VALUES ('450105', '江南区', '450100', 3);
INSERT INTO `sys_region` VALUES ('450107', '西乡塘区', '450100', 3);
INSERT INTO `sys_region` VALUES ('450108', '良庆区', '450100', 3);
INSERT INTO `sys_region` VALUES ('450109', '邕宁区', '450100', 3);
INSERT INTO `sys_region` VALUES ('450110', '武鸣区', '450100', 3);
INSERT INTO `sys_region` VALUES ('450123', '隆安县', '450100', 3);
INSERT INTO `sys_region` VALUES ('450124', '马山县', '450100', 3);
INSERT INTO `sys_region` VALUES ('450125', '上林县', '450100', 3);
INSERT INTO `sys_region` VALUES ('450126', '宾阳县', '450100', 3);
INSERT INTO `sys_region` VALUES ('450127', '横县', '450100', 3);
INSERT INTO `sys_region` VALUES ('450200', '柳州市', '450000', 2);
INSERT INTO `sys_region` VALUES ('450202', '城中区', '450200', 3);
INSERT INTO `sys_region` VALUES ('450203', '鱼峰区', '450200', 3);
INSERT INTO `sys_region` VALUES ('450204', '柳南区', '450200', 3);
INSERT INTO `sys_region` VALUES ('450205', '柳北区', '450200', 3);
INSERT INTO `sys_region` VALUES ('450206', '柳江区', '450200', 3);
INSERT INTO `sys_region` VALUES ('450222', '柳城县', '450200', 3);
INSERT INTO `sys_region` VALUES ('450223', '鹿寨县', '450200', 3);
INSERT INTO `sys_region` VALUES ('450224', '融安县', '450200', 3);
INSERT INTO `sys_region` VALUES ('450225', '融水苗族自治县', '450200', 3);
INSERT INTO `sys_region` VALUES ('450226', '三江侗族自治县', '450200', 3);
INSERT INTO `sys_region` VALUES ('450300', '桂林市', '450000', 2);
INSERT INTO `sys_region` VALUES ('450302', '秀峰区', '450300', 3);
INSERT INTO `sys_region` VALUES ('450303', '叠彩区', '450300', 3);
INSERT INTO `sys_region` VALUES ('450304', '象山区', '450300', 3);
INSERT INTO `sys_region` VALUES ('450305', '七星区', '450300', 3);
INSERT INTO `sys_region` VALUES ('450311', '雁山区', '450300', 3);
INSERT INTO `sys_region` VALUES ('450312', '临桂区', '450300', 3);
INSERT INTO `sys_region` VALUES ('450321', '阳朔县', '450300', 3);
INSERT INTO `sys_region` VALUES ('450323', '灵川县', '450300', 3);
INSERT INTO `sys_region` VALUES ('450324', '全州县', '450300', 3);
INSERT INTO `sys_region` VALUES ('450325', '兴安县', '450300', 3);
INSERT INTO `sys_region` VALUES ('450326', '永福县', '450300', 3);
INSERT INTO `sys_region` VALUES ('450327', '灌阳县', '450300', 3);
INSERT INTO `sys_region` VALUES ('450328', '龙胜各族自治县', '450300', 3);
INSERT INTO `sys_region` VALUES ('450329', '资源县', '450300', 3);
INSERT INTO `sys_region` VALUES ('450330', '平乐县', '450300', 3);
INSERT INTO `sys_region` VALUES ('450381', '荔浦市', '450300', 3);
INSERT INTO `sys_region` VALUES ('450332', '恭城瑶族自治县', '450300', 3);
INSERT INTO `sys_region` VALUES ('450400', '梧州市', '450000', 2);
INSERT INTO `sys_region` VALUES ('450403', '万秀区', '450400', 3);
INSERT INTO `sys_region` VALUES ('450405', '长洲区', '450400', 3);
INSERT INTO `sys_region` VALUES ('450406', '龙圩区', '450400', 3);
INSERT INTO `sys_region` VALUES ('450421', '苍梧县', '450400', 3);
INSERT INTO `sys_region` VALUES ('450422', '藤县', '450400', 3);
INSERT INTO `sys_region` VALUES ('450423', '蒙山县', '450400', 3);
INSERT INTO `sys_region` VALUES ('450481', '岑溪市', '450400', 3);
INSERT INTO `sys_region` VALUES ('450500', '北海市', '450000', 2);
INSERT INTO `sys_region` VALUES ('450502', '海城区', '450500', 3);
INSERT INTO `sys_region` VALUES ('450503', '银海区', '450500', 3);
INSERT INTO `sys_region` VALUES ('450512', '铁山港区', '450500', 3);
INSERT INTO `sys_region` VALUES ('450521', '合浦县', '450500', 3);
INSERT INTO `sys_region` VALUES ('450600', '防城港市', '450000', 2);
INSERT INTO `sys_region` VALUES ('450602', '港口区', '450600', 3);
INSERT INTO `sys_region` VALUES ('450603', '防城区', '450600', 3);
INSERT INTO `sys_region` VALUES ('450621', '上思县', '450600', 3);
INSERT INTO `sys_region` VALUES ('450681', '东兴市', '450600', 3);
INSERT INTO `sys_region` VALUES ('450700', '钦州市', '450000', 2);
INSERT INTO `sys_region` VALUES ('450702', '钦南区', '450700', 3);
INSERT INTO `sys_region` VALUES ('450703', '钦北区', '450700', 3);
INSERT INTO `sys_region` VALUES ('450721', '灵山县', '450700', 3);
INSERT INTO `sys_region` VALUES ('450722', '浦北县', '450700', 3);
INSERT INTO `sys_region` VALUES ('450800', '贵港市', '450000', 2);
INSERT INTO `sys_region` VALUES ('450802', '港北区', '450800', 3);
INSERT INTO `sys_region` VALUES ('450803', '港南区', '450800', 3);
INSERT INTO `sys_region` VALUES ('450804', '覃塘区', '450800', 3);
INSERT INTO `sys_region` VALUES ('450821', '平南县', '450800', 3);
INSERT INTO `sys_region` VALUES ('450881', '桂平市', '450800', 3);
INSERT INTO `sys_region` VALUES ('450900', '玉林市', '450000', 2);
INSERT INTO `sys_region` VALUES ('450902', '玉州区', '450900', 3);
INSERT INTO `sys_region` VALUES ('450903', '福绵区', '450900', 3);
INSERT INTO `sys_region` VALUES ('450921', '容县', '450900', 3);
INSERT INTO `sys_region` VALUES ('450922', '陆川县', '450900', 3);
INSERT INTO `sys_region` VALUES ('450923', '博白县', '450900', 3);
INSERT INTO `sys_region` VALUES ('450924', '兴业县', '450900', 3);
INSERT INTO `sys_region` VALUES ('450981', '北流市', '450900', 3);
INSERT INTO `sys_region` VALUES ('451000', '百色市', '450000', 2);
INSERT INTO `sys_region` VALUES ('451002', '右江区', '451000', 3);
INSERT INTO `sys_region` VALUES ('451003', '田阳区', '451000', 3);
INSERT INTO `sys_region` VALUES ('451022', '田东县', '451000', 3);
INSERT INTO `sys_region` VALUES ('451024', '德保县', '451000', 3);
INSERT INTO `sys_region` VALUES ('451026', '那坡县', '451000', 3);
INSERT INTO `sys_region` VALUES ('451027', '凌云县', '451000', 3);
INSERT INTO `sys_region` VALUES ('451028', '乐业县', '451000', 3);
INSERT INTO `sys_region` VALUES ('451029', '田林县', '451000', 3);
INSERT INTO `sys_region` VALUES ('451030', '西林县', '451000', 3);
INSERT INTO `sys_region` VALUES ('451031', '隆林各族自治县', '451000', 3);
INSERT INTO `sys_region` VALUES ('451081', '靖西市', '451000', 3);
INSERT INTO `sys_region` VALUES ('451082', '平果市', '451000', 3);
INSERT INTO `sys_region` VALUES ('451100', '贺州市', '450000', 2);
INSERT INTO `sys_region` VALUES ('451102', '八步区', '451100', 3);
INSERT INTO `sys_region` VALUES ('451103', '平桂区', '451100', 3);
INSERT INTO `sys_region` VALUES ('451121', '昭平县', '451100', 3);
INSERT INTO `sys_region` VALUES ('451122', '钟山县', '451100', 3);
INSERT INTO `sys_region` VALUES ('451123', '富川瑶族自治县', '451100', 3);
INSERT INTO `sys_region` VALUES ('451200', '河池市', '450000', 2);
INSERT INTO `sys_region` VALUES ('451202', '金城江区', '451200', 3);
INSERT INTO `sys_region` VALUES ('451203', '宜州区', '451200', 3);
INSERT INTO `sys_region` VALUES ('451221', '南丹县', '451200', 3);
INSERT INTO `sys_region` VALUES ('451222', '天峨县', '451200', 3);
INSERT INTO `sys_region` VALUES ('451223', '凤山县', '451200', 3);
INSERT INTO `sys_region` VALUES ('451224', '东兰县', '451200', 3);
INSERT INTO `sys_region` VALUES ('451225', '罗城仫佬族自治县', '451200', 3);
INSERT INTO `sys_region` VALUES ('451226', '环江毛南族自治县', '451200', 3);
INSERT INTO `sys_region` VALUES ('451227', '巴马瑶族自治县', '451200', 3);
INSERT INTO `sys_region` VALUES ('451228', '都安瑶族自治县', '451200', 3);
INSERT INTO `sys_region` VALUES ('451229', '大化瑶族自治县', '451200', 3);
INSERT INTO `sys_region` VALUES ('451300', '来宾市', '450000', 2);
INSERT INTO `sys_region` VALUES ('451302', '兴宾区', '451300', 3);
INSERT INTO `sys_region` VALUES ('451321', '忻城县', '451300', 3);
INSERT INTO `sys_region` VALUES ('451322', '象州县', '451300', 3);
INSERT INTO `sys_region` VALUES ('451323', '武宣县', '451300', 3);
INSERT INTO `sys_region` VALUES ('451324', '金秀瑶族自治县', '451300', 3);
INSERT INTO `sys_region` VALUES ('451381', '合山市', '451300', 3);
INSERT INTO `sys_region` VALUES ('451400', '崇左市', '450000', 2);
INSERT INTO `sys_region` VALUES ('451402', '江州区', '451400', 3);
INSERT INTO `sys_region` VALUES ('451421', '扶绥县', '451400', 3);
INSERT INTO `sys_region` VALUES ('451422', '宁明县', '451400', 3);
INSERT INTO `sys_region` VALUES ('451423', '龙州县', '451400', 3);
INSERT INTO `sys_region` VALUES ('451424', '大新县', '451400', 3);
INSERT INTO `sys_region` VALUES ('451425', '天等县', '451400', 3);
INSERT INTO `sys_region` VALUES ('451481', '凭祥市', '451400', 3);
INSERT INTO `sys_region` VALUES ('460000', '海南省', '0', 1);
INSERT INTO `sys_region` VALUES ('460100', '海口市', '460000', 2);
INSERT INTO `sys_region` VALUES ('460105', '秀英区', '460100', 3);
INSERT INTO `sys_region` VALUES ('460106', '龙华区', '460100', 3);
INSERT INTO `sys_region` VALUES ('460107', '琼山区', '460100', 3);
INSERT INTO `sys_region` VALUES ('460108', '美兰区', '460100', 3);
INSERT INTO `sys_region` VALUES ('460200', '三亚市', '460000', 2);
INSERT INTO `sys_region` VALUES ('460202', '海棠区', '460200', 3);
INSERT INTO `sys_region` VALUES ('460203', '吉阳区', '460200', 3);
INSERT INTO `sys_region` VALUES ('460204', '天涯区', '460200', 3);
INSERT INTO `sys_region` VALUES ('460205', '崖州区', '460200', 3);
INSERT INTO `sys_region` VALUES ('460300', '三沙市', '460000', 2);
INSERT INTO `sys_region` VALUES ('460400', '儋州市', '460000', 2);
INSERT INTO `sys_region` VALUES ('469001', '五指山市', '460000', 2);
INSERT INTO `sys_region` VALUES ('469002', '琼海市', '460000', 2);
INSERT INTO `sys_region` VALUES ('469005', '文昌市', '460000', 2);
INSERT INTO `sys_region` VALUES ('469006', '万宁市', '460000', 2);
INSERT INTO `sys_region` VALUES ('469007', '东方市', '460000', 2);
INSERT INTO `sys_region` VALUES ('469021', '定安县', '460000', 2);
INSERT INTO `sys_region` VALUES ('469022', '屯昌县', '460000', 2);
INSERT INTO `sys_region` VALUES ('469023', '澄迈县', '460000', 2);
INSERT INTO `sys_region` VALUES ('469024', '临高县', '460000', 2);
INSERT INTO `sys_region` VALUES ('469025', '白沙黎族自治县', '460000', 2);
INSERT INTO `sys_region` VALUES ('469026', '昌江黎族自治县', '460000', 2);
INSERT INTO `sys_region` VALUES ('469027', '乐东黎族自治县', '460000', 2);
INSERT INTO `sys_region` VALUES ('469028', '陵水黎族自治县', '460000', 2);
INSERT INTO `sys_region` VALUES ('469029', '保亭黎族苗族自治县', '460000', 2);
INSERT INTO `sys_region` VALUES ('469030', '琼中黎族苗族自治县', '460000', 2);
INSERT INTO `sys_region` VALUES ('500000', '重庆市', '0', 1);
INSERT INTO `sys_region` VALUES ('500000', '重庆市', '500000', 2);
INSERT INTO `sys_region` VALUES ('500101', '万州区', '500000', 3);
INSERT INTO `sys_region` VALUES ('500102', '涪陵区', '500000', 3);
INSERT INTO `sys_region` VALUES ('500103', '渝中区', '500000', 3);
INSERT INTO `sys_region` VALUES ('500104', '大渡口区', '500000', 3);
INSERT INTO `sys_region` VALUES ('500105', '江北区', '500000', 3);
INSERT INTO `sys_region` VALUES ('500106', '沙坪坝区', '500000', 3);
INSERT INTO `sys_region` VALUES ('500107', '九龙坡区', '500000', 3);
INSERT INTO `sys_region` VALUES ('500108', '南岸区', '500000', 3);
INSERT INTO `sys_region` VALUES ('500109', '北碚区', '500000', 3);
INSERT INTO `sys_region` VALUES ('500110', '綦江区', '500000', 3);
INSERT INTO `sys_region` VALUES ('500111', '大足区', '500000', 3);
INSERT INTO `sys_region` VALUES ('500112', '渝北区', '500000', 3);
INSERT INTO `sys_region` VALUES ('500113', '巴南区', '500000', 3);
INSERT INTO `sys_region` VALUES ('500114', '黔江区', '500000', 3);
INSERT INTO `sys_region` VALUES ('500115', '长寿区', '500000', 3);
INSERT INTO `sys_region` VALUES ('500116', '江津区', '500000', 3);
INSERT INTO `sys_region` VALUES ('500117', '合川区', '500000', 3);
INSERT INTO `sys_region` VALUES ('500118', '永川区', '500000', 3);
INSERT INTO `sys_region` VALUES ('500119', '南川区', '500000', 3);
INSERT INTO `sys_region` VALUES ('500120', '璧山区', '500000', 3);
INSERT INTO `sys_region` VALUES ('500151', '铜梁区', '500000', 3);
INSERT INTO `sys_region` VALUES ('500152', '潼南区', '500000', 3);
INSERT INTO `sys_region` VALUES ('500153', '荣昌区', '500000', 3);
INSERT INTO `sys_region` VALUES ('500154', '开州区', '500000', 3);
INSERT INTO `sys_region` VALUES ('500155', '梁平区', '500000', 3);
INSERT INTO `sys_region` VALUES ('500156', '武隆区', '500000', 3);
INSERT INTO `sys_region` VALUES ('500229', '城口县', '500000', 3);
INSERT INTO `sys_region` VALUES ('500230', '丰都县', '500000', 3);
INSERT INTO `sys_region` VALUES ('500231', '垫江县', '500000', 3);
INSERT INTO `sys_region` VALUES ('500233', '忠县', '500000', 3);
INSERT INTO `sys_region` VALUES ('500235', '云阳县', '500000', 3);
INSERT INTO `sys_region` VALUES ('500236', '奉节县', '500000', 3);
INSERT INTO `sys_region` VALUES ('500237', '巫山县', '500000', 3);
INSERT INTO `sys_region` VALUES ('500238', '巫溪县', '500000', 3);
INSERT INTO `sys_region` VALUES ('500240', '石柱土家族自治县', '500000', 3);
INSERT INTO `sys_region` VALUES ('500241', '秀山土家族苗族自治县', '500000', 3);
INSERT INTO `sys_region` VALUES ('500242', '酉阳土家族苗族自治县', '500000', 3);
INSERT INTO `sys_region` VALUES ('500243', '彭水苗族土家族自治县', '500000', 3);
INSERT INTO `sys_region` VALUES ('510000', '四川省', '0', 1);
INSERT INTO `sys_region` VALUES ('510100', '成都市', '510000', 2);
INSERT INTO `sys_region` VALUES ('510104', '锦江区', '510100', 3);
INSERT INTO `sys_region` VALUES ('510105', '青羊区', '510100', 3);
INSERT INTO `sys_region` VALUES ('510106', '金牛区', '510100', 3);
INSERT INTO `sys_region` VALUES ('510107', '武侯区', '510100', 3);
INSERT INTO `sys_region` VALUES ('510108', '成华区', '510100', 3);
INSERT INTO `sys_region` VALUES ('510112', '龙泉驿区', '510100', 3);
INSERT INTO `sys_region` VALUES ('510113', '青白江区', '510100', 3);
INSERT INTO `sys_region` VALUES ('510114', '新都区', '510100', 3);
INSERT INTO `sys_region` VALUES ('510115', '温江区', '510100', 3);
INSERT INTO `sys_region` VALUES ('510116', '双流区', '510100', 3);
INSERT INTO `sys_region` VALUES ('510117', '郫都区', '510100', 3);
INSERT INTO `sys_region` VALUES ('510121', '金堂县', '510100', 3);
INSERT INTO `sys_region` VALUES ('510129', '大邑县', '510100', 3);
INSERT INTO `sys_region` VALUES ('510131', '蒲江县', '510100', 3);
INSERT INTO `sys_region` VALUES ('510132', '新津县', '510100', 3);
INSERT INTO `sys_region` VALUES ('510181', '都江堰市', '510100', 3);
INSERT INTO `sys_region` VALUES ('510182', '彭州市', '510100', 3);
INSERT INTO `sys_region` VALUES ('510183', '邛崃市', '510100', 3);
INSERT INTO `sys_region` VALUES ('510184', '崇州市', '510100', 3);
INSERT INTO `sys_region` VALUES ('510185', '简阳市', '510100', 3);
INSERT INTO `sys_region` VALUES ('510300', '自贡市', '510000', 2);
INSERT INTO `sys_region` VALUES ('510302', '自流井区', '510300', 3);
INSERT INTO `sys_region` VALUES ('510303', '贡井区', '510300', 3);
INSERT INTO `sys_region` VALUES ('510304', '大安区', '510300', 3);
INSERT INTO `sys_region` VALUES ('510311', '沿滩区', '510300', 3);
INSERT INTO `sys_region` VALUES ('510321', '荣县', '510300', 3);
INSERT INTO `sys_region` VALUES ('510322', '富顺县', '510300', 3);
INSERT INTO `sys_region` VALUES ('510400', '攀枝花市', '510000', 2);
INSERT INTO `sys_region` VALUES ('510402', '东区', '510400', 3);
INSERT INTO `sys_region` VALUES ('510403', '西区', '510400', 3);
INSERT INTO `sys_region` VALUES ('510411', '仁和区', '510400', 3);
INSERT INTO `sys_region` VALUES ('510421', '米易县', '510400', 3);
INSERT INTO `sys_region` VALUES ('510422', '盐边县', '510400', 3);
INSERT INTO `sys_region` VALUES ('510500', '泸州市', '510000', 2);
INSERT INTO `sys_region` VALUES ('510502', '江阳区', '510500', 3);
INSERT INTO `sys_region` VALUES ('510503', '纳溪区', '510500', 3);
INSERT INTO `sys_region` VALUES ('510504', '龙马潭区', '510500', 3);
INSERT INTO `sys_region` VALUES ('510521', '泸县', '510500', 3);
INSERT INTO `sys_region` VALUES ('510522', '合江县', '510500', 3);
INSERT INTO `sys_region` VALUES ('510524', '叙永县', '510500', 3);
INSERT INTO `sys_region` VALUES ('510525', '古蔺县', '510500', 3);
INSERT INTO `sys_region` VALUES ('510600', '德阳市', '510000', 2);
INSERT INTO `sys_region` VALUES ('510603', '旌阳区', '510600', 3);
INSERT INTO `sys_region` VALUES ('510604', '罗江区', '510600', 3);
INSERT INTO `sys_region` VALUES ('510623', '中江县', '510600', 3);
INSERT INTO `sys_region` VALUES ('510681', '广汉市', '510600', 3);
INSERT INTO `sys_region` VALUES ('510682', '什邡市', '510600', 3);
INSERT INTO `sys_region` VALUES ('510683', '绵竹市', '510600', 3);
INSERT INTO `sys_region` VALUES ('510700', '绵阳市', '510000', 2);
INSERT INTO `sys_region` VALUES ('510703', '涪城区', '510700', 3);
INSERT INTO `sys_region` VALUES ('510704', '游仙区', '510700', 3);
INSERT INTO `sys_region` VALUES ('510705', '安州区', '510700', 3);
INSERT INTO `sys_region` VALUES ('510722', '三台县', '510700', 3);
INSERT INTO `sys_region` VALUES ('510723', '盐亭县', '510700', 3);
INSERT INTO `sys_region` VALUES ('510725', '梓潼县', '510700', 3);
INSERT INTO `sys_region` VALUES ('510726', '北川羌族自治县', '510700', 3);
INSERT INTO `sys_region` VALUES ('510727', '平武县', '510700', 3);
INSERT INTO `sys_region` VALUES ('510781', '江油市', '510700', 3);
INSERT INTO `sys_region` VALUES ('510800', '广元市', '510000', 2);
INSERT INTO `sys_region` VALUES ('510802', '利州区', '510800', 3);
INSERT INTO `sys_region` VALUES ('510811', '昭化区', '510800', 3);
INSERT INTO `sys_region` VALUES ('510812', '朝天区', '510800', 3);
INSERT INTO `sys_region` VALUES ('510821', '旺苍县', '510800', 3);
INSERT INTO `sys_region` VALUES ('510822', '青川县', '510800', 3);
INSERT INTO `sys_region` VALUES ('510823', '剑阁县', '510800', 3);
INSERT INTO `sys_region` VALUES ('510824', '苍溪县', '510800', 3);
INSERT INTO `sys_region` VALUES ('510900', '遂宁市', '510000', 2);
INSERT INTO `sys_region` VALUES ('510903', '船山区', '510900', 3);
INSERT INTO `sys_region` VALUES ('510904', '安居区', '510900', 3);
INSERT INTO `sys_region` VALUES ('510921', '蓬溪县', '510900', 3);
INSERT INTO `sys_region` VALUES ('510923', '大英县', '510900', 3);
INSERT INTO `sys_region` VALUES ('510981', '射洪市', '510900', 3);
INSERT INTO `sys_region` VALUES ('511000', '内江市', '510000', 2);
INSERT INTO `sys_region` VALUES ('511002', '市中区', '511000', 3);
INSERT INTO `sys_region` VALUES ('511011', '东兴区', '511000', 3);
INSERT INTO `sys_region` VALUES ('511024', '威远县', '511000', 3);
INSERT INTO `sys_region` VALUES ('511025', '资中县', '511000', 3);
INSERT INTO `sys_region` VALUES ('511083', '隆昌市', '511000', 3);
INSERT INTO `sys_region` VALUES ('511100', '乐山市', '510000', 2);
INSERT INTO `sys_region` VALUES ('511102', '市中区', '511100', 3);
INSERT INTO `sys_region` VALUES ('511111', '沙湾区', '511100', 3);
INSERT INTO `sys_region` VALUES ('511112', '五通桥区', '511100', 3);
INSERT INTO `sys_region` VALUES ('511113', '金口河区', '511100', 3);
INSERT INTO `sys_region` VALUES ('511123', '犍为县', '511100', 3);
INSERT INTO `sys_region` VALUES ('511124', '井研县', '511100', 3);
INSERT INTO `sys_region` VALUES ('511126', '夹江县', '511100', 3);
INSERT INTO `sys_region` VALUES ('511129', '沐川县', '511100', 3);
INSERT INTO `sys_region` VALUES ('511132', '峨边彝族自治县', '511100', 3);
INSERT INTO `sys_region` VALUES ('511133', '马边彝族自治县', '511100', 3);
INSERT INTO `sys_region` VALUES ('511181', '峨眉山市', '511100', 3);
INSERT INTO `sys_region` VALUES ('511300', '南充市', '510000', 2);
INSERT INTO `sys_region` VALUES ('511302', '顺庆区', '511300', 3);
INSERT INTO `sys_region` VALUES ('511303', '高坪区', '511300', 3);
INSERT INTO `sys_region` VALUES ('511304', '嘉陵区', '511300', 3);
INSERT INTO `sys_region` VALUES ('511321', '南部县', '511300', 3);
INSERT INTO `sys_region` VALUES ('511322', '营山县', '511300', 3);
INSERT INTO `sys_region` VALUES ('511323', '蓬安县', '511300', 3);
INSERT INTO `sys_region` VALUES ('511324', '仪陇县', '511300', 3);
INSERT INTO `sys_region` VALUES ('511325', '西充县', '511300', 3);
INSERT INTO `sys_region` VALUES ('511381', '阆中市', '511300', 3);
INSERT INTO `sys_region` VALUES ('511400', '眉山市', '510000', 2);
INSERT INTO `sys_region` VALUES ('511402', '东坡区', '511400', 3);
INSERT INTO `sys_region` VALUES ('511403', '彭山区', '511400', 3);
INSERT INTO `sys_region` VALUES ('511421', '仁寿县', '511400', 3);
INSERT INTO `sys_region` VALUES ('511423', '洪雅县', '511400', 3);
INSERT INTO `sys_region` VALUES ('511424', '丹棱县', '511400', 3);
INSERT INTO `sys_region` VALUES ('511425', '青神县', '511400', 3);
INSERT INTO `sys_region` VALUES ('511500', '宜宾市', '510000', 2);
INSERT INTO `sys_region` VALUES ('511502', '翠屏区', '511500', 3);
INSERT INTO `sys_region` VALUES ('511503', '南溪区', '511500', 3);
INSERT INTO `sys_region` VALUES ('511504', '叙州区', '511500', 3);
INSERT INTO `sys_region` VALUES ('511523', '江安县', '511500', 3);
INSERT INTO `sys_region` VALUES ('511524', '长宁县', '511500', 3);
INSERT INTO `sys_region` VALUES ('511525', '高县', '511500', 3);
INSERT INTO `sys_region` VALUES ('511526', '珙县', '511500', 3);
INSERT INTO `sys_region` VALUES ('511527', '筠连县', '511500', 3);
INSERT INTO `sys_region` VALUES ('511528', '兴文县', '511500', 3);
INSERT INTO `sys_region` VALUES ('511529', '屏山县', '511500', 3);
INSERT INTO `sys_region` VALUES ('511600', '广安市', '510000', 2);
INSERT INTO `sys_region` VALUES ('511602', '广安区', '511600', 3);
INSERT INTO `sys_region` VALUES ('511603', '前锋区', '511600', 3);
INSERT INTO `sys_region` VALUES ('511621', '岳池县', '511600', 3);
INSERT INTO `sys_region` VALUES ('511622', '武胜县', '511600', 3);
INSERT INTO `sys_region` VALUES ('511623', '邻水县', '511600', 3);
INSERT INTO `sys_region` VALUES ('511681', '华蓥市', '511600', 3);
INSERT INTO `sys_region` VALUES ('511700', '达州市', '510000', 2);
INSERT INTO `sys_region` VALUES ('511702', '通川区', '511700', 3);
INSERT INTO `sys_region` VALUES ('511703', '达川区', '511700', 3);
INSERT INTO `sys_region` VALUES ('511722', '宣汉县', '511700', 3);
INSERT INTO `sys_region` VALUES ('511723', '开江县', '511700', 3);
INSERT INTO `sys_region` VALUES ('511724', '大竹县', '511700', 3);
INSERT INTO `sys_region` VALUES ('511725', '渠县', '511700', 3);
INSERT INTO `sys_region` VALUES ('511781', '万源市', '511700', 3);
INSERT INTO `sys_region` VALUES ('511800', '雅安市', '510000', 2);
INSERT INTO `sys_region` VALUES ('511802', '雨城区', '511800', 3);
INSERT INTO `sys_region` VALUES ('511803', '名山区', '511800', 3);
INSERT INTO `sys_region` VALUES ('511822', '荥经县', '511800', 3);
INSERT INTO `sys_region` VALUES ('511823', '汉源县', '511800', 3);
INSERT INTO `sys_region` VALUES ('511824', '石棉县', '511800', 3);
INSERT INTO `sys_region` VALUES ('511825', '天全县', '511800', 3);
INSERT INTO `sys_region` VALUES ('511826', '芦山县', '511800', 3);
INSERT INTO `sys_region` VALUES ('511827', '宝兴县', '511800', 3);
INSERT INTO `sys_region` VALUES ('511900', '巴中市', '510000', 2);
INSERT INTO `sys_region` VALUES ('511902', '巴州区', '511900', 3);
INSERT INTO `sys_region` VALUES ('511903', '恩阳区', '511900', 3);
INSERT INTO `sys_region` VALUES ('511921', '通江县', '511900', 3);
INSERT INTO `sys_region` VALUES ('511922', '南江县', '511900', 3);
INSERT INTO `sys_region` VALUES ('511923', '平昌县', '511900', 3);
INSERT INTO `sys_region` VALUES ('512000', '资阳市', '510000', 2);
INSERT INTO `sys_region` VALUES ('512002', '雁江区', '512000', 3);
INSERT INTO `sys_region` VALUES ('512021', '安岳县', '512000', 3);
INSERT INTO `sys_region` VALUES ('512022', '乐至县', '512000', 3);
INSERT INTO `sys_region` VALUES ('513200', '阿坝藏族羌族自治州', '510000', 2);
INSERT INTO `sys_region` VALUES ('513201', '马尔康市', '513200', 3);
INSERT INTO `sys_region` VALUES ('513221', '汶川县', '513200', 3);
INSERT INTO `sys_region` VALUES ('513222', '理县', '513200', 3);
INSERT INTO `sys_region` VALUES ('513223', '茂县', '513200', 3);
INSERT INTO `sys_region` VALUES ('513224', '松潘县', '513200', 3);
INSERT INTO `sys_region` VALUES ('513225', '九寨沟县', '513200', 3);
INSERT INTO `sys_region` VALUES ('513226', '金川县', '513200', 3);
INSERT INTO `sys_region` VALUES ('513227', '小金县', '513200', 3);
INSERT INTO `sys_region` VALUES ('513228', '黑水县', '513200', 3);
INSERT INTO `sys_region` VALUES ('513230', '壤塘县', '513200', 3);
INSERT INTO `sys_region` VALUES ('513231', '阿坝县', '513200', 3);
INSERT INTO `sys_region` VALUES ('513232', '若尔盖县', '513200', 3);
INSERT INTO `sys_region` VALUES ('513233', '红原县', '513200', 3);
INSERT INTO `sys_region` VALUES ('513300', '甘孜藏族自治州', '510000', 2);
INSERT INTO `sys_region` VALUES ('513301', '康定市', '513300', 3);
INSERT INTO `sys_region` VALUES ('513322', '泸定县', '513300', 3);
INSERT INTO `sys_region` VALUES ('513323', '丹巴县', '513300', 3);
INSERT INTO `sys_region` VALUES ('513324', '九龙县', '513300', 3);
INSERT INTO `sys_region` VALUES ('513325', '雅江县', '513300', 3);
INSERT INTO `sys_region` VALUES ('513326', '道孚县', '513300', 3);
INSERT INTO `sys_region` VALUES ('513327', '炉霍县', '513300', 3);
INSERT INTO `sys_region` VALUES ('513328', '甘孜县', '513300', 3);
INSERT INTO `sys_region` VALUES ('513329', '新龙县', '513300', 3);
INSERT INTO `sys_region` VALUES ('513330', '德格县', '513300', 3);
INSERT INTO `sys_region` VALUES ('513331', '白玉县', '513300', 3);
INSERT INTO `sys_region` VALUES ('513332', '石渠县', '513300', 3);
INSERT INTO `sys_region` VALUES ('513333', '色达县', '513300', 3);
INSERT INTO `sys_region` VALUES ('513334', '理塘县', '513300', 3);
INSERT INTO `sys_region` VALUES ('513335', '巴塘县', '513300', 3);
INSERT INTO `sys_region` VALUES ('513336', '乡城县', '513300', 3);
INSERT INTO `sys_region` VALUES ('513337', '稻城县', '513300', 3);
INSERT INTO `sys_region` VALUES ('513338', '得荣县', '513300', 3);
INSERT INTO `sys_region` VALUES ('513400', '凉山彝族自治州', '510000', 2);
INSERT INTO `sys_region` VALUES ('513401', '西昌市', '513400', 3);
INSERT INTO `sys_region` VALUES ('513422', '木里藏族自治县', '513400', 3);
INSERT INTO `sys_region` VALUES ('513423', '盐源县', '513400', 3);
INSERT INTO `sys_region` VALUES ('513424', '德昌县', '513400', 3);
INSERT INTO `sys_region` VALUES ('513425', '会理县', '513400', 3);
INSERT INTO `sys_region` VALUES ('513426', '会东县', '513400', 3);
INSERT INTO `sys_region` VALUES ('513427', '宁南县', '513400', 3);
INSERT INTO `sys_region` VALUES ('513428', '普格县', '513400', 3);
INSERT INTO `sys_region` VALUES ('513429', '布拖县', '513400', 3);
INSERT INTO `sys_region` VALUES ('513430', '金阳县', '513400', 3);
INSERT INTO `sys_region` VALUES ('513431', '昭觉县', '513400', 3);
INSERT INTO `sys_region` VALUES ('513432', '喜德县', '513400', 3);
INSERT INTO `sys_region` VALUES ('513433', '冕宁县', '513400', 3);
INSERT INTO `sys_region` VALUES ('513434', '越西县', '513400', 3);
INSERT INTO `sys_region` VALUES ('513435', '甘洛县', '513400', 3);
INSERT INTO `sys_region` VALUES ('513436', '美姑县', '513400', 3);
INSERT INTO `sys_region` VALUES ('513437', '雷波县', '513400', 3);
INSERT INTO `sys_region` VALUES ('520000', '贵州省', '0', 1);
INSERT INTO `sys_region` VALUES ('520100', '贵阳市', '520000', 2);
INSERT INTO `sys_region` VALUES ('520102', '南明区', '520100', 3);
INSERT INTO `sys_region` VALUES ('520103', '云岩区', '520100', 3);
INSERT INTO `sys_region` VALUES ('520111', '花溪区', '520100', 3);
INSERT INTO `sys_region` VALUES ('520112', '乌当区', '520100', 3);
INSERT INTO `sys_region` VALUES ('520113', '白云区', '520100', 3);
INSERT INTO `sys_region` VALUES ('520115', '观山湖区', '520100', 3);
INSERT INTO `sys_region` VALUES ('520121', '开阳县', '520100', 3);
INSERT INTO `sys_region` VALUES ('520122', '息烽县', '520100', 3);
INSERT INTO `sys_region` VALUES ('520123', '修文县', '520100', 3);
INSERT INTO `sys_region` VALUES ('520181', '清镇市', '520100', 3);
INSERT INTO `sys_region` VALUES ('520200', '六盘水市', '520000', 2);
INSERT INTO `sys_region` VALUES ('520201', '钟山区', '520200', 3);
INSERT INTO `sys_region` VALUES ('520203', '六枝特区', '520200', 3);
INSERT INTO `sys_region` VALUES ('520221', '水城县', '520200', 3);
INSERT INTO `sys_region` VALUES ('520281', '盘州市', '520200', 3);
INSERT INTO `sys_region` VALUES ('520300', '遵义市', '520000', 2);
INSERT INTO `sys_region` VALUES ('520302', '红花岗区', '520300', 3);
INSERT INTO `sys_region` VALUES ('520303', '汇川区', '520300', 3);
INSERT INTO `sys_region` VALUES ('520304', '播州区', '520300', 3);
INSERT INTO `sys_region` VALUES ('520322', '桐梓县', '520300', 3);
INSERT INTO `sys_region` VALUES ('520323', '绥阳县', '520300', 3);
INSERT INTO `sys_region` VALUES ('520324', '正安县', '520300', 3);
INSERT INTO `sys_region` VALUES ('520325', '道真仡佬族苗族自治县', '520300', 3);
INSERT INTO `sys_region` VALUES ('520326', '务川仡佬族苗族自治县', '520300', 3);
INSERT INTO `sys_region` VALUES ('520327', '凤冈县', '520300', 3);
INSERT INTO `sys_region` VALUES ('520328', '湄潭县', '520300', 3);
INSERT INTO `sys_region` VALUES ('520329', '余庆县', '520300', 3);
INSERT INTO `sys_region` VALUES ('520330', '习水县', '520300', 3);
INSERT INTO `sys_region` VALUES ('520381', '赤水市', '520300', 3);
INSERT INTO `sys_region` VALUES ('520382', '仁怀市', '520300', 3);
INSERT INTO `sys_region` VALUES ('520400', '安顺市', '520000', 2);
INSERT INTO `sys_region` VALUES ('520402', '西秀区', '520400', 3);
INSERT INTO `sys_region` VALUES ('520403', '平坝区', '520400', 3);
INSERT INTO `sys_region` VALUES ('520422', '普定县', '520400', 3);
INSERT INTO `sys_region` VALUES ('520423', '镇宁布依族苗族自治县', '520400', 3);
INSERT INTO `sys_region` VALUES ('520424', '关岭布依族苗族自治县', '520400', 3);
INSERT INTO `sys_region` VALUES ('520425', '紫云苗族布依族自治县', '520400', 3);
INSERT INTO `sys_region` VALUES ('520500', '毕节市', '520000', 2);
INSERT INTO `sys_region` VALUES ('520502', '七星关区', '520500', 3);
INSERT INTO `sys_region` VALUES ('520521', '大方县', '520500', 3);
INSERT INTO `sys_region` VALUES ('520522', '黔西县', '520500', 3);
INSERT INTO `sys_region` VALUES ('520523', '金沙县', '520500', 3);
INSERT INTO `sys_region` VALUES ('520524', '织金县', '520500', 3);
INSERT INTO `sys_region` VALUES ('520525', '纳雍县', '520500', 3);
INSERT INTO `sys_region` VALUES ('520526', '威宁彝族回族苗族自治县', '520500', 3);
INSERT INTO `sys_region` VALUES ('520527', '赫章县', '520500', 3);
INSERT INTO `sys_region` VALUES ('520600', '铜仁市', '520000', 2);
INSERT INTO `sys_region` VALUES ('520602', '碧江区', '520600', 3);
INSERT INTO `sys_region` VALUES ('520603', '万山区', '520600', 3);
INSERT INTO `sys_region` VALUES ('520621', '江口县', '520600', 3);
INSERT INTO `sys_region` VALUES ('520622', '玉屏侗族自治县', '520600', 3);
INSERT INTO `sys_region` VALUES ('520623', '石阡县', '520600', 3);
INSERT INTO `sys_region` VALUES ('520624', '思南县', '520600', 3);
INSERT INTO `sys_region` VALUES ('520625', '印江土家族苗族自治县', '520600', 3);
INSERT INTO `sys_region` VALUES ('520626', '德江县', '520600', 3);
INSERT INTO `sys_region` VALUES ('520627', '沿河土家族自治县', '520600', 3);
INSERT INTO `sys_region` VALUES ('520628', '松桃苗族自治县', '520600', 3);
INSERT INTO `sys_region` VALUES ('522300', '黔西南布依族苗族自治州', '520000', 2);
INSERT INTO `sys_region` VALUES ('522301', '兴义市', '522300', 3);
INSERT INTO `sys_region` VALUES ('522302', '兴仁市', '522300', 3);
INSERT INTO `sys_region` VALUES ('522323', '普安县', '522300', 3);
INSERT INTO `sys_region` VALUES ('522324', '晴隆县', '522300', 3);
INSERT INTO `sys_region` VALUES ('522325', '贞丰县', '522300', 3);
INSERT INTO `sys_region` VALUES ('522326', '望谟县', '522300', 3);
INSERT INTO `sys_region` VALUES ('522327', '册亨县', '522300', 3);
INSERT INTO `sys_region` VALUES ('522328', '安龙县', '522300', 3);
INSERT INTO `sys_region` VALUES ('522600', '黔东南苗族侗族自治州', '520000', 2);
INSERT INTO `sys_region` VALUES ('522601', '凯里市', '522600', 3);
INSERT INTO `sys_region` VALUES ('522622', '黄平县', '522600', 3);
INSERT INTO `sys_region` VALUES ('522623', '施秉县', '522600', 3);
INSERT INTO `sys_region` VALUES ('522624', '三穗县', '522600', 3);
INSERT INTO `sys_region` VALUES ('522625', '镇远县', '522600', 3);
INSERT INTO `sys_region` VALUES ('522626', '岑巩县', '522600', 3);
INSERT INTO `sys_region` VALUES ('522627', '天柱县', '522600', 3);
INSERT INTO `sys_region` VALUES ('522628', '锦屏县', '522600', 3);
INSERT INTO `sys_region` VALUES ('522629', '剑河县', '522600', 3);
INSERT INTO `sys_region` VALUES ('522630', '台江县', '522600', 3);
INSERT INTO `sys_region` VALUES ('522631', '黎平县', '522600', 3);
INSERT INTO `sys_region` VALUES ('522632', '榕江县', '522600', 3);
INSERT INTO `sys_region` VALUES ('522633', '从江县', '522600', 3);
INSERT INTO `sys_region` VALUES ('522634', '雷山县', '522600', 3);
INSERT INTO `sys_region` VALUES ('522635', '麻江县', '522600', 3);
INSERT INTO `sys_region` VALUES ('522636', '丹寨县', '522600', 3);
INSERT INTO `sys_region` VALUES ('522700', '黔南布依族苗族自治州', '520000', 2);
INSERT INTO `sys_region` VALUES ('522701', '都匀市', '522700', 3);
INSERT INTO `sys_region` VALUES ('522702', '福泉市', '522700', 3);
INSERT INTO `sys_region` VALUES ('522722', '荔波县', '522700', 3);
INSERT INTO `sys_region` VALUES ('522723', '贵定县', '522700', 3);
INSERT INTO `sys_region` VALUES ('522725', '瓮安县', '522700', 3);
INSERT INTO `sys_region` VALUES ('522726', '独山县', '522700', 3);
INSERT INTO `sys_region` VALUES ('522727', '平塘县', '522700', 3);
INSERT INTO `sys_region` VALUES ('522728', '罗甸县', '522700', 3);
INSERT INTO `sys_region` VALUES ('522729', '长顺县', '522700', 3);
INSERT INTO `sys_region` VALUES ('522730', '龙里县', '522700', 3);
INSERT INTO `sys_region` VALUES ('522731', '惠水县', '522700', 3);
INSERT INTO `sys_region` VALUES ('522732', '三都水族自治县', '522700', 3);
INSERT INTO `sys_region` VALUES ('530000', '云南省', '0', 1);
INSERT INTO `sys_region` VALUES ('530100', '昆明市', '530000', 2);
INSERT INTO `sys_region` VALUES ('530102', '五华区', '530100', 3);
INSERT INTO `sys_region` VALUES ('530103', '盘龙区', '530100', 3);
INSERT INTO `sys_region` VALUES ('530111', '官渡区', '530100', 3);
INSERT INTO `sys_region` VALUES ('530112', '西山区', '530100', 3);
INSERT INTO `sys_region` VALUES ('530113', '东川区', '530100', 3);
INSERT INTO `sys_region` VALUES ('530114', '呈贡区', '530100', 3);
INSERT INTO `sys_region` VALUES ('530115', '晋宁区', '530100', 3);
INSERT INTO `sys_region` VALUES ('530124', '富民县', '530100', 3);
INSERT INTO `sys_region` VALUES ('530125', '宜良县', '530100', 3);
INSERT INTO `sys_region` VALUES ('530126', '石林彝族自治县', '530100', 3);
INSERT INTO `sys_region` VALUES ('530127', '嵩明县', '530100', 3);
INSERT INTO `sys_region` VALUES ('530128', '禄劝彝族苗族自治县', '530100', 3);
INSERT INTO `sys_region` VALUES ('530129', '寻甸回族彝族自治县', '530100', 3);
INSERT INTO `sys_region` VALUES ('530181', '安宁市', '530100', 3);
INSERT INTO `sys_region` VALUES ('530300', '曲靖市', '530000', 2);
INSERT INTO `sys_region` VALUES ('530302', '麒麟区', '530300', 3);
INSERT INTO `sys_region` VALUES ('530303', '沾益区', '530300', 3);
INSERT INTO `sys_region` VALUES ('530304', '马龙区', '530300', 3);
INSERT INTO `sys_region` VALUES ('530322', '陆良县', '530300', 3);
INSERT INTO `sys_region` VALUES ('530323', '师宗县', '530300', 3);
INSERT INTO `sys_region` VALUES ('530324', '罗平县', '530300', 3);
INSERT INTO `sys_region` VALUES ('530325', '富源县', '530300', 3);
INSERT INTO `sys_region` VALUES ('530326', '会泽县', '530300', 3);
INSERT INTO `sys_region` VALUES ('530381', '宣威市', '530300', 3);
INSERT INTO `sys_region` VALUES ('530400', '玉溪市', '530000', 2);
INSERT INTO `sys_region` VALUES ('530402', '红塔区', '530400', 3);
INSERT INTO `sys_region` VALUES ('530403', '江川区', '530400', 3);
INSERT INTO `sys_region` VALUES ('530423', '通海县', '530400', 3);
INSERT INTO `sys_region` VALUES ('530424', '华宁县', '530400', 3);
INSERT INTO `sys_region` VALUES ('530425', '易门县', '530400', 3);
INSERT INTO `sys_region` VALUES ('530426', '峨山彝族自治县', '530400', 3);
INSERT INTO `sys_region` VALUES ('530427', '新平彝族傣族自治县', '530400', 3);
INSERT INTO `sys_region` VALUES ('530428', '元江哈尼族彝族傣族自治县', '530400', 3);
INSERT INTO `sys_region` VALUES ('530481', '澄江市', '530400', 3);
INSERT INTO `sys_region` VALUES ('530500', '保山市', '530000', 2);
INSERT INTO `sys_region` VALUES ('530502', '隆阳区', '530500', 3);
INSERT INTO `sys_region` VALUES ('530521', '施甸县', '530500', 3);
INSERT INTO `sys_region` VALUES ('530523', '龙陵县', '530500', 3);
INSERT INTO `sys_region` VALUES ('530524', '昌宁县', '530500', 3);
INSERT INTO `sys_region` VALUES ('530581', '腾冲市', '530500', 3);
INSERT INTO `sys_region` VALUES ('530600', '昭通市', '530000', 2);
INSERT INTO `sys_region` VALUES ('530602', '昭阳区', '530600', 3);
INSERT INTO `sys_region` VALUES ('530621', '鲁甸县', '530600', 3);
INSERT INTO `sys_region` VALUES ('530622', '巧家县', '530600', 3);
INSERT INTO `sys_region` VALUES ('530623', '盐津县', '530600', 3);
INSERT INTO `sys_region` VALUES ('530624', '大关县', '530600', 3);
INSERT INTO `sys_region` VALUES ('530625', '永善县', '530600', 3);
INSERT INTO `sys_region` VALUES ('530626', '绥江县', '530600', 3);
INSERT INTO `sys_region` VALUES ('530627', '镇雄县', '530600', 3);
INSERT INTO `sys_region` VALUES ('530628', '彝良县', '530600', 3);
INSERT INTO `sys_region` VALUES ('530629', '威信县', '530600', 3);
INSERT INTO `sys_region` VALUES ('530681', '水富市', '530600', 3);
INSERT INTO `sys_region` VALUES ('530700', '丽江市', '530000', 2);
INSERT INTO `sys_region` VALUES ('530702', '古城区', '530700', 3);
INSERT INTO `sys_region` VALUES ('530721', '玉龙纳西族自治县', '530700', 3);
INSERT INTO `sys_region` VALUES ('530722', '永胜县', '530700', 3);
INSERT INTO `sys_region` VALUES ('530723', '华坪县', '530700', 3);
INSERT INTO `sys_region` VALUES ('530724', '宁蒗彝族自治县', '530700', 3);
INSERT INTO `sys_region` VALUES ('530800', '普洱市', '530000', 2);
INSERT INTO `sys_region` VALUES ('530802', '思茅区', '530800', 3);
INSERT INTO `sys_region` VALUES ('530821', '宁洱哈尼族彝族自治县', '530800', 3);
INSERT INTO `sys_region` VALUES ('530822', '墨江哈尼族自治县', '530800', 3);
INSERT INTO `sys_region` VALUES ('530823', '景东彝族自治县', '530800', 3);
INSERT INTO `sys_region` VALUES ('530824', '景谷傣族彝族自治县', '530800', 3);
INSERT INTO `sys_region` VALUES ('530825', '镇沅彝族哈尼族拉祜族自治县', '530800', 3);
INSERT INTO `sys_region` VALUES ('530826', '江城哈尼族彝族自治县', '530800', 3);
INSERT INTO `sys_region` VALUES ('530827', '孟连傣族拉祜族佤族自治县', '530800', 3);
INSERT INTO `sys_region` VALUES ('530828', '澜沧拉祜族自治县', '530800', 3);
INSERT INTO `sys_region` VALUES ('530829', '西盟佤族自治县', '530800', 3);
INSERT INTO `sys_region` VALUES ('530900', '临沧市', '530000', 2);
INSERT INTO `sys_region` VALUES ('530902', '临翔区', '530900', 3);
INSERT INTO `sys_region` VALUES ('530921', '凤庆县', '530900', 3);
INSERT INTO `sys_region` VALUES ('530922', '云县', '530900', 3);
INSERT INTO `sys_region` VALUES ('530923', '永德县', '530900', 3);
INSERT INTO `sys_region` VALUES ('530924', '镇康县', '530900', 3);
INSERT INTO `sys_region` VALUES ('530925', '双江拉祜族佤族布朗族傣族自治县', '530900', 3);
INSERT INTO `sys_region` VALUES ('530926', '耿马傣族佤族自治县', '530900', 3);
INSERT INTO `sys_region` VALUES ('530927', '沧源佤族自治县', '530900', 3);
INSERT INTO `sys_region` VALUES ('532300', '楚雄彝族自治州', '530000', 2);
INSERT INTO `sys_region` VALUES ('532301', '楚雄市', '532300', 3);
INSERT INTO `sys_region` VALUES ('532322', '双柏县', '532300', 3);
INSERT INTO `sys_region` VALUES ('532323', '牟定县', '532300', 3);
INSERT INTO `sys_region` VALUES ('532324', '南华县', '532300', 3);
INSERT INTO `sys_region` VALUES ('532325', '姚安县', '532300', 3);
INSERT INTO `sys_region` VALUES ('532326', '大姚县', '532300', 3);
INSERT INTO `sys_region` VALUES ('532327', '永仁县', '532300', 3);
INSERT INTO `sys_region` VALUES ('532328', '元谋县', '532300', 3);
INSERT INTO `sys_region` VALUES ('532329', '武定县', '532300', 3);
INSERT INTO `sys_region` VALUES ('532331', '禄丰县', '532300', 3);
INSERT INTO `sys_region` VALUES ('532500', '红河哈尼族彝族自治州', '530000', 2);
INSERT INTO `sys_region` VALUES ('532501', '个旧市', '532500', 3);
INSERT INTO `sys_region` VALUES ('532502', '开远市', '532500', 3);
INSERT INTO `sys_region` VALUES ('532503', '蒙自市', '532500', 3);
INSERT INTO `sys_region` VALUES ('532504', '弥勒市', '532500', 3);
INSERT INTO `sys_region` VALUES ('532523', '屏边苗族自治县', '532500', 3);
INSERT INTO `sys_region` VALUES ('532524', '建水县', '532500', 3);
INSERT INTO `sys_region` VALUES ('532525', '石屏县', '532500', 3);
INSERT INTO `sys_region` VALUES ('532527', '泸西县', '532500', 3);
INSERT INTO `sys_region` VALUES ('532528', '元阳县', '532500', 3);
INSERT INTO `sys_region` VALUES ('532529', '红河县', '532500', 3);
INSERT INTO `sys_region` VALUES ('532530', '金平苗族瑶族傣族自治县', '532500', 3);
INSERT INTO `sys_region` VALUES ('532531', '绿春县', '532500', 3);
INSERT INTO `sys_region` VALUES ('532532', '河口瑶族自治县', '532500', 3);
INSERT INTO `sys_region` VALUES ('532600', '文山壮族苗族自治州', '530000', 2);
INSERT INTO `sys_region` VALUES ('532601', '文山市', '532600', 3);
INSERT INTO `sys_region` VALUES ('532622', '砚山县', '532600', 3);
INSERT INTO `sys_region` VALUES ('532623', '西畴县', '532600', 3);
INSERT INTO `sys_region` VALUES ('532624', '麻栗坡县', '532600', 3);
INSERT INTO `sys_region` VALUES ('532625', '马关县', '532600', 3);
INSERT INTO `sys_region` VALUES ('532626', '丘北县', '532600', 3);
INSERT INTO `sys_region` VALUES ('532627', '广南县', '532600', 3);
INSERT INTO `sys_region` VALUES ('532628', '富宁县', '532600', 3);
INSERT INTO `sys_region` VALUES ('532800', '西双版纳傣族自治州', '530000', 2);
INSERT INTO `sys_region` VALUES ('532801', '景洪市', '532800', 3);
INSERT INTO `sys_region` VALUES ('532822', '勐海县', '532800', 3);
INSERT INTO `sys_region` VALUES ('532823', '勐腊县', '532800', 3);
INSERT INTO `sys_region` VALUES ('532900', '大理白族自治州', '530000', 2);
INSERT INTO `sys_region` VALUES ('532901', '大理市', '532900', 3);
INSERT INTO `sys_region` VALUES ('532922', '漾濞彝族自治县', '532900', 3);
INSERT INTO `sys_region` VALUES ('532923', '祥云县', '532900', 3);
INSERT INTO `sys_region` VALUES ('532924', '宾川县', '532900', 3);
INSERT INTO `sys_region` VALUES ('532925', '弥渡县', '532900', 3);
INSERT INTO `sys_region` VALUES ('532926', '南涧彝族自治县', '532900', 3);
INSERT INTO `sys_region` VALUES ('532927', '巍山彝族回族自治县', '532900', 3);
INSERT INTO `sys_region` VALUES ('532928', '永平县', '532900', 3);
INSERT INTO `sys_region` VALUES ('532929', '云龙县', '532900', 3);
INSERT INTO `sys_region` VALUES ('532930', '洱源县', '532900', 3);
INSERT INTO `sys_region` VALUES ('532931', '剑川县', '532900', 3);
INSERT INTO `sys_region` VALUES ('532932', '鹤庆县', '532900', 3);
INSERT INTO `sys_region` VALUES ('533100', '德宏傣族景颇族自治州', '530000', 2);
INSERT INTO `sys_region` VALUES ('533102', '瑞丽市', '533100', 3);
INSERT INTO `sys_region` VALUES ('533103', '芒市', '533100', 3);
INSERT INTO `sys_region` VALUES ('533122', '梁河县', '533100', 3);
INSERT INTO `sys_region` VALUES ('533123', '盈江县', '533100', 3);
INSERT INTO `sys_region` VALUES ('533124', '陇川县', '533100', 3);
INSERT INTO `sys_region` VALUES ('533300', '怒江傈僳族自治州', '530000', 2);
INSERT INTO `sys_region` VALUES ('533301', '泸水市', '533300', 3);
INSERT INTO `sys_region` VALUES ('533323', '福贡县', '533300', 3);
INSERT INTO `sys_region` VALUES ('533324', '贡山独龙族怒族自治县', '533300', 3);
INSERT INTO `sys_region` VALUES ('533325', '兰坪白族普米族自治县', '533300', 3);
INSERT INTO `sys_region` VALUES ('533400', '迪庆藏族自治州', '530000', 2);
INSERT INTO `sys_region` VALUES ('533401', '香格里拉市', '533400', 3);
INSERT INTO `sys_region` VALUES ('533422', '德钦县', '533400', 3);
INSERT INTO `sys_region` VALUES ('533423', '维西傈僳族自治县', '533400', 3);
INSERT INTO `sys_region` VALUES ('540000', '西藏自治区', '0', 1);
INSERT INTO `sys_region` VALUES ('540100', '拉萨市', '540000', 2);
INSERT INTO `sys_region` VALUES ('540102', '城关区', '540100', 3);
INSERT INTO `sys_region` VALUES ('540103', '堆龙德庆区', '540100', 3);
INSERT INTO `sys_region` VALUES ('540104', '达孜区', '540100', 3);
INSERT INTO `sys_region` VALUES ('540121', '林周县', '540100', 3);
INSERT INTO `sys_region` VALUES ('540122', '当雄县', '540100', 3);
INSERT INTO `sys_region` VALUES ('540123', '尼木县', '540100', 3);
INSERT INTO `sys_region` VALUES ('540124', '曲水县', '540100', 3);
INSERT INTO `sys_region` VALUES ('540127', '墨竹工卡县', '540100', 3);
INSERT INTO `sys_region` VALUES ('540200', '日喀则市', '540000', 2);
INSERT INTO `sys_region` VALUES ('540202', '桑珠孜区', '540200', 3);
INSERT INTO `sys_region` VALUES ('540221', '南木林县', '540200', 3);
INSERT INTO `sys_region` VALUES ('540222', '江孜县', '540200', 3);
INSERT INTO `sys_region` VALUES ('540223', '定日县', '540200', 3);
INSERT INTO `sys_region` VALUES ('540224', '萨迦县', '540200', 3);
INSERT INTO `sys_region` VALUES ('540225', '拉孜县', '540200', 3);
INSERT INTO `sys_region` VALUES ('540226', '昂仁县', '540200', 3);
INSERT INTO `sys_region` VALUES ('540227', '谢通门县', '540200', 3);
INSERT INTO `sys_region` VALUES ('540228', '白朗县', '540200', 3);
INSERT INTO `sys_region` VALUES ('540229', '仁布县', '540200', 3);
INSERT INTO `sys_region` VALUES ('540230', '康马县', '540200', 3);
INSERT INTO `sys_region` VALUES ('540231', '定结县', '540200', 3);
INSERT INTO `sys_region` VALUES ('540232', '仲巴县', '540200', 3);
INSERT INTO `sys_region` VALUES ('540233', '亚东县', '540200', 3);
INSERT INTO `sys_region` VALUES ('540234', '吉隆县', '540200', 3);
INSERT INTO `sys_region` VALUES ('540235', '聂拉木县', '540200', 3);
INSERT INTO `sys_region` VALUES ('540236', '萨嘎县', '540200', 3);
INSERT INTO `sys_region` VALUES ('540237', '岗巴县', '540200', 3);
INSERT INTO `sys_region` VALUES ('540300', '昌都市', '540000', 2);
INSERT INTO `sys_region` VALUES ('540302', '卡若区', '540300', 3);
INSERT INTO `sys_region` VALUES ('540321', '江达县', '540300', 3);
INSERT INTO `sys_region` VALUES ('540322', '贡觉县', '540300', 3);
INSERT INTO `sys_region` VALUES ('540323', '类乌齐县', '540300', 3);
INSERT INTO `sys_region` VALUES ('540324', '丁青县', '540300', 3);
INSERT INTO `sys_region` VALUES ('540325', '察雅县', '540300', 3);
INSERT INTO `sys_region` VALUES ('540326', '八宿县', '540300', 3);
INSERT INTO `sys_region` VALUES ('540327', '左贡县', '540300', 3);
INSERT INTO `sys_region` VALUES ('540328', '芒康县', '540300', 3);
INSERT INTO `sys_region` VALUES ('540329', '洛隆县', '540300', 3);
INSERT INTO `sys_region` VALUES ('540330', '边坝县', '540300', 3);
INSERT INTO `sys_region` VALUES ('540400', '林芝市', '540000', 2);
INSERT INTO `sys_region` VALUES ('540402', '巴宜区', '540400', 3);
INSERT INTO `sys_region` VALUES ('540421', '工布江达县', '540400', 3);
INSERT INTO `sys_region` VALUES ('540422', '米林县', '540400', 3);
INSERT INTO `sys_region` VALUES ('540423', '墨脱县', '540400', 3);
INSERT INTO `sys_region` VALUES ('540424', '波密县', '540400', 3);
INSERT INTO `sys_region` VALUES ('540425', '察隅县', '540400', 3);
INSERT INTO `sys_region` VALUES ('540426', '朗县', '540400', 3);
INSERT INTO `sys_region` VALUES ('540500', '山南市', '540000', 2);
INSERT INTO `sys_region` VALUES ('540502', '乃东区', '540500', 3);
INSERT INTO `sys_region` VALUES ('540521', '扎囊县', '540500', 3);
INSERT INTO `sys_region` VALUES ('540522', '贡嘎县', '540500', 3);
INSERT INTO `sys_region` VALUES ('540523', '桑日县', '540500', 3);
INSERT INTO `sys_region` VALUES ('540524', '琼结县', '540500', 3);
INSERT INTO `sys_region` VALUES ('540525', '曲松县', '540500', 3);
INSERT INTO `sys_region` VALUES ('540526', '措美县', '540500', 3);
INSERT INTO `sys_region` VALUES ('540527', '洛扎县', '540500', 3);
INSERT INTO `sys_region` VALUES ('540528', '加查县', '540500', 3);
INSERT INTO `sys_region` VALUES ('540529', '隆子县', '540500', 3);
INSERT INTO `sys_region` VALUES ('540530', '错那县', '540500', 3);
INSERT INTO `sys_region` VALUES ('540531', '浪卡子县', '540500', 3);
INSERT INTO `sys_region` VALUES ('540600', '那曲市', '540000', 2);
INSERT INTO `sys_region` VALUES ('540602', '色尼区', '540600', 3);
INSERT INTO `sys_region` VALUES ('540621', '嘉黎县', '540600', 3);
INSERT INTO `sys_region` VALUES ('540622', '比如县', '540600', 3);
INSERT INTO `sys_region` VALUES ('540623', '聂荣县', '540600', 3);
INSERT INTO `sys_region` VALUES ('540624', '安多县', '540600', 3);
INSERT INTO `sys_region` VALUES ('540625', '申扎县', '540600', 3);
INSERT INTO `sys_region` VALUES ('540626', '索县', '540600', 3);
INSERT INTO `sys_region` VALUES ('540627', '班戈县', '540600', 3);
INSERT INTO `sys_region` VALUES ('540628', '巴青县', '540600', 3);
INSERT INTO `sys_region` VALUES ('540629', '尼玛县', '540600', 3);
INSERT INTO `sys_region` VALUES ('540630', '双湖县', '540600', 3);
INSERT INTO `sys_region` VALUES ('542500', '阿里地区', '540000', 2);
INSERT INTO `sys_region` VALUES ('542521', '普兰县', '542500', 3);
INSERT INTO `sys_region` VALUES ('542522', '札达县', '542500', 3);
INSERT INTO `sys_region` VALUES ('542523', '噶尔县', '542500', 3);
INSERT INTO `sys_region` VALUES ('542524', '日土县', '542500', 3);
INSERT INTO `sys_region` VALUES ('542525', '革吉县', '542500', 3);
INSERT INTO `sys_region` VALUES ('542526', '改则县', '542500', 3);
INSERT INTO `sys_region` VALUES ('542527', '措勤县', '542500', 3);
INSERT INTO `sys_region` VALUES ('610000', '陕西省', '0', 1);
INSERT INTO `sys_region` VALUES ('610100', '西安市', '610000', 2);
INSERT INTO `sys_region` VALUES ('610102', '新城区', '610100', 3);
INSERT INTO `sys_region` VALUES ('610103', '碑林区', '610100', 3);
INSERT INTO `sys_region` VALUES ('610104', '莲湖区', '610100', 3);
INSERT INTO `sys_region` VALUES ('610111', '灞桥区', '610100', 3);
INSERT INTO `sys_region` VALUES ('610112', '未央区', '610100', 3);
INSERT INTO `sys_region` VALUES ('610113', '雁塔区', '610100', 3);
INSERT INTO `sys_region` VALUES ('610114', '阎良区', '610100', 3);
INSERT INTO `sys_region` VALUES ('610115', '临潼区', '610100', 3);
INSERT INTO `sys_region` VALUES ('610116', '长安区', '610100', 3);
INSERT INTO `sys_region` VALUES ('610117', '高陵区', '610100', 3);
INSERT INTO `sys_region` VALUES ('610118', '鄠邑区', '610100', 3);
INSERT INTO `sys_region` VALUES ('610122', '蓝田县', '610100', 3);
INSERT INTO `sys_region` VALUES ('610124', '周至县', '610100', 3);
INSERT INTO `sys_region` VALUES ('610200', '铜川市', '610000', 2);
INSERT INTO `sys_region` VALUES ('610202', '王益区', '610200', 3);
INSERT INTO `sys_region` VALUES ('610203', '印台区', '610200', 3);
INSERT INTO `sys_region` VALUES ('610204', '耀州区', '610200', 3);
INSERT INTO `sys_region` VALUES ('610222', '宜君县', '610200', 3);
INSERT INTO `sys_region` VALUES ('610300', '宝鸡市', '610000', 2);
INSERT INTO `sys_region` VALUES ('610302', '渭滨区', '610300', 3);
INSERT INTO `sys_region` VALUES ('610303', '金台区', '610300', 3);
INSERT INTO `sys_region` VALUES ('610304', '陈仓区', '610300', 3);
INSERT INTO `sys_region` VALUES ('610322', '凤翔县', '610300', 3);
INSERT INTO `sys_region` VALUES ('610323', '岐山县', '610300', 3);
INSERT INTO `sys_region` VALUES ('610324', '扶风县', '610300', 3);
INSERT INTO `sys_region` VALUES ('610326', '眉县', '610300', 3);
INSERT INTO `sys_region` VALUES ('610327', '陇县', '610300', 3);
INSERT INTO `sys_region` VALUES ('610328', '千阳县', '610300', 3);
INSERT INTO `sys_region` VALUES ('610329', '麟游县', '610300', 3);
INSERT INTO `sys_region` VALUES ('610330', '凤县', '610300', 3);
INSERT INTO `sys_region` VALUES ('610331', '太白县', '610300', 3);
INSERT INTO `sys_region` VALUES ('610400', '咸阳市', '610000', 2);
INSERT INTO `sys_region` VALUES ('610402', '秦都区', '610400', 3);
INSERT INTO `sys_region` VALUES ('610403', '杨陵区', '610400', 3);
INSERT INTO `sys_region` VALUES ('610404', '渭城区', '610400', 3);
INSERT INTO `sys_region` VALUES ('610422', '三原县', '610400', 3);
INSERT INTO `sys_region` VALUES ('610423', '泾阳县', '610400', 3);
INSERT INTO `sys_region` VALUES ('610424', '乾县', '610400', 3);
INSERT INTO `sys_region` VALUES ('610425', '礼泉县', '610400', 3);
INSERT INTO `sys_region` VALUES ('610426', '永寿县', '610400', 3);
INSERT INTO `sys_region` VALUES ('610428', '长武县', '610400', 3);
INSERT INTO `sys_region` VALUES ('610429', '旬邑县', '610400', 3);
INSERT INTO `sys_region` VALUES ('610430', '淳化县', '610400', 3);
INSERT INTO `sys_region` VALUES ('610431', '武功县', '610400', 3);
INSERT INTO `sys_region` VALUES ('610481', '兴平市', '610400', 3);
INSERT INTO `sys_region` VALUES ('610482', '彬州市', '610400', 3);
INSERT INTO `sys_region` VALUES ('610500', '渭南市', '610000', 2);
INSERT INTO `sys_region` VALUES ('610502', '临渭区', '610500', 3);
INSERT INTO `sys_region` VALUES ('610503', '华州区', '610500', 3);
INSERT INTO `sys_region` VALUES ('610522', '潼关县', '610500', 3);
INSERT INTO `sys_region` VALUES ('610523', '大荔县', '610500', 3);
INSERT INTO `sys_region` VALUES ('610524', '合阳县', '610500', 3);
INSERT INTO `sys_region` VALUES ('610525', '澄城县', '610500', 3);
INSERT INTO `sys_region` VALUES ('610526', '蒲城县', '610500', 3);
INSERT INTO `sys_region` VALUES ('610527', '白水县', '610500', 3);
INSERT INTO `sys_region` VALUES ('610528', '富平县', '610500', 3);
INSERT INTO `sys_region` VALUES ('610581', '韩城市', '610500', 3);
INSERT INTO `sys_region` VALUES ('610582', '华阴市', '610500', 3);
INSERT INTO `sys_region` VALUES ('610600', '延安市', '610000', 2);
INSERT INTO `sys_region` VALUES ('610602', '宝塔区', '610600', 3);
INSERT INTO `sys_region` VALUES ('610603', '安塞区', '610600', 3);
INSERT INTO `sys_region` VALUES ('610621', '延长县', '610600', 3);
INSERT INTO `sys_region` VALUES ('610622', '延川县', '610600', 3);
INSERT INTO `sys_region` VALUES ('610625', '志丹县', '610600', 3);
INSERT INTO `sys_region` VALUES ('610626', '吴起县', '610600', 3);
INSERT INTO `sys_region` VALUES ('610627', '甘泉县', '610600', 3);
INSERT INTO `sys_region` VALUES ('610628', '富县', '610600', 3);
INSERT INTO `sys_region` VALUES ('610629', '洛川县', '610600', 3);
INSERT INTO `sys_region` VALUES ('610630', '宜川县', '610600', 3);
INSERT INTO `sys_region` VALUES ('610631', '黄龙县', '610600', 3);
INSERT INTO `sys_region` VALUES ('610632', '黄陵县', '610600', 3);
INSERT INTO `sys_region` VALUES ('610681', '子长市', '610600', 3);
INSERT INTO `sys_region` VALUES ('610700', '汉中市', '610000', 2);
INSERT INTO `sys_region` VALUES ('610702', '汉台区', '610700', 3);
INSERT INTO `sys_region` VALUES ('610703', '南郑区', '610700', 3);
INSERT INTO `sys_region` VALUES ('610722', '城固县', '610700', 3);
INSERT INTO `sys_region` VALUES ('610723', '洋县', '610700', 3);
INSERT INTO `sys_region` VALUES ('610724', '西乡县', '610700', 3);
INSERT INTO `sys_region` VALUES ('610725', '勉县', '610700', 3);
INSERT INTO `sys_region` VALUES ('610726', '宁强县', '610700', 3);
INSERT INTO `sys_region` VALUES ('610727', '略阳县', '610700', 3);
INSERT INTO `sys_region` VALUES ('610728', '镇巴县', '610700', 3);
INSERT INTO `sys_region` VALUES ('610729', '留坝县', '610700', 3);
INSERT INTO `sys_region` VALUES ('610730', '佛坪县', '610700', 3);
INSERT INTO `sys_region` VALUES ('610800', '榆林市', '610000', 2);
INSERT INTO `sys_region` VALUES ('610802', '榆阳区', '610800', 3);
INSERT INTO `sys_region` VALUES ('610803', '横山区', '610800', 3);
INSERT INTO `sys_region` VALUES ('610822', '府谷县', '610800', 3);
INSERT INTO `sys_region` VALUES ('610824', '靖边县', '610800', 3);
INSERT INTO `sys_region` VALUES ('610825', '定边县', '610800', 3);
INSERT INTO `sys_region` VALUES ('610826', '绥德县', '610800', 3);
INSERT INTO `sys_region` VALUES ('610827', '米脂县', '610800', 3);
INSERT INTO `sys_region` VALUES ('610828', '佳县', '610800', 3);
INSERT INTO `sys_region` VALUES ('610829', '吴堡县', '610800', 3);
INSERT INTO `sys_region` VALUES ('610830', '清涧县', '610800', 3);
INSERT INTO `sys_region` VALUES ('610831', '子洲县', '610800', 3);
INSERT INTO `sys_region` VALUES ('610881', '神木市', '610800', 3);
INSERT INTO `sys_region` VALUES ('610900', '安康市', '610000', 2);
INSERT INTO `sys_region` VALUES ('610902', '汉滨区', '610900', 3);
INSERT INTO `sys_region` VALUES ('610921', '汉阴县', '610900', 3);
INSERT INTO `sys_region` VALUES ('610922', '石泉县', '610900', 3);
INSERT INTO `sys_region` VALUES ('610923', '宁陕县', '610900', 3);
INSERT INTO `sys_region` VALUES ('610924', '紫阳县', '610900', 3);
INSERT INTO `sys_region` VALUES ('610925', '岚皋县', '610900', 3);
INSERT INTO `sys_region` VALUES ('610926', '平利县', '610900', 3);
INSERT INTO `sys_region` VALUES ('610927', '镇坪县', '610900', 3);
INSERT INTO `sys_region` VALUES ('610928', '旬阳县', '610900', 3);
INSERT INTO `sys_region` VALUES ('610929', '白河县', '610900', 3);
INSERT INTO `sys_region` VALUES ('611000', '商洛市', '610000', 2);
INSERT INTO `sys_region` VALUES ('611002', '商州区', '611000', 3);
INSERT INTO `sys_region` VALUES ('611021', '洛南县', '611000', 3);
INSERT INTO `sys_region` VALUES ('611022', '丹凤县', '611000', 3);
INSERT INTO `sys_region` VALUES ('611023', '商南县', '611000', 3);
INSERT INTO `sys_region` VALUES ('611024', '山阳县', '611000', 3);
INSERT INTO `sys_region` VALUES ('611025', '镇安县', '611000', 3);
INSERT INTO `sys_region` VALUES ('611026', '柞水县', '611000', 3);
INSERT INTO `sys_region` VALUES ('620000', '甘肃省', '0', 1);
INSERT INTO `sys_region` VALUES ('620100', '兰州市', '620000', 2);
INSERT INTO `sys_region` VALUES ('620102', '城关区', '620100', 3);
INSERT INTO `sys_region` VALUES ('620103', '七里河区', '620100', 3);
INSERT INTO `sys_region` VALUES ('620104', '西固区', '620100', 3);
INSERT INTO `sys_region` VALUES ('620105', '安宁区', '620100', 3);
INSERT INTO `sys_region` VALUES ('620111', '红古区', '620100', 3);
INSERT INTO `sys_region` VALUES ('620121', '永登县', '620100', 3);
INSERT INTO `sys_region` VALUES ('620122', '皋兰县', '620100', 3);
INSERT INTO `sys_region` VALUES ('620123', '榆中县', '620100', 3);
INSERT INTO `sys_region` VALUES ('620200', '嘉峪关市', '620000', 2);
INSERT INTO `sys_region` VALUES ('620300', '金昌市', '620000', 2);
INSERT INTO `sys_region` VALUES ('620302', '金川区', '620300', 3);
INSERT INTO `sys_region` VALUES ('620321', '永昌县', '620300', 3);
INSERT INTO `sys_region` VALUES ('620400', '白银市', '620000', 2);
INSERT INTO `sys_region` VALUES ('620402', '白银区', '620400', 3);
INSERT INTO `sys_region` VALUES ('620403', '平川区', '620400', 3);
INSERT INTO `sys_region` VALUES ('620421', '靖远县', '620400', 3);
INSERT INTO `sys_region` VALUES ('620422', '会宁县', '620400', 3);
INSERT INTO `sys_region` VALUES ('620423', '景泰县', '620400', 3);
INSERT INTO `sys_region` VALUES ('620500', '天水市', '620000', 2);
INSERT INTO `sys_region` VALUES ('620502', '秦州区', '620500', 3);
INSERT INTO `sys_region` VALUES ('620503', '麦积区', '620500', 3);
INSERT INTO `sys_region` VALUES ('620521', '清水县', '620500', 3);
INSERT INTO `sys_region` VALUES ('620522', '秦安县', '620500', 3);
INSERT INTO `sys_region` VALUES ('620523', '甘谷县', '620500', 3);
INSERT INTO `sys_region` VALUES ('620524', '武山县', '620500', 3);
INSERT INTO `sys_region` VALUES ('620525', '张家川回族自治县', '620500', 3);
INSERT INTO `sys_region` VALUES ('620600', '武威市', '620000', 2);
INSERT INTO `sys_region` VALUES ('620602', '凉州区', '620600', 3);
INSERT INTO `sys_region` VALUES ('620621', '民勤县', '620600', 3);
INSERT INTO `sys_region` VALUES ('620622', '古浪县', '620600', 3);
INSERT INTO `sys_region` VALUES ('620623', '天祝藏族自治县', '620600', 3);
INSERT INTO `sys_region` VALUES ('620700', '张掖市', '620000', 2);
INSERT INTO `sys_region` VALUES ('620702', '甘州区', '620700', 3);
INSERT INTO `sys_region` VALUES ('620721', '肃南裕固族自治县', '620700', 3);
INSERT INTO `sys_region` VALUES ('620722', '民乐县', '620700', 3);
INSERT INTO `sys_region` VALUES ('620723', '临泽县', '620700', 3);
INSERT INTO `sys_region` VALUES ('620724', '高台县', '620700', 3);
INSERT INTO `sys_region` VALUES ('620725', '山丹县', '620700', 3);
INSERT INTO `sys_region` VALUES ('620800', '平凉市', '620000', 2);
INSERT INTO `sys_region` VALUES ('620802', '崆峒区', '620800', 3);
INSERT INTO `sys_region` VALUES ('620821', '泾川县', '620800', 3);
INSERT INTO `sys_region` VALUES ('620822', '灵台县', '620800', 3);
INSERT INTO `sys_region` VALUES ('620823', '崇信县', '620800', 3);
INSERT INTO `sys_region` VALUES ('620825', '庄浪县', '620800', 3);
INSERT INTO `sys_region` VALUES ('620826', '静宁县', '620800', 3);
INSERT INTO `sys_region` VALUES ('620881', '华亭市', '620800', 3);
INSERT INTO `sys_region` VALUES ('620900', '酒泉市', '620000', 2);
INSERT INTO `sys_region` VALUES ('620902', '肃州区', '620900', 3);
INSERT INTO `sys_region` VALUES ('620921', '金塔县', '620900', 3);
INSERT INTO `sys_region` VALUES ('620922', '瓜州县', '620900', 3);
INSERT INTO `sys_region` VALUES ('620923', '肃北蒙古族自治县', '620900', 3);
INSERT INTO `sys_region` VALUES ('620924', '阿克塞哈萨克族自治县', '620900', 3);
INSERT INTO `sys_region` VALUES ('620981', '玉门市', '620900', 3);
INSERT INTO `sys_region` VALUES ('620982', '敦煌市', '620900', 3);
INSERT INTO `sys_region` VALUES ('621000', '庆阳市', '620000', 2);
INSERT INTO `sys_region` VALUES ('621002', '西峰区', '621000', 3);
INSERT INTO `sys_region` VALUES ('621021', '庆城县', '621000', 3);
INSERT INTO `sys_region` VALUES ('621022', '环县', '621000', 3);
INSERT INTO `sys_region` VALUES ('621023', '华池县', '621000', 3);
INSERT INTO `sys_region` VALUES ('621024', '合水县', '621000', 3);
INSERT INTO `sys_region` VALUES ('621025', '正宁县', '621000', 3);
INSERT INTO `sys_region` VALUES ('621026', '宁县', '621000', 3);
INSERT INTO `sys_region` VALUES ('621027', '镇原县', '621000', 3);
INSERT INTO `sys_region` VALUES ('621100', '定西市', '620000', 2);
INSERT INTO `sys_region` VALUES ('621102', '安定区', '621100', 3);
INSERT INTO `sys_region` VALUES ('621121', '通渭县', '621100', 3);
INSERT INTO `sys_region` VALUES ('621122', '陇西县', '621100', 3);
INSERT INTO `sys_region` VALUES ('621123', '渭源县', '621100', 3);
INSERT INTO `sys_region` VALUES ('621124', '临洮县', '621100', 3);
INSERT INTO `sys_region` VALUES ('621125', '漳县', '621100', 3);
INSERT INTO `sys_region` VALUES ('621126', '岷县', '621100', 3);
INSERT INTO `sys_region` VALUES ('621200', '陇南市', '620000', 2);
INSERT INTO `sys_region` VALUES ('621202', '武都区', '621200', 3);
INSERT INTO `sys_region` VALUES ('621221', '成县', '621200', 3);
INSERT INTO `sys_region` VALUES ('621222', '文县', '621200', 3);
INSERT INTO `sys_region` VALUES ('621223', '宕昌县', '621200', 3);
INSERT INTO `sys_region` VALUES ('621224', '康县', '621200', 3);
INSERT INTO `sys_region` VALUES ('621225', '西和县', '621200', 3);
INSERT INTO `sys_region` VALUES ('621226', '礼县', '621200', 3);
INSERT INTO `sys_region` VALUES ('621227', '徽县', '621200', 3);
INSERT INTO `sys_region` VALUES ('621228', '两当县', '621200', 3);
INSERT INTO `sys_region` VALUES ('622900', '临夏回族自治州', '620000', 2);
INSERT INTO `sys_region` VALUES ('622901', '临夏市', '622900', 3);
INSERT INTO `sys_region` VALUES ('622921', '临夏县', '622900', 3);
INSERT INTO `sys_region` VALUES ('622922', '康乐县', '622900', 3);
INSERT INTO `sys_region` VALUES ('622923', '永靖县', '622900', 3);
INSERT INTO `sys_region` VALUES ('622924', '广河县', '622900', 3);
INSERT INTO `sys_region` VALUES ('622925', '和政县', '622900', 3);
INSERT INTO `sys_region` VALUES ('622926', '东乡族自治县', '622900', 3);
INSERT INTO `sys_region` VALUES ('622927', '积石山保安族东乡族撒拉族自治县', '622900', 3);
INSERT INTO `sys_region` VALUES ('623000', '甘南藏族自治州', '620000', 2);
INSERT INTO `sys_region` VALUES ('623001', '合作市', '623000', 3);
INSERT INTO `sys_region` VALUES ('623021', '临潭县', '623000', 3);
INSERT INTO `sys_region` VALUES ('623022', '卓尼县', '623000', 3);
INSERT INTO `sys_region` VALUES ('623023', '舟曲县', '623000', 3);
INSERT INTO `sys_region` VALUES ('623024', '迭部县', '623000', 3);
INSERT INTO `sys_region` VALUES ('623025', '玛曲县', '623000', 3);
INSERT INTO `sys_region` VALUES ('623026', '碌曲县', '623000', 3);
INSERT INTO `sys_region` VALUES ('623027', '夏河县', '623000', 3);
INSERT INTO `sys_region` VALUES ('630000', '青海省', '0', 1);
INSERT INTO `sys_region` VALUES ('630100', '西宁市', '630000', 2);
INSERT INTO `sys_region` VALUES ('630102', '城东区', '630100', 3);
INSERT INTO `sys_region` VALUES ('630103', '城中区', '630100', 3);
INSERT INTO `sys_region` VALUES ('630104', '城西区', '630100', 3);
INSERT INTO `sys_region` VALUES ('630105', '城北区', '630100', 3);
INSERT INTO `sys_region` VALUES ('630106', '湟中区', '630100', 3);
INSERT INTO `sys_region` VALUES ('630121', '大通回族土族自治县', '630100', 3);
INSERT INTO `sys_region` VALUES ('630123', '湟源县', '630100', 3);
INSERT INTO `sys_region` VALUES ('630200', '海东市', '630000', 2);
INSERT INTO `sys_region` VALUES ('630202', '乐都区', '630200', 3);
INSERT INTO `sys_region` VALUES ('630203', '平安区', '630200', 3);
INSERT INTO `sys_region` VALUES ('630222', '民和回族土族自治县', '630200', 3);
INSERT INTO `sys_region` VALUES ('630223', '互助土族自治县', '630200', 3);
INSERT INTO `sys_region` VALUES ('630224', '化隆回族自治县', '630200', 3);
INSERT INTO `sys_region` VALUES ('630225', '循化撒拉族自治县', '630200', 3);
INSERT INTO `sys_region` VALUES ('632200', '海北藏族自治州', '630000', 2);
INSERT INTO `sys_region` VALUES ('632221', '门源回族自治县', '632200', 3);
INSERT INTO `sys_region` VALUES ('632222', '祁连县', '632200', 3);
INSERT INTO `sys_region` VALUES ('632223', '海晏县', '632200', 3);
INSERT INTO `sys_region` VALUES ('632224', '刚察县', '632200', 3);
INSERT INTO `sys_region` VALUES ('632300', '黄南藏族自治州', '630000', 2);
INSERT INTO `sys_region` VALUES ('632321', '同仁县', '632300', 3);
INSERT INTO `sys_region` VALUES ('632322', '尖扎县', '632300', 3);
INSERT INTO `sys_region` VALUES ('632323', '泽库县', '632300', 3);
INSERT INTO `sys_region` VALUES ('632324', '河南蒙古族自治县', '632300', 3);
INSERT INTO `sys_region` VALUES ('632500', '海南藏族自治州', '630000', 2);
INSERT INTO `sys_region` VALUES ('632521', '共和县', '632500', 3);
INSERT INTO `sys_region` VALUES ('632522', '同德县', '632500', 3);
INSERT INTO `sys_region` VALUES ('632523', '贵德县', '632500', 3);
INSERT INTO `sys_region` VALUES ('632524', '兴海县', '632500', 3);
INSERT INTO `sys_region` VALUES ('632525', '贵南县', '632500', 3);
INSERT INTO `sys_region` VALUES ('632600', '果洛藏族自治州', '630000', 2);
INSERT INTO `sys_region` VALUES ('632621', '玛沁县', '632600', 3);
INSERT INTO `sys_region` VALUES ('632622', '班玛县', '632600', 3);
INSERT INTO `sys_region` VALUES ('632623', '甘德县', '632600', 3);
INSERT INTO `sys_region` VALUES ('632624', '达日县', '632600', 3);
INSERT INTO `sys_region` VALUES ('632625', '久治县', '632600', 3);
INSERT INTO `sys_region` VALUES ('632626', '玛多县', '632600', 3);
INSERT INTO `sys_region` VALUES ('632700', '玉树藏族自治州', '630000', 2);
INSERT INTO `sys_region` VALUES ('632701', '玉树市', '632700', 3);
INSERT INTO `sys_region` VALUES ('632722', '杂多县', '632700', 3);
INSERT INTO `sys_region` VALUES ('632723', '称多县', '632700', 3);
INSERT INTO `sys_region` VALUES ('632724', '治多县', '632700', 3);
INSERT INTO `sys_region` VALUES ('632725', '囊谦县', '632700', 3);
INSERT INTO `sys_region` VALUES ('632726', '曲麻莱县', '632700', 3);
INSERT INTO `sys_region` VALUES ('632800', '海西蒙古族藏族自治州', '630000', 2);
INSERT INTO `sys_region` VALUES ('632801', '格尔木市', '632800', 3);
INSERT INTO `sys_region` VALUES ('632802', '德令哈市', '632800', 3);
INSERT INTO `sys_region` VALUES ('632803', '茫崖市', '632800', 3);
INSERT INTO `sys_region` VALUES ('632821', '乌兰县', '632800', 3);
INSERT INTO `sys_region` VALUES ('632822', '都兰县', '632800', 3);
INSERT INTO `sys_region` VALUES ('632823', '天峻县', '632800', 3);
INSERT INTO `sys_region` VALUES ('640000', '宁夏回族自治区', '0', 1);
INSERT INTO `sys_region` VALUES ('640100', '银川市', '640000', 2);
INSERT INTO `sys_region` VALUES ('640104', '兴庆区', '640100', 3);
INSERT INTO `sys_region` VALUES ('640105', '西夏区', '640100', 3);
INSERT INTO `sys_region` VALUES ('640106', '金凤区', '640100', 3);
INSERT INTO `sys_region` VALUES ('640121', '永宁县', '640100', 3);
INSERT INTO `sys_region` VALUES ('640122', '贺兰县', '640100', 3);
INSERT INTO `sys_region` VALUES ('640181', '灵武市', '640100', 3);
INSERT INTO `sys_region` VALUES ('640200', '石嘴山市', '640000', 2);
INSERT INTO `sys_region` VALUES ('640202', '大武口区', '640200', 3);
INSERT INTO `sys_region` VALUES ('640205', '惠农区', '640200', 3);
INSERT INTO `sys_region` VALUES ('640221', '平罗县', '640200', 3);
INSERT INTO `sys_region` VALUES ('640300', '吴忠市', '640000', 2);
INSERT INTO `sys_region` VALUES ('640302', '利通区', '640300', 3);
INSERT INTO `sys_region` VALUES ('640303', '红寺堡区', '640300', 3);
INSERT INTO `sys_region` VALUES ('640323', '盐池县', '640300', 3);
INSERT INTO `sys_region` VALUES ('640324', '同心县', '640300', 3);
INSERT INTO `sys_region` VALUES ('640381', '青铜峡市', '640300', 3);
INSERT INTO `sys_region` VALUES ('640400', '固原市', '640000', 2);
INSERT INTO `sys_region` VALUES ('640402', '原州区', '640400', 3);
INSERT INTO `sys_region` VALUES ('640422', '西吉县', '640400', 3);
INSERT INTO `sys_region` VALUES ('640423', '隆德县', '640400', 3);
INSERT INTO `sys_region` VALUES ('640424', '泾源县', '640400', 3);
INSERT INTO `sys_region` VALUES ('640425', '彭阳县', '640400', 3);
INSERT INTO `sys_region` VALUES ('640500', '中卫市', '640000', 2);
INSERT INTO `sys_region` VALUES ('640502', '沙坡头区', '640500', 3);
INSERT INTO `sys_region` VALUES ('640521', '中宁县', '640500', 3);
INSERT INTO `sys_region` VALUES ('640522', '海原县', '640500', 3);
INSERT INTO `sys_region` VALUES ('650000', '新疆维吾尔自治区', '0', 1);
INSERT INTO `sys_region` VALUES ('650100', '乌鲁木齐市', '650000', 2);
INSERT INTO `sys_region` VALUES ('650102', '天山区', '650100', 3);
INSERT INTO `sys_region` VALUES ('650103', '沙依巴克区', '650100', 3);
INSERT INTO `sys_region` VALUES ('650104', '新市区', '650100', 3);
INSERT INTO `sys_region` VALUES ('650105', '水磨沟区', '650100', 3);
INSERT INTO `sys_region` VALUES ('650106', '头屯河区', '650100', 3);
INSERT INTO `sys_region` VALUES ('650107', '达坂城区', '650100', 3);
INSERT INTO `sys_region` VALUES ('650109', '米东区', '650100', 3);
INSERT INTO `sys_region` VALUES ('650121', '乌鲁木齐县', '650100', 3);
INSERT INTO `sys_region` VALUES ('650200', '克拉玛依市', '650000', 2);
INSERT INTO `sys_region` VALUES ('650202', '独山子区', '650200', 3);
INSERT INTO `sys_region` VALUES ('650203', '克拉玛依区', '650200', 3);
INSERT INTO `sys_region` VALUES ('650204', '白碱滩区', '650200', 3);
INSERT INTO `sys_region` VALUES ('650205', '乌尔禾区', '650200', 3);
INSERT INTO `sys_region` VALUES ('650400', '吐鲁番市', '650000', 2);
INSERT INTO `sys_region` VALUES ('650402', '高昌区', '650400', 3);
INSERT INTO `sys_region` VALUES ('650421', '鄯善县', '650400', 3);
INSERT INTO `sys_region` VALUES ('650422', '托克逊县', '650400', 3);
INSERT INTO `sys_region` VALUES ('650500', '哈密市', '650000', 2);
INSERT INTO `sys_region` VALUES ('650502', '伊州区', '650500', 3);
INSERT INTO `sys_region` VALUES ('650521', '巴里坤哈萨克自治县', '650500', 3);
INSERT INTO `sys_region` VALUES ('650522', '伊吾县', '650500', 3);
INSERT INTO `sys_region` VALUES ('652300', '昌吉回族自治州', '650000', 2);
INSERT INTO `sys_region` VALUES ('652301', '昌吉市', '652300', 3);
INSERT INTO `sys_region` VALUES ('652302', '阜康市', '652300', 3);
INSERT INTO `sys_region` VALUES ('652323', '呼图壁县', '652300', 3);
INSERT INTO `sys_region` VALUES ('652324', '玛纳斯县', '652300', 3);
INSERT INTO `sys_region` VALUES ('652325', '奇台县', '652300', 3);
INSERT INTO `sys_region` VALUES ('652327', '吉木萨尔县', '652300', 3);
INSERT INTO `sys_region` VALUES ('652328', '木垒哈萨克自治县', '652300', 3);
INSERT INTO `sys_region` VALUES ('652700', '博尔塔拉蒙古自治州', '650000', 2);
INSERT INTO `sys_region` VALUES ('652701', '博乐市', '652700', 3);
INSERT INTO `sys_region` VALUES ('652702', '阿拉山口市', '652700', 3);
INSERT INTO `sys_region` VALUES ('652722', '精河县', '652700', 3);
INSERT INTO `sys_region` VALUES ('652723', '温泉县', '652700', 3);
INSERT INTO `sys_region` VALUES ('652800', '巴音郭楞蒙古自治州', '650000', 2);
INSERT INTO `sys_region` VALUES ('652801', '库尔勒市', '652800', 3);
INSERT INTO `sys_region` VALUES ('652822', '轮台县', '652800', 3);
INSERT INTO `sys_region` VALUES ('652823', '尉犁县', '652800', 3);
INSERT INTO `sys_region` VALUES ('652824', '若羌县', '652800', 3);
INSERT INTO `sys_region` VALUES ('652825', '且末县', '652800', 3);
INSERT INTO `sys_region` VALUES ('652826', '焉耆回族自治县', '652800', 3);
INSERT INTO `sys_region` VALUES ('652827', '和静县', '652800', 3);
INSERT INTO `sys_region` VALUES ('652828', '和硕县', '652800', 3);
INSERT INTO `sys_region` VALUES ('652829', '博湖县', '652800', 3);
INSERT INTO `sys_region` VALUES ('652900', '阿克苏地区', '650000', 2);
INSERT INTO `sys_region` VALUES ('652901', '阿克苏市', '652900', 3);
INSERT INTO `sys_region` VALUES ('652902', '库车市', '652900', 3);
INSERT INTO `sys_region` VALUES ('652922', '温宿县', '652900', 3);
INSERT INTO `sys_region` VALUES ('652924', '沙雅县', '652900', 3);
INSERT INTO `sys_region` VALUES ('652925', '新和县', '652900', 3);
INSERT INTO `sys_region` VALUES ('652926', '拜城县', '652900', 3);
INSERT INTO `sys_region` VALUES ('652927', '乌什县', '652900', 3);
INSERT INTO `sys_region` VALUES ('652928', '阿瓦提县', '652900', 3);
INSERT INTO `sys_region` VALUES ('652929', '柯坪县', '652900', 3);
INSERT INTO `sys_region` VALUES ('653000', '克孜勒苏柯尔克孜自治州', '650000', 2);
INSERT INTO `sys_region` VALUES ('653001', '阿图什市', '653000', 3);
INSERT INTO `sys_region` VALUES ('653022', '阿克陶县', '653000', 3);
INSERT INTO `sys_region` VALUES ('653023', '阿合奇县', '653000', 3);
INSERT INTO `sys_region` VALUES ('653024', '乌恰县', '653000', 3);
INSERT INTO `sys_region` VALUES ('653100', '喀什地区', '650000', 2);
INSERT INTO `sys_region` VALUES ('653101', '喀什市', '653100', 3);
INSERT INTO `sys_region` VALUES ('653121', '疏附县', '653100', 3);
INSERT INTO `sys_region` VALUES ('653122', '疏勒县', '653100', 3);
INSERT INTO `sys_region` VALUES ('653123', '英吉沙县', '653100', 3);
INSERT INTO `sys_region` VALUES ('653124', '泽普县', '653100', 3);
INSERT INTO `sys_region` VALUES ('653125', '莎车县', '653100', 3);
INSERT INTO `sys_region` VALUES ('653126', '叶城县', '653100', 3);
INSERT INTO `sys_region` VALUES ('653127', '麦盖提县', '653100', 3);
INSERT INTO `sys_region` VALUES ('653128', '岳普湖县', '653100', 3);
INSERT INTO `sys_region` VALUES ('653129', '伽师县', '653100', 3);
INSERT INTO `sys_region` VALUES ('653130', '巴楚县', '653100', 3);
INSERT INTO `sys_region` VALUES ('653131', '塔什库尔干塔吉克自治县', '653100', 3);
INSERT INTO `sys_region` VALUES ('653200', '和田地区', '650000', 2);
INSERT INTO `sys_region` VALUES ('653201', '和田市', '653200', 3);
INSERT INTO `sys_region` VALUES ('653221', '和田县', '653200', 3);
INSERT INTO `sys_region` VALUES ('653222', '墨玉县', '653200', 3);
INSERT INTO `sys_region` VALUES ('653223', '皮山县', '653200', 3);
INSERT INTO `sys_region` VALUES ('653224', '洛浦县', '653200', 3);
INSERT INTO `sys_region` VALUES ('653225', '策勒县', '653200', 3);
INSERT INTO `sys_region` VALUES ('653226', '于田县', '653200', 3);
INSERT INTO `sys_region` VALUES ('653227', '民丰县', '653200', 3);
INSERT INTO `sys_region` VALUES ('654000', '伊犁哈萨克自治州', '650000', 2);
INSERT INTO `sys_region` VALUES ('654002', '伊宁市', '654000', 3);
INSERT INTO `sys_region` VALUES ('654003', '奎屯市', '654000', 3);
INSERT INTO `sys_region` VALUES ('654004', '霍尔果斯市', '654000', 3);
INSERT INTO `sys_region` VALUES ('654021', '伊宁县', '654000', 3);
INSERT INTO `sys_region` VALUES ('654022', '察布查尔锡伯自治县', '654000', 3);
INSERT INTO `sys_region` VALUES ('654023', '霍城县', '654000', 3);
INSERT INTO `sys_region` VALUES ('654024', '巩留县', '654000', 3);
INSERT INTO `sys_region` VALUES ('654025', '新源县', '654000', 3);
INSERT INTO `sys_region` VALUES ('654026', '昭苏县', '654000', 3);
INSERT INTO `sys_region` VALUES ('654027', '特克斯县', '654000', 3);
INSERT INTO `sys_region` VALUES ('654028', '尼勒克县', '654000', 3);
INSERT INTO `sys_region` VALUES ('654200', '塔城地区', '650000', 2);
INSERT INTO `sys_region` VALUES ('654201', '塔城市', '654200', 3);
INSERT INTO `sys_region` VALUES ('654202', '乌苏市', '654200', 3);
INSERT INTO `sys_region` VALUES ('654221', '额敏县', '654200', 3);
INSERT INTO `sys_region` VALUES ('654223', '沙湾县', '654200', 3);
INSERT INTO `sys_region` VALUES ('654224', '托里县', '654200', 3);
INSERT INTO `sys_region` VALUES ('654225', '裕民县', '654200', 3);
INSERT INTO `sys_region` VALUES ('654226', '和布克赛尔蒙古自治县', '654200', 3);
INSERT INTO `sys_region` VALUES ('654300', '阿勒泰地区', '650000', 2);
INSERT INTO `sys_region` VALUES ('654301', '阿勒泰市', '654300', 3);
INSERT INTO `sys_region` VALUES ('654321', '布尔津县', '654300', 3);
INSERT INTO `sys_region` VALUES ('654322', '富蕴县', '654300', 3);
INSERT INTO `sys_region` VALUES ('654323', '福海县', '654300', 3);
INSERT INTO `sys_region` VALUES ('654324', '哈巴河县', '654300', 3);
INSERT INTO `sys_region` VALUES ('654325', '青河县', '654300', 3);
INSERT INTO `sys_region` VALUES ('654326', '吉木乃县', '654300', 3);
INSERT INTO `sys_region` VALUES ('659001', '石河子市', '650000', 2);
INSERT INTO `sys_region` VALUES ('659002', '阿拉尔市', '650000', 2);
INSERT INTO `sys_region` VALUES ('659003', '图木舒克市', '650000', 2);
INSERT INTO `sys_region` VALUES ('659004', '五家渠市', '650000', 2);
INSERT INTO `sys_region` VALUES ('659005', '北屯市', '650000', 2);
INSERT INTO `sys_region` VALUES ('659006', '铁门关市', '650000', 2);
INSERT INTO `sys_region` VALUES ('659007', '双河市', '650000', 2);
INSERT INTO `sys_region` VALUES ('659008', '可克达拉市', '650000', 2);
INSERT INTO `sys_region` VALUES ('659009', '昆玉市', '650000', 2);
INSERT INTO `sys_region` VALUES ('659010', '胡杨河市', '650000', 2);
INSERT INTO `sys_region` VALUES ('710000', '台湾省', '0', 1);
INSERT INTO `sys_region` VALUES ('810000', '香港特别行政区', '0', 1);
INSERT INTO `sys_region` VALUES ('820000', '澳门特别行政区', '0', 1);
COMMIT;

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role` (
  `role_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '角色ID',
  `role_name` varchar(30) NOT NULL COMMENT '角色名称',
  `role_key` varchar(100) NOT NULL COMMENT '角色权限字符串',
  `role_sort` int(4) NOT NULL COMMENT '显示顺序',
  `data_scope` char(1) DEFAULT '1' COMMENT '数据范围（1：全部数据权限 2：自定数据权限 3：本部门数据权限 4：本部门及以下数据权限）',
  `status` char(1) NOT NULL COMMENT '角色状态（0正常 1停用）',
  `del_flag` char(1) DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='角色信息表';

-- ----------------------------
-- Records of sys_role
-- ----------------------------
BEGIN;
INSERT INTO `sys_role` VALUES (1, '超级管理员', 'admin', 1, '1', '0', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '超级管理员');
INSERT INTO `sys_role` VALUES (2, '普通角色', 'common', 2, '2', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-08-29 23:05:42', '普通角色');
INSERT INTO `sys_role` VALUES (3, '系统管理员', 'systemadmin', 3, '1', '0', '0', 'admin', '2020-08-29 23:00:20', 'admin', '2020-08-29 23:02:37', '系统管理员');
INSERT INTO `sys_role` VALUES (4, '校长', 'rector', 4, '1', '0', '0', 'admin', '2020-08-30 14:08:55', 'admin', '2020-09-17 14:25:46', '');
INSERT INTO `sys_role` VALUES (5, '院长', 'dean', 5, '1', '0', '0', 'admin', '2020-08-30 14:09:38', '', NULL, NULL);
INSERT INTO `sys_role` VALUES (6, '教师', 'teacher', 6, '1', '0', '0', 'admin', '2020-08-30 14:10:21', 'admin', '2020-09-22 15:55:02', '');
INSERT INTO `sys_role` VALUES (7, '学生会主席', 'studentleader', 7, '1', '0', '0', 'admin', '2020-08-30 14:11:11', '', NULL, NULL);
INSERT INTO `sys_role` VALUES (8, '学生会人员', 'studentunionstaff', 8, '1', '0', '0', 'admin', '2020-08-30 14:14:16', '', NULL, NULL);
INSERT INTO `sys_role` VALUES (9, '学生', 'student', 9, '1', '0', '0', 'admin', '2020-08-30 17:04:22', 'admin', '2020-09-22 15:54:40', '');
COMMIT;

-- ----------------------------
-- Table structure for sys_role_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_dept`;
CREATE TABLE `sys_role_dept` (
  `role_id` bigint(20) NOT NULL COMMENT '角色ID',
  `dept_id` bigint(20) NOT NULL COMMENT '部门ID',
  PRIMARY KEY (`role_id`,`dept_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='角色和部门关联表';

-- ----------------------------
-- Records of sys_role_dept
-- ----------------------------
BEGIN;
INSERT INTO `sys_role_dept` VALUES (2, 100);
INSERT INTO `sys_role_dept` VALUES (2, 101);
INSERT INTO `sys_role_dept` VALUES (2, 105);
COMMIT;

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu` (
  `role_id` bigint(20) NOT NULL COMMENT '角色ID',
  `menu_id` bigint(20) NOT NULL COMMENT '菜单ID',
  PRIMARY KEY (`role_id`,`menu_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='角色和菜单关联表';

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
BEGIN;
INSERT INTO `sys_role_menu` VALUES (2, 1);
INSERT INTO `sys_role_menu` VALUES (2, 2);
INSERT INTO `sys_role_menu` VALUES (2, 3);
INSERT INTO `sys_role_menu` VALUES (2, 100);
INSERT INTO `sys_role_menu` VALUES (2, 101);
INSERT INTO `sys_role_menu` VALUES (2, 102);
INSERT INTO `sys_role_menu` VALUES (2, 103);
INSERT INTO `sys_role_menu` VALUES (2, 104);
INSERT INTO `sys_role_menu` VALUES (2, 105);
INSERT INTO `sys_role_menu` VALUES (2, 106);
INSERT INTO `sys_role_menu` VALUES (2, 107);
INSERT INTO `sys_role_menu` VALUES (2, 108);
INSERT INTO `sys_role_menu` VALUES (2, 109);
INSERT INTO `sys_role_menu` VALUES (2, 110);
INSERT INTO `sys_role_menu` VALUES (2, 111);
INSERT INTO `sys_role_menu` VALUES (2, 112);
INSERT INTO `sys_role_menu` VALUES (2, 113);
INSERT INTO `sys_role_menu` VALUES (2, 114);
INSERT INTO `sys_role_menu` VALUES (2, 115);
INSERT INTO `sys_role_menu` VALUES (2, 500);
INSERT INTO `sys_role_menu` VALUES (2, 501);
INSERT INTO `sys_role_menu` VALUES (2, 1000);
INSERT INTO `sys_role_menu` VALUES (2, 1001);
INSERT INTO `sys_role_menu` VALUES (2, 1002);
INSERT INTO `sys_role_menu` VALUES (2, 1003);
INSERT INTO `sys_role_menu` VALUES (2, 1004);
INSERT INTO `sys_role_menu` VALUES (2, 1005);
INSERT INTO `sys_role_menu` VALUES (2, 1006);
INSERT INTO `sys_role_menu` VALUES (2, 1007);
INSERT INTO `sys_role_menu` VALUES (2, 1008);
INSERT INTO `sys_role_menu` VALUES (2, 1009);
INSERT INTO `sys_role_menu` VALUES (2, 1010);
INSERT INTO `sys_role_menu` VALUES (2, 1011);
INSERT INTO `sys_role_menu` VALUES (2, 1012);
INSERT INTO `sys_role_menu` VALUES (2, 1013);
INSERT INTO `sys_role_menu` VALUES (2, 1014);
INSERT INTO `sys_role_menu` VALUES (2, 1015);
INSERT INTO `sys_role_menu` VALUES (2, 1016);
INSERT INTO `sys_role_menu` VALUES (2, 1017);
INSERT INTO `sys_role_menu` VALUES (2, 1018);
INSERT INTO `sys_role_menu` VALUES (2, 1019);
INSERT INTO `sys_role_menu` VALUES (2, 1020);
INSERT INTO `sys_role_menu` VALUES (2, 1021);
INSERT INTO `sys_role_menu` VALUES (2, 1022);
INSERT INTO `sys_role_menu` VALUES (2, 1023);
INSERT INTO `sys_role_menu` VALUES (2, 1024);
INSERT INTO `sys_role_menu` VALUES (2, 1025);
INSERT INTO `sys_role_menu` VALUES (2, 1026);
INSERT INTO `sys_role_menu` VALUES (2, 1027);
INSERT INTO `sys_role_menu` VALUES (2, 1028);
INSERT INTO `sys_role_menu` VALUES (2, 1029);
INSERT INTO `sys_role_menu` VALUES (2, 1030);
INSERT INTO `sys_role_menu` VALUES (2, 1031);
INSERT INTO `sys_role_menu` VALUES (2, 1032);
INSERT INTO `sys_role_menu` VALUES (2, 1033);
INSERT INTO `sys_role_menu` VALUES (2, 1034);
INSERT INTO `sys_role_menu` VALUES (2, 1035);
INSERT INTO `sys_role_menu` VALUES (2, 1036);
INSERT INTO `sys_role_menu` VALUES (2, 1037);
INSERT INTO `sys_role_menu` VALUES (2, 1038);
INSERT INTO `sys_role_menu` VALUES (2, 1039);
INSERT INTO `sys_role_menu` VALUES (2, 1040);
INSERT INTO `sys_role_menu` VALUES (2, 1041);
INSERT INTO `sys_role_menu` VALUES (2, 1042);
INSERT INTO `sys_role_menu` VALUES (2, 1043);
INSERT INTO `sys_role_menu` VALUES (2, 1044);
INSERT INTO `sys_role_menu` VALUES (2, 1045);
INSERT INTO `sys_role_menu` VALUES (2, 1046);
INSERT INTO `sys_role_menu` VALUES (2, 1047);
INSERT INTO `sys_role_menu` VALUES (2, 1048);
INSERT INTO `sys_role_menu` VALUES (2, 1049);
INSERT INTO `sys_role_menu` VALUES (2, 1050);
INSERT INTO `sys_role_menu` VALUES (2, 1051);
INSERT INTO `sys_role_menu` VALUES (2, 1052);
INSERT INTO `sys_role_menu` VALUES (2, 1053);
INSERT INTO `sys_role_menu` VALUES (2, 1054);
INSERT INTO `sys_role_menu` VALUES (2, 1055);
INSERT INTO `sys_role_menu` VALUES (2, 1056);
INSERT INTO `sys_role_menu` VALUES (2, 1057);
INSERT INTO `sys_role_menu` VALUES (2, 1058);
INSERT INTO `sys_role_menu` VALUES (2, 1059);
INSERT INTO `sys_role_menu` VALUES (2, 1060);
INSERT INTO `sys_role_menu` VALUES (2, 1061);
INSERT INTO `sys_role_menu` VALUES (3, 2);
INSERT INTO `sys_role_menu` VALUES (3, 109);
INSERT INTO `sys_role_menu` VALUES (3, 110);
INSERT INTO `sys_role_menu` VALUES (3, 111);
INSERT INTO `sys_role_menu` VALUES (3, 112);
INSERT INTO `sys_role_menu` VALUES (3, 1047);
INSERT INTO `sys_role_menu` VALUES (3, 1048);
INSERT INTO `sys_role_menu` VALUES (3, 1049);
INSERT INTO `sys_role_menu` VALUES (3, 1050);
INSERT INTO `sys_role_menu` VALUES (3, 1051);
INSERT INTO `sys_role_menu` VALUES (3, 1052);
INSERT INTO `sys_role_menu` VALUES (3, 1053);
INSERT INTO `sys_role_menu` VALUES (3, 1054);
INSERT INTO `sys_role_menu` VALUES (3, 1055);
INSERT INTO `sys_role_menu` VALUES (3, 1056);
INSERT INTO `sys_role_menu` VALUES (4, 1);
INSERT INTO `sys_role_menu` VALUES (4, 100);
INSERT INTO `sys_role_menu` VALUES (4, 101);
INSERT INTO `sys_role_menu` VALUES (4, 103);
INSERT INTO `sys_role_menu` VALUES (4, 1000);
INSERT INTO `sys_role_menu` VALUES (4, 1001);
INSERT INTO `sys_role_menu` VALUES (4, 1002);
INSERT INTO `sys_role_menu` VALUES (4, 1003);
INSERT INTO `sys_role_menu` VALUES (4, 1004);
INSERT INTO `sys_role_menu` VALUES (4, 1005);
INSERT INTO `sys_role_menu` VALUES (4, 1006);
INSERT INTO `sys_role_menu` VALUES (4, 1007);
INSERT INTO `sys_role_menu` VALUES (4, 1008);
INSERT INTO `sys_role_menu` VALUES (4, 1009);
INSERT INTO `sys_role_menu` VALUES (4, 1010);
INSERT INTO `sys_role_menu` VALUES (4, 1011);
INSERT INTO `sys_role_menu` VALUES (4, 1016);
INSERT INTO `sys_role_menu` VALUES (4, 1017);
INSERT INTO `sys_role_menu` VALUES (4, 1018);
INSERT INTO `sys_role_menu` VALUES (4, 1019);
INSERT INTO `sys_role_menu` VALUES (4, 1064);
INSERT INTO `sys_role_menu` VALUES (4, 1065);
INSERT INTO `sys_role_menu` VALUES (4, 1066);
INSERT INTO `sys_role_menu` VALUES (4, 1068);
INSERT INTO `sys_role_menu` VALUES (4, 1070);
INSERT INTO `sys_role_menu` VALUES (4, 1071);
INSERT INTO `sys_role_menu` VALUES (4, 1072);
INSERT INTO `sys_role_menu` VALUES (4, 1073);
INSERT INTO `sys_role_menu` VALUES (4, 1074);
INSERT INTO `sys_role_menu` VALUES (4, 1075);
INSERT INTO `sys_role_menu` VALUES (4, 1076);
INSERT INTO `sys_role_menu` VALUES (4, 1077);
INSERT INTO `sys_role_menu` VALUES (4, 1078);
INSERT INTO `sys_role_menu` VALUES (4, 1079);
INSERT INTO `sys_role_menu` VALUES (4, 1080);
INSERT INTO `sys_role_menu` VALUES (4, 1081);
INSERT INTO `sys_role_menu` VALUES (4, 1082);
INSERT INTO `sys_role_menu` VALUES (5, 1);
INSERT INTO `sys_role_menu` VALUES (5, 100);
INSERT INTO `sys_role_menu` VALUES (5, 101);
INSERT INTO `sys_role_menu` VALUES (5, 103);
INSERT INTO `sys_role_menu` VALUES (5, 104);
INSERT INTO `sys_role_menu` VALUES (5, 1000);
INSERT INTO `sys_role_menu` VALUES (5, 1001);
INSERT INTO `sys_role_menu` VALUES (5, 1002);
INSERT INTO `sys_role_menu` VALUES (5, 1003);
INSERT INTO `sys_role_menu` VALUES (5, 1004);
INSERT INTO `sys_role_menu` VALUES (5, 1005);
INSERT INTO `sys_role_menu` VALUES (5, 1006);
INSERT INTO `sys_role_menu` VALUES (5, 1007);
INSERT INTO `sys_role_menu` VALUES (5, 1008);
INSERT INTO `sys_role_menu` VALUES (5, 1009);
INSERT INTO `sys_role_menu` VALUES (5, 1010);
INSERT INTO `sys_role_menu` VALUES (5, 1011);
INSERT INTO `sys_role_menu` VALUES (5, 1016);
INSERT INTO `sys_role_menu` VALUES (5, 1017);
INSERT INTO `sys_role_menu` VALUES (5, 1018);
INSERT INTO `sys_role_menu` VALUES (5, 1019);
INSERT INTO `sys_role_menu` VALUES (5, 1020);
INSERT INTO `sys_role_menu` VALUES (5, 1021);
INSERT INTO `sys_role_menu` VALUES (5, 1022);
INSERT INTO `sys_role_menu` VALUES (5, 1023);
INSERT INTO `sys_role_menu` VALUES (5, 1024);
INSERT INTO `sys_role_menu` VALUES (6, 1064);
INSERT INTO `sys_role_menu` VALUES (6, 1065);
INSERT INTO `sys_role_menu` VALUES (6, 1066);
INSERT INTO `sys_role_menu` VALUES (6, 1068);
INSERT INTO `sys_role_menu` VALUES (6, 1070);
INSERT INTO `sys_role_menu` VALUES (6, 1071);
INSERT INTO `sys_role_menu` VALUES (6, 1072);
INSERT INTO `sys_role_menu` VALUES (6, 1073);
INSERT INTO `sys_role_menu` VALUES (6, 1074);
INSERT INTO `sys_role_menu` VALUES (6, 1075);
INSERT INTO `sys_role_menu` VALUES (6, 1076);
INSERT INTO `sys_role_menu` VALUES (6, 1077);
INSERT INTO `sys_role_menu` VALUES (6, 1078);
INSERT INTO `sys_role_menu` VALUES (6, 1079);
INSERT INTO `sys_role_menu` VALUES (6, 1080);
INSERT INTO `sys_role_menu` VALUES (6, 1081);
INSERT INTO `sys_role_menu` VALUES (6, 1082);
INSERT INTO `sys_role_menu` VALUES (7, 1);
INSERT INTO `sys_role_menu` VALUES (7, 100);
INSERT INTO `sys_role_menu` VALUES (7, 1000);
INSERT INTO `sys_role_menu` VALUES (7, 1001);
INSERT INTO `sys_role_menu` VALUES (7, 1002);
INSERT INTO `sys_role_menu` VALUES (7, 1003);
INSERT INTO `sys_role_menu` VALUES (7, 1004);
INSERT INTO `sys_role_menu` VALUES (7, 1005);
INSERT INTO `sys_role_menu` VALUES (7, 1006);
INSERT INTO `sys_role_menu` VALUES (8, 1);
INSERT INTO `sys_role_menu` VALUES (8, 100);
INSERT INTO `sys_role_menu` VALUES (8, 1000);
INSERT INTO `sys_role_menu` VALUES (9, 1064);
INSERT INTO `sys_role_menu` VALUES (9, 1065);
INSERT INTO `sys_role_menu` VALUES (9, 1066);
INSERT INTO `sys_role_menu` VALUES (9, 1072);
INSERT INTO `sys_role_menu` VALUES (9, 1073);
INSERT INTO `sys_role_menu` VALUES (9, 1075);
INSERT INTO `sys_role_menu` VALUES (9, 1076);
INSERT INTO `sys_role_menu` VALUES (9, 1078);
COMMIT;

-- ----------------------------
-- Table structure for sys_table_sequence
-- ----------------------------
DROP TABLE IF EXISTS `sys_table_sequence`;
CREATE TABLE `sys_table_sequence` (
  `seq_name` varchar(100) NOT NULL COMMENT '序列名',
  `min_val` bigint(20) NOT NULL DEFAULT '1' COMMENT '最小值',
  `max_val` bigint(20) NOT NULL COMMENT '最大值',
  `current_val` bigint(20) NOT NULL DEFAULT '1' COMMENT '当前值',
  `increment_val` bigint(20) NOT NULL DEFAULT '1' COMMENT '增量值',
  `seq_cycle` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否循环',
  `seq_desc` varchar(256) DEFAULT '' COMMENT '序列描述',
  PRIMARY KEY (`seq_name`) USING BTREE,
  KEY `seq_name` (`seq_name`,`min_val`,`max_val`,`current_val`,`increment_val`,`seq_cycle`,`seq_desc`(191)) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='序列表';

-- ----------------------------
-- Records of sys_table_sequence
-- ----------------------------
BEGIN;
INSERT INTO `sys_table_sequence` VALUES ('check_opinion_id_seq', 8, 9999999999, 28, 1, 0, '审核回退原因编号序列');
INSERT INTO `sys_table_sequence` VALUES ('user_task_id_seq', 1, 9999999999, 22, 1, 0, '用户任务编号序列');
INSERT INTO `sys_table_sequence` VALUES ('user_task_picture_id_seq', 1, 9999999999, 23, 1, 0, '用户任务图片序列号');
INSERT INTO `sys_table_sequence` VALUES ('user_task_video_id_seq', 1, 9999999999, 7, 1, 0, '用户任务视频序列号');
COMMIT;

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
  `user_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '用户ID',
  `dept_id` bigint(20) DEFAULT NULL COMMENT '部门ID',
  `login_name` varchar(30) NOT NULL COMMENT '登录账号',
  `user_name` varchar(30) DEFAULT '' COMMENT '用户昵称',
  `user_type` varchar(2) DEFAULT '00' COMMENT '用户类型（00系统用户 01注册用户）',
  `email` varchar(50) DEFAULT '' COMMENT '用户邮箱',
  `phonenumber` varchar(11) DEFAULT '' COMMENT '手机号码',
  `sex` char(1) DEFAULT '0' COMMENT '用户性别（0男 1女 2未知）',
  `avatar` varchar(100) DEFAULT '' COMMENT '头像路径',
  `password` varchar(50) DEFAULT '' COMMENT '密码',
  `salt` varchar(20) DEFAULT '' COMMENT '盐加密',
  `status` char(1) DEFAULT '0' COMMENT '帐号状态（0正常 1停用）',
  `del_flag` char(1) DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `login_ip` varchar(50) DEFAULT '' COMMENT '最后登陆IP',
  `login_date` datetime DEFAULT NULL COMMENT '最后登陆时间',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`user_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='用户信息表';

-- ----------------------------
-- Records of sys_user
-- ----------------------------
BEGIN;
INSERT INTO `sys_user` VALUES (1, 103, 'admin', '起源', '00', 'eden@163.com', '17761628991', '1', '', '29c67a30398638269fe600f73a054934', '111111', '0', '0', '127.0.0.1', '2020-11-02 13:59:55', 'admin', '2018-03-16 11:33:00', 'ry', '2020-11-02 13:59:54', '管理员');
INSERT INTO `sys_user` VALUES (2, 105, '', '若依', '00', 'ry@qq.com', '15666666666', '1', '', '8e6d98b90472783cc73c17047ddccf36', '222222', '0', '2', '127.0.0.1', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '测试员');
INSERT INTO `sys_user` VALUES (3, 129, '1715925168', '焦洋', '00', '838855804@qq.com', '17761628992', '0', '', '52abc35ad3e24bd1207eceeb68661a03', '3c87af', '0', '0', '127.0.0.1', '2020-09-17 14:29:17', 'admin', '2020-08-29 21:33:26', 'admin', '2020-09-17 14:29:17', '');
COMMIT;

-- ----------------------------
-- Table structure for sys_user_online
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_online`;
CREATE TABLE `sys_user_online` (
  `sessionId` varchar(50) NOT NULL DEFAULT '' COMMENT '用户会话id',
  `login_name` varchar(50) DEFAULT '' COMMENT '登录账号',
  `dept_name` varchar(50) DEFAULT '' COMMENT '部门名称',
  `ipaddr` varchar(50) DEFAULT '' COMMENT '登录IP地址',
  `login_location` varchar(255) DEFAULT '' COMMENT '登录地点',
  `browser` varchar(50) DEFAULT '' COMMENT '浏览器类型',
  `os` varchar(50) DEFAULT '' COMMENT '操作系统',
  `status` varchar(10) DEFAULT '' COMMENT '在线状态on_line在线off_line离线',
  `start_timestamp` datetime DEFAULT NULL COMMENT 'session创建时间',
  `last_access_time` datetime DEFAULT NULL COMMENT 'session最后访问时间',
  `expire_time` int(5) DEFAULT '0' COMMENT '超时时间，单位为分钟',
  PRIMARY KEY (`sessionId`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='在线用户记录';

-- ----------------------------
-- Table structure for sys_user_post
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_post`;
CREATE TABLE `sys_user_post` (
  `user_id` bigint(20) NOT NULL COMMENT '用户ID',
  `post_id` bigint(20) NOT NULL COMMENT '岗位ID',
  PRIMARY KEY (`user_id`,`post_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='用户与岗位关联表';

-- ----------------------------
-- Records of sys_user_post
-- ----------------------------
BEGIN;
INSERT INTO `sys_user_post` VALUES (1, 1);
INSERT INTO `sys_user_post` VALUES (2, 2);
COMMIT;

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role` (
  `user_id` bigint(20) NOT NULL COMMENT '用户ID',
  `role_id` bigint(20) NOT NULL COMMENT '角色ID',
  PRIMARY KEY (`user_id`,`role_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='用户和角色关联表';

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
BEGIN;
INSERT INTO `sys_user_role` VALUES (1, 1);
INSERT INTO `sys_user_role` VALUES (2, 2);
INSERT INTO `sys_user_role` VALUES (3, 4);
COMMIT;

-- ----------------------------
-- Table structure for task_picture
-- ----------------------------
DROP TABLE IF EXISTS `task_picture`;
CREATE TABLE `task_picture` (
  `picture_id` varchar(64) NOT NULL COMMENT '图片名称',
  `task_cpid` varchar(64) NOT NULL COMMENT '任务Id',
  `picture_url` varchar(255) DEFAULT '' COMMENT '图片地址',
  `picture_status` char(1) DEFAULT '' COMMENT '图片状态',
  `picture_name` varchar(128) DEFAULT '' COMMENT '图片名称',
  `create_by` varchar(30) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `update_by` varchar(30) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`picture_id`,`task_cpid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of task_picture
-- ----------------------------
BEGIN;
INSERT INTO `task_picture` VALUES ('P0000000018', 'T5ad41599812393783', '/profile/upload/2020/09/11/03cd3e47-a8f8-400c-b10b-4cfed61a5638.jpg', '0', '03cd3e47-a8f8-400c-b10b-4cfed61a5638', 'admin', '2020-09-11 16:20:48', NULL, NULL);
INSERT INTO `task_picture` VALUES ('P0000000019', 'T5ad41599812393783', '/profile/upload/2020/09/11/cca84c90-d927-487c-9e92-90c1e8f08ea1.jpg', '0', 'cca84c90-d927-487c-9e92-90c1e8f08ea1', 'admin', '2020-09-11 16:20:48', NULL, NULL);
INSERT INTO `task_picture` VALUES ('P0000000020', 'Taf381600065254499', '/profile/upload/2020/09/14/4880b356-00a2-46f2-8aa8-cb86965461b2.jpg', '0', '4880b356-00a2-46f2-8aa8-cb86965461b2', 'admin', '2020-09-14 14:35:38', NULL, NULL);
INSERT INTO `task_picture` VALUES ('P0000000021', 'Taf381600065254499', '/profile/upload/2020/09/14/8a4e24d1-d7fa-42fb-a91b-67d2288fa242.jpg', '0', '8a4e24d1-d7fa-42fb-a91b-67d2288fa242', 'admin', '2020-09-14 14:35:38', NULL, NULL);
INSERT INTO `task_picture` VALUES ('P0000000022', 'Taf381600065254499', '/profile/upload/2020/09/14/1c1107de-6d98-4f4b-81bd-68f2da19d440.jpg', '0', '1c1107de-6d98-4f4b-81bd-68f2da19d440', 'admin', '2020-09-14 14:35:38', NULL, NULL);
COMMIT;

-- ----------------------------
-- Table structure for task_video
-- ----------------------------
DROP TABLE IF EXISTS `task_video`;
CREATE TABLE `task_video` (
  `video_id` varchar(64) NOT NULL COMMENT '视频Id',
  `video_url` varchar(128) DEFAULT '' COMMENT '视频上传路径',
  `video_name` varchar(128) DEFAULT '' COMMENT '视频名称',
  `video_status` char(1) DEFAULT '' COMMENT '视频状态',
  `task_cvid` varchar(64) DEFAULT '' COMMENT '任务Id',
  `create_by` varchar(30) DEFAULT '' COMMENT '创建时间',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(30) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`video_id`) USING BTREE,
  KEY `task_cid` (`task_cvid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='用户发布视频表';

-- ----------------------------
-- Records of task_video
-- ----------------------------
BEGIN;
INSERT INTO `task_video` VALUES ('V0000000005', '/profile/upload/2020/09/11/9d45a3d1-9259-430c-90d5-2d5819dc0ae5.mp4', '9d45a3d1-9259-430c-90d5-2d5819dc0ae5', '0', 'T5ad41599812393783', 'admin', '2020-09-11 16:20:48', '', NULL);
INSERT INTO `task_video` VALUES ('V0000000006', '/profile/upload/2020/09/14/e296af47-f687-4ef6-bfbd-6e11cdde13d2.mp4', 'e296af47-f687-4ef6-bfbd-6e11cdde13d2', '0', 'Taf381600065254499', 'admin', '2020-09-14 14:35:38', '', NULL);
COMMIT;

-- ----------------------------
-- Table structure for user_task
-- ----------------------------
DROP TABLE IF EXISTS `user_task`;
CREATE TABLE `user_task` (
  `task_id` varchar(64) NOT NULL COMMENT '任务id',
  `task_name` varchar(64) DEFAULT '' COMMENT '任务名称',
  `task_description` text COMMENT '任务描述',
  `task_status` char(1) DEFAULT '' COMMENT '任务状态 0: 待审核 1：审核通过 2：审核不通过',
  `money` varchar(64) DEFAULT '' COMMENT '赏金',
  `phonenumber` varchar(11) DEFAULT '' COMMENT '电话号码',
  `address` varchar(128) DEFAULT '' COMMENT '地址',
  `check_id` varchar(30) DEFAULT '' COMMENT '检查者Id',
  `check_opinion` varchar(255) DEFAULT '' COMMENT '审核不通过的意见',
  `check_name` varchar(30) DEFAULT '' COMMENT '审核人',
  `task_type` char(1) DEFAULT '' COMMENT '任务类型 0：悬赏、1....',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`task_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='用户发布任务表';

-- ----------------------------
-- Records of user_task
-- ----------------------------
BEGIN;
INSERT INTO `user_task` VALUES ('T5ad41599812393783', '测试01', '用于测试', '1', '20', '17761628999', '河南省南阳市', '1', '', 'admin', '', 'admin', '2020-09-11 16:20:52', '', '2020-09-14 14:24:21');
INSERT INTO `user_task` VALUES ('Taf381600065254499', '测试02', '用于测试02', '2', '32', '17761628997', '南阳理工学院', '1', 'C00000000001', 'admin', '', 'admin', '2020-09-14 14:35:40', 'admin', '2020-09-14 14:56:13');
COMMIT;

-- ----------------------------
-- Table structure for user_task_picture
-- ----------------------------
DROP TABLE IF EXISTS `user_task_picture`;
CREATE TABLE `user_task_picture` (
  `task_id` varchar(64) NOT NULL DEFAULT '' COMMENT '任务 id',
  `picture_id` varchar(64) NOT NULL DEFAULT '' COMMENT '图片Id',
  PRIMARY KEY (`picture_id`,`task_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='任务图片联表';

-- ----------------------------
-- Table structure for user_task_video
-- ----------------------------
DROP TABLE IF EXISTS `user_task_video`;
CREATE TABLE `user_task_video` (
  `task_id` varchar(255) NOT NULL COMMENT '任务Id',
  `video_id` varchar(255) NOT NULL COMMENT '视频Id',
  PRIMARY KEY (`task_id`,`video_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='任务视频关联表';

-- ----------------------------
-- Function structure for next_lpad_seq_val
-- ----------------------------
DROP FUNCTION IF EXISTS `next_lpad_seq_val`;
delimiter ;;
CREATE FUNCTION `edendb`.`next_lpad_seq_val`(name varchar(64), len int)
 RETURNS varchar(64) CHARSET utf8mb4
begin
    declare val varchar(64);
    set val = lpad(next_seq_val(name), len, 0);
    return val;
end
;;
delimiter ;

-- ----------------------------
-- Function structure for next_prefix_seq_val
-- ----------------------------
DROP FUNCTION IF EXISTS `next_prefix_seq_val`;
delimiter ;;
CREATE FUNCTION `edendb`.`next_prefix_seq_val`(name varchar(64), len int, prefix varchar(5))
 RETURNS varchar(64) CHARSET utf8mb4
begin
    declare val varchar(64);
    set val = concat(prefix, next_lpad_seq_val(name, len));
    return val;
end
;;
delimiter ;

-- ----------------------------
-- Function structure for next_seq_val
-- ----------------------------
DROP FUNCTION IF EXISTS `next_seq_val`;
delimiter ;;
CREATE FUNCTION `edendb`.`next_seq_val`(name varchar(100))
 RETURNS bigint(20)
begin
    declare _current_val bigint(20);
    select current_val into _current_val from sys_table_sequence where seq_name = name for update;
    if _current_val is not null then
        update sys_table_sequence
        set current_val = if ((current_val + increment_val) > max_val, if (seq_cycle = true, min_val, max_val), current_val + increment_val)
        where seq_name = name;
    end if;
    return _current_val;
end
;;
delimiter ;

SET FOREIGN_KEY_CHECKS = 1;
