## 平台简介
该项目基于 若依框架开发的一个校园悬赏互助平台项目。
## 内置功能

1.  用户管理：用户是系统操作者，该功能主要完成系统用户配置。
2.  部门管理：配置系统组织机构（公司、部门、小组），树结构展现支持数据权限。
3.  岗位管理：配置系统用户所属担任职务。
4.  菜单管理：配置系统菜单，操作权限，按钮权限标识等。
5.  角色管理：角色菜单权限分配、设置角色按机构进行数据范围权限划分。
6.  字典管理：对系统中经常使用的一些较为固定的数据进行维护。
7.  参数管理：对系统动态配置常用参数。
8.  通知公告：系统通知公告信息发布维护。
9.  操作日志：系统正常操作日志记录和查询；系统异常信息日志记录和查询。
10. 登录日志：系统登录日志记录查询包含登录异常。
11. 在线用户：当前系统中活跃用户状态监控。
12. 定时任务：在线（添加、修改、删除)任务调度包含执行结果日志。
13. 代码生成：前后端代码的生成（java、html、xml、sql）支持CRUD下载 。
14. 系统接口：根据业务代码自动生成相关的api接口文档。
15. 服务监控：监视当前系统CPU、内存、磁盘、堆栈等相关信息。
16. 在线构建器：拖动表单元素生成相应的HTML代码。
17. 连接池监视：监视当前系统数据库连接池状态，可进行分析SQL找出系统性能瓶颈。
-----------------------------------------------------------------------------
以上是若依框架所拥有的功能
-----------------------------------------------------------------------------
18. 校园用户管理：对校园学生用户进行管理。
19. 校园任务审核：对校内学校及学生用户发布的功能进行审核。
20. 校园任务管理：对校内发布的任务进行统一管理。
21. 搜索历史管理：微信小程序客户端的用户搜索的搜索历史进行管理。
22. 任务类型管理：对用户发布的任务类型进行归类管理。

........继续迭代
# eden

#### 介绍
我的梦想起源的地方

#### 软件架构
1、系统环境

Java EE 8
Servlet 3.0
Apache Maven 3
2、主框架

Spring Boot 2.2.x
Spring Framework 5.2.x
Apache Shiro 1.7
3、持久层

Apache MyBatis 3.5.x
Hibernate Validation 6.0.x
Alibaba Druid 1.2.x
4、视图层

Bootstrap 3.3.7
Thymeleaf 3.0.x


#### 安装教程

#准备工作
JDK >= 1.8 (推荐1.8版本)
Mysql >= 5.7.0 (推荐5.7版本)
Maven >= 3.0
# 运行系统
1、前往Gitee下载页面(https://gitee.com/JY1218/eden.git (opens new window))下载解压到工作目录
2、导入到Eclipse，菜单 File -> Import，然后选择 Maven -> Existing Maven Projects，点击 Next> 按钮，选择工作目录，然后点击 Finish 按钮，即可成功导入。
Eclipse会自动加载Maven依赖包，初次加载会比较慢（根据自身网络情况而定）
3、创建数据库ry并导入数据脚本ry_2021xxxx.sql，quartz.sql
4、打开项目运行com.eden.RuoYiApplication.java   
5、打开浏览器，输入：(http://localhost:80 (opens new window)) （默认账户/密码 admin/admin123）
若能正确展示登录页面，并能成功登录，菜单及页面展示正常，则表明环境搭建成功

建议使用Git克隆，因为克隆的方式可以和RuoYi随时保持更新同步。使用Git命令克隆

git clone https://gitee.com/JY1218/eden.git
# 必要配置
修改数据库连接，编辑resources目录下的application-druid.yml
# 数据源配置
spring:
    datasource:
        type: com.alibaba.druid.pool.DruidDataSource
        driverClassName: com.mysql.cj.jdbc.Driver
        druid:
            # 主库数据源
            master:
                url: 数据库地址
                username: 数据库账号
                password: 数据库密码
修改服务器配置，编辑resources目录下的application.yml
# 开发环境配置
server:
  # 服务器的HTTP端口，默认为80
  port: 端口
  servlet:
    # 应用的访问路径
    context-path: /应用路径
# 部署系统
打包工程文件
在eden项目的bin目录下执行package.bat打包Web工程，生成war/jar包文件。
然后会在项目下生成target文件夹包含war或jar

提示

多模块版本会生成在eden/eden-admin模块下target文件夹

#部署工程文件
1、jar部署方式
使用命令行执行：java –jar eden.jar 或者执行脚本：eden/bin/run.bat

2、war部署方式
eden/pom.xml中的packaging修改为war，放入tomcat服务器webapps

   <packaging>war</packaging>
提示

多模块版本在eden/eden-admin模块下修改pom.xml

SpringBoot去除内嵌Tomcat（PS：此步骤不重要，因为不排除也能在容器中部署war）
<!-- 多模块排除内置tomcat -->
<dependency>
	<groupId>org.springframework.boot</groupId>
	<artifactId>spring-boot-starter-web</artifactId>
	<exclusions>
		<exclusion>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-tomcat</artifactId>
		</exclusion>
	</exclusions>
</dependency>
		
<!-- 单应用排除内置tomcat -->		
<exclusions>
	<exclusion>
		<artifactId>spring-boot-starter-tomcat</artifactId>
		<groupId>org.springframework.boot</groupId>
	</exclusion>
</exclusions>
# 常见问题
如果使用Mac需要修改application.yml文件路径profile
如果使用Linux 提示表不存在，设置大小写敏感配置在/etc/my.cnf添加lower_case_table_names=1，重启MYSQL服务
如果提示当前权限不足，无法写入文件请检查application.yml中的profile路径或logback.xml中的log.path路径是否有可读可写操作权限

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)

