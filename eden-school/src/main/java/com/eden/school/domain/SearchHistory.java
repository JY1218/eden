package com.eden.school.domain;

import com.eden.common.annotation.Excel;
import com.eden.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 用户搜索历史记录对象 search_history
 *
 * @author jiaoyang
 * @date 2020-12-23
 */
public class SearchHistory extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 搜索历史主键 */
    private Long searchId;

    /** 学号 */
    private String loginName;

    /** 搜索内容 */
    @Excel(name = "搜索内容")
    private String content;

    public void setSearchId(Long searchId)
    {
        this.searchId = searchId;
    }

    public Long getSearchId()
    {
        return searchId;
    }
    public void setLoginName(String loginName)
    {
        this.loginName = loginName;
    }

    public String getLoginName()
    {
        return loginName;
    }
    public void setContent(String content)
    {
        this.content = content;
    }

    public String getContent()
    {
        return content;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("searchId", getSearchId())
                .append("loginName", getLoginName())
                .append("content", getContent())
                .append("createTime", getCreateTime())
                .append("createBy", getCreateBy())
                .append("updateTime", getUpdateTime())
                .append("updateBy", getUpdateBy())
                .toString();
    }
}