package com.eden.school.mapper;

import com.eden.school.domain.SchoolUser;

import java.util.List;

/**
 * 学校用户信息Mapper接口
 *
 * @author jiaoyang
 * @date 2020-09-01
 */
public interface SchoolUserMapper {
    /**
     * 查询学校用户信息
     *
     * @param userId 学校用户信息ID
     * @return 学校用户信息
     */
    public SchoolUser selectSchoolUserById(Long userId);

    /**
     * 查询学校用户信息列表
     *
     * @param schoolUser 学校用户信息
     * @return 学校用户信息集合
     */
    public List<SchoolUser> selectSchoolUserList(SchoolUser schoolUser);

    /**
     * 新增学校用户信息
     *
     * @param schoolUser 学校用户信息
     * @return 结果
     */
    public int insertSchoolUser(SchoolUser schoolUser);

    /**
     * 修改学校用户信息
     *
     * @param schoolUser 学校用户信息
     * @return 结果
     */
    public int updateSchoolUser(SchoolUser schoolUser);

    /**
     * 删除学校用户信息
     *
     * @param userId 学校用户信息ID
     * @return 结果
     */
    public int deleteSchoolUserById(Long userId);

    /**
     * 批量删除学校用户信息
     *
     * @param userIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteSchoolUserByIds(String[] userIds);

    /**
     * 通过用户名查询用户
     *
     * @param userName 用户名
     * @return 用户对象信息
     */
    public SchoolUser selectSchoolUserByLoginName(String userName);

    /**
     * 通过手机号码查询用户
     *
     * @param phoneNumber 手机号码
     * @return 用户对象信息
     */
    public SchoolUser selectSchoolUserByPhoneNumber(String phoneNumber);
    /**
     * 通过邮箱查询用户
     *
     * @param email 邮箱
     * @return 用户对象信息
     */
    public SchoolUser selectSchoolUserByEmail(String email);
    /**
     * 校验用户名称是否唯一
     *
     * @param loginName 登录名称
     * @return 结果
     */
    public int checkSchoolLoginNameUnique(String loginName);

    /**
     * 校验手机号码是否唯一
     *
     * @param phonenumber 手机号码
     * @return 结果
     */
    public SchoolUser checkSchoolPhoneUnique(String phonenumber);

    /**
     * 校验email是否唯一
     *
     * @param email 用户邮箱
     * @return 结果
     */
    public SchoolUser checkSchoolEmailUnique(String email);
}
