package com.eden.school.mapper;

import com.eden.school.domain.SchoolUserOnline;

import java.util.List;

/**
 * 在线学生用户记录Mapper接口
 *
 * @author jiaoyang
 * @date 2020-12-24
 */
public interface SchoolUserOnlineMapper {
    /**
     * 查询在线学生用户记录
     *
     * @param sessionId 在线学生用户记录ID
     * @return 在线学生用户记录
     */
    public SchoolUserOnline selectSchoolUserOnlineById(String sessionId);

    /**
     * 查询在线学生用户记录列表
     *
     * @param schoolUserOnline 在线学生用户记录
     * @return 在线学生用户记录集合
     */
    public List<SchoolUserOnline> selectSchoolUserOnlineList(SchoolUserOnline schoolUserOnline);

    /**
     * 新增在线学生用户记录
     *
     * @param schoolUserOnline 在线学生用户记录
     * @return 结果
     */
    public int insertSchoolUserOnline(SchoolUserOnline schoolUserOnline);

    /**
     * 修改在线学生用户记录
     *
     * @param schoolUserOnline 在线学生用户记录
     * @return 结果
     */
    public int updateSchoolUserOnline(SchoolUserOnline schoolUserOnline);

    /**
     * 删除在线学生用户记录
     *
     * @param sessionId 在线学生用户记录ID
     * @return 结果
     */
    public int deleteSchoolUserOnlineById(String sessionId);

    /**
     * 批量删除在线学生用户记录
     *
     * @param sessionIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteSchoolUserOnlineByIds(String[] sessionIds);

    /**
     * 保存会话信息
     *
     * @param online 会话信息
     * @return 结果
     */
    void saveOnline(SchoolUserOnline online);
}