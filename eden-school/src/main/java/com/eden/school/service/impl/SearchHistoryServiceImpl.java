package com.eden.school.service.impl;

import java.util.List;
import com.eden.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.eden.school.mapper.SearchHistoryMapper;
import com.eden.school.domain.SearchHistory;
import com.eden.school.service.ISearchHistoryService;
import com.eden.common.core.text.Convert;

/**
 * 用户搜索历史记录Service业务层处理
 *
 * @author jiaoyang
 * @date 2020-12-23
 */
@Service
public class SearchHistoryServiceImpl implements ISearchHistoryService
{
    @Autowired
    private SearchHistoryMapper searchHistoryMapper;

    /**
     * 查询用户搜索历史记录
     *
     * @param searchId 用户搜索历史记录ID
     * @return 用户搜索历史记录
     */
    @Override
    public SearchHistory selectSearchHistoryById(Long searchId)
    {
        return searchHistoryMapper.selectSearchHistoryById(searchId);
    }

    /**
     * 查询用户搜索历史记录列表
     *
     * @param searchHistory 用户搜索历史记录
     * @return 用户搜索历史记录
     */
    @Override
    public List<SearchHistory> selectSearchHistoryList(SearchHistory searchHistory)
    {
        return searchHistoryMapper.selectSearchHistoryList(searchHistory);
    }

    /**
     * 新增用户搜索历史记录
     *
     * @param searchHistory 用户搜索历史记录
     * @return 结果
     */
    @Override
    public int insertSearchHistory(SearchHistory searchHistory)
    {
        searchHistory.setCreateTime(DateUtils.getNowDate());
        return searchHistoryMapper.insertSearchHistory(searchHistory);
    }

    /**
     * 修改用户搜索历史记录
     *
     * @param searchHistory 用户搜索历史记录
     * @return 结果
     */
    @Override
    public int updateSearchHistory(SearchHistory searchHistory)
    {
        searchHistory.setUpdateTime(DateUtils.getNowDate());
        return searchHistoryMapper.updateSearchHistory(searchHistory);
    }

    /**
     * 删除用户搜索历史记录对象
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteSearchHistoryByIds(String ids)
    {
        return searchHistoryMapper.deleteSearchHistoryByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除用户搜索历史记录信息
     *
     * @param searchId 用户搜索历史记录ID
     * @return 结果
     */
    @Override
    public int deleteSearchHistoryById(Long searchId)
    {
        return searchHistoryMapper.deleteSearchHistoryById(searchId);
    }
}