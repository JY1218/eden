package com.eden.school.service;

import com.eden.school.domain.SchoolUser;

import java.util.List;

/**
 * 学校用户信息Service接口
 *
 * @author jiaoyang
 * @date 2020-09-01
 */
public interface ISchoolUserService {
    /**
     * 查询学校用户信息
     *
     * @param userId 学校用户信息ID
     * @return 学校用户信息
     */
    public SchoolUser selectSchoolUserById(Long userId);

    /**
     * 查询学校用户信息列表
     *
     * @param schoolUser 学校用户信息
     * @return 学校用户信息集合
     */
    public List<SchoolUser> selectSchoolUserList(SchoolUser schoolUser);

    /**
     * 新增学校用户信息
     *
     * @param schoolUser 学校用户信息
     * @return 结果
     */
    public int insertSchoolUser(SchoolUser schoolUser);

    /**
     * 修改学校用户信息
     *
     * @param schoolUser 学校用户信息
     * @return 结果
     */
    public int updateSchoolUser(SchoolUser schoolUser);

    /**
     * 批量删除学校用户信息
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteSchoolUserByIds(String ids);

    /**
     * 删除学校用户信息信息
     *
     * @param userId 学校用户信息ID
     * @return 结果
     */
    public int deleteSchoolUserById(Long userId);

    String checkSchoolLoginNameUnique(String loginName);

    String checkSchoolPhoneUnique(SchoolUser schoolUser);

    String checkSchoolEmailUnique(SchoolUser schoolUser);

    int resetSchoolUserPwd(SchoolUser user);

    /**
     * 按照登录名查找人员信息
     *
     * @param createBy
     * @return
     */
    public SchoolUser selectSchoolUserByLoginName(String createBy);
}
