package com.eden.school.service;

import java.util.List;
import com.eden.school.domain.SearchHistory;

/**
 * 用户搜索历史记录Service接口
 *
 * @author jiaoyang
 * @date 2020-12-23
 */
public interface ISearchHistoryService
{
    /**
     * 查询用户搜索历史记录
     *
     * @param searchId 用户搜索历史记录ID
     * @return 用户搜索历史记录
     */
    public SearchHistory selectSearchHistoryById(Long searchId);

    /**
     * 查询用户搜索历史记录列表
     *
     * @param searchHistory 用户搜索历史记录
     * @return 用户搜索历史记录集合
     */
    public List<SearchHistory> selectSearchHistoryList(SearchHistory searchHistory);

    /**
     * 新增用户搜索历史记录
     *
     * @param searchHistory 用户搜索历史记录
     * @return 结果
     */
    public int insertSearchHistory(SearchHistory searchHistory);

    /**
     * 修改用户搜索历史记录
     *
     * @param searchHistory 用户搜索历史记录
     * @return 结果
     */
    public int updateSearchHistory(SearchHistory searchHistory);

    /**
     * 批量删除用户搜索历史记录
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteSearchHistoryByIds(String ids);

    /**
     * 删除用户搜索历史记录信息
     *
     * @param searchId 用户搜索历史记录ID
     * @return 结果
     */
    public int deleteSearchHistoryById(Long searchId);
}