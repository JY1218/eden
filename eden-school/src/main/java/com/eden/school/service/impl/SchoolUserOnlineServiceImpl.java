package com.eden.school.service.impl;

import com.eden.common.constant.ShiroConstants;
import com.eden.common.core.text.Convert;
import com.eden.common.utils.StringUtils;
import com.eden.school.domain.SchoolUserOnline;
import com.eden.school.mapper.SchoolUserOnlineMapper;
import com.eden.school.service.ISchoolUserOnlineService;
import org.apache.shiro.cache.Cache;
import org.apache.shiro.cache.ehcache.EhCacheManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.Deque;
import java.util.List;

/**
 * 在线学生用户记录Service业务层处理
 *
 * @author jiaoyang
 * @date 2020-12-24
 */
@Service
public class SchoolUserOnlineServiceImpl implements ISchoolUserOnlineService {
    @Autowired
    private SchoolUserOnlineMapper schoolUserOnlineMapper;
    @Autowired
    private EhCacheManager ehCacheManager;

    /**
     * 查询在线学生用户记录
     *
     * @param sessionId 在线学生用户记录ID
     * @return 在线学生用户记录
     */
    @Override
    public SchoolUserOnline selectSchoolUserOnlineById(String sessionId) {
        return schoolUserOnlineMapper.selectSchoolUserOnlineById(sessionId);
    }

    @Override
    public void saveOnline(SchoolUserOnline online) {
        schoolUserOnlineMapper.saveOnline(online);
    }

    /**
     * 查询在线学生用户记录列表
     *
     * @param schoolUserOnline 在线学生用户记录
     * @return 在线学生用户记录
     */
    @Override
    public List<SchoolUserOnline> selectSchoolUserOnlineList(SchoolUserOnline schoolUserOnline) {
        return schoolUserOnlineMapper.selectSchoolUserOnlineList(schoolUserOnline);
    }

    /**
     * 新增在线学生用户记录
     *
     * @param schoolUserOnline 在线学生用户记录
     * @return 结果
     */
    @Override
    public int insertSchoolUserOnline(SchoolUserOnline schoolUserOnline) {
        return schoolUserOnlineMapper.insertSchoolUserOnline(schoolUserOnline);
    }

    /**
     * 修改在线学生用户记录
     *
     * @param schoolUserOnline 在线学生用户记录
     * @return 结果
     */
    @Override
    public int updateSchoolUserOnline(SchoolUserOnline schoolUserOnline) {
        return schoolUserOnlineMapper.updateSchoolUserOnline(schoolUserOnline);
    }

    /**
     * 删除在线学生用户记录对象
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteSchoolUserOnlineByIds(String ids) {
        return schoolUserOnlineMapper.deleteSchoolUserOnlineByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除在线学生用户记录信息
     *
     * @param sessionId 在线学生用户记录ID
     * @return 结果
     */
    @Override
    public int deleteSchoolUserOnlineById(String sessionId) {
        return schoolUserOnlineMapper.deleteSchoolUserOnlineById(sessionId);
    }

    /**
     * 强退用户
     *
     * @param sessionId 会话ID
     */
    @Override
    public void forceLogout(String sessionId) {
        schoolUserOnlineMapper.deleteSchoolUserOnlineById(sessionId);
    }

    /**
     * 清理用户缓存
     *
     * @param loginName 登录名称
     * @param sessionId 会话ID
     */
    @Override
    public void removeUserCache(String loginName, String sessionId) {
        Cache<String, Deque<Serializable>> cache = ehCacheManager.getCache(ShiroConstants.SYS_USERCACHE);
        Deque<Serializable> deque = cache.get(loginName);
        if (StringUtils.isEmpty(deque) || deque.size() == 0) {
            return;
        }
        deque.remove(sessionId);
    }
}