package com.eden.school.service;

import com.eden.school.domain.SchoolUserOnline;

import java.util.List;

/**
 * 在线学生用户记录Service接口
 *
 * @author jiaoyang
 * @date 2020-12-24
 */
public interface ISchoolUserOnlineService {
    /**
     * 查询在线学生用户记录
     *
     * @param sessionId 在线学生用户记录ID
     * @return 在线学生用户记录
     */
    public SchoolUserOnline selectSchoolUserOnlineById(String sessionId);

    /**
     * 保存会话信息
     *
     * @param online 会话信息
     */
    public void saveOnline(SchoolUserOnline online);

    /**
     * 查询在线学生用户记录列表
     *
     * @param schoolUserOnline 在线学生用户记录
     * @return 在线学生用户记录集合
     */
    public List<SchoolUserOnline> selectSchoolUserOnlineList(SchoolUserOnline schoolUserOnline);

    /**
     * 新增在线学生用户记录
     *
     * @param schoolUserOnline 在线学生用户记录
     * @return 结果
     */
    public int insertSchoolUserOnline(SchoolUserOnline schoolUserOnline);

    /**
     * 修改在线学生用户记录
     *
     * @param schoolUserOnline 在线学生用户记录
     * @return 结果
     */
    public int updateSchoolUserOnline(SchoolUserOnline schoolUserOnline);

    /**
     * 批量删除在线学生用户记录
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteSchoolUserOnlineByIds(String ids);

    /**
     * 删除在线学生用户记录信息
     *
     * @param sessionId 在线学生用户记录ID
     * @return 结果
     */
    public int deleteSchoolUserOnlineById(String sessionId);

    /**
     * 强退用户
     *
     * @param sessionId 会话ID
     */
    public void forceLogout(String sessionId);

    /**
     * 清理用户缓存
     *
     * @param loginName 登录名称
     * @param sessionId 会话ID
     */
    public void removeUserCache(String loginName, String sessionId);
}