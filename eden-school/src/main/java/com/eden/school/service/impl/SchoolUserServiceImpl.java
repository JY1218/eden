package com.eden.school.service.impl;

import com.eden.common.constant.UserConstants;
import com.eden.common.core.text.Convert;
import com.eden.common.utils.DateUtils;
import com.eden.common.utils.StringUtils;
import com.eden.school.domain.SchoolUser;
import com.eden.school.mapper.SchoolUserMapper;
import com.eden.school.service.ISchoolUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 学校用户信息Service业务层处理
 *
 * @author jiaoyang
 * @date 2020-09-01
 */
@Service
public class SchoolUserServiceImpl implements ISchoolUserService {
    @Autowired
    private SchoolUserMapper schoolUserMapper;

    /**
     * 查询学校用户信息
     *
     * @param userId 学校用户信息ID
     * @return 学校用户信息
     */
    @Override
    public SchoolUser selectSchoolUserById(Long userId) {
        return schoolUserMapper.selectSchoolUserById(userId);
    }

    /**
     * 查询学校用户信息列表
     *
     * @param schoolUser 学校用户信息
     * @return 学校用户信息
     */
    @Override
    public List<SchoolUser> selectSchoolUserList(SchoolUser schoolUser) {
        return schoolUserMapper.selectSchoolUserList(schoolUser);
    }

    /**
     * 新增学校用户信息
     *
     * @param schoolUser 学校用户信息
     * @return 结果
     */
    @Override
    public int insertSchoolUser(SchoolUser schoolUser) {
        schoolUser.setUserType(UserConstants.REGISTER_USER_TYPE);
        schoolUser.setCreateTime(DateUtils.getNowDate());
        return schoolUserMapper.insertSchoolUser(schoolUser);
    }

    /**
     * 修改学校用户信息
     *
     * @param schoolUser 学校用户信息
     * @return 结果
     */
    @Override
    public int updateSchoolUser(SchoolUser schoolUser) {
        schoolUser.setUpdateTime(DateUtils.getNowDate());
        return schoolUserMapper.updateSchoolUser(schoolUser);
    }

    /**
     * 删除学校用户信息对象
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteSchoolUserByIds(String ids) {
        return schoolUserMapper.deleteSchoolUserByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除学校用户信息信息
     *
     * @param userId 学校用户信息ID
     * @return 结果
     */
    @Override
    public int deleteSchoolUserById(Long userId) {
        return schoolUserMapper.deleteSchoolUserById(userId);
    }

    /**
     * 校验登录名称是否唯一
     *
     * @param loginName 用户名
     * @return
     */
    @Override
    public String checkSchoolLoginNameUnique(String loginName) {
        int count = schoolUserMapper.checkSchoolLoginNameUnique(loginName);
        if (count > 0) {
            return UserConstants.USER_NAME_NOT_UNIQUE;
        }
        return UserConstants.USER_NAME_UNIQUE;
    }

    /**
     * 校验手机号码是否唯一
     *
     * @param schoolUser 用户信息
     * @return
     */
    @Override
    public String checkSchoolPhoneUnique(SchoolUser schoolUser) {
        Long userId = StringUtils.isNull(schoolUser.getUserId()) ? -1L : schoolUser.getUserId();
        SchoolUser info = schoolUserMapper.checkSchoolPhoneUnique(schoolUser.getPhonenumber());
        if (StringUtils.isNotNull(info) && info.getUserId().longValue() != userId.longValue()) {
            return UserConstants.USER_PHONE_NOT_UNIQUE;
        }
        return UserConstants.USER_PHONE_UNIQUE;
    }

    /**
     * 校验email是否唯一
     *
     * @param schoolUser 用户信息
     * @return
     */
    @Override
    public String checkSchoolEmailUnique(SchoolUser schoolUser) {
        Long userId = StringUtils.isNull(schoolUser.getUserId()) ? -1L : schoolUser.getUserId();
        SchoolUser info = schoolUserMapper.checkSchoolEmailUnique(schoolUser.getEmail());
        if (StringUtils.isNotNull(info) && info.getUserId().longValue() != userId.longValue()) {
            return UserConstants.USER_EMAIL_NOT_UNIQUE;
        }
        return UserConstants.USER_EMAIL_UNIQUE;
    }

    @Override
    public int resetSchoolUserPwd(SchoolUser user) {
        return updateSchoolUser(user);
    }

    @Override
    public SchoolUser selectSchoolUserByLoginName(String loginName) {
        return schoolUserMapper.selectSchoolUserByLoginName(loginName);
    }
}
