package com.eden.web.controller.school;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.eden.common.annotation.Log;
import com.eden.common.enums.BusinessType;
import com.eden.school.domain.SearchHistory;
import com.eden.school.service.ISearchHistoryService;
import com.eden.common.core.controller.BaseController;
import com.eden.common.core.domain.AjaxResult;
import com.eden.common.utils.poi.ExcelUtil;
import com.eden.common.core.page.TableDataInfo;

/**
 * 用户搜索历史记录Controller
 *
 * @author jiaoyang
 * @date 2020-12-23
 */
@Controller
@RequestMapping("/school/searchHistory")
public class SearchHistoryController extends BaseController
{
    private String prefix = "school/searchHistory";

    @Autowired
    private ISearchHistoryService searchHistoryService;

    @RequiresPermissions("school:searchHistory:view")
    @GetMapping()
    public String searchHistory()
    {
        return prefix + "/searchHistory";
    }

    /**
     * 查询用户搜索历史记录列表
     */
    @RequiresPermissions("school:searchHistory:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(SearchHistory searchHistory)
    {
        startPage();
        List<SearchHistory> list = searchHistoryService.selectSearchHistoryList(searchHistory);
        return getDataTable(list);
    }

    /**
     * 导出用户搜索历史记录列表
     */
    @RequiresPermissions("school:searchHistory:export")
    @Log(title = "用户搜索历史记录", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(SearchHistory searchHistory)
    {
        List<SearchHistory> list = searchHistoryService.selectSearchHistoryList(searchHistory);
        ExcelUtil<SearchHistory> util = new ExcelUtil<SearchHistory>(SearchHistory.class);
        return util.exportExcel(list, "searchHistory");
    }

    /**
     * 新增用户搜索历史记录
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存用户搜索历史记录
     */
    @RequiresPermissions("school:searchHistory:add")
    @Log(title = "用户搜索历史记录", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(SearchHistory searchHistory)
    {
        return toAjax(searchHistoryService.insertSearchHistory(searchHistory));
    }

    /**
     * 修改用户搜索历史记录
     */
    @GetMapping("/edit/{searchId}")
    public String edit(@PathVariable("searchId") Long searchId, ModelMap mmap)
    {
        SearchHistory searchHistory = searchHistoryService.selectSearchHistoryById(searchId);
        mmap.put("searchHistory", searchHistory);
        return prefix + "/edit";
    }

    /**
     * 修改保存用户搜索历史记录
     */
    @RequiresPermissions("school:searchHistory:edit")
    @Log(title = "用户搜索历史记录", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(SearchHistory searchHistory)
    {
        return toAjax(searchHistoryService.updateSearchHistory(searchHistory));
    }

    /**
     * 删除用户搜索历史记录
     */
    @RequiresPermissions("school:searchHistory:remove")
    @Log(title = "用户搜索历史记录", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(searchHistoryService.deleteSearchHistoryByIds(ids));
    }
}