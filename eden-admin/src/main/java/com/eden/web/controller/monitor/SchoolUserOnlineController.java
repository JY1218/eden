package com.eden.web.controller.monitor;

import java.util.List;

import com.eden.common.core.text.Convert;
import com.eden.common.enums.OnlineStatus;
import com.eden.framework.shiro.session.OnlineSession;
import com.eden.framework.shiro.session.OnlineSessionDAO;
import com.eden.framework.util.ShiroUtils;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.eden.common.annotation.Log;
import com.eden.common.enums.BusinessType;
import com.eden.school.domain.SchoolUserOnline;
import com.eden.school.service.ISchoolUserOnlineService;
import com.eden.common.core.controller.BaseController;
import com.eden.common.core.domain.AjaxResult;
import com.eden.common.utils.poi.ExcelUtil;
import com.eden.common.core.page.TableDataInfo;

/**
 * 在线学生用户记录Controller
 *
 * @author jiaoyang
 * @date 2020-12-24
 */
@Controller
@RequestMapping("/monitor/schoolUserOnline")
public class SchoolUserOnlineController extends BaseController
{
    private String prefix = "monitor/online";

    @Autowired
    private ISchoolUserOnlineService schoolUserOnlineService;

    @Autowired
    private OnlineSessionDAO onlineSessionDAO;

    @RequiresPermissions("monitor:schoolUserOnline:view")
    @GetMapping()
    public String schoolUserOnline()
    {
        return prefix + "/schoolUserOnline";
    }

    /**
     * 查询在线学生用户记录列表
     */
    @RequiresPermissions("monitor:schoolUserOnline:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(SchoolUserOnline schoolUserOnline)
    {
        startPage();
        List<SchoolUserOnline> list = schoolUserOnlineService.selectSchoolUserOnlineList(schoolUserOnline);
        return getDataTable(list);
    }

    /**
     * 导出在线学生用户记录列表
     */
    @RequiresPermissions("monitor:schoolUserOnline:export")
    @Log(title = "在线学生用户记录", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(SchoolUserOnline schoolUserOnline)
    {
        List<SchoolUserOnline> list = schoolUserOnlineService.selectSchoolUserOnlineList(schoolUserOnline);
        ExcelUtil<SchoolUserOnline> util = new ExcelUtil<SchoolUserOnline>(SchoolUserOnline.class);
        return util.exportExcel(list, "schoolUserOnline");
    }

    /**
     * 新增在线学生用户记录
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存在线学生用户记录
     */
    @RequiresPermissions("monitor:schoolUserOnline:add")
    @Log(title = "在线学生用户记录", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(SchoolUserOnline schoolUserOnline)
    {
        return toAjax(schoolUserOnlineService.insertSchoolUserOnline(schoolUserOnline));
    }

    /**
     * 修改在线学生用户记录
     */
    @GetMapping("/edit/{sessionId}")
    public String edit(@PathVariable("sessionId") String sessionId, ModelMap mmap)
    {
        SchoolUserOnline schoolUserOnline = schoolUserOnlineService.selectSchoolUserOnlineById(sessionId);
        mmap.put("schoolUserOnline", schoolUserOnline);
        return prefix + "/edit";
    }

    /**
     * 修改保存在线学生用户记录
     */
    @RequiresPermissions("monitor:schoolUserOnline:edit")
    @Log(title = "在线学生用户记录", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(SchoolUserOnline schoolUserOnline)
    {
        return toAjax(schoolUserOnlineService.updateSchoolUserOnline(schoolUserOnline));
    }

    /**
     * 删除在线学生用户记录
     */
    @RequiresPermissions("monitor:schoolUserOnline:remove")
    @Log(title = "在线学生用户记录", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(schoolUserOnlineService.deleteSchoolUserOnlineByIds(ids));
    }
    @RequiresPermissions(value = { "monitor:schoolUserOnline:batchForceLogout", "monitor:online:forceLogout" }, logical = Logical.OR)
    @Log(title = "在线学生用户", businessType = BusinessType.FORCE)
    @PostMapping("/batchForceLogout")
    @ResponseBody
    public AjaxResult batchForceLogout(String ids)
    {
        for (String sessionId : Convert.toStrArray(ids))
        {
            SchoolUserOnline online = schoolUserOnlineService.selectSchoolUserOnlineById(sessionId);
            if (online == null)
            {
                return error("用户已下线");
            }
            OnlineSession onlineSession = (OnlineSession) onlineSessionDAO.readSession(online.getSessionId());
            if (onlineSession == null)
            {
                return error("用户已下线");
            }
            if (sessionId.equals(ShiroUtils.getSessionId()))
            {
                return error("当前登陆用户无法强退");
            }
            onlineSessionDAO.delete(onlineSession);
            online.setStatus(OnlineStatus.off_line);
            schoolUserOnlineService.saveOnline(online);
            schoolUserOnlineService.removeUserCache(online.getLoginName(), sessionId);
        }
        return success();
    }
}