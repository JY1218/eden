package com.eden.web.controller.system;

import com.eden.common.annotation.Log;
import com.eden.common.core.controller.BaseController;
import com.eden.common.core.domain.AjaxResult;
import com.eden.common.enums.BusinessType;
import com.eden.common.utils.poi.ExcelUtil;
import com.eden.system.domain.SysRegion;
import com.eden.system.service.ISysRegionService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 地区Controller
 *
 * @author jiaoyang
 * @date 2020-09-02
 */
@Controller
@RequestMapping("/system/region")
public class SysRegionController extends BaseController {
    private String prefix = "system/region";

    @Autowired
    private ISysRegionService sysRegionService;

    @RequiresPermissions("system:region:view")
    @GetMapping()
    public String region() {
        return prefix + "/region";
    }


    /**
     * 查询地区列表
     */
    @GetMapping("/list")
    @ResponseBody
    public String list() {
        String data = String.valueOf(sysRegionService.selectSysRegionJson(new SysRegion()));
        return data;
    }

    /**
     * 查询地区列表
     */
    @RequiresPermissions("system:region:list")
    @PostMapping("/list")
    @ResponseBody
    public String list(SysRegion sysRegion) {
        String data = String.valueOf(sysRegionService.selectSysRegionList(sysRegion));
        return data;
    }

    /**
     * 导出地区列表
     */
    @RequiresPermissions("system:region:export")
    @Log(title = "地区", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(SysRegion sysRegion) {
        List<SysRegion> list = sysRegionService.selectSysRegionList(sysRegion);
        ExcelUtil<SysRegion> util = new ExcelUtil<SysRegion>(SysRegion.class);
        return util.exportExcel(list, "region");
    }

    /**
     * 新增地区
     */
    @GetMapping("/add")
    public String add() {
        return prefix + "/add";
    }

    /**
     * 新增保存地区
     */
    @RequiresPermissions("system:region:add")
    @Log(title = "地区", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(SysRegion sysRegion) {
        return toAjax(sysRegionService.insertSysRegion(sysRegion));
    }

    /**
     * 修改地区
     */
    @GetMapping("/edit/{regionId}")
    public String edit(@PathVariable("regionId") String regionId, ModelMap mmap) {
        SysRegion sysRegion = sysRegionService.selectSysRegionById(regionId);
        mmap.put("sysRegion", sysRegion);
        return prefix + "/edit";
    }

    /**
     * 修改保存地区
     */
    @RequiresPermissions("system:region:edit")
    @Log(title = "地区", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(SysRegion sysRegion) {
        return toAjax(sysRegionService.updateSysRegion(sysRegion));
    }

    /**
     * 删除地区
     */
    @RequiresPermissions("system:region:remove")
    @Log(title = "地区", businessType = BusinessType.DELETE)
    @PostMapping("/remove")
    @ResponseBody
    public AjaxResult remove(String ids) {
        return toAjax(sysRegionService.deleteSysRegionByIds(ids));
    }
}