package com.eden.web.controller.websocket;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.eden.common.annotation.Log;
import com.eden.common.enums.BusinessType;
import com.eden.websocket.domain.Message;
import com.eden.websocket.service.IMessageService;
import com.eden.common.core.controller.BaseController;
import com.eden.common.core.domain.AjaxResult;
import com.eden.common.utils.poi.ExcelUtil;
import com.eden.common.core.page.TableDataInfo;

/**
 * 消息Controller
 *
 * @author eden
 * @date 2020-12-21
 */
@Controller
@RequestMapping("/websocket/message")
public class MessageController extends BaseController
{
    private String prefix = "websocket/message";

    @Autowired
    private IMessageService messageService;

    @RequiresPermissions("websocket:message:view")
    @GetMapping()
    public String message()
    {
        return prefix + "/message";
    }

    /**
     * 查询消息列表
     */
    @RequiresPermissions("websocket:message:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(Message message)
    {
        startPage();
        List<Message> list = messageService.selectMessageList(message);
        return getDataTable(list);
    }

    /**
     * 导出消息列表
     */
    @RequiresPermissions("webscoket:message:export")
    @Log(title = "消息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(Message message)
    {
        List<Message> list = messageService.selectMessageList(message);
        ExcelUtil<Message> util = new ExcelUtil<Message>(Message.class);
        return util.exportExcel(list, "message");
    }

    /**
     * 新增消息
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存消息
     */
    @RequiresPermissions("websocket:message:add")
    @Log(title = "消息", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(Message message)
    {
        return toAjax(messageService.insertMessage(message));
    }

    /**
     * 修改消息
     */
    @GetMapping("/edit/{msgId}")
    public String edit(@PathVariable("msgId") Integer msgId, ModelMap mmap)
    {
        Message message = messageService.selectMessageById(msgId);
        mmap.put("message", message);
        return prefix + "/edit";
    }

    /**
     * 修改保存消息
     */
    @RequiresPermissions("websocket:message:edit")
    @Log(title = "消息", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(Message message)
    {
        return toAjax(messageService.updateMessage(message));
    }

    /**
     * 删除消息
     */
    @RequiresPermissions("websocket:message:remove")
    @Log(title = "消息", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(messageService.deleteMessageByIds(ids));
    }
}