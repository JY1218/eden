package com.eden.web.controller.wechat;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.eden.common.annotation.Log;
import com.eden.common.enums.BusinessType;
import com.eden.wechat.domain.WxIconList;
import com.eden.wechat.service.IWxIconListService;
import com.eden.common.core.controller.BaseController;
import com.eden.common.core.domain.AjaxResult;
import com.eden.common.utils.poi.ExcelUtil;
import com.eden.common.core.page.TableDataInfo;

/**
 * 微信首页导航Controller
 *
 * @author jiaoyang
 * @date 2020-12-02
 */
@Controller
@RequestMapping("/wechat/iconlist")
public class WxIconListController extends BaseController
{
    private String prefix = "wechat/iconlist";

    @Autowired
    private IWxIconListService wxIconListService;

    @RequiresPermissions("wechat:iconlist:view")
    @GetMapping()
    public String iconlist()
    {
        return prefix + "/iconlist";
    }

    /**
     * 查询微信首页导航列表
     */
    @RequiresPermissions("wechat:iconlist:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(WxIconList wxIconList)
    {
        startPage();
        List<WxIconList> list = wxIconListService.selectWxIconListList(wxIconList);
        return getDataTable(list);
    }

    /**
     * 导出微信首页导航列表
     */
    @RequiresPermissions("wechat:iconlist:export")
    @Log(title = "微信首页导航", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(WxIconList wxIconList)
    {
        List<WxIconList> list = wxIconListService.selectWxIconListList(wxIconList);
        ExcelUtil<WxIconList> util = new ExcelUtil<WxIconList>(WxIconList.class);
        return util.exportExcel(list, "iconlist");
    }

    /**
     * 新增微信首页导航
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存微信首页导航
     */
    @RequiresPermissions("wechat:iconlist:add")
    @Log(title = "微信首页导航", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(WxIconList wxIconList)
    {
        return toAjax(wxIconListService.insertWxIconList(wxIconList));
    }

    /**
     * 修改微信首页导航
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Integer id, ModelMap mmap)
    {
        WxIconList wxIconList = wxIconListService.selectWxIconListById(id);
        mmap.put("wxIconList", wxIconList);
        return prefix + "/edit";
    }

    /**
     * 修改保存微信首页导航
     */
    @RequiresPermissions("wechat:iconlist:edit")
    @Log(title = "微信首页导航", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(WxIconList wxIconList)
    {
        return toAjax(wxIconListService.updateWxIconList(wxIconList));
    }

    /**
     * 删除微信首页导航
     */
    @RequiresPermissions("wechat:iconlist:remove")
    @Log(title = "微信首页导航", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(wxIconListService.deleteWxIconListByIds(ids));
    }
}