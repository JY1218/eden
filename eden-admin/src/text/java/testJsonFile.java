import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.eden.EdenApplication;
import com.eden.common.utils.file.FileUtils;
import com.eden.system.domain.SysRegion;
import com.eden.system.mapper.SysRegionMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @program: eden
 * @description: ${description}
 * @author: Mr.Jiao
 * @create: 2020-09-01 15:55
 */
/*@RunWith(SpringRunner.class)
@SpringBootTest(classes = EdenApplication.class)*/
public class testJsonFile {
    @Autowired
    private SysRegionMapper sysRegionMapper;

    @Test
    public void test() {
        //设置市
        String file = FileUtils.readJsonFile("H:\\eden\\china.json");
        JSONArray objects = JSON.parseArray(file);
        for (int i = 0; i < objects.size(); i++) {
            //设置省
            SysRegion province = new SysRegion();
            JSONObject provinces = (JSONObject) objects.get(i);
            province.setRegionCode((String) provinces.get("code"));
            province.setN((String) provinces.get("name"));
            province.setRegionParentId("0");
            province.setRegionLevel(1);
            //设置市
            //  sysRegionMapper.insertSysRegion(province);
            JSONArray cityList = JSON.parseArray(provinces.get("cityList").toString());
            if (cityList.size() > 0) {
                for (int j = 0; j < cityList.size(); j++) {
                    SysRegion city = new SysRegion();
                    JSONObject citys = (JSONObject) cityList.get(j);
                    city.setRegionCode((String) citys.get("code"));
                    city.setN((String) citys.get("name"));
                    city.setRegionParentId((String) provinces.get("code"));
                    city.setRegionLevel(2);
                    //     sysRegionMapper.insertSysRegion(city);
                    //设置县

                    JSONArray areaList = JSON.parseArray(citys.get("areaList").toString());
                    if (areaList.size() > 0) {
                        for (int k = 0; k < areaList.size(); k++) {
                            SysRegion county = new SysRegion();
                            JSONObject countys = (JSONObject) areaList.get(k);
                            county.setRegionCode((String) countys.get("code"));
                            county.setN((String) countys.get("name"));
                            county.setRegionParentId((String) citys.get("code"));
                            county.setRegionLevel(3);
                            //          sysRegionMapper.insertSysRegion(county);
                            System.out.println(county);
                        }
                    }
                }
            }
        }
    }
}
