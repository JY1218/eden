package com.eden.framework.data.cache;

import org.springframework.data.redis.cache.RedisCacheWriter;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.util.Assert;

import java.time.Duration;
import java.util.Set;

/**
 * @description:
 * @author: wenqing
 * @date: 2020/4/7 11:00
 * @version: 1.0
 */

public interface INtRedisCacheWriter extends RedisCacheWriter {

    /**
     * Create new {@link INtRedisCacheWriter} without locking behavior.
     *
     * @param connectionFactory must not be {@literal null}.
     * @return new instance of {@link NtRedisCacheWriter}.
     */
    static INtRedisCacheWriter nonLockingRedisCacheWriter(RedisConnectionFactory connectionFactory) {

        Assert.notNull(connectionFactory, "ConnectionFactory must not be null!");

        return new NtRedisCacheWriter(connectionFactory);
    }

    /**
     * Create new {@link RedisCacheWriter} with locking behavior.
     *
     * @param connectionFactory must not be {@literal null}.
     * @return new instance of {@link NtRedisCacheWriter}.
     */
    static INtRedisCacheWriter lockingRedisCacheWriter(RedisConnectionFactory connectionFactory) {

        Assert.notNull(connectionFactory, "ConnectionFactory must not be null!");

        return new NtRedisCacheWriter(connectionFactory, Duration.ofMillis(50));
    }

    Set keys(String name, byte[] pattern);

}
