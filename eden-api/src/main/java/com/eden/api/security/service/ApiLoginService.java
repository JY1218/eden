package com.eden.api.security.service;

import com.eden.api.controller.vo.ApiLoginReq;
import com.eden.api.controller.vo.ApiLoginResp;
import com.eden.api.security.LoginUser;
import com.eden.common.constant.ApiConstants;
import com.eden.common.constant.Constants;
import com.eden.common.constant.ShiroConstants;
import com.eden.common.constant.UserConstants;
import com.eden.common.enums.UserStatus;
import com.eden.common.exception.user.*;
import com.eden.common.utils.*;
import com.eden.framework.manager.AsyncManager;
import com.eden.framework.manager.factory.AsyncFactory;
import com.eden.framework.shiro.service.SysPasswordService;
import com.eden.school.domain.SchoolUser;
import com.eden.school.service.ISchoolUserService;
import com.eden.system.domain.SysRole;
import com.eden.system.service.ISysMenuService;
import com.eden.system.service.ISysRoleService;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authc.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

/**
 * @description:
 * @author: wenqing
 * @date: 2020/1/14 22:13
 * @version: 1.0
 */
@Slf4j
@Service
public class ApiLoginService {

    @Autowired
    private SysPasswordService passwordService;

    @Autowired
    private ISchoolUserService schoolUserService;

    @Autowired
    private ITokenService tokenService;

    @Autowired
    private ISysMenuService menuService;

    @Autowired
    private ISysRoleService roleService;

    public ApiLoginResp login(ApiLoginReq apiLoginReq) {
        try {
            return loginHandler(apiLoginReq);
        } catch (CaptchaException e) {
            throw new AuthenticationException(e.getMessage(), e);
        } catch (UserNotExistsException e) {
            throw new UnknownAccountException(e.getMessage(), e);
        } catch (UserPasswordNotMatchException e) {
            throw new IncorrectCredentialsException(e.getMessage(), e);
        } catch (UserPasswordRetryLimitExceedException e) {
            throw new ExcessiveAttemptsException(e.getMessage(), e);
        } catch (UserBlockedException e) {
            throw new LockedAccountException(e.getMessage(), e);
        } catch (RoleBlockedException e) {
            throw new LockedAccountException(e.getMessage(), e);
        } catch (Exception e) {
            throw new AuthenticationException(e.getMessage(), e);
        }
    }


    private ApiLoginResp loginHandler(ApiLoginReq apiLoginReq) {
        String username = apiLoginReq.getUsername();
        String password = apiLoginReq.getPassword();
        ApiLoginResp apiLoginResp = new ApiLoginResp();
        // 验证码校验
        if (StringUtils.isNotNull(ServletUtils.getRequest().getAttribute(ShiroConstants.CURRENT_CAPTCHA))) {
            AsyncManager.me().execute(AsyncFactory.recordLogininfor(username, Constants.LOGIN_FAIL, MessageUtils.message("user.jcaptcha.error")));
            throw new CaptchaException();
        }
        // 用户名或密码为空 错误
        if (StringUtils.isEmpty(username) || StringUtils.isEmpty(password)) {
            AsyncManager.me().execute(AsyncFactory.recordLogininfor(username, Constants.LOGIN_FAIL, MessageUtils.message("not.null")));
            throw new UserNotExistsException();
        }
        // 密码如果不在指定范围内 错误
        if (password.length() < UserConstants.PASSWORD_MIN_LENGTH
                || password.length() > UserConstants.PASSWORD_MAX_LENGTH) {
            AsyncManager.me().execute(AsyncFactory.recordLogininfor(username, Constants.LOGIN_FAIL, MessageUtils.message("user.password.not.match")));
            throw new UserPasswordNotMatchException();
        }

        // 用户名不在指定范围内 错误
        if (username.length() < UserConstants.USERNAME_MIN_LENGTH
                || username.length() > UserConstants.USERNAME_MAX_LENGTH) {
            AsyncManager.me().execute(AsyncFactory.recordLogininfor(username, Constants.LOGIN_FAIL, MessageUtils.message("user.password.not.match")));
            throw new UserPasswordNotMatchException();
        }

        // 查询用户信息
        SchoolUser user = schoolUserService.selectSchoolUserByLoginName(username);
        if (user == null) {
            AsyncManager.me().execute(AsyncFactory.recordLogininfor(username, Constants.LOGIN_FAIL, MessageUtils.message("user.not.exists")));
            throw new UserNotExistsException();
        }
        if (!"0".equals(user.getDelFlag())) {
            AsyncManager.me().execute(AsyncFactory.recordLogininfor(username, Constants.LOGIN_FAIL, MessageUtils.message("user.not.exists")));
            throw new UserNotExistsException();
        }
        if (UserStatus.DELETED.getCode().equals(user.getDelFlag())) {
            AsyncManager.me().execute(AsyncFactory.recordLogininfor(username, Constants.LOGIN_FAIL, MessageUtils.message("user.password.delete")));
            throw new UserDeleteException();
        }

        if (UserStatus.DISABLE.getCode().equals(user.getStatus())) {
            AsyncManager.me().execute(AsyncFactory.recordLogininfor(username, Constants.LOGIN_FAIL, MessageUtils.message("user.blocked", user.getRemark())));
            throw new UserBlockedException();
        }

        passwordService.validateSchoolUser(user, password);

        AsyncManager.me().execute(AsyncFactory.recordLogininfor(username, Constants.LOGIN_SUCCESS, MessageUtils.message("user.login.success")));
        recordLoginInfo(user);
        apiLoginResp.setToken(createToken(user, apiLoginReq));
        return apiLoginResp;
    }

    /**
     * 记录登录信息
     */
    public void recordLoginInfo(SchoolUser user) {
        user.setLoginIp(IpUtils.getIpAddr(ServletUtils.getRequest()));
        user.setLoginDate(DateUtils.getNowDate());
        schoolUserService.updateSchoolUser(user);
    }

    /**
     * 创建登录用户token
     *
     * @param user
     * @return token
     */
    private String createToken(SchoolUser user, ApiLoginReq apiLoginReq) {
        Set<String> permissions;
        if (user.isAdmin() || hasSuperRole(user.getRole())) {
            permissions = new HashSet<>(1);
            permissions.add(ApiConstants.ALL_PERMISSION_KEY);
        } else {
            permissions = menuService.selectPermsBySchoolUserId(user.getUserId());
        }
        Set<String> roles = roleService.selectRoleKeysByRoleId(user.getRoleId());
        LoginUser loginUser = new LoginUser(user, permissions, roles);
        return tokenService.createToken(loginUser);
    }

    /**
     * 判断角色集是否包含超级管理角色
     *
     * @param role
     * @return
     */
    private boolean hasSuperRole(SysRole role) {
        if (role.isAdmin() || ApiConstants.SUPER_ROLE_KEY.equals(role.getRoleKey())) {
            return true;
        }
        return false;
    }

    /**
     * 登出
     */
    public void logout() {
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        if (StringUtils.isNotNull(loginUser)) {
            String userName = loginUser.getUsername();
            // 删除用户缓存记录
            tokenService.delLoginUser(loginUser.getToken());
            // 记录用户退出日志
            AsyncManager.me().execute(AsyncFactory.recordLogininfor(userName, Constants.LOGOUT, MessageUtils.message("user.logout.success")));
        }
    }
}
