package com.eden.api.security.web;

import lombok.extern.slf4j.Slf4j;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

/**
 * @description: ServletContext监听
 * @author: wenqing
 * @date: 2019/11/16 22:03
 */

@Slf4j
@WebListener
public class ApiServletContextListener implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        log.info("contextInitialized...");
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        log.info("contextDestroyed...");
    }
}
