package com.eden.api.security.annotation;

import java.lang.annotation.*;

/**
 * @description: 操作请求忽略身份认证 允许匿名访问
 * @author: wenqing
 * @date: 2019/11/04 16:30
 */

@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface AnonymousAccess {

}
