package com.eden.api.security.annotation;

import com.eden.api.security.enums.RequiredMode;

import java.lang.annotation.*;

/**
 * @description: 操作请求所需权限标识符
 * @author: wenqing
 * @date: 2019/11/04 22:49
 */

@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface PermissionsRequired {

    String[] value();

    RequiredMode requiredMode() default RequiredMode.AND;
}
