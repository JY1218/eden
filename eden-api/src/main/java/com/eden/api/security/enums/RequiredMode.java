package com.eden.api.security.enums;

/**
 * @description: 模式
 * @author: wenqing
 * @date: 2019/11/04 22:54
 */


public enum RequiredMode {

    AND, OR, NONE;

    RequiredMode() {
    }
}
