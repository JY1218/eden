package com.eden.api.security.annotation;

import com.eden.api.security.enums.RequiredMode;

import java.lang.annotation.*;

/**
 * @description: 操作请求所需角色标识符
 * @author: wenqing
 * @date: 2019/11/17 0:17
 */

@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface RolesRequired {

    String[] value();

    RequiredMode requiredMode() default RequiredMode.AND;
}
