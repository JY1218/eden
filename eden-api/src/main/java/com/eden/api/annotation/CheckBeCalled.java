package com.eden.api.annotation;

import java.lang.annotation.*;

/**
 * @description: 检查是否被调用
 * @author: wenqing
 * @date: 2020/1/10 14:01
 * @version: 1.0
 */

@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface CheckBeCalled {
}
