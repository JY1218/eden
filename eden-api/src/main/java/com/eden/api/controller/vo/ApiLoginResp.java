package com.eden.api.controller.vo;

import lombok.Data;

/**
 * @description:
 * @author: wenqing
 * @date: 2020/3/4 10:53
 * @version: 1.0
 */

@Data
public class ApiLoginResp {

    /** token */
    private String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
