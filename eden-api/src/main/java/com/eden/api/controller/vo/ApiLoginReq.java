package com.eden.api.controller.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

/**
 * @description: 登录请求vo
 * @author: wenqing
 * @date: 2020/2/25 14:40
 * @version: 1.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ApiLoginReq {

    @ApiModelProperty("用户名")
    @NotBlank(message = "登录用户名不能为空")
    private String username;
    @ApiModelProperty("密码")
    @NotBlank(message = "登录密码不能为空")
    private String password;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
