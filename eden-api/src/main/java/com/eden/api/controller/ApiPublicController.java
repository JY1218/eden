package com.eden.api.controller;


import com.eden.api.controller.vo.ApiResult;
import com.eden.api.security.JwtUser;
import com.eden.common.config.Global;
import com.eden.common.utils.StringUtils;
import com.eden.common.utils.file.FileUploadUtils;
import com.eden.common.utils.file.MimeTypeUtils;
import com.eden.common.utils.uuid.IdUtils;
import com.eden.school.domain.SchoolUser;
import com.eden.system.domain.SysDept;
import com.eden.system.service.ISysDeptService;
import com.eden.task.domain.TaskPicture;
import com.eden.task.domain.TaskType;
import com.eden.task.domain.UserTask;
import com.eden.task.service.ITaskPictureService;
import com.eden.task.service.ITaskTypeService;
import com.eden.task.service.IUserTaskService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

import static com.eden.api.controller.vo.ApiResult.error;
import static com.eden.api.controller.vo.ApiResult.success;

/**
 * 客户端发布接口
 */
@Slf4j
@Api("客户端任务发布接口集合")
@RestController
@RequestMapping("/api/public")
public class ApiPublicController {
    @Autowired
    private IUserTaskService userTaskService;

    @Autowired
    private ITaskPictureService taskPictureService;
    @Autowired
    private ITaskTypeService taskTypeService;
    @Autowired
    private ISysDeptService sysDeptService;

    @ApiOperation("任务类型查询接口")
    @PostMapping("/taskTypeList")
    public ApiResult taskTypeList(@Validated @RequestBody TaskType taskType) {
        List<TaskType> taskTypes = taskTypeService.selectTaskTypeList(taskType);
        return success("查询成功！！", taskTypes);
    }

    /**
     * 文件上传对应的方法
     *
     * @return
     * @throws IOException
     */
    @PostMapping("/uploadFile")
    public ApiResult uploadFile(@RequestParam("image") MultipartFile image, HttpSession session) throws IOException {
        //文件路径
        String picturePath = "";
        // 上传文件路径
        String uploadFilePath = Global.getUploadPath();
        //获取文件后缀名
        String extension = FileUploadUtils.getExtension(image);
        //判断文件是否是允许的类型
        if (FileUploadUtils.isAllowedExtension(extension, MimeTypeUtils.IMAGE_VIDEO_TYPE_EXTENSION)) {
            //判断是否是图片类型
            if (FileUploadUtils.isFileTypeExtension(extension, MimeTypeUtils.IMAGE_TYPE_EXTENSION)) {
                //上传并返回新文件名称
                if (StringUtils.isNotNull(image)) {
                    picturePath = FileUploadUtils.upload(uploadFilePath, image);
                }
            } else {
                return error("该文件不是图片类型");
            }
        } else {
            return error("文件不是系统允许的类型");
        }
        return success("上传成功", picturePath);
    }


    /**
     * 客户端发布接口
     *
     * @param task
     * @return
     */
    @ApiOperation("任务发布接口")
    @PostMapping("/taskPublic")
    @Transactional
    public ApiResult taskPublic(@Validated @RequestBody UserTask task) {
        //随机生成任务ID
        String uuid = IdUtils.fastSimpleUUID().substring(0, 6);
        String taskId = "T" + uuid + System.currentTimeMillis();
        SchoolUser user = JwtUser.getInstance().getLoginUser().getUser();
        SysDept sysDept = sysDeptService.selectDeptById(user.getDeptId());
        String createBy = JwtUser.getInstance().getLoginUser().getUser().getLoginName();
        if (StringUtils.isNotNull(task)) {
            task.setTaskId(taskId);
            task.setTaskStatus("0");
            task.setPhonenumber(user.getPhonenumber());
            task.setAddress(sysDept.getParentName());
            task.setCreateBy(createBy);
            userTaskService.insertUserTask(task);
            if (task.getPictureUrls().size() != 0) {
                for (String pictureUrl : task.getPictureUrls()) {
                    String puuid = IdUtils.fastSimpleUUID().substring(0, 4);
                    String pictureId = "P" + puuid + System.currentTimeMillis();
                    TaskPicture picture = new TaskPicture();
                    picture.setPictureId(pictureId);
                    picture.setPictureStatus("1");
                    picture.setTaskCpid(taskId);
                    picture.setPictureName(FileUploadUtils.getFileName(pictureUrl));
                    picture.setPictureUrl(pictureUrl);
                    picture.setCreateBy(createBy);
                    taskPictureService.insertTaskPicture(picture);
                }
            }
        } else {
            return error("对不起，您发布的任务为空！不能发布！");
        }
        return success("任务发布成功！！！请等待系统审核！");
    }


}
