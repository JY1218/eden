package com.eden.api.controller;

import com.eden.api.controller.vo.ApiResult;
import com.eden.api.security.JwtUser;
import com.eden.api.security.LoginUser;
import com.eden.common.utils.DateUtils;
import com.eden.common.utils.StringUtils;
import com.eden.school.domain.SchoolUser;
import com.eden.school.domain.SearchHistory;
import com.eden.school.service.ISearchHistoryService;
import com.eden.task.domain.UserTask;
import com.eden.task.service.IUserTaskService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static com.eden.api.controller.vo.ApiResult.error;
import static com.eden.api.controller.vo.ApiResult.success;

/**
 * search页面的请求
 */
@Slf4j
@Api("搜索任务页面接口")
@RestController
@RequestMapping("/api/search")
public class ApiSearchController {
    @Autowired
    private IUserTaskService userTaskService;
    @Autowired
    private ISearchHistoryService searchHistoryService;

    @ApiOperation("根据任务名称查询任务信息")
    @GetMapping("/searchTasksByTaskName")
    public ApiResult searchTasksByTaskName(@RequestParam(value = "taskName") String taskName) {
        if (StringUtils.isNotEmpty(taskName)) {
            LoginUser loginUser = JwtUser.getInstance().getLoginUser();
            SchoolUser user = loginUser.getUser();
            SearchHistory searchHistory = new SearchHistory();
            searchHistory.setLoginName(user.getLoginName());
            searchHistory.setContent(taskName);
            searchHistory.setCreateTime(DateUtils.getNowDate());
            searchHistory.setCreateBy(user.getUserName());
            searchHistoryService.insertSearchHistory(searchHistory);
            List<UserTask> tasks = userTaskService.selectUserTaskListByTaskName(taskName);
            return success("查询任务信息成功", tasks);
        } else
            return error("搜索的内容为空");
    }

}
