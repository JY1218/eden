package com.eden.api.controller.vo;

import com.eden.common.constant.HttpStatus;
import com.eden.common.utils.StringUtils;


import java.util.Date;
import java.util.HashMap;

/**
 * 操作消息提醒
 *
 * @author wenqing
 */
public class ApiResult extends HashMap<String, Object> {

    private static final long serialVersionUID = 1L;

    /** 成功状态值 */
    private static final String RESULT_STATUS_SUCCESS = "success";

    /** 错误状态值 */
    private static final String RESULT_STATUS_ERROR = "error";

    /** 成功状态 */
    public static final String STATUS_TAG = "status";

    /** 状态码 */
    public static final String CODE_TAG = "code";

    /** 返回内容 */
    public static final String MESSAGE_TAG = "message";

    /** 时间戳 */
    public static final String TIMESTAMP_TAG = "timestamp";

    /** 数据对象 */
    public static final String DATA_TAG = "data";

    /**
     * 初始化一个新创建的 ApiResult 对象，使其表示一个空消息。
     */
    public ApiResult() {
    }

    /**
     * 初始化一个新创建的 ApiResult 对象
     *
     * @param code 状态码
     * @param message 返回内容
     */
    public ApiResult(int code, String message) {
        super.put(CODE_TAG, code);
        super.put(MESSAGE_TAG, message);
    }

    /**
     * 初始化一个新创建的 ApiResult 对象
     *
     * @param code 状态码
     * @param message 返回内容
     * @param data 数据对象
     */
    public ApiResult(String status, int code, String message, Object data) {
        super.put(STATUS_TAG, status);
        super.put(CODE_TAG, code);
        super.put(MESSAGE_TAG, message);
        super.put(TIMESTAMP_TAG, new Date());
        if (StringUtils.isNotNull(data)) {
            super.put(DATA_TAG, data);
        }
    }

    /**
     * 返回成功消息
     *
     * @return 成功消息
     */
    public static ApiResult success() {
        return ApiResult.success("操作成功");
    }

    /**
     * 返回成功数据
     *
     * @return 成功消息
     */
    public static ApiResult success(Object data) {
        return ApiResult.success("操作成功", data);
    }

    /**
     * 返回成功消息
     *
     * @param message 返回内容
     * @return 成功消息
     */
    public static ApiResult success(String message) {
        return ApiResult.success(message, null);
    }

    /**
     * 返回成功消息
     *
     * @param message 返回内容
     * @param data 数据对象
     * @return 成功消息
     */
    public static ApiResult success(String message, Object data) {
        return new ApiResult(RESULT_STATUS_SUCCESS, HttpStatus.SUCCESS, message, data);
    }

    /**
     * 返回错误消息
     *
     * @return
     */
    public static ApiResult error() {
        return ApiResult.error("操作失败");
    }

    /**
     * 返回错误消息
     *
     * @param message 返回内容
     * @return 警告消息
     */
    public static ApiResult error(String message) {
        return ApiResult.error(message, null);
    }

    /**
     * 返回错误消息
     *
     * @param code 状态码
     * @param message 返回内容
     * @return 警告消息
     */
    public static ApiResult error(int code, String message) {
        return new ApiResult(RESULT_STATUS_ERROR, code, message, null);
    }


    /**
     * 返回错误消息
     *
     * @param message 返回内容
     * @param data 数据对象
     * @return 警告消息
     */
    public static ApiResult error(String message, Object data) {
        return new ApiResult(RESULT_STATUS_ERROR, HttpStatus.ERROR, message, data);
    }

    /**
     * 返回错误消息
     * @param code 返回码
     * @param message 返回内容
     * @param data 数据对象
     * @return 警告消息
     */
    public static ApiResult error(int code, String message, Object data) {
        return new ApiResult(RESULT_STATUS_ERROR, code, message, data);
    }
}
