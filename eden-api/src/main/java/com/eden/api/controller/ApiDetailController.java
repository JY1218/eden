package com.eden.api.controller;


import com.eden.api.controller.vo.ApiResult;
import com.eden.task.domain.UserTask;
import com.eden.task.service.IUserTaskService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static com.eden.api.controller.vo.ApiResult.success;

/**
 * index页面的请求
 */
@Slf4j
@Api("客户端首页接口")
@RestController
@RequestMapping("/api/detail")
public class ApiDetailController {
    @Autowired
    private IUserTaskService userTaskService;

    @ApiOperation("根据不同的任务id去查询详细内容")
    @PostMapping("/showTaskDetail")
    public ApiResult showTaskDetail(@Validated @RequestBody UserTask userTask) {
        UserTask task = userTaskService.selectTaskDetailApi(userTask);
        return success(task);
    }
}
