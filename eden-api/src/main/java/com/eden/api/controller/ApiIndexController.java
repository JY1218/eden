package com.eden.api.controller;

import com.eden.api.controller.vo.ApiResult;
import com.eden.system.domain.SysDept;
import com.eden.system.service.ISysDeptService;
import com.eden.task.domain.TaskType;
import com.eden.task.domain.UserTask;
import com.eden.task.service.ITaskTypeService;
import com.eden.task.service.IUserTaskService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * index页面的请求
 */
@Slf4j
@Api("客户端首页接口")
@RestController
@RequestMapping("/api/index")
public class ApiIndexController extends ApiBaseController {

    @Autowired
    private ITaskTypeService taskTypeService;
    @Autowired
    private IUserTaskService userTaskService;
    @Autowired
    private ISysDeptService sysDeptService;

    //加载微信小程序的导航图标集合
    @ApiOperation("加载微信小程序的导航图标集合")
    @PostMapping("/iconlist")
    public ApiResult taskTypeList(@Validated @RequestBody TaskType taskType) {
        List<TaskType> taskTypes = taskTypeService.selectTaskTypeList(taskType);
        return success("导航图标信息加载成功", taskTypes);
    }

    //加载任务卡片内容
    @ApiOperation("加载微信小程序的任务集合---所有")
    @PostMapping("/taskList")
    public ApiResult selectAllTasks(@Validated @RequestBody UserTask taskList) {
        List<UserTask> userTasks = userTaskService.selectUserTaskListApi(taskList);
        return success(userTasks);
    }

    //根据不同的任务类型去查询任务卡内容
    @ApiOperation("根据不同的任务类型去查询任务卡内容")
    @PostMapping("/taskListByTypeId")
    public ApiResult taskListByTypeId(@Validated @RequestBody UserTask userTask) {
        List<UserTask> userTasks = userTaskService.selectUserTaskListApi(userTask);
        return success(userTasks);
    }
}
